import {IJsonPatchData} from "./IJsonPatchData";

export interface IDocumentLog {
  dateEdit: string;
  id: string;
  idDoc: string;
  jsonNew: any;
  jsonOld?: any;
  jsonPatch?: IJsonPatchData[];
  md5New: string;
  md5Old?: string;
  userName: string;
}
