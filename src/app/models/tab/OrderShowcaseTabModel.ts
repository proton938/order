import {Sorting} from "../Sorting";

export class OrderShowcaseTabModel {
   alias: string;
   title: string;
   filter: string;
   forbidCreate?: boolean;
   sorting: Sorting;

   constructor(alias: string, title: string, filter: string, forbidCreate?: boolean, sorting?: Sorting) {
      this.alias = alias;
      this.title = title;
      this.filter = filter;
      this.forbidCreate = forbidCreate;
      this.sorting = sorting;
   }
}
