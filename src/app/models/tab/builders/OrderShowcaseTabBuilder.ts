import { AuthorizationService, SessionStorage } from "@reinform-cdp/security";
import { OrderShowcaseTabModel } from "../OrderShowcaseTabModel";
import { OrderType } from "../../order/OrderType";
import {Sorting} from "../../Sorting";

export class OrderShowcaseTabBuilderFactory {
    static getBuilder(id: string) {
        if (id === 'mka') {
            return new MkaOrderShowcaseTabBuilder();
        } else {
            return new GeneralOrderShowcaseTabBuilder();
        }
    }
}

export interface OrderShowcaseTabBuilder {
    build(authorizationService: AuthorizationService, session: SessionStorage, orderTypes: OrderType[]): OrderShowcaseTabModel[];
}

export class MkaOrderShowcaseTabBuilder implements OrderShowcaseTabBuilder {

    constructor() {
    }

    build(authorizationService: AuthorizationService, session: SessionStorage, orderTypes: OrderType[]): OrderShowcaseTabModel[] {
        let result: OrderShowcaseTabModel[] = [];
        result.push(new OrderShowcaseTabModel(
            'mka',
            'Распорядительные документы МКА',
            this.getMkaOrdersFilter(orderTypes),
            true
    ));
        return result;
    }

    private getMkaOrdersFilter(orderTypes: OrderType[]): string {
        let orderTypesCodes = orderTypes.filter(orderType => orderType.orderMKA)
            .map(val => `${val.code}`);
        return `(${orderTypesCodes.map(el => 'docTypeCode:(\"' + el + "\")").join(" OR ")})`;
    }
}

export class GeneralOrderShowcaseTabBuilder implements OrderShowcaseTabBuilder {

    constructor() {
    }

    build(authorizationService: AuthorizationService, session: SessionStorage, orderTypes: OrderType[]): OrderShowcaseTabModel[] {
        let result: OrderShowcaseTabModel[] = [];

        result.push(new OrderShowcaseTabModel(
            'all',
            'Все РД',
            this.getOrderTypesFilter(orderTypes)
        ));

        //todo: убрать
        // if (this.authorizationService.check('ORDER_DOCUMENT_OD')) {
        //     result.push(new OrderShowcaseTabModel(
        //         'mainActivity',
        //         'РД по основной деятельности',
        //         this.getOrderTypesFilter('ORDER_DOCUMENT_OD')
        //     ));
        // }
        //todo: убрать
        // if (this.authorizationService.check('ORDER_DOCUMENT_LS')) {
        //     result.push(new OrderShowcaseTabModel(
        //         'personnel',
        //         'РД по личному составу',
        //         this.getOrderTypesFilter('ORDER_DOCUMENT_LS')
        //     ));
        // }

        //TODO: permissions
        // if (this.authorizationService.check('MGGT_ADMORDER_MY')) {
        result.push(new OrderShowcaseTabModel(
            'myOrders',
            'Мои РД',
            this.getMyOrdersFilter(orderTypes, session)
        ));
        // }
        result.push(new OrderShowcaseTabModel(
            'myOrdersForApproval',
            'Мои РД на согласовании',
            this.getMyOrdersForApprovalFilter(orderTypes, session),
            null,
            new Sorting('createDate', 'desc', 'По дате создания РД')
        ));
        //todo: убрать
        // if (this.authorizationService.check('MGGT_ADMORDER_SUB')) {
        // result.push(new OrderShowcaseTabModel(
        //     'depOrders',
        //     'РД подразделения',
        //     this.getDepOrdersFilter()
        // ));
        // }

        // if (this.authorizationService.check('MGGT_ADMORDER_LIST')) {
        result.push(new OrderShowcaseTabModel(
            'mailing',
            'Рассылка РД',
            this.getMailingOrdersFilter(orderTypes, session)
        ));
        // }


        // if (this.authorizationService.check('MGGT_ADMORDER_SCAN')) {
        result.push(new OrderShowcaseTabModel(
            'scanning',
            'Сканирование',
            this.getScanningOrdersFilter(orderTypes)
        ));
        // }

        return result;

    }

    private getMyOrdersFilter(orderTypes: OrderType[], session: SessionStorage): string {
        let userSearchFields = [
            'accountNamesExecutor',
            'accountNameHead',
            'accountNameRegistar',
            'accountNameAuthor',
            'accountNameResponsibleExecutor',
            'accountNamesAgreedBy',
            'accountNamesAgreedByHistory',
        ];
        let userFilter = "(" + userSearchFields.map(el => el + ':(' + session.login() + ")").join(" OR ") + ")";
        return this.getFilterString([this.getOrderTypesFilter(orderTypes), userFilter]);
    }

    private getMyOrdersForApprovalFilter(orderTypes: OrderType[], session: SessionStorage): string {
        let userSearchFields = [
            'accountNamesExecutor',
            'accountNameHead',
            'accountNameRegistar',
            'accountNameAuthor',
            'accountNameResponsibleExecutor',
            'accountNamesAgreedBy',
            'accountNamesAgreedByHistory',
        ];
        let userFilter = "(" + userSearchFields.map(el => el + ':(' + session.login() + ")").join(" OR ") + ")";
        userFilter += ' AND docStatus:(На согласовании)';
        return this.getFilterString([this.getOrderTypesFilter(orderTypes), userFilter]);
    }

    private getDepOrdersFilter(orderTypes: OrderType[], session: SessionStorage): string {
        let depFilter = 'authorDep:(' + session.departmentFullName() + ")";
        return this.getFilterString([this.getOrderTypesFilter(orderTypes), depFilter]);
    }

    private getMailingOrdersFilter(orderTypes: OrderType[], session: SessionStorage): string {
        let mailingFilter = 'mailListAccountNames:(' + session.login() + ")";
        return this.getFilterString([this.getOrderTypesFilter(orderTypes), mailingFilter]);
    }

    private getScanningOrdersFilter(orderTypes: OrderType[]): string {
        let scanningFilter = 'statusAnalytic:(Действующий) AND scanAttached:(false)';
        return this.getFilterString([this.getOrderTypesFilter(orderTypes), scanningFilter]);
    }

    private getFilterString(filterStrings: string[]) {
        return filterStrings.filter(val => val && val.length).join(" AND ");
    }

    private getOrderTypesFilter(orderTypes: OrderType[], accessAction?: string): string {
        let orderTypesCodes = orderTypes.filter(orderType => !orderType.orderMKA)
            .filter(orderType => accessAction ? orderType.accessAction === accessAction : true)
            .map(val => `${val.code}`);
        return `(${orderTypesCodes.map(el => 'docTypeCode:(\"' + el + "\")").join(" OR ")})`;
    }

}
