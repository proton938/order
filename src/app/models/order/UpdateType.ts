import {formatAsDate} from "../../services/date-util.service";

export class UpdateType{
    updatedDocumentID: string;
    updatedDocumentType: string;
    updatedDocumentTypeCode: string;
    updatedDocumentDate: Date;
    updatedDocumentNum: string;
    docName: string;

    public static processDocName(doc: UpdateType): UpdateType {
        doc.docName = doc.updatedDocumentType + (doc.updatedDocumentNum ? " № " + doc.updatedDocumentNum : "") +
            (doc.updatedDocumentDate? " от " + formatAsDate(doc.updatedDocumentDate) : "");
        return doc;
    }

    public static cleanDocName(doc: UpdateType): UpdateType {
        delete doc.docName;
        return doc;
    }
}
