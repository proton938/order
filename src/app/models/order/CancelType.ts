import {formatAsDate} from "../../services/date-util.service";

export class CancelType{
    canceledDocumentID: string;
    canceledDocumentNum: string;
    canceledDocumentDate: Date;
    canceledDocumentType: string;
    canceledDocumentTypeCode: string;
    docName?: string;

    public static processDocName(doc: CancelType): CancelType {
        doc.docName = doc.canceledDocumentType + (doc.canceledDocumentNum ? " № " + doc.canceledDocumentNum : "") +
            (doc.canceledDocumentDate ? " от " + formatAsDate(doc.canceledDocumentDate) : "");
        return doc;
    }

    public static cleanDocName(doc: CancelType): CancelType {
        delete doc.docName;
        return doc;
    }
}
