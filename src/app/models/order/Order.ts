import { ExFileType } from "@reinform-cdp/widgets";
import {FileType} from "./FileType";
import {UpdateType} from "./UpdateType";
import {CancelType} from "./CancelType";
import {User} from "./User";
import {UserBean} from "@reinform-cdp/nsi-resource";
import {IFileInFolderInfo} from "@reinform-cdp/file-resource";
import {ApprovalDuration} from "./ApprovalDuration";
import { NSIDocumentType } from "@reinform-cdp/core";

export class Order {
    documentID: string;
    bpmProcess: OrderProcess;
    documentTypeCode: string;
    documentTypeValue: string;
    documentYear: number;
    documentNumber: string;
    documentDate: Date;
    documentExpireDate: Date;
    canceledDocument?: CancelType;
    updatedDocument?: UpdateType[];
    documentStatusCode: string;
    documentStatusValue: string;
    documentStatusColor: string;
    documentContent: string;
    address: string;
    approvedDocumentNumber: string;
    createDateTime: Date;
    createAuthor: User;
    barcode: number;
    barcodeText?: string;
    folderID: string;
    files: FileType[];
    draftFilesRegInfo: FileType;
    draftFiles: OrderDraftFile;
    draftFilesWithoutAtt: OrderDraftFile;
    draftFilesWithoutAttWord: OrderDraftFile;
    attach: OrderDraftFile;
    attachWord: OrderDraftFile;
    head: User;
    executor: User[];
    registar: User;
    responsibleExecutor?: User;
    mailList: User[];
    mailDateTime: Date;
    previousDocument?: PreviousDocument;
    psoInputID: number;
    psoOutputID: number[];
    signingElectronically: boolean | string;
    approval: OrderApproval;
    approvalHistory: OrderApprovalHistory;
    SigningOnPaper: OrderSigningOnPaper[];
    documentThemeCode: string;
    documentThemeValue: string;
    additionalMaterials: FileType[];
    PublishingSite?: boolean | string;
    linkedDocument?: LinkedDocument[];
    isogdXmlGuid?: string;
    systemCode?: string;
    systemParentID?: string;
    activeApprover?: string;
    responsibleDep?: {
        departmentFull: string;
        departmentShort: string;
        departmentCode: string;
    };
    documentComment?: string;
    documentText?: string;
    returnToEditing?: boolean;
    returnToEditingSeq?:boolean;
    ds: Ds;
    review: any[];
    reviewInner?: ReviewInner[];
    privacy:NSIDocumentType;  
    receivedFromMKA:ReceivedFromMKA;
}

export class ReceivedFromMKA{
    FromMKA:boolean;
    receivedDate:Date;
}
export class ReviewInner { // Внутреннее ознакомление
    reviewBy: User; // Ознакомился
    factReviewBy: User; // Ознакомился фактически (делегант)
    sentReviewBy: User; // Кто отправил на ознакомление
    reviewPlanDate: Date; // Плановая дата ознакомления
    reviewFactDate: Date; // Фактическая дата ознакомления
    reviewNote: string; // Комментарии
}

export class LinkedDocument {
    linkedDocumentID: string;
    linkedDocumentType: string;
}

export class PreviousDocument {
    previousDocumentID: string;
    previousDocumentNum: string;
    previousDocumentDate: Date;
}

export class OrderSigningOnPaper {
    taskId: string;
    signingResult: boolean;
    signingFactDate: Date;
    noteSigning: OrderNote;
    File: FileType[];

    constructor(taskId: string) {
        this.taskId = taskId;
    }
}

export class OrderApproval {
    approvalCycle: OrderApprovalList;

    constructor(approvalCycleNum: number) {
        this.approvalCycle = new OrderApprovalList(approvalCycleNum);
    }
}

export class OrderApprovalHistory {
    approvalCycle: OrderApprovalHistoryList[];

    constructor() {
        this.approvalCycle = [];
    }
}

export class OrderApprovalList {
    approvalCycleNum: string;
    approvalCycleDate: Date;
    agreed: OrderApprovalListItem[];
    approvalTermAll?: ApprovalTerm;

    constructor(approvalCycleNum: number) {
        this.approvalCycleNum = '' + approvalCycleNum;
        this.approvalCycleDate = new Date();
        this.agreed = [];
    }
}

export class OrderApprovalHistoryList extends OrderApprovalList {
    approvalCycleFile: FileType[];
}

export class OrderApprovalListItem {
    approvalNum: string;
    approvalTime: string;
    agreedBy: OrderApprover;
    factAgreedBy?: OrderApprover;
    approvalPlanDate?: Date;
    approvalFactDate?: Date;
    approvalResult?: string;
    approvalType: string;
    approvalTypeCode: string;
    approvalNote?: string;
    fileApproval?: FileType[];
    agreedDs?: OrderApprovalListItemSignature;
    added?: {
        additionalAgreed: {
            accountName: string;
            fio: string;
            iofShort: string;
        }
        addedNote: string;
    }[];
    approvalTerm?: ApprovalTerm;
    responsibleExecutorComment?: string;
    Parallel?: boolean;
    Signer?: boolean;

    constructor() {
        this.approvalTerm = {};
    }
}

export class ApprovalTerm {
    code?: string;
    name?: string;
    duration?: number;

    static fromApprovalDuration(approvalDuration: ApprovalDuration): ApprovalTerm {
        return {
            code: approvalDuration.Code,
            name: approvalDuration.Name,
            duration: parseInt(approvalDuration.Duration)
        }
    }
}

export class Ds {
    dsLastName: string;
    dsName: string;
    dsPosition: string;
    dsCN: string;
    dsFrom: Date;
    dsTo: Date;
}

export class OrderApprovalListItemSignature {
    agreedDsLastName: string;
    agreedDsName: string;
    agreedDsPosition: string;
    agreedDsCN: string;
    agreedDsFrom: Date;
    agreedDsTo: Date;
}

export class OrderApprover {
    post: string;
    fio: string;
    fioFull: string;
    accountName: string;
    fioShort: string;
    iofShort: string;
    phone: string;

    fioPost?: string;

    static fromUserBean(user: UserBean): OrderApprover {
        return OrderApprover.processOrderApprover({
            post: user.post,
            fio: user.displayName,
            fioFull: user.displayName,
            accountName: user.accountName,
            fioShort: user.fioShort,
            iofShort: user.iofShort,
            phone: user.telephoneNumber
        });
    }

    static fromUser(user: User): OrderApprover {
        return OrderApprover.processOrderApprover({
            post: user.post,
            fio: user.fio || (<any>user).fioFull,
            fioFull: user.fio || (<any>user).fioFull,
            accountName: user.accountName,
            fioShort: user.fioShort,
            iofShort: user.iofShort,
            phone: user.phoneNumber
        });
    }

    public static processOrderApprover(approver: OrderApprover): OrderApprover {
        approver.fioPost = `${approver.fioFull} (${approver.post ? approver.post : ''})`;
        return approver;
    }

    public static cleanOrderApprover(approver: OrderApprover): OrderApprover {
        delete approver.fioPost;
        return approver;
    }
}

export class OrderDraftFile {
    draftFilesID: string;
    draftFilesType: string;
    draftFilesName: string;
    draftFilesSize: string;
    draftFilesSigned: boolean;
    draftFilesDate: Date;
    draftFilesMimeType: string;

    static fromFileInFolderInfo(info: IFileInFolderInfo) {
        return OrderDraftFile.create(info.versionSeriesGuid, info.fileName, info.fileSize.toString(), false, new Date(info.dateCreated), info.fileType, info.mimeType);
    }

    static toExFileType(file: OrderDraftFile): ExFileType {
        return ExFileType.create(file.draftFilesID, file.draftFilesName, parseInt(file.draftFilesSize),
            file.draftFilesSigned, file.draftFilesDate.getTime(), file.draftFilesType, file.draftFilesMimeType);
    }

    static fromFileType(file: FileType): OrderDraftFile {
      return OrderDraftFile.create(file.fileID, file.fileName, file.fileSize.toString(),
          file.fileSigned, file.fileDate, file.fileType, file.mimeType);
  }

    static fromExFileType(file: ExFileType): OrderDraftFile {
        return OrderDraftFile.create(file.idFile, file.nameFile, file.sizeFile.toString(),
            file.signed, new Date(<number> file.dateFile), file.typeFile, file.mimeType)
    }

    static create(idFile?: string, nameFile?: string, sizeFile?: string, signed?: boolean, dateFile?: Date, typeFile?: string, classFile?: string): OrderDraftFile {
        let result: OrderDraftFile = new OrderDraftFile();

        result.draftFilesID = (idFile) ? idFile : result.draftFilesID;
        result.draftFilesName = (nameFile) ? nameFile : result.draftFilesName;
        result.draftFilesSize = (sizeFile) ? sizeFile : result.draftFilesSize;
        result.draftFilesSigned = (signed) ? signed : result.draftFilesSigned;
        result.draftFilesDate = (dateFile) ? dateFile : result.draftFilesDate;
        result.draftFilesType = (typeFile) ? typeFile : result.draftFilesType;
        result.draftFilesMimeType = (classFile) ? classFile : result.draftFilesMimeType;

        return result;
    }

}

export class OrderNote {
    noteText: string;
    noteAuthor: string;
    noteAuthorFIO: string;
    noteDate: Date;
}

export class OrderProcessType {
    date: Date;
    bpmProcessName: string;
    bpmProcessCode: string;
    note: OrderNote;
}

export class OrderProcess {
    bpmProcessId: string;
    bpmProcessDel: OrderProcessType;
    bpmProcessStart: OrderProcessType;
}
