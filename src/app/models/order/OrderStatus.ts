export class OrderStatus {
    static PROJECT: string = 'PROJECT';
    static REGISTER: string = 'REGISTER';
    static REGISTERSCAN: string = 'REGISTERSCAN';
    static REGISTERMAIL: string = 'REGISTERMAIL';
    static REGISTERPUBLISH: string = 'REGISTERPUBLISH';
    static CANCELED: string = 'CANCELED';
    static DELETED: string = 'DELETED';

    code: string;
    name: string;
    color: string;
    statusAnalytic: string;
}
