export class ApprovalType {
    static approval: string =  'approval';
    static agreed: string =  'agreed';
    static assent: string =  'assent';

	approvalType: string;
	approvalTypeCode: string;
	approvalDuration: string;
	buttonNo: string;
	buttonNoColor: string;
	buttonYes: string;
	buttonYesColor: string;
    department: string;
    duration: any;
}
