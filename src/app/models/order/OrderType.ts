export class OrderType {
  code: string;
  name: string;
  accessAction?: string;
  codeTemplateSingElectron?: string;
  codeTemplate?: string;
  accessEI?: string;
  application?: boolean;
  prereg?: boolean;
  singElectron?: boolean | string;
  publishing: boolean;
  orderMKA?: boolean;
}