export class FileType {
    fileID: string;
    fileType: string;
    fileName: string;
    fileSize: string;
    fileSigned: boolean;
    fileDate: Date;
    mimeType: string;

    static create(idFile?: string, nameFile?: string, sizeFile?: string, signed?: boolean, dateFile?: Date, typeFile?: string, classFile?: string): FileType {
        let result: FileType = new FileType();

        result.fileID = (idFile) ? idFile : result.fileID;
        result.fileName = (nameFile) ? nameFile : result.fileName;
        result.fileSize = (sizeFile) ? sizeFile : result.fileSize;
        result.fileSigned = (signed) ? signed : result.fileSigned;
        result.fileDate = (dateFile) ? dateFile : result.fileDate;
        result.fileType = (typeFile) ? typeFile : result.fileType;
        result.mimeType = (classFile) ? classFile : result.mimeType;

        return result;
    }

}
