export class EmailGroup {
    code: string;
    name: string;
    users: string[];
    sendSystem: {
        code: string;
    }[];
}
