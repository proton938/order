import {IDocumentUser} from "../core/abstracts/IDocumentUser";
import {DocumentBase} from "./DocumentBase";
import * as _ from 'lodash';
import {UserBean} from "@reinform-cdp/nsi-resource";

// @dynamic
export class DocumentUser extends DocumentBase<IDocumentUser, DocumentUser>{
  post: string = '';
  login: string = '';
  fioFull: string = '';

  updateFromUserBean(user: UserBean) {
    if (user) {
      this.post = user.post;
      this.login = user.accountName;
      this.fioFull = user.displayName;
    }
  }

  toUserBean(): UserBean {
    let user = new UserBean();
    user.accountName = this.login;
    user.post = this.post;
    user.displayName = this.fioFull;
    return user;
  }

  static buildArray(users: IDocumentUser[]): DocumentUser[] {
    return (users && users.length) ? _.map(users, u => {
      let user = new DocumentUser();
      user.build(u);
      return user;
    }) : [];
  }
}
