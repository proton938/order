import {DocumentBase} from './DocumentBase';
import {IDocumentFromMKA} from "./abstracts/IDocumentFromMKA";

export class DocumentFromMKA extends DocumentBase<IDocumentFromMKA, DocumentFromMKA> {
  FromMKA: boolean = null;
  receivedDate: Date = null;
}
