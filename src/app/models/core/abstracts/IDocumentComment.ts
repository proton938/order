import {IDocumentUser} from './IDocumentUser';
import {IDocumentBase} from "./IDocumentBase";

export interface IDocumentComment extends IDocumentBase {
  commentDate?: string;
  commentUser?: IDocumentUser;
  commentText?: string;
}
