import {IDocumentBase} from "./IDocumentBase";

export interface IDocumentStatus extends IDocumentBase {
  code?: string;
  name?: string;
  color?: string;
}
