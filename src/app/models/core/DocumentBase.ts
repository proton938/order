import * as _ from 'lodash';
import * as angular from 'angular';
import {IDocumentConverter} from "../IDocumentConverter";
import {IDocumentBase} from "./abstracts/IDocumentBase";

export class DocumentBase<R extends IDocumentBase, T extends DocumentBase<R, T>> implements IDocumentConverter<R> {
  documentId: string = ''; // Идентификатор документа

  build(i: R) {
    let o = this;
    if (i) {
      _.forIn(o, (val: any, key: string) => {
        if (val === '') {
          o[key] = _.toString(i[key]);
        } else if (val === null) {
          if (typeof i[key] === 'boolean' || typeof i[key] === 'number') {
            o[key] = i[key];
          } else if (typeof i[key] === 'string') {
            if (i[key].isDateTime())
              o[key] = i[key].stringToDateTime();
            else if (i[key].isDate())
              o[key] = i[key].stringToDate();
            else if (i[key].isNumber())
              o[key] = i[key].stringToNumber();
            else if (i[key].isBoolean())
              o[key] = i[key].stringToBoolean();
            else if (i[key].isUndefined() || i[key].isNull() || i[key].isEmpty())
              o[key] = null;
            else
              o[key] = i[key];
          }
        }
      });
    }
  }

  min(): R {
    let result: any = angular.copy(this);
    _.forIn(result, (value, key) => {
      if (_.isFunction(value)) {
      } else if (_.isNull(value) || _.isUndefined(value)) {
        delete result[key];
      } else if (_.isString(value) && value.trim() === '') {
        delete result[key];
      } else if (_.isDate(value)) {
        result[key] = this.dateToString(value);
      } else if (_.isNumber(value) || _.isBoolean(value) || _.isString(value)) {
        result[key] = value;
      } else if (_.isArray(value)) {
        if (value.length === 0) {
          delete result[key];
        } else {
          let minArr = [];
          value.forEach(r => {
            if (_.isNull(r) || _.isUndefined(r)) {
            } else if (_.isString(r) && r.trim() === '') {
            } else if (_.isDate(r)) {
              minArr.push(this.dateToString(r));
            } else if (_.isNumber(r) || _.isBoolean(r) || _.isString(r)) {
              minArr.push(r);
            } else {
              let minObj = r.min();
              if (minObj.isEmptyObject()) {
              } else {
                minArr.push(minObj);
              }
            }
          });
          if (minArr.length === 0) {
            delete result[key];
          } else {
            result[key] = minArr;
          }
        }
      } else {
        let minObj = (<any>value).min();
        if (minObj.isEmptyObject()) {
          delete result[key];
        } else {
          result[key] = minObj;
        }
      }
    });
    return result;
  }

  private isEmptyObject(): boolean {
    for (let i in this) {
      if (this.hasOwnProperty(i) && typeof this[i] !== 'function') {
        return false;
      }
    }
    return true;
  }

  private dateToString(date: Date): string {
    let _date = date.getFullYear() + '-' + ('0' + (date.getMonth() + 1)).slice(-2) + '-' + ('0' + date.getDate()).slice(-2);
    let _time = ('0' + date.getHours()).slice(-2) + ':' + ('0' + date.getMinutes()).slice(-2) + ':' + ('0' + date.getSeconds()).slice(-2);
    return (_time === '00:00:00') ? _date : _date + 'T' + _time;
  }
}
