import {IJsonPatchData} from "./IJsonPatchData";

export interface IDocumentLogExtended<T> {
  dateEdit: string;
  docType: string;
  id: string;
  idDoc: string;
  jsonOnDelete: T;
  jsonOnInsert: T;
  jsonPatch?: IJsonPatchData[];
  notes: string;
  userName: string;
}
