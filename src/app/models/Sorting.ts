export class Sorting {
  name: string;
  value: string;
  text: string;

  constructor(name: string, value: string, text: string) {
    this.name = name;
    this.value = value;
    this.text = text;
  }
}
