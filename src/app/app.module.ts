import * as angular from "angular";
import {CdpReporterPreviewServiceProvider, CdpReporterResourceModule} from "@reinform-cdp/reporter-resource";
import {FileResourceModule, FileResourceServiceProvider} from "@reinform-cdp/file-resource";
import {NsiResourceServiceProvider} from "@reinform-cdp/nsi-resource";
import {SearchResourceModule, SolarResourceServiceProvider} from '@reinform-cdp/search-resource';
import {NgSelectModule} from '@ng-select/ng-select';
import {ToastrModule} from 'ngx-toastr';
import {BlockUIModule} from 'ng-block-ui';
import {FormsModule} from '@angular/forms';
import {LaddaModule} from 'angular2-ladda';
import {CdpBpmComponentsModule} from '@reinform-cdp/bpm-components';
import {
  AlertModule, BsDropdownModule, ButtonsModule, CollapseModule, PaginationModule, TabsModule,
  TimepickerModule
} from 'ngx-bootstrap';
import {WidgetsModule} from '@reinform-cdp/widgets';
import {SdoComponents} from '@sdo/components';
import {AppComponent} from './app.component';
import {ModuleWithProviders, NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {UIRouterUpgradeModule} from '@uirouter/angular-hybrid';
import {statesConfig} from './states.config';
import {AttachFileModalComponent} from "./components/common/attach-file-modal/attach-file-modal.component";
import {OrderComponent} from './components/order/card/order/order.component';
import {DesktopOrderComponent} from './components/order/card/order/desktop/desktop.component';
import {MobileOrderComponent} from './components/order/card/order/mobile/mobile.component';
import {ApprovalListComponent} from './components/common/approval-list/approval-list.component';
import {DesktopApprovalListComponent} from './components/common/approval-list/desktop/desktop.component';
import {ApprovalRowsComponent} from './components/common/approval-list/desktop/rows.component';
import {MobileApprovalListComponent} from './components/common/approval-list/mobile/mobile.component';
import {OrderEditComponent} from './components/order/card/order-edit/order-edit.component';
import {OrderApprovalsComponent} from './components/order/card/order-approvals/order-approvals.component';
import {MobileOrderApprovalsComponent} from './components/order/card/order-approvals/mobile/order-approvals.component';
import {OrderRelatedComponent} from './components/order/card/order-related/order-related.component';
import {MobileOrderRelatedComponent} from './components/order/card/order-related/mobile/order-related.component';
import {OrderViewComponent} from './components/order/card/order-view/order-view.component';
import {MyAuthDirective} from "./directives/my-auth.directive";
import {OrderInitiateComponent} from './components/order/card/order-initiate/order-initiate.component';
import {UploadFileModalComponent} from './components/order/card/upload-file/upload-file.modal';
import {DropzoneComponent} from "./components/common/dropzone/dropzone.component";
import {DropzoneModule} from "ngx-dropzone-wrapper";
import {ServiceInformationModalComponent} from './components/order/card/service-information/service-information.modal';
import {HttpErrorInterceptor} from "./services/service-error.interceptor";
import {HTTP_INTERCEPTORS} from "@angular/common/http";
import {ShowcaseComponent} from './components/showcase/showcase.component';
import {DesktopShowcaseComponent} from './components/showcase/desktop/desktop.component';
import {MobileShowcaseComponent} from './components/showcase/mobile/mobile.component';
import {OrderListComponent} from './components/order/list/order-list/order-list.component';
import {MobileOrderListComponent} from './components/order/list/order-list/mobile/mobile.component';
import {OrderListSortingComponent} from './components/order/list/order-list-sorting/order-list-sorting.component';
import {MobileOrderListSortingComponent} from './components/order/list/order-list-sorting/mobile/mobile.component';
import {OrderListViewPlainComponent} from './components/order/list/order-list-view-plain/order-list-view-plain.component';
import {OrderListViewCardComponent} from './components/order/list/order-list-view-card/order-list-view-card.component';
import {OrderListViewTableComponent} from './components/order/list/order-list-view-table/order-list-view-table.component';
import {OrderListViewIndentComponent} from './components/order/list/order-list-view-indent/order-list-view-indent.component';
import {OrderListFilterComponent} from './components/order/list/order-list-filter/order-list-filter.component';
import {MobileOrderListFilterComponent} from './components/order/list/order-list-filter/mobile/mobile.component';
import {ScanningOrderListComponent} from './components/order/scanning-list/scanning-order-list/scanning-order-list.component';
import {ScanningOrderListViewTableComponent} from './components/order/scanning-list/scanning-order-list-view-table/scanning-order-list-view-table.component';
import {ScanningOrderListFilterComponent} from './components/order/scanning-list/scanning-order-list-filter/scanning-order-list-filter.component';
import {NotScannedAttachComponent} from './components/common/not-scanned-attach/not-scanned-attach.component';
import {AttachOriginalsModalComponent} from './components/order/card/attach-originals/attach-originals.modal';
import {OrderPublishComponent} from './components/order/tasks/order-publish/order-publish.component';
import {OrderInfoComponent} from './components/order/tasks/order-info/order-info.component';
import {ProcessScanComponent} from './components/order/tasks/process-scan/process-scan.component';
import {OrderPrepareComponent} from './components/order/tasks/order-prepare/order-prepare.component';
import {OrderPrepareEditComponent} from './components/order/tasks/order-prepare-edit/order-prepare-edit.component';
import {ApprovalSublistEditComponent} from './components/order/tasks/approval-sublist-edit/approval-sublist-edit.component';
import {ApprovalHistoryComponent} from './components/order/tasks/approval-history/approval-history.component';
import {OrderPrintandsignComponent} from './components/order/tasks/order-printandsign/order-printandsign.component';
import {OrderIntroComponent} from './components/order/tasks/order-intro/order-intro.component';
import {OrderApproveComponent} from './components/order/tasks/order-approve/order-approve.component';
import {OrderApproveNewComponent} from './components/order/tasks/order-approve/order-approve-new.component';
import {OrderApproveParallelComponent} from './components/order/tasks/order-approve-parallel/order-approve-parallel.component';
import {OrderApproveDesicionComponent} from './components/order/tasks/order-approve-desicion/order-approve-desicion.component';
import {RedirectToTaskModalComponent} from './components/order/tasks/redirect-to-task/redirect-to-task.modal';
import {AddApproversModalComponent} from './components/order/tasks/add-approvers/add-approvers.modal';
import {ApprovalListAddComponent} from './components/order/tasks/approval-list-add/approval-list-add.component';
import {DesktopApprovalListAddComponent} from './components/order/tasks/approval-list-add/desktop/desktop.component';
import {OrderModifyComponent} from "./components/order/tasks/order-edit/order-edit.component";
import {OrderModifyNewComponent} from './components/order/tasks/order-edit/order-edit-new.component';
import {ProcessRegisterComponent} from './components/order/tasks/process-register/process-register.component';
import {ReviewMkaDocComponent} from './components/order/tasks/review-mka-doc/review-mka-doc.component';
import {AddEmployeeModalComponent} from "./components/order/tasks/review-mka-doc/add-employee/add-employee.modal"
import {SelectLdapUsersComponent} from './components/common/select-ldap-users/select-ldap-users.component';
import {OrderReiewComponent} from "./components/order/card/order-review/order-review.component";
import {OrderInnerAssignmentsComponent} from "./components/order/card/order-inner-assignments/order-inner-assignments.component";
import {ApprovalListEditComponent} from './components/order/tasks/approval-list-edit/approval-list-edit.component';
import {ApprovalSublistEditRowComponent} from "./components/order/tasks/approval-sublist-edit/row.component";
import {CompoundReportService} from "./services/compound-report.service";
import {UrlService, TransitionService, Transition} from '@uirouter/core';
import {setAngularJSGlobal, UpgradeModule} from '@angular/upgrade/static';
import {SkeletonModule} from '@reinform-cdp/skeleton';
import {CdpLoggerModule} from '@reinform-cdp/logger';
import {CoreModule} from '@reinform-cdp/core';
import {OrderAttachFileComponent} from "./components/order/tasks/order-attach-file/order-attach-file.component";
import {OrderInitiateEditOrdersComponent} from "./components/order/card/order-initiate-edit-orders/order-initiate-edit-orders.component";
import {OrderAssignPermissComponent} from "./components/order/tasks/order-assign-permiss/order-assign-permiss.component";
import {OrderApproveDocComponent} from "./components/order/tasks/order-approve-doc/order-approve-doc.component";
import {OrderNegotiationSignComponent} from "./components/order/tasks/order-negotiation-sign/order-negotiation-sign.component";
import {OrderAcceptChangesComponent} from "./components/order/tasks/order-accept-changes/order-accept-changes.component";
import {OrderReworkDocComponent} from "./components/order/tasks/order-rework-doc/order-rework-doc.component";
import {OrderJsonEditorComponent} from "./components/order/card/order-json/json-editor.component";
import {DesktopApprovalListOldComponent} from "./components/common/approval-list/old/desktop/desktop.component";
import {MobileApprovalListOldComponent} from "./components/common/approval-list/old/mobile/mobile.component";
import {ApprovalRowOldComponent} from "./components/common/approval-list/old/desktop/rows.component";
import {ApprovalListOldComponent} from "./components/common/approval-list/old/approval-list-old.component";
import {ApprovalHistoryOldComponent} from "./components/order/tasks/approval-history/approval-history-old.component";
import {ApprovalListEditOldComponent} from "./components/order/tasks/approval-list-edit/approval-list-edit-old.component";
import {OrderPrepareEditOldComponent} from "./components/order/tasks/order-prepare-edit/order-prepare-edit-old.component";
import {ShowcaseBuilderModule} from "@reinform-cdp/showcase-builder";
import {FileHistoryComponent} from "./components/common/file-history/file-history.component";
import {OrderReviewInnerComponent} from "./components/order/tasks/order-review-inner/order-review-inner.component";
import {OrderCardReiewInnerComponent} from "./components/order/card/order-review-inner/order-review-inner.component";
import {OrderMobileReiewInnerComponent} from "./components/order/card/order-review-inner/mobile/order-mobile-review-inner.component";
import {OrderDesktopReiewInnerComponent} from "./components/order/card/order-review-inner/desktop/order-desktop-review-inner.component";
import {TextMaskModule} from "angular2-text-mask";
import {InstructionListComponent} from "./components/instruction/list/list.component";

declare let document: any;
setAngularJSGlobal(angular);

@NgModule({
  imports: [
    FormsModule,
    BrowserModule,
    LaddaModule,
    ToastrModule.forRoot(),
    TabsModule.forRoot(),
    PaginationModule.forRoot(),
    AlertModule.forRoot(),
    WidgetsModule.forRoot(),
    CdpBpmComponentsModule.forRoot(),
    SearchResourceModule.forRoot(),
    FileResourceModule.forRoot(),
    CdpReporterResourceModule.forRoot(),
    BlockUIModule.forRoot(),
    NgSelectModule,
    CollapseModule,
    ButtonsModule.forRoot(),
    BsDropdownModule.forRoot(),
    TimepickerModule.forRoot(),
    UIRouterUpgradeModule.forRoot({states: statesConfig}),
    DropzoneModule,
    SkeletonModule.forRoot(),
    CdpLoggerModule.forRoot(),
    CoreModule.forRoot('SDO', '/main/', 'Главная', '/app/sdo/bpm'),
    ShowcaseBuilderModule,
    SdoComponents.forRoot(),
    TextMaskModule
  ],
  declarations: [
    MyAuthDirective,
    AppComponent,
    OrderComponent,
    DesktopOrderComponent,
    MobileOrderComponent,
    OrderJsonEditorComponent,
    ApprovalListComponent,
    DropzoneComponent,
    DesktopApprovalListComponent,
    ApprovalRowsComponent,
    ApprovalSublistEditRowComponent,
    MobileApprovalListComponent,
    OrderEditComponent,
    OrderApprovalsComponent,
    MobileOrderApprovalsComponent,
    OrderRelatedComponent,
    MobileOrderRelatedComponent,
    OrderViewComponent,
    OrderInitiateComponent,
    OrderInitiateEditOrdersComponent,
    UploadFileModalComponent,
    ServiceInformationModalComponent,
    ShowcaseComponent,
    DesktopShowcaseComponent,
    MobileShowcaseComponent,
    OrderListComponent,
    MobileOrderListComponent,
    OrderListSortingComponent,
    MobileOrderListSortingComponent,
    OrderListViewPlainComponent,
    OrderListViewCardComponent,
    OrderListViewTableComponent,
    AttachFileModalComponent,
    OrderListViewIndentComponent,
    OrderListFilterComponent,
    MobileOrderListFilterComponent,
    ScanningOrderListComponent,
    ScanningOrderListViewTableComponent,
    ScanningOrderListFilterComponent,
    NotScannedAttachComponent,
    AttachOriginalsModalComponent,
    OrderPublishComponent,
    OrderInfoComponent,
    ProcessScanComponent,
    OrderPrepareComponent,
    SelectLdapUsersComponent,
    OrderPrepareEditComponent,
    ApprovalSublistEditComponent,
    ApprovalListEditComponent,
    OrderInfoComponent,
    ApprovalHistoryComponent,
    OrderPrintandsignComponent,
    OrderIntroComponent,
    OrderReviewInnerComponent,
    OrderApproveComponent,
    OrderApproveNewComponent,
    OrderApproveParallelComponent,
    OrderApproveDesicionComponent,
    OrderNegotiationSignComponent,
    OrderModifyComponent,
    OrderModifyNewComponent,
    RedirectToTaskModalComponent,
    AddApproversModalComponent,
    AddEmployeeModalComponent,
    ApprovalListAddComponent,
    DesktopApprovalListAddComponent,
    ProcessRegisterComponent,
    ReviewMkaDocComponent,
    OrderReiewComponent,
    OrderCardReiewInnerComponent,
    OrderMobileReiewInnerComponent,
    OrderDesktopReiewInnerComponent,
    OrderInnerAssignmentsComponent,
    OrderAttachFileComponent,
    OrderAssignPermissComponent,
    OrderApproveDocComponent,
    OrderAcceptChangesComponent,
    OrderReworkDocComponent,
    DesktopApprovalListOldComponent,
    ApprovalRowOldComponent,
    MobileApprovalListOldComponent,
    ApprovalListOldComponent,
    ApprovalHistoryOldComponent,
    ApprovalListEditOldComponent,
    OrderPrepareEditOldComponent,
    FileHistoryComponent,
    InstructionListComponent
  ],
  providers: [],
  exports: [AppComponent],
  entryComponents: [UploadFileModalComponent, AttachOriginalsModalComponent, AttachFileModalComponent, ServiceInformationModalComponent,
    RedirectToTaskModalComponent, AddApproversModalComponent, AddEmployeeModalComponent]
})
export class OrderSystem {
  constructor(private upgrade: UpgradeModule) {
  }

  ngDoBootstrap() {
    this.upgrade.bootstrap(document, ['order.system'], {strictDi: false});
  }
}

export const ng1Module = angular.module('order.system', [
  'ui.router',
  'ui.router.upgrade',
  'cdp.skeleton',
  'cdp.core',
  'cdp.bpm.components',
  'cdp.widgets',
  'cdp.logger',
  'cdp.security',

  'cdp.reporter.resource'
])
  .config(['cdpNsiResourceServiceProvider', (nsiResourceServiceProvider: NsiResourceServiceProvider) => {
    nsiResourceServiceProvider.root = '/mdm/api/v1';
  }]).config(['cdpFileResourceServiceProvider', (fileResourceServiceProvider: FileResourceServiceProvider) => {
    fileResourceServiceProvider.root = '/filestore/v1';
  }])
  .config(['cdpSolarResourceServiceProvider', (solarResourceServiceProvider: SolarResourceServiceProvider) => {
    solarResourceServiceProvider.root = '/app/sdo/search';
  }])
  .config(['cdpReporterPreviewServiceProvider', (cdpReporterPreviewServiceProvider: CdpReporterPreviewServiceProvider) => {
    cdpReporterPreviewServiceProvider.rootReporter = '/reporter/v1';
  }]);


// �������������, ����� ��������� ��� ������-������� ( workbox). ������ index.html
ng1Module.run([
  '$transitions',
  '$q',
  function ($transitions: TransitionService, $q: ng.IQService) {
    $transitions.onStart({}, function (trans: Transition) {
      if (window.sessionStorage && window.sessionStorage['cdp-reload']) {
        delete window.sessionStorage['cdp-reload'];
        window.location.reload();
        console.log('New version arrived');
      } else {
        trans.options().reload = false;
      }
    });
  }
]);

