import { Component, OnInit } from '@angular/core';
import { OrderType } from "../../models/order/OrderType";
import { OrderStatus } from "../../models/order/OrderStatus";
import { OrderTheme } from "../../models/order/OrderTheme";
import { DepartmentBean } from "@reinform-cdp/nsi-resource";
import { NSIDocumentType } from "@reinform-cdp/core";

@Component({
  selector: 'mggt-showcase',
  templateUrl: './showcase.component.html',
  styleUrls: ['./showcase.component.scss']
})
export class ShowcaseComponent implements OnInit {
  constructor() {
  }

  ngOnInit() {
  }
}

export interface IOrderShowcaseDicts {
  availableYears: string[];
  district: any[];
  orderTypes: OrderType[];
  statusAnalytics: string[];
  orderStatuses: OrderStatus[];
  orderThemes: OrderTheme[];
  departments: DepartmentBean[];
  privacyTypes:NSIDocumentType[];
}


export interface OrderTypesFilter {
  filter(orderTypes: OrderType[]): OrderType[];
}
export class GeneralOrderTypesFilter implements OrderTypesFilter {
  filter(orderTypes: OrderType[]): OrderType[] {
    return orderTypes;
  }
}
export class MkaOrderTypesFilter implements OrderTypesFilter {
  filter(orderTypes: OrderType[]): OrderType[] {
    return orderTypes.filter(ot => ot.orderMKA);
  }
}
export class OrderTypesFilterFactory {
  static getFilter(id: string) {
    if (id === 'mka') {
      return new MkaOrderTypesFilter();
    } else {
      return new GeneralOrderTypesFilter();
    }
  }
}
