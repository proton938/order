import * as _ from "lodash";
import { Component, OnInit, ViewChild } from '@angular/core';
import { LoadingStatus } from "@reinform-cdp/widgets";
import { StateService, Transition } from "@uirouter/core";
import { DepartmentBean, NsiResourceService } from "@reinform-cdp/nsi-resource";
import { SessionStorage, AuthorizationService } from "@reinform-cdp/security";
import { BreadcrumbsService } from "@reinform-cdp/skeleton";
import { forkJoin } from "rxjs/index";
import { OrderShowcaseTabModel } from "../../../models/tab/OrderShowcaseTabModel";
import { OrderType } from "../../../models/order/OrderType";
import { OrderStatus } from "../../../models/order/OrderStatus";
import { OrderTheme } from "../../../models/order/OrderTheme";
import {
  OrderShowcaseTabBuilder,
  OrderShowcaseTabBuilderFactory
} from "../../../models/tab/builders/OrderShowcaseTabBuilder";
import { OrderTypeService } from "../../../services/order-type.service";
import { OrderListComponent } from "../../order/list/order-list/order-list.component";
import { ScanningOrderListComponent } from "../../order/scanning-list/scanning-order-list/scanning-order-list.component";
import { IOrderShowcaseDicts, OrderTypesFilterFactory } from "../showcase.component";
import { NSIDocumentType } from "@reinform-cdp/core";

@Component({
  selector: 'mggt-desktop-showcase',
  templateUrl: './desktop.component.html',
  styleUrls: ['./desktop.component.scss']
})
export class DesktopShowcaseComponent implements OnInit {

  loading: LoadingStatus = LoadingStatus.LOADING;
  success = false;
  dicts: IOrderShowcaseDicts;

  _search: string = '';
  search: string = '';

  title: string;

  activeTab: string;
  suffix: string;
  tabs: OrderShowcaseTabModel[] = [];

  @ViewChild(OrderListComponent)
  tab: OrderListComponent;

  @ViewChild(ScanningOrderListComponent)
  scanningTab: ScanningOrderListComponent;

  constructor(public $state: StateService, public nsiRestService: NsiResourceService, public orderTypeService: OrderTypeService,
    public session: SessionStorage, public authorizationService: AuthorizationService, public breadcrumbsService: BreadcrumbsService, public transition: Transition) {
  }

  ngOnInit() {

    this.updateBreadcrumbs();

    this.loading = LoadingStatus.LOADING;

    let currentYear = (new Date()).getFullYear();
    let availableYears = _.range(currentYear, 2003).map(year => {
      return year.toString()
    });

    forkJoin([
      this.nsiRestService.get('District'),
      this.orderTypeService.geOrderTypes(),
      this.nsiRestService.get('mggt_order_OrderStatuses'),
      this.nsiRestService.get('mggt_order_OrderThemes'),
      this.nsiRestService.departments(),
      this.nsiRestService.get('mggt_privacy')
    ]).subscribe((response: [any[], OrderType[], OrderStatus[], OrderTheme[], DepartmentBean[],NSIDocumentType[]]) => {
      let orderTypesFilterId = this.transition.params()['orderTypesFilter'];
      let orderTypesFilter = OrderTypesFilterFactory.getFilter(orderTypesFilterId);

      this.dicts = {
        availableYears: availableYears,
        district: response[0],
        orderTypes: orderTypesFilter.filter(response[1]),
        statusAnalytics: response[2].map(item => item.statusAnalytic).filter((value, index, self) => self.indexOf(value) === index),
        orderStatuses: response[2],
        orderThemes: response[3],
        departments: response[4],
        privacyTypes:response[5]
      };

      this.title = this.transition.params()['title'];

      this.suffix = this.transition.params()['suffix'];
      let tabBuilder: OrderShowcaseTabBuilder = OrderShowcaseTabBuilderFactory.getBuilder(this.transition.params()['tabBuilder']);
      this.tabs = tabBuilder.build(this.authorizationService, this.session, this.dicts.orderTypes);
      this.getFromStorage();
      let activeTab = this.transition.params()['activeTab'];
      if (activeTab) {
        this.activeTab = activeTab;
      }
      this.saveToStorage();

      if (this.activeTab !== 'mka') {
        this.dicts.orderTypes = this.dicts.orderTypes.filter(orderType => !orderType.orderMKA);
      }

      this.loading = LoadingStatus.SUCCESS;
      this.success = true;

    }, () => {
      this.loading = LoadingStatus.ERROR;
    });

  }

  onCommonSearchChange() {
    this.search = this._search;
    this.saveToStorage();
  }

  saveToStorage(): void {
    localStorage[`${this.session.login()}.orders${this.suffix}.common`] = this.search;
    localStorage[`${this.session.login()}.orders${this.suffix}.tab`] = this.activeTab;
  }

  getFromStorage(): void {
    if (localStorage[`${this.session.login()}.orders${this.suffix}.common`]) {
      this.search = localStorage[`${this.session.login()}.orders${this.suffix}.common`];
      this._search = this.search;
    }

    if (localStorage[`${this.session.login()}.orders${this.suffix}.tab`]) {
      this.activeTab = localStorage[`${this.session.login()}.orders${this.suffix}.tab`]
    } else {
      this.activeTab = this.tabs && this.tabs.length && (this.tabs.length > 0) ? this.tabs[0].alias : null;
    }
  }

  isActive(index: string): boolean {
    return index === this.activeTab;
  }

  activate(index: string): boolean {
    if (this.activeTab === index) {
      return false;
    } else {
      this.activeTab = index;
      this.saveToStorage();
      return true;
    }
  }

  updateBreadcrumbs() {
    this.breadcrumbsService.setBreadcrumbsChain([{
      title: 'Приказы',
      url: null
    }]);
  }

  searchChanged() {
    if (this.tab) {
      this.tab.commonChange(this.search);
    }
    if (this.scanningTab) {
      this.scanningTab.commonChange(this.search);
    }
  }


}
