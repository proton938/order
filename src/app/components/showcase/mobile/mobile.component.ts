import { StateService, Transition } from '@uirouter/core';
import { Component, ViewChild } from '@angular/core';
import { DesktopShowcaseComponent } from "../desktop/desktop.component";
import { AuthorizationService, SessionStorage } from "@reinform-cdp/security";
import { NsiResourceService } from "@reinform-cdp/nsi-resource";
import { BreadcrumbsService } from "@reinform-cdp/skeleton";
import { OrderTypeService } from "../../../services/order-type.service";
import { MobileOrderListComponent } from '../../order/list/order-list/mobile/mobile.component';

@Component({
  selector: 'mggt-mobile-showcase',
  templateUrl: './mobile.component.html',
  styleUrls: ['./mobile.component.scss'],
})
export class MobileShowcaseComponent extends DesktopShowcaseComponent {
  hideOvers: boolean = true;
  @ViewChild(MobileOrderListComponent)
  mobileTab: MobileOrderListComponent;

  @ViewChild(MobileOrderListComponent)
  mobileScanningTab: MobileOrderListComponent;

  constructor(public $state: StateService, public nsiRestService: NsiResourceService, public orderTypeService: OrderTypeService,
    public session: SessionStorage, public authorizationService: AuthorizationService, public breadcrumbsService: BreadcrumbsService, public transition: Transition) {
      super($state, nsiRestService, orderTypeService, session, authorizationService, breadcrumbsService, transition);
  }
  activate(index: string): boolean {
    if (this.isActive(index)) {
      this.hideOvers = !this.hideOvers;
      return false;
    }

    this.hideOvers = true;
    return super.activate(index);
  }

  searchChanged() {
    if (this.mobileTab) {
      this.mobileTab.commonChange(this.search);
    }
    if (this.mobileScanningTab) {
      this.mobileScanningTab.commonChange(this.search);
    }
  }
}
