import {BsModalRef} from "ngx-bootstrap";
import {Component, OnInit} from '@angular/core';
import {StateService} from "@uirouter/core";

export class RedirectToTaskSettings {
    documentID: string;
    documentTypeValue: string;
}

@Component({
    selector: 'order-modal-content',
    templateUrl: './redirect-to-task.modal.html'
})
export class RedirectToTaskModalComponent implements OnInit {

    linked: RedirectToTaskSettings;

    constructor(private bsModalRef: BsModalRef, public $state: StateService) {
    }

    ngOnInit() {
    }

    cancel() {
        this.bsModalRef.hide();
    }

}
