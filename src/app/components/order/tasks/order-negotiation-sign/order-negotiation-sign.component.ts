import { CdpReporterResourceService } from '@reinform-cdp/reporter-resource';
import { NsiResourceService } from '@reinform-cdp/nsi-resource';
import * as _ from 'lodash';
import { formatDate } from '@angular/common';
import { catchError, map, mergeMap, tap } from 'rxjs/internal/operators';
import { FileResourceService, IFileInFolderInfo, IFileType } from '@reinform-cdp/file-resource';
import { from, of, throwError } from 'rxjs';
import { BsModalService } from 'ngx-bootstrap';
import * as angular from 'angular';
import { Component, OnInit } from '@angular/core';
import { Ds, Order, OrderApprovalListItem, OrderDraftFile, OrderApprover } from '../../../../models/order/Order';
import { ExFileType, LoadingStatus } from '@reinform-cdp/widgets';
import { ICertificateInfoEx, SessionStorage, SignService } from '@reinform-cdp/security';
import { ActiveTaskService, ActivityResourceService, ITask, CdpBpmHistoryResourceService } from '@reinform-cdp/bpm-components';
import { OrderType } from '../../../../models/order/OrderType';
import { ProcessComponent } from '../order-process';
import { ToastrService } from 'ngx-toastr';
import { OrderService } from '../../../../services/order.service';
import { Observable } from 'rxjs/Rx';
import { OrderTypeService } from '../../../../services/order-type.service';
import { ApprovalType } from '../../../../models/order/ApprovalType';
import { RegisterDocumentService } from '../../../../services/register-document.service';
import { RedirectToTaskModalComponent, RedirectToTaskSettings } from '../redirect-to-task/redirect-to-task.modal';
import { FileType } from '../../../../models/order/FileType';
import { CancelDocumentService } from '../../../../services/cancel-document.service';
import * as moment_ from 'moment';
import { DropzoneConfigInterface } from 'ngx-dropzone-wrapper';
import { AddApproversModalComponent } from '../add-approvers/add-approvers.modal';
import { OrderActivitiService } from '../../../../services/order-activiti.service';
import {HelperService} from '../../../../services/helper.service';

const moment = moment_;

@Component({
  selector: 'mggt-order-negotiation-sign',
  templateUrl: './order-negotiation-sign.component.html',
  styleUrls: ['./order-negotiation-sign.component.scss']
})
export class OrderNegotiationSignComponent extends ProcessComponent implements OnInit {

  loadingStatus: LoadingStatus;
  success = false;
  task: ITask;
  order: Order;
  origOrder: Order;
  orderType: OrderType;
  showDocumentText: boolean;

  loopCounter: number;
  lastApprovalListItem: boolean;
  approvalState: boolean;

  isParallel: boolean;
  isMainApprover: boolean;
  needToCloseCycle: boolean;

  approvalNote: string = '';

  showApprovalList: boolean = false;

  sendingBackToWork: boolean;
  agreeing: boolean;
  approving: boolean;
  addingApprovals: boolean = false;
  approvalTypes: ApprovalType[];
  approvalType: ApprovalType;

  isAllowedAddingApprovals: boolean = true;


  openSignDialog: () => void;
  openMobileSignDialog: () => void;
  orderFile: ExFileType = new ExFileType();
  isMobile: boolean = false;
  approvalDepth: number = 0;

  checkSign: string;

  dropzoneConfig: DropzoneConfigInterface;

  constructor(public activeTaskService: ActiveTaskService,
    private orderTypeService: OrderTypeService,
    public activityResourceService: ActivityResourceService,
    private registerDocumentService: RegisterDocumentService,
    private modalService: BsModalService,
    private cancelDocumentService: CancelDocumentService,
    private orderActivitiService: OrderActivitiService,
    public signService: SignService,
    toastr: ToastrService,
    orderService: OrderService,
    fileResourceService: FileResourceService,
    nsiResourceService: NsiResourceService,
    sessionStorage: SessionStorage,
    reporterResourceService: CdpReporterResourceService,
    public helper: HelperService,
    public historyResourceService: CdpBpmHistoryResourceService) {
    super(toastr, orderService, fileResourceService, nsiResourceService, sessionStorage, reporterResourceService);
  }

  ngOnInit() {
    this.helper.beforeUnload.init();
    this.loadingStatus = LoadingStatus.LOADING;
    this.activeTaskService.getTask().pipe(
      mergeMap(task => {
        this.task = task;

        return this.getEntityIdVar(task);
      }),
      mergeMap(id => {
        return this.orderService.get(id);
      }),
      mergeMap(order => {
        this.order = order;
        this.origOrder = angular.copy(order);

        // const loopCounterVar = this.task.variables.find(v => {
        //   return v.name === 'loopCounter';
        // });

        // if (loopCounterVar) {
        //   this.loopCounter = <number>loopCounterVar.value;
        // } else {
        let taskOwnerLogin: string;
        // Определяем реального владельца задачи
        if (!!this.task && !!this.task.owner) {
          taskOwnerLogin = this.task.owner; // Владелец делегированной задачи
        } else if (!!this.task && !!this.task.assignee && this.task.assignee !== this.session.login()) {
          taskOwnerLogin = this.task.assignee; // Задача делегирована, но только взята в работу (не перезагружена)
        } else taskOwnerLogin = this.session.login();
        this.loopCounter = this.order.approval.approvalCycle.agreed.findIndex(agree => {
          return agree.agreedBy.accountName === taskOwnerLogin && !agree.approvalResult;
          //this.session.login();
        });
        // }

        if (this.loopCounter === undefined) {
          this.toastr.error('Не удалось определить номер согласующего в листе согласования.');
          this.loadingStatus = LoadingStatus.ERROR;

          return throwError('Не удалось определить номер согласующего в листе согласования.');
        }

        this.approvalState = this.order.approval.approvalCycle.agreed[this.loopCounter].approvalTypeCode === ApprovalType.approval;
        this.approvalDepth = this.order.approval.approvalCycle.agreed[this.loopCounter].approvalNum.split('.').length;
        this.lastApprovalListItem = this.loopCounter === this.order.approval.approvalCycle.agreed.length - 1;

        this.needToCloseCycle = true;

        // if (!this.order.activeApprover) {
        //   this.order.activeApprover = this.session.fioShort();
        // }

        if (!this.order.documentDate) {
          this.order.documentDate = new Date();
        }

        if (!this.order.approval.approvalCycle.agreed[this.loopCounter].approvalPlanDate) {
          this.order.approval.approvalCycle.agreed[this.loopCounter].approvalPlanDate = this.task.dueDate;
          return this.updateOrder().pipe(
            mergeMap(() => {
              return this.loadDictionaries();
            })
          );
        } else {
          return this.loadDictionaries();
        }
      }),
      mergeMap(() => {
        if (this.order.approval.approvalCycle.agreed[this.loopCounter].added && this.order.approval.approvalCycle.agreed[this.loopCounter].added.length > 0) {
          this.isAllowedAddingApprovals = false;
        }

        return this.signService.checkSign();
      }),
      mergeMap(res => {
        this.checkSign = res;
        this.orderFile = OrderDraftFile.toExFileType(this.order.draftFilesWithoutAttWord);
        return this.nsiResourceService.getDictFromCache('mggt_order_ApprovalType');
      })
    ).subscribe((response) => {
      this.approvalTypes = response.value;
      this.order.approval.approvalCycle.agreed.forEach((a) => {
        if (!a.approvalTime && a.approvalTypeCode) {
          const approvalType = this.approvalTypes.find(at => at.approvalTypeCode === a.approvalTypeCode);
          a.approvalTime = approvalType.approvalDuration;
        }
      });

      // .find(agreed => agreed.agreedBy.accountName === this.session.login());
      // берем запись по процессу а не ищем по логину
      const approval = this.order.approval.approvalCycle.agreed[this.loopCounter];
      this.approvalType = this.approvalTypes.find(type => type.approvalTypeCode === approval.approvalTypeCode);
      this.initDropzone();
      this.updateCurrentAgree();
      this.loadingStatus = LoadingStatus.SUCCESS;
      this.success = true;
    }, (error) => {
      console.error(error);
      this.loadingStatus = LoadingStatus.ERROR;
    });
  }

  ngOnDestroy() {
    this.helper.beforeUnload.destroy();
  }

  updateCurrentAgree() {
    let currentAgree = this.order.approval.approvalCycle.agreed[this.loopCounter];
    if (currentAgree.agreedBy.accountName == this.session.login()) {
      currentAgree.factAgreedBy=null;
      return;
    }
    // currentAgree.agreedBy =
    currentAgree.factAgreedBy = OrderApprover.fromUserBean({
      post: this.session.post(),
      displayName: this.session.fullName(),
      accountName: this.session.login(),
      fioShort: this.session.fioShort(),
      iofShort: this.session.iofShort(),
      telephoneNumber: this.session.telephoneNumber()
    });
  }

  loadDictionaries(): Observable<any> {
    return this.orderTypeService.geOrderTypes().pipe(
      tap(orderTypes => {
        this.orderType = orderTypes.find(val => val.code === this.order.documentTypeCode);
        // TODO: check OASI_ADMORDER_OGS
        this.showDocumentText = this.orderType.accessAction === 'OASI_ADMORDER_OGS';
      })
    )
  }

  initDropzone() {
    let component = this;
    this.dropzoneConfig = {
      autoProcessQueue: true,
      parallelUploads: 1,
      params: {
        fileType: 'MkaDocOther',
        docSourceReference: 'UI'
      },
      init: function () {
        this.on("processing", function (file) {
          this.options.params.folderGuid = component.order.folderID;
          this.options.params.docEntityID = component.order.documentID;
        });
        this.on("error", function (file, errorMessage) {
          component.toastr.warning(errorMessage.message ? errorMessage.message : errorMessage, 'Ошибка');
          this.removeFile(file);
        });
        this.on("success", function (file, guid) {
          let agreed = component.order.approval.approvalCycle.agreed[component.loopCounter];
          agreed.fileApproval = agreed.fileApproval || [];
          const fileType: FileType = FileType.create(guid, file.name, file.size.toString(), false, new Date(), file.type, 'MkaDocOther');
          agreed.fileApproval.push(fileType);
          this.removeFile(file);
        });
      }
    }
  }

  sendBackToWork() {
    if (!this.approvalNote) {
      this.toastr.error('Не указан комментарий');
      return;
    }
    this.helper.beforeUnload.start();
    this.sendingBackToWork = true;
    const ApprovalDurations = this.order.approval.approvalCycle.agreed.map((agreed) => (agreed.approvalTime)).join(',');
    this.checkProcess().pipe(
      mergeMap(() => {
        let agreed = this.order.approval.approvalCycle.agreed[this.loopCounter];
        return this.approve(this.order, agreed, this.task.id, false, {
          approvalNote: this.approvalNote
        }, this.needToCloseCycle)
      }),
      mergeMap(() => this.updateOrder()),
      mergeMap(() => {
        const params: any = [
          { name: 'AddApproval', value: false },
          { name: 'IsApproved', value: false }
        ];

        return from(this.activityResourceService.finishTask(parseInt(this.task.id), params));
      })
    ).subscribe(
      () => {
        this.helper.beforeUnload.stop();
        console.log('subscribe');
        this.sendingBackToWork = false;
        this.toastr.success('Документ отправлен на доработку.');
        window.location.href = '/main/#/app/tasks';
      },
      (err) => {
        this.helper.beforeUnload.stop();
        console.log('subscribe err', err);
        this.sendingBackToWork = false;
      });
  }

  beforeSign = function (fileInfo: ExFileType, folderGuid, certEx) {
    const isElectronSign = this.isSigningElectronically();
    if(isElectronSign) {
      this.order.ds = this.makeDS(certEx);
    }

    return this.makeApproveFile(fileInfo, certEx).toPromise();
  }.bind(this);

  makeApproveFile(fileInfo: ExFileType, certEx?): Observable<any> {
    // let ds = this.makeDS(certEx);
    let draftFilesWithoutAttPdf;
    const isElectronSign = this.isSigningElectronically();
    return this.checkProcess().pipe(
      mergeMap(() => {
        let agreed = this.order.approval.approvalCycle.agreed[this.loopCounter];
        return this.approve(this.order, agreed, this.task.id, true, {
          approvalNote: this.approvalNote,
          agreedDs: isElectronSign && certEx ? null : this.makeDS(certEx, 'agreedDs')
        }, false)
      }),
      mergeMap(() => {
        return this.applyOrderStamp(this.orderFile);
      }),
      mergeMap(id => {
        return this.convertFileTypeToPdf(id, fileInfo.nameFile);
      }),
      mergeMap((draftFilePdf: IFileInFolderInfo) => {
        return this.appendLS(draftFilePdf.versionSeriesGuid);
      }),
      mergeMap((extendedDraftFilePdf: IFileInFolderInfo) => {
        const fileName = `${this.orderType.name.split(' ')[0]} № ${this.order.documentNumber} от ${formatDate(this.order.documentDate, 'dd.MM.yyyy', 'en')}.pdf`;
        return this.appendAttach(extendedDraftFilePdf.versionSeriesGuid, fileName);
      }),
      mergeMap((orderFile: FileType) => {
        if (isElectronSign) {
          const exFile = ExFileType.create(orderFile.fileID, orderFile.fileName, +orderFile.fileSize, false,
            new Date(orderFile.fileDate).getTime(), orderFile.fileType, orderFile.mimeType);

          _.extend(this.orderFile, exFile);

          return this.setOrderFile(orderFile).pipe(
            map(_ => exFile)
          );
        } else {
          this.orderFile = OrderDraftFile.toExFileType(this.order.draftFiles);

          return this.setOrderFile(orderFile).pipe(
            map(_ => this.orderFile)
          );
        }
      })
    )
  }

  private appendLS(draftFilePdfId: string) {
      return from(this.reporterResourceService.nsi2Filenet(<any>{
          options: {
              jsonSourceDescr: "",
              onProcess: "Утвердить проект РД",
              placeholderCode: "",
              strictCheckMode: true,
              xwpfResultDocumentDescr: 'Лист согласования с основными согласующими.docx',
              xwpfTemplateDocumentDescr: ""
          },
          jsonTxt: this.helper.toJson({ document: this.order }),
          rootJsonPath: '$',
          nsiTemplate: {
              templateCode: 'AdmOrderLS'
          },
          filenetDestination: {
              fileName: 'Лист согласования с основными согласующими.docx',
              fileType: 'MkaDocOther',
              folderGuid: this.order.folderID,
              fileAttrs: [
                  { attrName: 'docEntityID', attrValues: [this.order.documentID] },
                  { attrName: 'docSourceReference', attrValues: ['UI'] },
              ]
          }

      })).pipe(
          mergeMap((result: any) => {
              return this.convertFileTypeToPdf(result.versionSeriesGuid, result.fileName);
          }),
          mergeMap((result: any) => {
              let request: any = {
                  filenetDestination: {
                      fileAttrs: [
                          { attrName: 'docEntityID', attrValues: [this.order.documentID] },
                          { attrName: 'docSourceReference', attrValues: ['UI'] }
                      ],
                      fileName: `Проект РД с листом согласования.pdf`,
                      fileType: 'MkaDocOther',
                      folderGuid: this.order.folderID,
                  },
                  versionSeriesGuids: [draftFilePdfId, result.versionSeriesGuid]
              };

              return this.reporterResourceService.pdfConcatenateFiles(request)
          }),
      );
  }

    private appendAttach(extendedDraftFilePdfId: string, fileName: string) {
        let observable;
        if (!this.order.attach && !this.order.attachWord) {
            observable = from(this.fileResourceService.copyFile(extendedDraftFilePdfId, fileName));
        } else {
            // let pdfIdObservable = !this.order.attachWord ? of(this.order.attach.draftFilesID) :
            //     this.convertFileTypeToPdf(this.order.attachWord.draftFilesID, this.order.attachWord.draftFilesName).pipe(
            //         map(file => file.versionSeriesGuid)
            //     );
            let pdfIdObservable = this.convertToPdfIfNecessary(this.order.attach ? this.order.attach : this.order.attachWord);
            observable = pdfIdObservable
                .pipe(
                    mergeMap((attachPdf: OrderDraftFile)=>{//pdfId: string) => {
                      let pdfId: string = attachPdf.draftFilesID;
                        let request: any = {
                            filenetDestination: {
                                fileAttrs: [
                                    {attrName: 'docEntityID', attrValues: [this.order.documentID]},
                                    {attrName: 'docSourceReference', attrValues: ['UI']}
                                ],
                                fileName: fileName,
                                fileType: 'MkaDocOther',
                                folderGuid: this.order.folderID,
                            },
                            versionSeriesGuids: [extendedDraftFilePdfId, pdfId]
                        };

                        return this.reporterResourceService.pdfConcatenateFiles(request)
                    })
                );
        }
        return observable.pipe(
            map((fileType: any) => FileType.create(fileType.versionSeriesGuid, fileType.fileName, fileType.fileSize.toString(),
                false, new Date(), fileType.fileType, fileType.mimeType)
            )
        )
    }

  makeDS(certEx, prefix: string = 'ds'): any {
    return certEx ? {
      [prefix + 'LastName']:  certEx.lastName,
      [prefix + 'Name']:      certEx.firstName,
      [prefix + 'Position']:  certEx.position,
      [prefix + 'CN']:        certEx.serialNumber,
      [prefix + 'From']:      moment(certEx.from, 'DD.MM.YYYY HH:mm:ss').toDate(),
      [prefix + 'To']:        moment(certEx.till, 'DD.MM.YYYY HH:mm:ss').toDate()
    } : null;
  }

  applyOrderStamp(file: ExFileType, signInfo?: { ds: Ds }): Observable<string> {
    return from(this.reporterResourceService.filenet2Filenet(<any>{
      jsonTxt: JSON.stringify(this.order),
      filenetTemplate: {
        templateVersionSeriesGuid: file.idFile
      },
      filenetDestination: {
        reportFileName: file.nameFile,
        reportFileType: file.typeFile,
        reportFolderGuid: this.order.folderID,
        fileName: file.nameFile,
        fileType: file.typeFile,
        folderGuid: this.order.folderID
      },
      rootJsonPath: "$",
      options: {
        onProcess: file.nameFile.replace('.docx', ''),
        placeholderCode: 'sn',
        strictCheckMode: true,
        xwpfResultDocumentDescr: file.nameFile,
        xwpfTemplateDocumentDescr: ""
      }
    })).pipe(
      map(response => response.versionSeriesGuid)
    );
  }

  afterSign = function (result: any) {
    if (result.error) {
      this.helper.beforeUnload.stop();
      console.error(result);
      this.orderFile = OrderDraftFile.toExFileType(this.order.draftFilesWithoutAttWord);
      this.approving = false;
      return;
    }
    let canceledDocument = this.order.canceledDocument;
    let certEx: ICertificateInfoEx = result.result.certEx;
    let isElectronSign = this.isSigningElectronically();
    this.orderFile.signed = true;

    if (isElectronSign) {
      _.find(this.order.files, (f: FileType) => f.fileID === this.orderFile.idFile).fileSigned = true;
    } else {
      this.order.draftFiles.draftFilesSigned = true;
    }

    // this.order.activeApprover = null;

    this.updateOrder().pipe(
      mergeMap(() => {
        return this.cancelDocumentService.cancelDocument(canceledDocument, this.order.documentDate);
      }),
      mergeMap(() => {
        const params: any[] = [
          { name: 'AddApproval', value: false },
          { name: 'IsApproved', value: true }
        ];

        return from(this.activityResourceService.finishTask(parseInt(this.task.id), params));
      })
    ).subscribe(() => {
      this.helper.beforeUnload.stop();
      this.approving = false;
      if (isElectronSign) {
        this.toastr.success('Электронный документ успешно подписан');
      } else {
        this.toastr.success('Электронный документ успешно согласован');
      }

      window.location.href = '/main/#/app/tasks';
    }, (err) => {
      this.helper.beforeUnload.stop();
      console.error(err);
      if (err.documentID && err.documentTypeValue) {
        this.redirectToTask({
          documentID: err.documentID,
          documentTypeValue: err.documentTypeValue
        });
      }
      this.approving = false;
    });
  }.bind(this);

  approveAndSendForward() {
    this.helper.beforeUnload.start();
    this.approving = true;
    let canceledDocument = this.order.canceledDocument;
    this.checkProcess().pipe(
      mergeMap(() => {
        return this.registerDocumentService.checkLinkedDocuments(this.order);
      }),
      mergeMap(() => {
        return this.cancelDocumentService.checkCanceledDocument(canceledDocument);
      }),
      mergeMap(() => {
        return this.checkLinkedDocumentsRegistration();
      }),
      catchError(error => {
        this.toastr.error(error);
        return throwError(error);
      }),
      mergeMap(() => {
        return this.generateDocumentNumber();
      }),
      mergeMap(() => {
        return this.generateBarcode();
      })
    ).subscribe(() => {
      if (this.agreedSigner) {
        switch(this.agreedSigner.approvalTypeCode) {
          case 'approval':
          case 'agreed':
            if (this.isMobile) {
              this.openMobileSignDialog();
            } else {
              this.openSignDialog();
            }
            break;
          case 'assent':
            this.makeApproveFile(this.orderFile).subscribe(file => {
              this.helper.success('Электронный документ успешно согласован');
              this.updateOrder().pipe(
                mergeMap(() => {
                  return this.cancelDocumentService.cancelDocument(canceledDocument, this.order.documentDate);
                }),
                mergeMap(() => {
                  const params: any[] = [
                    { name: 'AddApproval', value: false },
                    { name: 'IsApproved', value: true }
                  ];
                  return from(this.activityResourceService.finishTask(parseInt(this.task.id), params));
                })
              ).subscribe(() => {
                this.helper.beforeUnload.stop();
                this.approving = false;
                this.helper.success('Электронный документ успешно согласован');
                window.location.href = '/main/#/app/tasks';
              }, (err) => {
                this.helper.beforeUnload.stop();
                console.error(err);
                if (err.documentID && err.documentTypeValue) {
                  this.redirectToTask({
                    documentID: err.documentID,
                    documentTypeValue: err.documentTypeValue
                  });
                }
                this.approving = false;
              });
            }, error => {
              this.approving = false;
              this.helper.error(error);
            });
            break;
        }
      }

    }, () => {
      this.helper.beforeUnload.stop();
      this.approving = false;
    });
  }

  getNextApprover() {
    let agreed = this.order.approval.approvalCycle.agreed;
    let approvalNum = agreed[this.loopCounter].approvalNum;
    let nextApprover = agreed.filter(a => !a.approvalFactDate)
      .filter(a => a.approvalNum.startsWith(approvalNum + '.'))
      .find(a => true);
    if (!nextApprover) {
      let nextApprovalNum = '' + (parseInt(approvalNum) + 1);
      nextApprover = agreed.find(a => a.approvalNum === nextApprovalNum);
    }
    if (!nextApprover) {
      nextApprover = agreed.filter(a => !a.approvalFactDate)
        .find(a => true);
    }
    return nextApprover;
  }

  deleteApprovalFile = function (fileInfo: IFileType) {
    from(this.fileResourceService.deleteFile(fileInfo.idFile)).subscribe(
      () => {
        let index = this.order.approval.approvalCycle.agreed[this.loopCounter].fileApproval.findIndex(file => file.fileID === fileInfo.idFile);
        this.order.approval.approvalCycle.agreed[this.loopCounter].fileApproval.splice(index, 1);
      }
    )
  }.bind(this);

  addApproval() {
    this.helper.beforeUnload.start();
    let approval = this.order.approval.approvalCycle.agreed[this.loopCounter];
    const options = {
      initialState: {
        systemCode: this.order.systemCode,
        documentTypeCode: this.order.documentTypeCode,
        dueDate: this.task.dueDate,
        except: _.map(this.order.approval.approvalCycle.agreed, (a) => (a.agreedBy.accountName)),
        approvalNum: approval.approvalNum
      },
      ignoreBackdropClick: true,
      'class': 'modal-xl modal-lg'
    };
    const ref = this.modalService.show(AddApproversModalComponent, options);
    this.modalService.onHide.pipe(
      mergeMap(() => {
        return this.checkProcess();
      }),
      mergeMap(() => {
        let component = <AddApproversModalComponent>ref.content;
        if (component.submit) {
          this.addingApprovals = true;
          let agreed = this.order.approval.approvalCycle.agreed[this.loopCounter];
          agreed.added = agreed.added || [];
          component.agreed.forEach((added,index) => {
            if(!added.approvalNum)
              added.approvalNum=approval.approvalNum+'.'+(index+1).toString();
            agreed.added.push({
              additionalAgreed: {
                accountName: added.agreedBy.accountName,
                fio: added.agreedBy.fioFull,
                iofShort: added.agreedBy.iofShort
              },
              addedNote: added.note
            });
          });
          let newApprovers: OrderApprovalListItem[] = component.agreed.map((added) => {
            return {
              approvalNum: added.approvalNum,
              approvalTime: added.approvalTime,
              agreedBy: added.agreedBy,
              approvalType: added.approvalType,
              approvalTypeCode: added.approvalTypeCode,
              Parallel: false,
              Signer: false,
            }
          });
          newApprovers.reverse().forEach(newApprover => {
            this.order.approval.approvalCycle.agreed.splice(this.loopCounter, 0, newApprover)
          });
          return this.updateOrder().pipe(
            mergeMap(() => {
              // return this.orderActivitiService.addApprovers(this.task.id, this.order.approval.approvalCycle.agreed);
              const ApprovalUsers = newApprovers.map((agreed) => (agreed.agreedBy.accountName)).join(',');
              const ApprovalDurations = newApprovers.map((agreed) => (agreed.approvalTime)).join(',');
              const params = [
                { name: 'AddApproval', value: true },
                { name: 'ApprovalUsers', value: ApprovalUsers },
                { name: 'ApprovalDurations', value: ApprovalDurations },
              ];

              return from(this.activityResourceService.finishTask(parseInt(this.task.id), params));
            }),
            tap(() => {
              this.helper.beforeUnload.stop();
              window.location.href = '/main/#/app/tasks';
            })
          );
        } else {
          return throwError('cancelled');
        }
      })
    ).subscribe(() => {
      this.helper.beforeUnload.stop();
      this.addingApprovals = false;
    }, () => {
      this.helper.beforeUnload.stop();
      this.addingApprovals = false;
    });
  }

  private checkProcess(): Observable<any> {
    return this.historyResourceService.getHistoricTaskInstance(this.task.id).pipe(
      mergeMap((response) => {
        // console.log(response);
        return this.historyResourceService.getHistoricProcessInstance(response.processInstanceId);
      }),
      mergeMap((response) => {
        console.log(response);
        if (response.endTime) {
          this.toastr.warning('Отозвано инициатором РД');
          window.location.href = '/main/#/app/tasks';
          return throwError('Отозвано инициатором РД');
        }
        return of('');
      })
    );
  }

  isSigningElectronically() {
    return this.order.signingElectronically && this.order.signingElectronically.toString() === 'true';
  }

  private redirectToTask(linked: RedirectToTaskSettings) {
    const options = {
      initialState: {
        linked: linked,
      },
      'class': 'modal-lg'
    };
    const ref = this.modalService.show(RedirectToTaskModalComponent, options);
    this.modalService.onHide.pipe(
      mergeMap(() => {
        return of('');
      })
    );
  }

  get agreedSigner(): OrderApprovalListItem {
      return _.find(this.order.approval.approvalCycle.agreed, i => i.Signer);
  }
}
