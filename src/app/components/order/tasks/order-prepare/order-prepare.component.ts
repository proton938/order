import { ExFileType, LoadingStatus } from "@reinform-cdp/widgets";
import * as angular from "angular";
import * as _ from "lodash";
import { FileResourceService } from "@reinform-cdp/file-resource";
import { ActiveTaskService, ActivityResourceService, ITask,IProcessDefinition } from "@reinform-cdp/bpm-components";
import { Observable } from "rxjs/Rx";
import { DepartmentBean, NsiResourceService, UserBean } from "@reinform-cdp/nsi-resource";
import { AuthorizationService, SessionStorage } from "@reinform-cdp/security";
import { ToastrService } from "ngx-toastr";
import { forkJoin, from,throwError,of } from "rxjs/index";
import { mergeMap, tap,catchError,map } from "rxjs/internal/operators";
import { Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { OrderApprovalListItem, OrderDraftFile } from "../../../../models/order/Order";
import { OrderType } from "../../../../models/order/OrderType";
import { OrderTypeService } from "../../../../services/order-type.service";
import { OrderService } from "../../../../services/order.service";
import { ProcessComponent } from "../order-process";
import { ApprovalType } from "../../../../models/order/ApprovalType";
import { DropzoneConfigInterface } from "ngx-dropzone-wrapper";
import { CdpReporterResourceService } from "@reinform-cdp/reporter-resource";
import { ApprovalListEditComponent } from "../approval-list-edit/approval-list-edit.component";
import { EmailGroup } from "../../../../models/order/EmailGroup";
import { OrderStatus } from "../../../../models/order/OrderStatus";
import { UserService } from "../../../../services/user.service";
import { ApprovalDuration } from "../../../../models/order/ApprovalDuration";
import { OrderNsiService } from "../../../../services/order-nsi.service";
import { NSIDocumentType } from "@reinform-cdp/core";
import {HelperService} from "../../../../services/helper.service";
import { FileExtService } from "../../../../services/file-ext.service";

@Component({
  selector: 'mggt-order-prepare',
  templateUrl: './order-prepare.component.html',
  styleUrls: ['./order-prepare.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class OrderPrepareComponent extends ProcessComponent implements OnInit {

  loadingStatus: LoadingStatus;
  success = false;
  task: ITask;
  account: any = {};
  orderType: OrderType;

  orderTypes: OrderType[];
  emailGroups: EmailGroup[];
  orderStatuses: OrderStatus[];
  signUsers: UserBean[];
  executives: UserBean[];
  approvers: UserBean[];
  approvalTypes: ApprovalType[];
  approvalDurations: ApprovalDuration[];
  privacyTypes:NSIDocumentType[];

  saving: boolean;
  sendingToApproval: boolean;
  sendingToRegistration: boolean;
  creatingDraftFile: boolean;
  creatingDraftFilesWithoutAtt: boolean;
  combiningDraftFile: boolean;

  // dzDraftFilesWithoutAttOptions: DropzoneConfigInterface;
  // dzDraftFilesAttOptions: DropzoneConfigInterface;

  draftFilesWithoutAtt: ExFileType[] = [];
  attach: ExFileType[] = [];

  hasPreregPermission: boolean;
  canViewRegButton: boolean;
  filePathForWord:string;
  showButton: boolean = false;

  startLock: boolean = true;
  onDeletedFileNoApp: boolean = false;
  onDeletedFileApp: boolean = true;
  onDeletedCreateFileApp: boolean = false;

  @ViewChild(ApprovalListEditComponent)
  appovalListEdit: ApprovalListEditComponent;

  constructor(private orderTypeService: OrderTypeService,
    public helper: HelperService,
    private activeTaskService: ActiveTaskService, private authorizationService: AuthorizationService,
    private activityResourceService: ActivityResourceService,
    private userService: UserService, private orderNsiService: OrderNsiService,
    private fileService:FileExtService,
    toastr: ToastrService, orderService: OrderService, fileResourceService: FileResourceService,
    nsiResourceService: NsiResourceService, session: SessionStorage,
    reporterResourceService: CdpReporterResourceService) {
    super(toastr, orderService, fileResourceService, nsiResourceService, session, reporterResourceService);
  }

  ngOnInit() {
    this.helper.beforeUnload.init();
    this.loadingStatus = LoadingStatus.LOADING;
    // TODO: OASI_ADMORDER_PREREGBUTTON
    this.hasPreregPermission = this.authorizationService.check('OASI_ADMORDER_PREREGBUTTON');
    this.canViewRegButton = this.session.hasPermission('SDO_ORDER_ORDER_REG_WITHOUT_APPROVE');

    this.account.accountName = this.session.login();
    this.account.fio = this.session.name();
    this.account.post = this.session.post();

    this.activeTaskService.getTask().pipe(
      mergeMap(task => {
        this.task = task;
        return this.getEntityIdVar(task);
      }),
      mergeMap(id => {
        return this.orderService.get(id);
      }),
      mergeMap(order => {
        this.order = order;
        this.origOrder = angular.copy(order);

        this.order.responsibleExecutor = {
          fio: this.session.fullName(),
          fioFull: this.session.fullName(),
          accountName: this.session.login(),
          post: this.session.post(),
          email: this.session.mail(),
          phoneNumber: this.session.telephoneNumber(),
          iofShort: this.session.iofShort()
        };

        // this.initDropzone();
        return this.loadDictionaries();
      })
    ).subscribe(() => {
      if(this.order.attachWord){
        this.attach = [OrderDraftFile.toExFileType(this.order.attachWord)];
      }
      else if (this.order.attach) {
        this.attach = [OrderDraftFile.toExFileType(this.order.attach)];
      }

      if(this.order.draftFilesWithoutAttWord&&!this.order.draftFiles){
        this.draftFilesWithoutAtt = [OrderDraftFile.toExFileType(this.order.draftFilesWithoutAttWord)];
        this.showButton = true;
      }
      else if (this.order.draftFilesWithoutAtt) {
        this.draftFilesWithoutAtt = [OrderDraftFile.toExFileType(this.order.draftFilesWithoutAtt)];
        let fileExtension = this.draftFilesWithoutAtt[0].nameFile.split('.');
        if (fileExtension[fileExtension.length-1] != 'pdf') {
          this.showButton = true;
        }
      }
      
      this.loadingStatus = LoadingStatus.SUCCESS;
      this.success = true;

      from(this.fileResourceService.getFolderinfo(this.order.folderID)).subscribe(result => {
        this.filePathForWord = result.path;
      });

      if (this.draftFilesWithoutAtt.length > 0) {
        this.onDeletedFileNoApp = false;
      }
      if (this.attach.length > 0) {
        this.onDeletedFileApp = false;
      }
      if (this.order.draftFiles) {
        this.onDeletedCreateFileApp = false;
      }

    }, () => {
      this.loadingStatus = LoadingStatus.ERROR;
    });


  }

  ngOnDestroy() {
    this.helper.beforeUnload.destroy();
  }


  loadDictionaries() {
    return forkJoin([
      this.orderTypeService.geOrderTypes(),
      this.nsiResourceService.departments(),
      this.nsiResourceService.get('EmailGroups'),
      this.nsiResourceService.get('mggt_order_OrderStatuses'),
      this.nsiResourceService.get('mggt_order_ApprovalType'),
      this.nsiResourceService.get('mggt_order_AppDuration'),
      this.nsiResourceService.get('mggt_privacy')
    ]).pipe(
      mergeMap((result: [OrderType[], DepartmentBean[], EmailGroup[], OrderStatus[], ApprovalType[], ApprovalDuration[],NSIDocumentType[]]) => {
        this.orderTypes = result[0];
        this.orderType = this.orderTypes.find(_ => {
          return _.code === this.order.documentTypeCode;
        });

        if (!this.order.hasOwnProperty('signingElectronically')) {
          this.order.signingElectronically = this.orderType.singElectron.toString() == 'true';
        }

        if (!this.order.hasOwnProperty('PublishingSite')) {
          this.order.PublishingSite = String(this.orderType.publishing) === 'true';
        }

        const userDepartmentCode = this.session.departmentCode();
        const userResponsibleDep = result[1].find(dep => dep.description === userDepartmentCode);

        if (userResponsibleDep) {
          this.order.responsibleDep = {
            departmentCode: userResponsibleDep.description,
            departmentFull: userResponsibleDep.name,
            departmentShort: null
          };
        }

        this.emailGroups = result[2].filter(emailGroup => {
          return emailGroup.sendSystem && emailGroup.sendSystem.find(sendSystem => {
            return sendSystem.code === 'SDO_ORDER';
          });
        });
        this.orderStatuses = result[3];
        this.approvalTypes = result[4];
        this.approvalDurations = result[5];
        this.privacyTypes=result[6];
        return this.userService.getSign(this.orderType);
      }),
      mergeMap((sign) => {
        this.signUsers = sign;

        return this.userService.getExecutives(this.orderType);
      }),
      mergeMap(executives => {
        const signAccounts = this.signUsers.map((u) => u.accountName);

        this.executives = executives.filter((e) => signAccounts.indexOf(e.accountName) !== -1);
        
        return this.userService.getApprovers(this.orderType);
      }),
      tap(approvers => {
        this.approvers = approvers;
      })
    )
  }

  // initDropzone() {
  //   const dzOptions = {
  //     autoProcessQueue: true,
  //     parallelUploads: 1,
  //     dictDefaultMessage: 'Загрузить файл. <br/> Допустимые расширения: doc, docx',
  //     params: {
  //       fileType: 'MkaDocOther',
  //       docSourceReference: 'UI',
  //       folderGuid: this.order.folderID,
  //       docEntityID: this.order.documentID
  //     },
  //     accept: (file, done) => {
  //       let fileName: string = file.name;
  //       let match = /.*\.docx?/.exec(fileName.toLowerCase());
  //       if (match) {
  //         done();
  //       } else {
  //         done("Допустимые расширения: doc, docx");
  //       }
  //     }
  //   };

  //   const dzOptionsAttachment = {
  //     autoProcessQueue: true,
  //     parallelUploads: 1,
  //     dictDefaultMessage: 'Загрузить файл. <br/> Допустимые расширения: doc, docx, pdf',
  //     params: {
  //       fileType: 'MkaDocOther',
  //       docSourceReference: 'UI',
  //       folderGuid: this.order.folderID,
  //       docEntityID: this.order.documentID
  //     },
  //     accept: (file, done) => {
  //       let fileName: string = file.name;
  //       let match = /.*\.docx?|\.pdf/.exec(fileName.toLowerCase());
  //       if (match) {
  //         done();
  //       } else {
  //         done("Допустимые расширения: doc, docx, pdf");
  //       }
  //     }
  //   };

  //   let component = this;
  //   this.dzDraftFilesWithoutAttOptions = _.extend(_.cloneDeep(dzOptions), {
  //     init: function () {
  //       this.on("success", function (file, guid) {
  //         this.removeFile(file);
  //         component.order.draftFilesWithoutAtt = OrderDraftFile.create(guid, file.name, file.size.toString(), false, new Date(), 'MkaDocOther', file.type);
  //       });
  //       this.on('error', function (file, errorMessage) {
  //         component.toastr.warning(errorMessage.message ? errorMessage.message : errorMessage, 'Ошибка');
  //         this.removeFile(file);
  //       });
  //     }
  //   });

  //   this.dzDraftFilesAttOptions = _.extend(_.cloneDeep(dzOptionsAttachment), {
  //     init: function () {
  //       this.on("success", function (file, guid) {
  //         this.removeFile(file);
  //         component.order.attach = OrderDraftFile.create(guid, file.name, file.size.toString(), false, new Date(), 'MkaDocOther', file.type);
  //       });
  //       this.on('error', function (file, errorMessage) {
  //         component.toastr.warning(errorMessage.message ? errorMessage.message : errorMessage, 'Ошибка');
  //         this.removeFile(file);
  //       });
  //     }
  //   });
  // }


  saveAndCreateDraftFile() {
    if (!this.validate()) {
      return;
    }
    this.creatingDraftFile = true;
    this.updateOrder().pipe(
      mergeMap(() => {
        const templateCode = this.order.signingElectronically ? this.orderType.codeTemplateSingElectron : this.orderType.codeTemplate;
        return this.generateDraftFilesWithoutAtt(templateCode);
      })
    ).subscribe((result: OrderDraftFile) => {
      this.creatingDraftFile = false;
      this.order.draftFiles = result;
    }, () => {
      this.creatingDraftFile = false;
    });
  }

  validate() {
    if (!this.order.documentTypeCode || !this.order.documentYear || !this.order.documentContent ||
      !this.order.executor || !this.order.mailList || this.order.executor.length === 0 || this.order.mailList.length === 0) {
      this.toastr.error('Не заполнены обязательные поля');
      return false;
    }
    if (!this.order.head) {
      this.toastr.error('Не заполнен руководитель, утверждающий документ');
      return false;
    }

    if (this.hasNotFilledApprover()) {
      this.toastr.error('Не заполнен список или срок согласования');
      return false;
    }

    const double = this.checkDoubleAgreed();
    if (double) {
      this.toastr.error(`В листе согласования повторяется согласующий ${double.agreedBy.fioFull}`);
      return false;
    }
    return true;
  }

  hasNotFilledApprover() {
    return this.order.approval.approvalCycle.agreed.some((a) => {
      if (!a.approvalType) {
        return true;
      }

      if (!a.agreedBy) {
        return true;
      } else if (!a.agreedBy.accountName) {
        return true;
      }

      return false;
    });
  }

  checkDoubleAgreed() {
    let items = this.order.approval.approvalCycle.agreed.slice(0),
      testItem;

    while (items.length) {
      testItem = items.shift();
      if (items.find(item => testItem.approvalTypeCode === item.approvalTypeCode
        && testItem.agreedBy.accountName === item.agreedBy.accountName)) {
        return testItem;
      }
    }
    return null;

  }

  saveDocument() {
    if (!this.validate()) {
      return;
    }
    console.log('order', this.order);
    this.helper.beforeUnload.start();
    this.saving = true;
    this.updateOrder().subscribe(() => {
      this.helper.beforeUnload.stop();
      this.saving = false;
    }, () => {
      this.helper.beforeUnload.stop();
      this.saving = false;
    });
  }


  saveAndSendToApproval() {

      // if (this.startLock) {
      //     this.toastr.error('Не найден файл проект приказа');
      //     return;
      // }

    // if (this.onDeletedFileNoApp || this.onDeletedCreateFileApp) {
    //   this.toastr.error('Не найден файл проект приказа');
    //   return;
    // }

    if (!this.validate()) {
      return;
    }
    if (!this.order.draftFiles) {
      this.toastr.error('Не найден файл проект приказа');
      return;
    }
    if (!this.order.draftFilesWithoutAttWord) {
      this.toastr.error('Файл "Проект РД без приложений" был переформирован. Нужно сформировать заново файл "Проект РД"');
      return;
    }
    if(!!this.order.attach&&this.order.attach.draftFilesDate>this.order.draftFiles.draftFilesDate){
      this.toastr.error('Были обновлены файлы приложения. Нужно сформировать заново файл "Проект РД"');
      return;
    }

    // if (this.onDeletedFileNoApp) {
    //   this.toastr.error('Не найден файл проект приказа');
    //   return;
    // }

    // if (this.onDeletedCreateFileApp) {
    //   this.toastr.error('Не найден файл проект приказа');
    //   return;
    // }

/*
    if (!this.order.draftFiles) {
      this.toastr.error('Не найден файл проект приказа');
      return;
    }
*/
    this.order.signingElectronically = this.order.signingElectronically.toString() == 'true';

    if (!this.order.signingElectronically && (!this.order.approval.approvalCycle.agreed || this.order.approval.approvalCycle.agreed.length === 0)) {
      this.toastr.error('Для отправки на согласование необходимо выбрать согласующих');
      return false;
    }

    if (this.order.approval && this.order.approval.approvalCycle && this.order.approval.approvalCycle.agreed) {
      let agreed = this.order.approval.approvalCycle.agreed;

      const isNotValid = agreed.some((a) => {
        const isNotValid = !a.approvalTypeCode || !a.agreedBy;// || !a.approvalTerm || !a.approvalTerm.duration;

        return isNotValid;
      });

      if (isNotValid) {
        console.log(agreed);
        this.toastr.error('Не заполнен список согласования');
        return;
      }

      // if (!this.orderService.signDurationCode) {
      //   console.log(agreed);
      //   this.toastr.error('Не заполнен список или срок согласования');
      //   return;
      // }

      const isNotValidDuration = agreed.some((a) => !a.approvalTerm || !a.approvalTerm.duration);      
      if (isNotValidDuration) {
        console.log(agreed);
        this.toastr.error('Не заполнен список или срок согласования');
        return;
      }
      

      let parallelApproversCount = agreed.filter(a => a.Parallel).length;
      if (parallelApproversCount > 0 && parallelApproversCount < 2) {
        this.toastr.error('В блоке "Параллельное согласование" должны быть выбраны минимум 2 согласующих');
        return;
      }
    }
    if (this.order.signingElectronically && !this.hasApproval()) {
      this.toastr.error('Не указано утверждающее лицо');
      return;
    }
    if (!this.order.signingElectronically && !_.some(this.order.approval.approvalCycle.agreed, (item) => {
      return item.agreedBy.accountName === this.order.head.accountName
          /*&& item.approvalTypeCode === 'agreed'*/ && item.Signer;
    })) {
      this.toastr.error('Не указано утверждающее лицо');
      return;
    }
    this.order.activeApprover = this.order.approval.approvalCycle.agreed[0].agreedBy.fioShort;
    this.helper.beforeUnload.start();
    this.sendingToApproval = true;
    this.orderNsiService.fillApprovalPlanDates(this.order.approval.approvalCycle.agreed).pipe(
      mergeMap(() => {
        console.log("Result:");
        console.log(this.order.approval.approvalCycle.agreed);
        return this.updateOrder();
      }),
      mergeMap(() => {
        let hasParallelApprovals = this.order.approval.approvalCycle.agreed.some(a => a.Parallel);
        return this.activityResourceService.finishTask(parseInt(this.task.id), [
          { name: 'ElApprovalVar', value: true },
          { name: 'IsParallel', value: hasParallelApprovals }
        ])
      })
    ).subscribe(() => {
      this.helper.beforeUnload.stop();
      this.sendingToApproval = false;
      this.toastr.success('Документ отправлен на согласование.');
      window.location.href = '/main/#/app/tasks';
    }, () => {
      this.helper.beforeUnload.stop();
      this.sendingToApproval = false;
    });
  }

  // private fillApprovalPlanDates(): Observable<any> {
  //   return forkJoin(
  //     this.order.approval.approvalCycle.agreed.map(a => this.fillApprovalPlanDate(a))
  //   )
  // }

  // private fillApprovalPlanDate(agreed: OrderApprovalListItem): Observable<any> {
  //   let duration = agreed.approvalTerm.duration;
  //   let now = new Date();
  //   return this.orderNsiService.addDuration(now, 'P' + duration + 'D').pipe(
  //     tap(result => {
  //       let date = new Date(result);
  //       agreed.approvalPlanDate = new Date(date.getFullYear(), date.getMonth(), date.getDate(), 23, 59, 59, 0);
  //     })
  //   )
  // }

  saveAndSendToRegistration() {
    if (!this.validate()) {
      return;
    }
    if ((!this.order.privacy||!this.order.privacy.code)){ //&& !this.order.draftFiles) {
      if (!this.order.draftFiles) {
        this.toastr.error('Не найден файл проект приказа');
        return;
      }
      if (!this.order.draftFilesWithoutAttWord) {
        this.toastr.error('Файл "Проект РД без приложений" был переформирован. Нужно сформировать заново файл "Проект РД"');
        return;
      }
      if(!!this.order.attach&&this.order.attach.draftFilesDate>this.order.draftFiles.draftFilesDate){
        this.toastr.error('Были обновлены файлы приложения. Нужно сформировать заново файл "Проект РД"');
        return;
      }
      // this.toastr.error('Не найден файл проект приказа');
      // return;
    }
    this.helper.beforeUnload.start();
    this.sendingToRegistration = true;
    this.updateOrder().pipe(
      mergeMap(() => {
        return this.activityResourceService.finishTask(parseInt(this.task.id), [
          { name: 'ElApprovalVar', value: false }
        ])
      })
    ).subscribe(() => {
      this.helper.beforeUnload.stop();
      this.sendingToRegistration = false;
      this.toastr.success('Документ отправлен на регистрацию.');
      window.location.href = '/main/#/app/tasks';
    }, () => {
      this.helper.beforeUnload.stop();
      this.sendingToRegistration = false;
    });
  }

  hasApproval() {
    if (!this.order.approval || !this.order.approval.approvalCycle || !this.order.approval.approvalCycle.agreed) {
      return false;
    }
    return _.some(this.order.approval.approvalCycle.agreed, (a) => {
      return a.approvalTypeCode === ApprovalType.approval;
    })
  }

  isApplication() {
    return this.orderType.application;
  }

  isPrereg() {
    return this.orderType.prereg;
  }

  isValidApproval = (): boolean => {
    let valid: boolean = true;
    if (this.order.approval && this.order.approval.approvalCycle && this.order.approval.approvalCycle.agreed) {
      valid = !this.order.approval.approvalCycle.agreed.some(agreed => !(agreed.agreedBy && agreed.agreedBy.accountName));
    }
    return valid;
  }

  createDraftFilesWithoutAtt() {
      from(this.fileResourceService.getFolderinfo(this.order.folderID)).subscribe(result => {
          this.filePathForWord = result.path;
          this.creatingDraftFilesWithoutAtt = true;
          const templateCode = this.order.signingElectronically ? this.orderType.codeTemplateSingElectron : this.orderType.codeTemplate;
          this.generateDraftFilesWithoutAtt(templateCode).pipe(mergeMap((fileType) => {
            return this.updateOrderPart(ord => {
              ord.draftFilesWithoutAtt = fileType;
            }).pipe(map(()=>{
              this.order.draftFilesWithoutAtt = fileType;
              this.origOrder.draftFilesWithoutAtt=fileType;
              return fileType;
            }));
          }),tap(result => {
              // this.order.draftFilesWithoutAtt = result;
              this.draftFilesWithoutAtt = [OrderDraftFile.toExFileType(result)];
          }),mergeMap(()=>{
            return this.activityResourceService.getProcessDefinitions({ key: 'sdord_updateAccessRules', latest: true });
          }),mergeMap((response) => {
            const processes = response.data;
            if (processes.length <= 0) {
              return throwError('Процесс sdord_updateAccessRules не найден');
            } else {
              return of(processes[0]);
            }
          }),
          mergeMap((process: IProcessDefinition) => {
            return this.activityResourceService.initProcess({
              processDefinitionId: process.id,
              variables: [{
                name: 'EntityIdVar',
                value: this.order.documentID
              }, {
                name: 'FileIdVar',
                value: this.draftFilesWithoutAtt[0].idFile
              }, {
                name: 'PrincipalIdVar',
                value: this.session.login()
              }]
            });
          }),catchError(error => {
            console.log(error);
            this.creatingDraftFilesWithoutAtt = false;  
            return throwError(error);
          })).subscribe(()=>{
            this.creatingDraftFilesWithoutAtt = false;
          });
      });
    this.showButton = true;
    this.onDeletedFileNoApp = false;
  }

  onDraftFilesWithoutAttDelete = () => {
    this.deleteDraftFilesWithoutAtt().subscribe(
      () => {
        this.draftFilesWithoutAtt = [];
        delete this.order.draftFilesWithoutAtt;
        delete this.order.draftFilesWithoutAttWord;
        delete this.origOrder.draftFilesWithoutAtt;
        delete this.origOrder.draftFilesWithoutAttWord;
      },
      () => {
        this.draftFilesWithoutAtt = [];
      }
    )
  };

  changeDraftFilesWithoutAtt = () => {
    const draftFilesWithoutAtt = this.draftFilesWithoutAtt[0];
    if (draftFilesWithoutAtt) {
      this.order.draftFilesWithoutAtt = OrderDraftFile.fromExFileType(draftFilesWithoutAtt);
      from(this.activityResourceService.getProcessDefinitions({ key: 'sdord_updateAccessRules', latest: true })).pipe(
        mergeMap((response) => {
          const processes = response.data;
          if (processes.length <= 0) {
            return throwError('Процесс sdord_updateAccessRules не найден');
          } else {
            return of(processes[0]);
          }
        }),
        mergeMap((process: IProcessDefinition) => {
          return this.activityResourceService.initProcess({
            processDefinitionId: process.id,
            variables: [{
              name: 'EntityIdVar',
              value: this.order.documentID
            }, {
              name: 'FileIdVar',
              value: this.draftFilesWithoutAtt[0].idFile
            }, {
              name: 'PrincipalIdVar',
              value: this.session.login()
            }]
          });
        })
      ).subscribe();
      this.onDeletedFileNoApp = false;
    } else {
      // this.order.draftFilesWithoutAtt = null;
      this.onDeletedFileNoApp = true;
    }
    this.creatingDraftFilesWithoutAtt = false;
    this.showButton = true;
  };

  onAttachDelete = () => {
    this.deleteAttach().subscribe(
      () => {
        this.attach = [];
        delete this.order.attach;
        delete this.order.attachWord;
        delete this.origOrder.attach;
        delete this.origOrder.attachWord;
      },
      () => {
        this.attach = [];
      }
    );
  };

  changeDraftFilesAtt = () => {
    const attach = this.attach[0];
    if (attach) {
      this.order.attach = OrderDraftFile.fromExFileType(attach);
      this.onDeletedFileApp = false;
    } else {
      // this.order.attach = null;
      this.onDeletedFileApp = true;
    }
  };

  onDraftFilesDelete = () => {
    this.deleteDraftFiles().subscribe(
      () => {
        delete this.order.draftFiles;
        delete this.origOrder.draftFiles;
        delete this.order.draftFilesWithoutAttWord;
        delete this.origOrder.draftFilesWithoutAttWord;
        if(this.order.draftFilesWithoutAtt){
          this.draftFilesWithoutAtt = [OrderDraftFile.toExFileType(this.order.draftFilesWithoutAtt)];
          this.showButton=true;
        }
      },
      () => {
        this.order.draftFiles = null;

      });
    this.onDeletedCreateFileApp = true;
  };

  combineDraftFile() {
    this.combiningDraftFile = true;
    this.createOrderDraftFile().pipe(mergeMap((fileType) => {
      let draftFilesWithoutAtt = this.order.draftFilesWithoutAtt;
      let draftFilesWithoutAttWord = this.order.draftFilesWithoutAttWord;
      return this.updateOrderPart(ord => {
        ord.draftFiles = fileType;
        ord.draftFilesWithoutAtt=draftFilesWithoutAtt;
        ord.draftFilesWithoutAttWord=draftFilesWithoutAttWord;
      }).pipe(map(()=>{
        this.order.draftFiles = fileType;
        this.origOrder.draftFiles=fileType;
        this.origOrder.draftFilesWithoutAtt=draftFilesWithoutAtt;
        this.origOrder.draftFilesWithoutAttWord=draftFilesWithoutAttWord;
        return fileType;
      }));
    })).subscribe((fileType) => {
      this.draftFilesWithoutAtt = [OrderDraftFile.toExFileType(this.order.draftFilesWithoutAtt)];
      this.showButton = false;
      // if (this.order.attach) {
      //   this.attach = [OrderDraftFile.toExFileType(this.order.attach)];
      // }
      // this.order.draftFiles = fileType;
      this.combiningDraftFile = false;
      this.onDeletedCreateFileApp = false;
      this.startLock = false;
    }, () => {
      this.combiningDraftFile = false;
    });
  }

  headChanged() {
    this.appovalListEdit.headChanged();
  }

  signingElectronicallyChanged() {
    this.appovalListEdit.headChanged();
  }
}
