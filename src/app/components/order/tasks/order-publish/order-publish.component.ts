import {CdpReporterResourceService} from "@reinform-cdp/reporter-resource";
import {NsiResourceService} from "@reinform-cdp/nsi-resource";
import {SessionStorage} from "@reinform-cdp/security";
import {FileResourceService} from "@reinform-cdp/file-resource";
import {ToastrService} from "ngx-toastr";
import {mergeMap, tap} from "rxjs/internal/operators";
import {from} from "rxjs/index";
import {ActiveTaskService, ActivityResourceService, ITask} from "@reinform-cdp/bpm-components";
import {LoadingStatus} from "@reinform-cdp/widgets";
import {Component, OnInit} from '@angular/core';
import {OrderService} from "../../../../services/order.service";
import {ProcessComponent} from "../order-process";
import {HelperService} from "../../../../services/helper.service";

@Component({
    selector: 'mggt-order-publish',
    templateUrl: './order-publish.component.html',
    styleUrls: ['./order-publish.component.scss']
})
export class OrderPublishComponent extends ProcessComponent implements OnInit {

    loadingStatus: LoadingStatus;
    success = false;
    task: ITask;

    submitting: boolean;

    constructor(private activeTaskService: ActiveTaskService,
                public helper: HelperService,
                private activityResourceService: ActivityResourceService,
                toastr: ToastrService, orderService: OrderService, fileResourceService: FileResourceService,
                nsiResourceService: NsiResourceService, session: SessionStorage, reporterResourceService: CdpReporterResourceService) {
        super(toastr, orderService, fileResourceService, nsiResourceService, session, reporterResourceService);
    }

    ngOnInit() {
        this.helper.beforeUnload.init();
        this.loadingStatus = LoadingStatus.LOADING;
        this.activeTaskService.getTask().pipe(
            mergeMap(task => {
                this.task = task;
                return this.getEntityIdVar(task);
            }),
            mergeMap(id => {
                return this.orderService.get(id);
            }),
            tap(order => {
                this.order = order;
            })
        ).subscribe(() => {
            this.loadingStatus = LoadingStatus.SUCCESS;
            this.success = true;
        }, () => {
            this.loadingStatus = LoadingStatus.ERROR;
        });
    }

    ngOnDestroy() {
        this.helper.beforeUnload.destroy();
    }

    execute() {
        this.helper.beforeUnload.start();
        this.submitting = true;
        from(this.activityResourceService.finishTask(parseInt(this.task.id), [
            {name: 'ElApprovalVar', value: true}
        ])).subscribe(() => {
            this.helper.beforeUnload.stop();
            this.submitting = false;
            window.location.href = '/main/#/app/tasks';
        }, () => {
            this.helper.beforeUnload.stop();
            this.submitting = false;
        });
    }

}
