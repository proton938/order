import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrderPublishComponent } from './order-publish.component';

describe('OrderPublishComponent', () => {
  let component: OrderPublishComponent;
  let fixture: ComponentFixture<OrderPublishComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrderPublishComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderPublishComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
