import { ExFileType, LoadingStatus } from "@reinform-cdp/widgets";
import * as angular from "angular";
import * as _ from "lodash";
import { FileResourceService } from "@reinform-cdp/file-resource";
import { ActiveTaskService, ActivityResourceService, ITask } from "@reinform-cdp/bpm-components";
import { Observable } from "rxjs/Rx";
import { DepartmentBean, NsiResourceService, UserBean } from "@reinform-cdp/nsi-resource";
import { AuthorizationService, SessionStorage } from "@reinform-cdp/security";
import { ToastrService } from "ngx-toastr";
import { forkJoin,from } from "rxjs/index";
import { mergeMap, tap } from "rxjs/internal/operators";
import { Component, OnInit, ViewChild } from '@angular/core';
import { OrderApprovalListItem, OrderDraftFile, OrderApproval, OrderApprover, ApprovalTerm } from "../../../../models/order/Order";
import { OrderType } from "../../../../models/order/OrderType";
import { OrderTypeService } from "../../../../services/order-type.service";
import { OrderService } from "../../../../services/order.service";
import { ProcessComponent } from "../order-process";
import { ApprovalType } from "../../../../models/order/ApprovalType";
import { DropzoneConfigInterface } from "ngx-dropzone-wrapper";
import { CdpReporterResourceService } from "@reinform-cdp/reporter-resource";
import { ApprovalListEditComponent } from "../approval-list-edit/approval-list-edit.component";
import { EmailGroup } from "../../../../models/order/EmailGroup";
import { OrderStatus } from "../../../../models/order/OrderStatus";
import { UserService } from "../../../../services/user.service";
import { ApprovalDuration } from "../../../../models/order/ApprovalDuration";
import { OrderNsiService } from "../../../../services/order-nsi.service";
import {ApprovalNode, getApprovalsNodes, isRoot} from "../../../../services/approval-util.service";
import { User } from "../../../../models/order/User";
import {HelperService} from "../../../../services/helper.service";

@Component({
  selector: 'mggt-order-assign-permiss',
  templateUrl: './order-assign-permiss.component.html'
})
export class OrderAssignPermissComponent extends ProcessComponent implements OnInit {

  loadingStatus: LoadingStatus;
  success = false;
  task: ITask;
  account: any = {};
  responsibleUser:UserBean;
  
  saving: boolean;
   

  constructor(private orderTypeService: OrderTypeService,
    public helper: HelperService,
    private activeTaskService: ActiveTaskService, private authorizationService: AuthorizationService,
    private activityResourceService: ActivityResourceService,
    private userService: UserService, private orderNsiService: OrderNsiService,
    toastr: ToastrService, orderService: OrderService, fileResourceService: FileResourceService,
    nsiResourceService: NsiResourceService, session: SessionStorage,
    reporterResourceService: CdpReporterResourceService) {
    super(toastr, orderService, fileResourceService, nsiResourceService, session, reporterResourceService);
  }

  ngOnInit() {
    this.helper.beforeUnload.init();
    this.loadingStatus = LoadingStatus.LOADING;
    
    this.account.accountName = this.session.login();
    this.account.fio = this.session.name();
    this.account.post = this.session.post();

    this.activeTaskService.getTask().pipe(
      mergeMap(task => {
        this.task = task;
        return this.getEntityIdVar(task);
      }),
      mergeMap(id => {
        return this.orderService.get(id);
      }),
      mergeMap(order => {
        this.order = order;
        this.origOrder = angular.copy(order);
        let ResponsibleVar=this.task.variables.find(item=>item.name=='ResponsibleVar');
        if(!ResponsibleVar||!ResponsibleVar.value){
          this.toastr.error('Нет ResponsibleVar!');
          throw "Нет ResponsibleVar";
        }
        return this.loadDictionaries(ResponsibleVar.value.toString());
      })
    ).subscribe(() => {     

      this.loadingStatus = LoadingStatus.SUCCESS;
      this.success = true;
    }, () => {
      this.loadingStatus = LoadingStatus.ERROR;
    });
  }

  ngOnDestroy() {
    this.helper.beforeUnload.destroy();
  }

  loadDictionaries(ResponsibleVar:string) {
    return from(this.nsiResourceService.ldapUser(ResponsibleVar).then(user=>{
      this.responsibleUser=user;
    }));    
  }
  saveAndProcess() {
    this.helper.beforeUnload.start();
    this.saving = true;
    from(this.activityResourceService.finishTask(parseInt(this.task.id), [])).subscribe(() => {
      this.helper.beforeUnload.stop();
      this.saving = false;
      this.toastr.success('Задача завершена.');
      window.location.href = '/main/#/app/tasks';
    }, () => {
      this.helper.beforeUnload.stop();
      this.saving = false;
    });
  }  

}
