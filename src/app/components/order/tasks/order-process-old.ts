import { CdpReporterResourceService, PdfService } from '@reinform-cdp/reporter-resource';
import { FileResourceService, IFileInFolderInfo } from '@reinform-cdp/file-resource';
import { Observable, forkJoin, from, of, throwError } from 'rxjs';
import { SessionStorage } from '@reinform-cdp/security';
import { NsiResourceService } from '@reinform-cdp/nsi-resource';
import { catchError, map, mergeMap, tap } from 'rxjs/internal/operators';
import * as angular from 'angular';
import * as _ from 'lodash';
import { ToastrService } from 'ngx-toastr';
import { ITask } from '@reinform-cdp/bpm-components';
import {
    Ds, Order, OrderApprovalHistory, OrderApprovalHistoryList, OrderApprovalListItem,
    OrderApprovalListItemSignature, OrderApprover, OrderDraftFile
} from '../../../models/order/Order';
import { OrderService } from '../../../services/order.service';
import { formatDate } from '@angular/common';
import { FileType } from '../../../models/order/FileType';
import { ApprovalType } from '../../../models/order/ApprovalType';
import { OrderStatus } from '../../../models/order/OrderStatus';
import { OrderTypeService } from '../../../services/order-type.service';
import { OrderType } from '../../../models/order/OrderType';
import { ExFileType } from '@reinform-cdp/widgets';

export class ProcessOldComponent {

    public order: Order;
    public origOrder: Order;

    orderType: OrderType;
    orderTypes: OrderType[];

    constructor(public toastr: ToastrService, public orderService: OrderService, public fileResourceService: FileResourceService,
                public nsiResourceService: NsiResourceService, public session: SessionStorage,
                public reporterResourceService: CdpReporterResourceService, public orderTypeService: OrderTypeService,
                public pdfService: PdfService,
    ) {

    }

    getEntityIdVar(task: ITask): Observable<string> {
        let _EntityIdVar = task.variables.find(v => {
            return v.name === 'EntityIdVar';
        });
        if (_EntityIdVar) {
            return of(<string>_EntityIdVar.value);
        } else {
            this.toastr.error('Отсутствует переменная задачи\'EntityIdVar\'');
            return throwError('Отсутствует переменная задачи\'EntityIdVar\'');
        }
    }

    updateOrder(): Observable<any> {
        return this.orderService.update(this.order.documentID, this.orderService.diff(this.origOrder, this.order)).pipe(
            tap(order => {
                this.order = order;
                this.origOrder = angular.copy(order);
            })
        );
    }

    approve(order: Order, agreeIndex: number, taskId, approve: boolean, approvalParams: { approvalNote: string, agreedDs?: OrderApprovalListItemSignature }): Observable<any> {
        if (!order.approval || !order.approval.approvalCycle || !order.approval.approvalCycle.agreed || order.approval.approvalCycle.agreed.length === 0) {
            return throwError('Лист согласования не заполнен.');
        }
        let agreeds = order.approval.approvalCycle.agreed;
        if (agreeIndex < 0 || agreeIndex >= agreeds.length) {
            return throwError('Некорректный индекс согласующего.');
        }
        let agreed = agreeds[agreeIndex];

        if (!approve && !approvalParams.approvalNote) {
            return throwError('Для отрицательного решения необходимо заполнить комментарий.');
        }

        /*if (approve && !approvalParams.agreedDs && (agreed.approvalTypeCode === ApprovalType.agreed || agreed.approvalTypeCode === ApprovalType.approval)) {
            return throwError('Для положительного решения необходимо заполнить информацию об электронной подписи.');
        }*/

        if (approve && agreed.approvalTypeCode === ApprovalType.approval && order.signingElectronically &&
            !order.documentNumber && order.ds
            && order.files && this.order.files.length && this.order.files[0].fileSigned) {
            return throwError('Утверждаемый документ должен быть зарегистрирован и подписан.');
        }

        return from(this.nsiResourceService.get('mggt_order_ApprovalType')).pipe(
            mergeMap((approvalTypes: ApprovalType[]) => {
                let approvalType = approvalTypes.find(at => at.approvalTypeCode === agreed.approvalTypeCode);
                agreed.approvalResult = approve ? approvalType.buttonYes : approvalType.buttonNo;

                agreed.approvalNote = approvalParams.approvalNote;
                agreed.agreedDs = approvalParams.agreedDs || null;
                agreed.approvalFactDate = new Date();

                return this.fillUserInfo(agreed);
            }),
            mergeMap(() => {
                if (!approve) {
                    return this.closeCycle(order);
                } else {
                    return of('');
                }
            }),
            mergeMap(() => {
                return this.updateOrder();
            })
        );
    }

    fillUserInfo(agreed: OrderApprovalListItem): Observable<any> {
        let plannedAgreedBy = agreed.agreedBy.accountName;
        let login = this.session.login();
        if (login !== plannedAgreedBy) {
            return this.getOrderApprover(login).pipe(
                tap(approver => agreed.factAgreedBy = approver),
                mergeMap(() => this.fillAgreedBy(agreed))
            )
        } else {
            return this.fillAgreedBy(agreed);
        }
    }

    fillAgreedBy(agreed: OrderApprovalListItem): Observable<any> {
        if (!agreed.agreedBy.fioShort) {
            return this.getOrderApprover(agreed.agreedBy.accountName).pipe(
                tap(approver => {
                    agreed.agreedBy = approver;
                })
            )
        } else {
            return of('');
        }
    }

    getOrderApprover(login: string) {
        return from(this.nsiResourceService.ldapUser(login)).pipe(
            map(userBean => {
                return OrderApprover.fromUserBean(userBean);
            })
        );

    }

    closeCycle(order: Order): Observable<any> {
        order.approvalHistory = order.approvalHistory || new OrderApprovalHistory();
        let approvalCycle = <OrderApprovalHistoryList>angular.copy(order.approval.approvalCycle);

        let draftFile = order.draftFiles;
        let fileName = `${this.getFileName(draftFile.draftFilesName)}-согласование-${formatDate(new Date(), 'yyyy-MM-dd HH:mm:ss', 'en')}.${this.getFileExtension(draftFile.draftFilesName)}`;


        return this.copyDraftFile(fileName).pipe(
            mergeMap(fileType => {
                approvalCycle.approvalCycleFile = approvalCycle.approvalCycleFile || [];
                approvalCycle.approvalCycleFile.push(fileType);

                order.approvalHistory.approvalCycle = order.approvalHistory.approvalCycle || [];
                order.approvalHistory.approvalCycle.push(approvalCycle);
                order.approval.approvalCycle = null;

                if (order.draftFiles.draftFilesSigned) {
                    // TODO: signature, delete sign
                    // this.fileResourceService.delete
                    order.draftFiles.draftFilesSigned = false;
                }
                return of('');
            })
        )
    }

    getFileName(fullFileName: string): string {
        let dotIndex = fullFileName.lastIndexOf('.');
        if (dotIndex < 0) {
            return fullFileName;
        } else {
            return fullFileName.substring(0, dotIndex);
        }
    }

    getFileExtension(fullFileName: string): string {
        let dotIndex = fullFileName.lastIndexOf('.');
        if (dotIndex < 0) {
            return '';
        } else {
            return fullFileName.substring(dotIndex + 1, fullFileName.length);
        }
    }

    copyDraftFile(targetFileName: string): Observable<FileType> {
        let name: string = targetFileName.split('.').slice(0, -1).join('.') + '-согласование-' +
            formatDate(new Date(), 'yyyy-MM-dd HH:mm', 'en') + '.' +
            targetFileName.split('.').slice(-1).join('');
        return from(this.fileResourceService.copyFile(this.order.draftFiles.draftFilesID, name)).pipe(
            map((fileType) => {
                return FileType.create(fileType.versionSeriesGuid, fileType.fileName, fileType.fileSize.toString(),
                    false, new Date(), fileType.fileType, fileType.mimeType);
            })
        );
    }

    copyDraftFileWithoutAtt(targetFileName: string): Observable<FileType> {
        return from(this.fileResourceService.copyFile(this.order.draftFilesWithoutAtt.draftFilesID, targetFileName)).pipe(
            map((fileType) => {
                return FileType.create(fileType.versionSeriesGuid, fileType.fileName, fileType.fileSize.toString(),
                    false, new Date(), fileType.fileType, fileType.mimeType);
            })
        );
    }

    copyFile(fileID: string, targetFileName: string): Observable<FileType> {
        return from(this.fileResourceService.copyFile(fileID, targetFileName)).pipe(
            map((fileType) => {
                return FileType.create(fileType.versionSeriesGuid, fileType.fileName, fileType.fileSize.toString(),
                    false, new Date(), fileType.fileType, fileType.mimeType);
            })
        );
    }

    updateDocumentNumberBarcodeAndDs(ds?: Ds): Observable<any> {
        return this.generateDocumentNumber().pipe(
            mergeMap(() => {
                return this.generateBarcode();
            }),
            mergeMap(() => {
                this.order.ds = ds;
                return this.updateOrder();
            })
        )
    }

    cleanupOrderFiles(): Observable<any> {
        let fileIds = [];
        if (this.order.signingElectronically && this.order.files) {
            fileIds = this.order.files.map(file => file.fileID);
        } else if (!this.order.signingElectronically && this.order.draftFilesRegInfo) {
            fileIds = [this.order.draftFilesRegInfo.fileID];
        }
        return this.deleteFiles(fileIds);
    }

    setOrderFile(file: FileType) {
        let observable: Observable<any>;
        if (this.order.signingElectronically) {
            observable = this.addOrderFile(file);
        } else {
            this.order.draftFilesRegInfo = file;
            observable = of('');
        }
        return observable.pipe(
            mergeMap(() => {
                return this.updateOrder();
            }),
            mergeMap(() => {
                if (this.order.signingElectronically && this.order.documentStatusCode === OrderStatus.REGISTERSCAN) {
                    return this.orderService.send(this.order.documentID, true);
                }
                return of('');
            })
        )
    }

    registerOrder(ds?: Ds): Observable<FileType> {
        if (!this.order.draftFiles || !this.order.draftFiles.draftFilesID) {
            this.toastr.error('Файл проекта приказа не найден.');
            return throwError('Файл проекта приказа не найден.');
        }

        let draftFilesWithoutAttPdf;
        //Обновляем документ
        return this.checkLinkedDocumentsRegistration().pipe(
            catchError(error => {
                this.toastr.error(error);
                return throwError(error);
            }),
            mergeMap(() => {
                if (!this.order.documentDate) {
                    this.order.documentDate = new Date();
                }

                return this.updateDocumentNumberBarcodeAndDs(ds);
            }),
            mergeMap(() => {
                return this.orderTypeService.geOrderTypes();
            }),
            mergeMap((response: OrderType[]) => {
                this.orderTypes = response;
                this.orderType = this.orderTypes.find(_ => {
                    return _.code === this.order.documentTypeCode;
                });

                const file = ExFileType.create(
                    this.order.draftFilesWithoutAttWord.draftFilesID,
                    this.order.draftFilesWithoutAttWord.draftFilesName,
                    +this.order.draftFilesWithoutAttWord.draftFilesSize,
                    false,
                    new Date(this.order.draftFilesWithoutAttWord.draftFilesDate).getTime(),
                    this.order.draftFilesWithoutAttWord.draftFilesType,
                    this.order.draftFilesWithoutAttWord.draftFilesMimeType
                );

                return this.applyOrderStamp(file, { ds: ds });
            }),
            mergeMap((id) => {
                return this.convertFileTypeToPdf(id, this.order.draftFilesWithoutAttWord.draftFilesName);
            }),
            mergeMap((draftFilePdf: IFileInFolderInfo) => {
                draftFilesWithoutAttPdf = FileType.create(
                    draftFilePdf.versionSeriesGuid, draftFilePdf.fileName, draftFilePdf.fileSize.toString(), false,
                    new Date(draftFilePdf.dateCreated), draftFilePdf.fileType, draftFilePdf.mimeType
                );

                if (!this.order.attachWord) {
                    const fileName = `${this.orderType.name.split(' ')[0]} № ${this.order.documentNumber} от ${formatDate(this.order.documentDate, 'dd.MM.yyyy', 'en')}.pdf`;

                    return from(this.fileResourceService.copyFile(draftFilesWithoutAttPdf.fileID, fileName)).pipe(
                        mergeMap((fileType) => {
                            return of(
                                FileType.create(fileType.versionSeriesGuid, fileType.fileName, fileType.fileSize.toString(),
                                    false, new Date(), fileType.fileType, fileType.mimeType)
                            );
                        })
                    );
                }

                return this.convertToPdfIfNecessary(this.order.attachWord)
                    .pipe(
                        mergeMap((attachPdf: OrderDraftFile) => {
                            let request: any = {
                                filenetDestination: {
                                    fileAttrs: [
                                        { attrName: 'docEntityID', attrValues: [this.order.documentID] },
                                        { attrName: 'docSourceReference', attrValues: ['UI'] }
                                    ],
                                    fileName: `${this.orderType.name.split(' ')[0]} № ${this.order.documentNumber} от ${formatDate(this.order.documentDate, 'dd.MM.yyyy', 'en')}.pdf`,
                                    fileType: 'MkaDocOther',
                                    folderGuid: this.order.folderID,
                                },
                                versionSeriesGuids: [draftFilesWithoutAttPdf.fileID, attachPdf.draftFilesID]
                            };

                            return this.reporterResourceService.pdfConcatenateFiles(request)
                        }),
                        mergeMap((result: any) => {
                            return of(FileType.create(result.versionSeriesGuid, result.fileName, result.fileSize.toString(), false,
                                new Date(result.dateCreated), result.fileType, result.mimeType));
                        })
                    );
            }),
            mergeMap((orderFile: FileType) => {
                return of(orderFile);
            })
        )
    }

    applyOrderStamp(file: ExFileType, signInfo: { ds: Ds }): Observable<string> {
        const fileName = `${this.orderType.name.split(' ')[0]} № ${this.order.documentNumber} от ${formatDate(this.order.documentDate, 'dd.MM.yyyy', 'en')} рег. данные.docx`;

        return from(this.reporterResourceService.filenet2Filenet(<any>{
            jsonTxt: JSON.stringify(this.order),
            filenetTemplate: {
                templateVersionSeriesGuid: file.idFile
            },
            filenetDestination: {
                reportFileName: file.nameFile,
                reportFileType: file.typeFile,
                reportFolderGuid: this.order.folderID,
                fileName: fileName,
                fileType: file.typeFile,
                folderGuid: this.order.folderID
            },
            rootJsonPath: '$',
            options: {
                onProcess: file.nameFile.replace('.docx', ''),
                placeholderCode: 'sn',
                strictCheckMode: true,
                xwpfResultDocumentDescr: fileName,
                xwpfTemplateDocumentDescr: ''
            }
        })).pipe(
            map(response => response.versionSeriesGuid)
        );
    }

    getOrderFileName(order) {
        let orderType = order.documentTypeValue;
        orderType = orderType.indexOf(' ') > -1 ? orderType.substring(0, orderType.indexOf(' ')) : orderType;
        return `${orderType} № ${order.documentNumber} от ${formatDate(order.documentDate, 'dd.MM.yyyy', 'en')}.pdf`
    }

    checkLinkedDocumentsRegistration(): Observable<any> {
        if (!this.order.linkedDocument || !this.order.linkedDocument.length) {
            return of('');
        }
        return forkJoin(
            this.order.linkedDocument.map(doc => this.orderService.get(doc.linkedDocumentID))
        ).pipe(
            mergeMap((docs: Order[]) => {
                let doc = docs.find(doc => !doc.documentNumber || !doc.documentDate);
                if (doc) {
                    return throwError('Связанный документ ' + doc.documentID + ' не зарегистрирован.')
                } else {
                    return of('');
                }
            })
        )
    }

    addOrderFile(file: FileType): Observable<any> {
        this.order.files = [file];
        if (this.order.documentStatusCode === OrderStatus.REGISTER) {
            this.order.documentStatusCode = OrderStatus.REGISTERSCAN;
            return this.processDocumentStatus();
        } else {
            return of('');
        }
    }

    generateDocumentNumber(): Observable<any> {
        if (this.order.documentNumber) {
            return of('');
        } else {
            return this.orderService.nextNumber(this.order.documentTypeCode, this.order.documentYear).pipe(
                tap(number => this.order.documentNumber = number)
            )
        }
    }

    generateBarcode(): Observable<any> {
        if (this.order.barcode) {
            return of('');
        } else {
            return this.orderService.nextBarcode().pipe(
                tap(barcode => this.order.barcode = barcode)
            )
        }
    }

    processDocumentStatus(): Observable<any> {
        let statusCode = this.order.documentStatusCode;
        if (!statusCode) {
            if (this.order.documentNumber) {
                statusCode = OrderStatus.REGISTER;
            } else {
                statusCode = OrderStatus.PROJECT;
            }
        }
        return from(this.nsiResourceService.getBy('mggt_order_OrderStatuses', {
            nickAttr: 'code',
            values: [statusCode]
        })).pipe(
            tap((statuses: OrderStatus[]) => {
                if (statuses.length) {
                    let status = statuses[0];
                    this.order.documentStatusCode = status.code;
                    this.order.documentStatusValue = status.name;
                    this.order.documentStatusColor = status.color;
                }
            })
        )
    }

    deleteFiles(ids: string[]): Observable<any> {
        if (!ids || !ids.length) {
            return of('');
        }
        return forkJoin(ids.map(id => this.fileResourceService.deleteFile(id)));
    }

    createOrderDraftFile(): Observable<OrderDraftFile> {
        return this.convertToPdfIfNecessary(this.order.draftFilesWithoutAtt).pipe(
            mergeMap(draftFilesWithoutAtt => {
                this.order.draftFilesWithoutAtt = draftFilesWithoutAtt;
                if (!this.order.attach || !this.order.attach.draftFilesID) {
                    const fileName = `Проект РД.pdf`;

                    return this.copyDraftFileWithoutAtt(fileName).pipe(
                        mergeMap((fileType) => {
                            return of(OrderDraftFile.fromFileType(fileType));
                        })
                    );
                }

                return this.convertToPdfIfNecessary(this.order.attach).pipe(
                    mergeMap(attach => {
                        this.order.attachWord = _.cloneDeep(this.order.attach);
                        this.order.attachWord.draftFilesDate = this.order.attach.draftFilesDate;
                        this.order.attach = attach;
                        let request: any = {
                            filenetDestination: {
                                fileAttrs: [
                                    { attrName: 'docEntityID', attrValues: [this.order.documentID] },
                                    { attrName: 'docSourceReference', attrValues: ['UI'] }
                                ],
                                // fileName: this.getFileName(this.order.draftFilesWithoutAtt.draftFilesName) + '.pdf',
                                fileName: 'Проект РД.pdf',
                                fileType: 'MkaDocOther',
                                folderGuid: this.order.folderID,
                            },
                            versionSeriesGuids: [draftFilesWithoutAtt.draftFilesID, attach.draftFilesID]
                        };
                        return this.reporterResourceService.pdfConcatenateFiles(request)
                    }),
                    mergeMap((response: IFileInFolderInfo) => {
                        console.log('pdfConcatenateFiles', response);
                        return of(OrderDraftFile.fromFileInFolderInfo(response));
                    })
                );
            })
        );
    }

    convertToPdfIfNecessary(file: OrderDraftFile): Observable<OrderDraftFile> {
        if (file.draftFilesMimeType === 'application/pdf') {
            return of(file);
        } else {
            let request: any = {
                filenetDestination: {
                    fileAttrs: [
                        { attrName: 'docEntityID', attrValues: [this.order.documentID] },
                        { attrName: 'docSourceReference', attrValues: ['UI'] }
                    ],
                    fileName: this.getFileName(file.draftFilesName) + '.pdf',
                    fileType: 'MkaDocOther',
                    folderGuid: this.order.folderID
                },
                wordVersionSeriesGuid: file.draftFilesID
            };
            return from(this.reporterResourceService.word2pdf(request)).pipe(
                map((response: IFileInFolderInfo) => {
                    return OrderDraftFile.fromFileInFolderInfo(response);
                })
            )
        }
    }

    convertFileTypeToPdf(fileId: string, fileName: string): Observable<IFileInFolderInfo> {
        let request: any = {
            filenetDestination: {
                fileAttrs: [
                    { attrName: 'docEntityID', attrValues: [this.order.documentID] },
                    { attrName: 'docSourceReference', attrValues: ['UI'] }
                ],
                fileName: this.getFileName(fileName) + '.pdf',
                fileType: 'MkaDocOther',
                folderGuid: this.order.folderID
            },
            wordVersionSeriesGuid: fileId
        };
        return from(this.reporterResourceService.word2pdf(request));
    }
}
