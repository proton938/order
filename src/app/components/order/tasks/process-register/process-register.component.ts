import { ToastrService } from 'ngx-toastr';
import { Observable, of, throwError } from 'rxjs';
import { mergeMap, tap,catchError } from 'rxjs/internal/operators';
import { ActiveTaskService, ActivityResourceService, ITask } from '@reinform-cdp/bpm-components';
import { LoadingStatus, AlertService } from '@reinform-cdp/widgets';
import { Component, OnInit } from '@angular/core';
import { Ds, Order } from '../../../../models/order/Order';
import { OrderService } from '../../../../services/order.service';
import { RegisterDocumentService } from '../../../../services/register-document.service';
import { CancelDocumentService } from '../../../../services/cancel-document.service';
import { ProcessComponent } from '../order-process';
import { NsiResourceService, UserBean } from '@reinform-cdp/nsi-resource';
import { SessionStorage, User } from '@reinform-cdp/security';
import { FileResourceService } from '@reinform-cdp/file-resource';
import { CdpReporterResourceService } from '@reinform-cdp/reporter-resource';
import * as angular from 'angular';
import { OrderTypeService } from '../../../../services/order-type.service';
import { OrderType } from '../../../../models/order/OrderType';
import {HelperService} from '../../../../services/helper.service';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'mggt-process-register',
  templateUrl: './process-register.component.html',
  styleUrls: ['./process-register.component.scss']
})
export class ProcessRegisterComponent extends ProcessComponent implements OnInit {

  loadingStatus: LoadingStatus;
  success = false;
  task: ITask;
  order: Order;
  registrars: any[];
  orderType: OrderType;

  submitting: boolean;

  constructor(private orderTypeService: OrderTypeService,
              public helper: HelperService,
              private activeTaskService: ActiveTaskService, private activityResourceService: ActivityResourceService, private cancelDocumentService: CancelDocumentService,
    toastr: ToastrService, orderService: OrderService, fileResourceService: FileResourceService, private registerDocumentService: RegisterDocumentService,
    nsiResourceService: NsiResourceService, public session: SessionStorage, reporterResourceService: CdpReporterResourceService, private alertService: AlertService) {
    super(toastr, orderService, fileResourceService, nsiResourceService, session, reporterResourceService);
  }

  ngOnInit() {
    this.helper.beforeUnload.init();
    this.loadingStatus = LoadingStatus.LOADING;

    this.activeTaskService.getTask().pipe(
      mergeMap(task => {
        this.task = task;
        return this.getEntityIdVar(task);
      }),
      mergeMap(id => {
        return this.orderService.get(id);
      }),
      mergeMap((order: Order) => {
        this.order = order;
        this.origOrder = angular.copy(order);
        
        return this.orderTypeService.geOrderTypes();
      }),
      tap(async (orderTypes: OrderType[]) => {
        this.orderType = orderTypes.find(_ => {
          return _.code === this.order.documentTypeCode;
        });

        await this.loadDictionaries();
      }),
    ).subscribe(() => {
      this.loadingStatus = LoadingStatus.SUCCESS;
      this.order.registar = (this.registrars || []).find((r) => {
        return r.accountName === this.session.login();
      });

      this.success = true;
    }, () => {
      this.loadingStatus = LoadingStatus.ERROR;
    });
  }

  ngOnDestroy() {
    this.helper.beforeUnload.destroy();
  }

  loadDictionaries() {
    return this.nsiResourceService.searchUsers({ group: ['MGGT_ADMORDER_REGISTER'] }).then((values) => {
      this.registrars = values;
    });
  }

  public register(ds?: Ds) {
    this.helper.beforeUnload.start();
    this.submitting = true;
    if (!!this.order.privacy && !!this.order.privacy.code) {
      this.registerOrderNewPrivacy(this.orderType, ds).pipe(
        mergeMap(() => {
          return this.activityResourceService.finishTask(parseInt(this.task.id), [{
            name: 'EntityDescriptionVar',
            value: this.order.documentTypeValue.split(' ')[0] + ' № ' + this.order.documentNumber + ' ' + this.order.documentContent
          }]);
        })
      ).subscribe(() => {
        this.helper.beforeUnload.stop();
        this.toastr.info('Электронный документ успешно зарегистрирован');
        this.submitting = false;
        window.location.href = '/main/#/app/tasks';
      }, () => {
        this.helper.beforeUnload.stop();
        this.submitting = false;
      });
      return;
    }

    this.registerOrderNew(this.orderType, ds).pipe(
      mergeMap((file: any) => {
        this.order.draftFilesRegInfo = file;
        return this.updateOrder();
      }),
      mergeMap(() => {
        return this.activityResourceService.finishTask(parseInt(this.task.id), [{
          name: 'EntityDescriptionVar',
          value: this.order.documentTypeValue.split(' ')[0] + ' № ' + this.order.documentNumber + ' ' + this.order.documentContent
        }]);
      })
    ).subscribe(() => {
      this.toastr.info('Электронный документ успешно зарегистрирован');
      this.submitting = false;
      this.helper.beforeUnload.stop();
      window.location.href = '/main/#/app/tasks';
    }, () => {
      this.helper.beforeUnload.stop();
      this.submitting = false;
    });
  }

  registerOrderNewPrivacy(orderType: OrderType, ds?: Ds): Observable<any> {
    return this.checkLinkedDocumentsRegistration().pipe(
      catchError(error => {
        this.toastr.error(error);
        return throwError(error);
      }),
      mergeMap(() => {
        if (!this.order.documentDate) {
          this.order.documentDate = new Date();
        }
        return this.updateDocumentNumberBarcodeAndDs(ds);
      })      
    );
  }

}
