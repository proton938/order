import { CdpReporterResourceService } from "@reinform-cdp/reporter-resource";
import { NsiResourceService } from "@reinform-cdp/nsi-resource";
import { BlockUI, NgBlockUI } from "ng-block-ui";
import * as _ from "lodash";
import { mergeMap, tap } from "rxjs/internal/operators";
import { FileResourceService } from "@reinform-cdp/file-resource";
import * as angular from "angular";
import { Component, OnInit } from '@angular/core';
import { Order } from "../../../../models/order/Order";
import { ExFileType, LoadingStatus } from "@reinform-cdp/widgets";
import { SessionStorage } from "@reinform-cdp/security";
import { ActiveTaskService, ActivityResourceService, ITask } from "@reinform-cdp/bpm-components";
import { OrderType } from "../../../../models/order/OrderType";
import { ProcessComponent } from "../order-process";
import { ToastrService } from "ngx-toastr";
import { OrderService } from "../../../../services/order.service";
import { Observable } from "rxjs/Rx";
import { OrderTypeService } from "../../../../services/order-type.service";
import {HelperService} from "../../../../services/helper.service";

@Component({
  selector: 'mggt-order-approve-desicion',
  templateUrl: './order-approve-desicion.component.html',
  styleUrls: ['./order-approve-desicion.component.scss']
})
export class OrderApproveDesicionComponent extends ProcessComponent implements OnInit {

  loadingStatus: LoadingStatus;
  success = false;
  task: ITask;
  order: Order;
  origOrder: Order;
  orderType: OrderType;
  showDocumentText: boolean;

  sendingForward: boolean = false;
  sendingBackToWork: boolean = false;

  orderFile: ExFileType = new ExFileType();
  isMobile: boolean = false;

  @BlockUI('orderApproveDesicion') blockUI: NgBlockUI;

  constructor(private activeTaskService: ActiveTaskService,
    public helper: HelperService,
    private orderTypeService: OrderTypeService,
    private activityResourceService: ActivityResourceService,
    toastr: ToastrService,
    orderService: OrderService,
    fileResourceService: FileResourceService,
    nsiResourceService: NsiResourceService,
    sessionStorage: SessionStorage,
    reporterResourceService: CdpReporterResourceService) {
    super(toastr, orderService, fileResourceService, nsiResourceService, sessionStorage, reporterResourceService);
  }

  ngOnInit() {
    this.helper.beforeUnload.init();
    this.loadingStatus = LoadingStatus.LOADING;
    this.activeTaskService.getTask().pipe(
      mergeMap(task => {
        this.task = task;
        return this.getEntityIdVar(task);
      }),
      mergeMap(id => {
        return this.orderService.get(id);
      }),
      tap(order => {
        this.order = order;
        this.origOrder = angular.copy(order);

        if (!this.order.documentDate) {
          this.order.documentDate = new Date();
        }

        return this.loadDictionaries();
      }),
    ).subscribe((response) => {
      this.loadingStatus = LoadingStatus.SUCCESS;
      this.success = true;
    }, (error) => {
      console.error(error);
      this.loadingStatus = LoadingStatus.ERROR;
    });
  }

  ngOnDestroy() {
    this.helper.beforeUnload.destroy();
  }

  loadDictionaries(): Observable<any> {
    return this.orderTypeService.geOrderTypes().pipe(
      tap(orderTypes => {
        this.orderType = orderTypes.find(val => val.code === this.order.documentTypeCode);
        // TODO: check OASI_ADMORDER_OGS
        this.showDocumentText = this.orderType.accessAction === 'OASI_ADMORDER_OGS';
      })
    )
  }


  sendBackToWork() {
    this.helper.beforeUnload.start();
    this.sendingBackToWork = true;
    this.blockUI.start();

    this.closeCycle(this.order).pipe(
      mergeMap(() => {
        return this.updateOrder();
      }),
      mergeMap(() => {
        return this.activityResourceService.finishTask(parseInt(this.task.id), [
          { name: 'IsRework', value: true },
          { name: 'IsSequence', value: false },
        ]);
      })
    ).subscribe(
      () => {
        this.helper.beforeUnload.stop();
        this.blockUI.stop();
        this.sendingBackToWork = false;
        this.toastr.success('Документ отправлен на доработку.');
        window.location.href = '/main/#/app/tasks';
      },
      () => {
        this.helper.beforeUnload.stop();
        this.blockUI.stop();
        this.sendingBackToWork = false;
      }
    );
  }

  sendForward() {
    this.helper.beforeUnload.start();
    this.sendingForward = true;
    this.blockUI.start()

    const IsSequence = this.order.approval.approvalCycle.agreed.some((agreed) => {
      return agreed.Parallel !== true && agreed.Signer !== true;
    });

    this.activityResourceService.finishTask(parseInt(this.task.id), [
      { name: 'IsSequence', value: IsSequence },
    ]).then(
      () => {
        this.helper.beforeUnload.stop();
        this.blockUI.stop();
        this.sendingForward = false;
        window.location.href = '/main/#/app/tasks';
      },
      () => {
        this.helper.beforeUnload.stop();
        this.blockUI.stop();
        this.sendingForward = false;
      }
    );
  }

}
