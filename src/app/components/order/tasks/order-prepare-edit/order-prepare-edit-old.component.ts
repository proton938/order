import * as _ from "lodash";
import { FileResourceService } from "@reinform-cdp/file-resource";
import { ToastrService } from "ngx-toastr";
import { DropzoneConfigInterface } from "ngx-dropzone-wrapper";
import { Observable, Subject, concat, forkJoin, from, of } from "rxjs";
import {
    catchError, debounceTime, distinctUntilChanged, filter, map, switchMap,
    tap
} from "rxjs/internal/operators";
import { NsiResourceService, UserBean } from "@reinform-cdp/nsi-resource";
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Order } from "../../../../models/order/Order";
import { OrderType } from "../../../../models/order/OrderType";
import { CancelType } from "../../../../models/order/CancelType";
import { UpdateType } from "../../../../models/order/UpdateType";
import { User } from "../../../../models/order/User";
import { EmailGroup } from "../../../../models/order/EmailGroup";
import { OrderTypeService } from "../../../../services/order-type.service";
import { OrderStatus } from "../../../../models/order/OrderStatus";
import { ApprovalType } from "../../../../models/order/ApprovalType";
import { RegisterDocumentService } from "../../../../services/register-document.service";
import { CdpSolrResourceService } from "@reinform-cdp/search-resource";

export class OrderPrepareDicts {
    orderTypes: OrderType[];
    emailGroups: EmailGroup[];
    orderStatuses: OrderStatus[];
    executives: UserBean[];
}

@Component({
    selector: 'mggt-order-prepare-edit-old',
    templateUrl: './order-prepare-edit-old.component.html'
})
export class OrderPrepareEditOldComponent implements OnInit {

    @Input() order: Order;
    @Input() preregFields: boolean;
    @Input() isPrereg: boolean;
    @Input() hasPreregPermission: boolean;
    @Input() account: any;
    @Input() dicts: OrderPrepareDicts;

    @Output() onHeadChanged: EventEmitter<any> = new EventEmitter<any>();

    orderType: OrderType;

    orderTypes: OrderType[];
    availableYears: number[];
    availableCanceledDocuments: CancelType[];
    availableUpdatedDocuments: UpdateType[];
    executives: User[];
    emailGroups: EmailGroup[];
    documentTypes: string[] = [];

    selectedEmailGroups: EmailGroup[];

    canceledDocuments$: Observable<CancelType[]>;
    canceledDocumentsLoading = false;
    canceledDocumentsInput$ = new Subject<string>();
    canceledDocument: CancelType[];

    updatedDocuments$: Observable<UpdateType[]>;
    updatedDocumentsLoading = false;
    updatedDocumentsInput$ = new Subject<string>();
    updatedDocuments: UpdateType[];

    subscribers$: Observable<User[]>;
    subscribersInput$ = new Subject<string>();

    executors$: Observable<User[]>;
    executorsLoading = false;
    executorsInput$ = new Subject<string>();

    dropzoneConfig: DropzoneConfigInterface;
    additionalMaterials: any[] = [];

    constructor(private orderTypeService: OrderTypeService, private nsiResourceService: NsiResourceService,
                private registerDocumentService: RegisterDocumentService, private toastr: ToastrService,
                private fileResourceService: FileResourceService, private cdpSolrResourceService: CdpSolrResourceService) {
    }

    ngOnInit() {
        this.availableYears = _.range(2016, 2028);

        //setting initial values for comboboxes (showing until dictionaries is loaded)
        // @ts-ignore
        this.orderTypes = [{
            code: this.order.documentTypeCode,
            name: this.order.documentTypeValue,
            singElectron: false,
            publishing: true
        }];

        this.orderTypes = this.dicts.orderTypes;
        this.emailGroups = this.dicts.emailGroups;
        this.documentTypes = this.dicts.orderStatuses
            .filter(status => status.statusAnalytic === 'Действующий')
            .map(status => status.name);


        this.searchCanceledDocuments('').toPromise().then(response => {
            this.canceledDocument = response;

            this.canceledDocuments$ = concat(
                of(this.canceledDocument),
                this.canceledDocumentsInput$.pipe(
                    debounceTime(300),
                    distinctUntilChanged(),
                    tap(() => this.canceledDocumentsLoading = true),
                    switchMap((term: string) => this.searchCanceledDocuments(term)),
                    catchError(() => of([])),
                    tap(() => this.canceledDocumentsLoading = false)
                )
            );
        });

        this.searchUpdatedDocuments('').toPromise().then(response => {
            this.updatedDocuments = response;
            this.updatedDocuments$ = concat(
                of(this.updatedDocuments),
                this.updatedDocumentsInput$.pipe(
                    debounceTime(300),
                    distinctUntilChanged(),
                    tap(() => this.updatedDocumentsLoading = true),
                    switchMap((term: string) => this.searchUpdatedDocuments(term)),
                    catchError(() => of([])),
                    tap(() => this.updatedDocumentsLoading = false)
                )
            );
        })

        this.subscribers$ = concat(
            of([]),
            this.subscribersInput$.pipe(
                debounceTime(300),
                distinctUntilChanged(),
                filter(term => term && term.length > 1),
                switchMap((term: string) => this.searchSubscribers(term)),
                catchError(() => of([]))
            )
        );

        this.executors$ = concat(
            of([]),
            this.executorsInput$.pipe(
                debounceTime(300),
                distinctUntilChanged(),
                filter(term => term && term.length > 2),
                tap(() => this.executorsLoading = true),
                switchMap((term: string) => this.searchExecutors(term)),
                catchError(() => of([])),
                tap(() => this.executorsLoading = false)
            )
        );

        this.orderType = this.orderTypes.find(ot => {
            return ot.code === this.order.documentTypeCode;
        });
        if (this.order.additionalMaterials) {
            this.additionalMaterials = this.order.additionalMaterials.map(file => ({
                nameFile: file.fileName,
                idFile: file.fileID,
                sizeFile: file.fileSize,
                dateFile: file.fileDate,
                signed: file.fileSize,
                mimeType: file.mimeType,
                typeFile: file.fileType
            }));
        }
        this.executives = this.dicts.executives.map(user => User.fromUserBean(user));
    }

    findUser(users: User[], login: string) {
        return users.find(user => {
            return user.accountName === login;
        });
    }

    addEmailGroup(emailGroup: EmailGroup) {
        this.order.mailList = this.order.mailList || [];
        forkJoin(emailGroup.users.filter(login => !this.findUser(this.order.mailList, login))
            .map(login => this.nsiResourceService.ldapUser(login))).pipe(
            map(users => {
                return users.filter(user => !!user.mail).map(user => User.fromUserBean(user));
            })
        ).subscribe((users) => {
            this.order.mailList = this.order.mailList.concat(users);
        })
    }

    hasApproval() {
        if (!this.order.approval || !this.order.approval.approvalCycle || !this.order.approval.approvalCycle.agreed) {
            return false;
        }
        return this.order.approval.approvalCycle.agreed.some(a => {
            return a.approvalTypeCode === ApprovalType.approval;
        })
    }

    getApproval() {
        if (!this.order.approval || !this.order.approval.approvalCycle || !this.order.approval.approvalCycle.agreed) {
            return null;
        }
        return this.order.approval.approvalCycle.agreed.find(a => {
            return a.approvalTypeCode === ApprovalType.approval;
        })
    }

    deleteAdditionalMaterials = (fileInfo: any) => {
        from(this.fileResourceService.deleteFile(fileInfo.idFile)).subscribe(() => {
            let index = this.order.additionalMaterials.findIndex(mat => mat.fileID === fileInfo.idFile);
            this.order.additionalMaterials.splice(index, 1);
            this.additionalMaterials = this.order.additionalMaterials.map(file => ({
                nameFile: file.fileName,
                idFile: file.fileID,
                sizeFile: file.fileSize,
                dateFile: file.fileDate,
                signed: file.fileSize,
                mimeType: file.mimeType,
                typeFile: file.fileType
            }));
        });
    };

    changeAdditionalMaterials = () => {
        if (this.additionalMaterials.length > 0) {
            this.order.additionalMaterials = this.additionalMaterials.map(file => ({
                fileName: file.nameFile,
                fileID: file.idFile,
                fileSize: file.sizeFile,
                fileDate: file.dateFile,
                fileSigned: file.signed,
                mimeType: file.mimeType,
                fileType: file.typeFile
            }));
        } else {
            this.order.additionalMaterials = []
        }
    };

    generateDocumentNumber() {
        return this.registerDocumentService.registerDocument(this.order).subscribe(() => {
        });
    }

    searchSubscribers(query): Observable<User[]> {
        return from(this.nsiResourceService.searchUsers({ fio: query })).pipe(
            map(users => users.filter(user => !!user.mail).map(user => User.fromUserBean(user)))
        );
    }

    searchExecutors(query): Observable<User[]> {
        return from(this.nsiResourceService.searchUsers({ fio: query })).pipe(
            map((users) => {
                users = users.filter((u: any) => !u.lock);
                return users.map(user => User.fromUserBean(user));
            })
        );
    }

    buildSearchQuery(query): string {
        const statuses = this.documentTypes.map(docType => `docStatus:(${docType})`);
        const searchQuery = [
            `(${statuses.join(" OR ")})`,
            query ? `((docName:(*${query}*)) OR (docNumber:(*${query}*)))` : null,
            `-fromMKA:true`
        ].filter(obj => obj).join(" AND ");
        return searchQuery;
    }

    searchCanceledDocuments(query): Observable<CancelType[]> {
        return from(this.cdpSolrResourceService.query({
            types: ['SDO_ORDER_ORDER'],
            query: this.buildSearchQuery(query),
            pageSize: 100
        })).pipe(
            map(result => {
                return _.map(result.docs, (doc: any) => {
                    return {
                        canceledDocumentID: doc.sys_documentId,
                        canceledDocumentNum: doc.docNumber,
                        canceledDocumentDate: doc.docDate,
                        canceledDocumentType: doc.docType,
                        canceledDocumentTypeCode: doc.docTypeCode,
                        docName: doc.docName
                    };
                });
            })
        );
    }

    searchUpdatedDocuments(query): Observable<UpdateType[]> {
        return from(this.cdpSolrResourceService.query({
            types: ['SDO_ORDER_ORDER'],
            query: this.buildSearchQuery(query),
            pageSize: 100
        })).pipe(
            map(result => {
                return _.map(result.docs, (doc: any) => {
                    return {
                        updatedDocumentID: doc.sys_documentId,
                        updatedDocumentNum: doc.docNumber,
                        updatedDocumentDate: doc.docDate,
                        updatedDocumentType: doc.docType,
                        updatedDocumentTypeCode: doc.docTypeCode,
                        docName: doc.docName
                    };
                });
            })
        );
    }

    headChanged() {
        this.onHeadChanged.emit();
    }

}
