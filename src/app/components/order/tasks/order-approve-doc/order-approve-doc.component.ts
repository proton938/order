import { CdpReporterResourceService } from "@reinform-cdp/reporter-resource";
import { NsiResourceService } from "@reinform-cdp/nsi-resource";
import * as _ from "lodash";
import { FileResourceService } from "@reinform-cdp/file-resource";
import { BsModalService } from "ngx-bootstrap";
import { Component} from '@angular/core';
import { ActiveTaskService, ActivityResourceService, CdpBpmHistoryResourceService } from "@reinform-cdp/bpm-components";
import { ToastrService } from "ngx-toastr";
import { OrderService } from "../../../../services/order.service";
import { OrderTypeService } from "../../../../services/order-type.service";
import { RegisterDocumentService } from "../../../../services/register-document.service";
import { CancelDocumentService } from "../../../../services/cancel-document.service";
import { OrderActivitiService } from "../../../../services/order-activiti.service";
import { OrderApproveNewComponent } from "../order-approve/order-approve-new.component";
import { SessionStorage, SignService } from "@reinform-cdp/security";
import { mergeMap } from "rxjs/internal/operators";
import { from, of, throwError } from "rxjs/index";
import { LoadingStatus } from "@reinform-cdp/widgets";
import * as angular from 'angular';
import { ApprovalType } from "../../../../models/order/ApprovalType";
import { DomSanitizer } from '@angular/platform-browser';
import { OrderApprover } from "../../../../models/order/Order";
import {HelperService} from "../../../../services/helper.service";

@Component({
  selector: 'mggt-order-approve-doc',
  templateUrl: './order-approve-doc.component.html',
})
export class OrderApproveDocComponent extends OrderApproveNewComponent {
  stopping: boolean;
  draftPath:string;
  constructor(public domSanitizationService: DomSanitizer,
    activeTaskService: ActiveTaskService,
    helper: HelperService,
    orderTypeService: OrderTypeService,
    activityResourceService: ActivityResourceService,
    registerDocumentService: RegisterDocumentService,
    modalService: BsModalService,
    cancelDocumentService: CancelDocumentService,
    orderActivitiService: OrderActivitiService,
    signService: SignService,
    toastr: ToastrService,
    orderService: OrderService,
    fileResourceService: FileResourceService,
    nsiResourceService: NsiResourceService,
    sessionStorage: SessionStorage,
    reporterResourceService: CdpReporterResourceService,
    historyResourceService: CdpBpmHistoryResourceService) {
    super(activeTaskService, helper, orderTypeService, activityResourceService, registerDocumentService, modalService, cancelDocumentService, orderActivitiService, signService, toastr, orderService, fileResourceService, nsiResourceService, sessionStorage, reporterResourceService, historyResourceService);
  }
  ngOnInit() {
    this.helper.beforeUnload.init();
    this.loadingStatus = LoadingStatus.LOADING;
    this.activeTaskService.getTask().pipe(
      mergeMap(task => {
        this.task = task;
        this.isNegotiationProjectSign = false;
        return this.getEntityIdVar(task);
      }),
      mergeMap(id => {
        return this.orderService.get(id);
      }),
      mergeMap(order => {
        this.order = order;
        this.origOrder = angular.copy(order);

        let taskOwnerLogin: string;
        // Определяем реального владельца задачи
        if (!!this.task && !!this.task.owner) {
          this.taskOwnerLogin = this.task.owner; // Владелец делегированной задачи
        } else if (!!this.task && !!this.task.assignee && this.task.assignee !== this.session.login()) {
            this.taskOwnerLogin = this.task.assignee; // Задача делегирована, но только взята в работу (не перезагружена)
        } else {
            this.taskOwnerLogin = this.session.login();
        }
        let agreed = OrderApproveNewComponent.getCurrentAgreed(this.order, this.taskOwnerLogin);

        if (agreed === undefined) {
          this.toastr.error('Не удалось определить номер согласующего в листе согласования.');
          this.loadingStatus = LoadingStatus.ERROR;

          return throwError('Не удалось определить номер согласующего в листе согласования.');
        }

        this.approvalState = agreed.approvalTypeCode === ApprovalType.approval;
        this.approvalDepth = agreed.approvalNum.split('.').length;
        this.lastApprovalListItem = agreed === _.last(this.order.approval.approvalCycle.agreed);

        this.isMainApprover = this.approvalDepth === 1;
        this.isParallel = agreed.Parallel;
        this.needToCloseCycle = this.isMainApprover && !this.isParallel;

        // if (!this.order.activeApprover) {
        //   this.order.activeApprover = this.session.fioShort();
        // }

        if (!this.order.documentDate) {
          this.order.documentDate = new Date();
        }

        if (!agreed.approvalPlanDate) {
          agreed.approvalPlanDate = this.task.dueDate;
          return this.updateOrder().pipe(
            mergeMap(() => {
              return this.loadDictionaries();
            })
          );
        } else {
          return this.loadDictionaries();
        }
      }),
      mergeMap(() => {        
        return from(this.fileResourceService.getFolderinfo(this.order.folderID).then(folderInfo=>{
          let domainName=document.domain;
          if(!!this.order.draftFiles.draftFilesName)
            this.draftPath=`ms-word:ofe|u|https://${domainName}/alfresco/aos${folderInfo.path}/${this.order.draftFiles.draftFilesName}`;
        }));
      }),
      mergeMap(() => {
        let agreed = OrderApproveNewComponent.getCurrentAgreed(this.order, this.taskOwnerLogin);
        if (agreed.added && agreed.added.length > 0) {
          this.isAllowedAddingApprovals = false;
        }

        return this.signService.checkSign();
      }),
      mergeMap(res => {
        this.checkSign = res;
        return this.nsiResourceService.getDictFromCache('mggt_order_ApprovalType');
      })
    ).subscribe((response) => {
      this.approvalTypes = response.value;
      this.order.approval.approvalCycle.agreed.forEach((a) => {
        if (!a.approvalTime && a.approvalTypeCode) {
          const approvalType = this.approvalTypes.find(at => at.approvalTypeCode === a.approvalTypeCode);
          a.approvalTime = approvalType.approvalDuration;
        }
      });

      // .find(agreed => agreed.agreedBy.accountName === this.session.login());
      // берем запись по процессу а не ищем по логину
      const agreed =  OrderApproveNewComponent.getCurrentAgreed(this.order, this.taskOwnerLogin);
      this.approvalType = this.approvalTypes.find(type => type.approvalTypeCode === agreed.approvalTypeCode);
      this.initDropzone();
      this.loadingStatus = LoadingStatus.SUCCESS;
      this.success = true;
    }, (error) => {
      console.error(error);
      this.loadingStatus = LoadingStatus.ERROR;
    });
  }

  ngOnDestroy() {
    this.helper.beforeUnload.destroy();
  }

  agreeAndSendForwardHandler() {
    this.agreeing = true;
    this.refreshAndUpdateOrder(order => {
        let agreed = OrderApproveNewComponent.getCurrentAgreed(order, this.taskOwnerLogin);
        OrderApproveNewComponent.updateCurrentAgree(order, this.taskOwnerLogin, this.session);
        return this.approve(this.order, agreed, this.task.id, true, {
            approvalNote: this.approvalNote
        }, false)
    }).pipe(
      mergeMap((order ) => {
        this.order = order;
        this.origOrder = angular.copy(order);

        return this.activityResourceService.finishTask(parseInt(this.task.id), []);
      })
    ).subscribe((result: any) => {
      this.agreeing = false;
      this.toastr.success('Документ успешно согласован.');
      window.location.href = '/main/#/app/tasks';
    }, (e) => {
      console.log(e);
      this.agreeing = false;
    });
  }
  
  sendBackToWork() {
    if (!this.approvalNote) {
      this.toastr.error('Не указан комментарий');
      return;
    }
    this.sendingBackToWork = true;
    this.refreshAndUpdateOrder(order => {
      let agreed = OrderApproveNewComponent.getCurrentAgreed(order, this.taskOwnerLogin);
      OrderApproveNewComponent.updateCurrentAgree(order, this.taskOwnerLogin, this.session);
      return this.approve(this.order, agreed, this.task.id, false, {
        approvalNote: this.approvalNote
      }, this.needToCloseCycle)
    }).pipe(
      mergeMap((order) => {
        this.order = order;
        this.origOrder = angular.copy(order);

        return from(this.activityResourceService.finishTask(parseInt(this.task.id), []));
      })
    ).subscribe(
      () => {
        console.log('subscribe');
        this.sendingBackToWork = false;
        this.toastr.success('Документ отправлен на доработку.');
        window.location.href = '/main/#/app/tasks';
    },
    (err) => {
        console.log('subscribe err', err);
        this.sendingBackToWork = false;
    });
  }

  stopEdit() {
    this.helper.beforeUnload.start();
    this.stopping = true;
    this.refreshAndUpdateOrder(order => {
        let agreed = OrderApproveNewComponent.getCurrentAgreed(order, this.taskOwnerLogin);
        agreed.approvalFactDate=new Date();
        agreed.approvalResult='Редактирование завершено';
        return of(order);
    }).pipe(mergeMap((order) => {
      this.order = order;
      this.origOrder = angular.copy(order);

      return this.activityResourceService.finishTask(parseInt(this.task.id), []);
    })).subscribe(() => {
      this.helper.beforeUnload.stop();
      this.stopping = false;
      this.toastr.success('Задача завершена!');
      window.location.href = '/main/#/app/tasks';
    }, err => {
      this.helper.beforeUnload.stop();
      console.log('subscribe err', err);
      this.stopping = false;
    });
  }

  getApprovalResultColor(approvalTypeCode: string, approvalResult: string) {
    if (this.approvalTypes) {
        let approvalType = this.approvalTypes[approvalTypeCode];
        if (approvalType) {
            if (approvalResult === approvalType.buttonYes) {
                return 'label-' + approvalType.buttonYesColor;
            } else if (approvalResult === approvalType.buttonNo) {
                return 'label-' + approvalType.buttonNoColor;
            }
        }
    }
  }
}
