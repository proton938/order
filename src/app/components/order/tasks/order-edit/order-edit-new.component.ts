import { Transition } from '@uirouter/core';
import { ExFileType, LoadingStatus } from "@reinform-cdp/widgets";
import * as angular from "angular";
import * as _ from "lodash";
import { FileResourceService } from "@reinform-cdp/file-resource";
import { ActiveTaskService, ActivityResourceService, ITask,IProcessDefinition } from "@reinform-cdp/bpm-components";
import { Observable } from "rxjs/Rx";
import { DepartmentBean, NsiResourceService, UserBean } from "@reinform-cdp/nsi-resource";
import { AuthorizationService, SessionStorage } from "@reinform-cdp/security";
import { ToastrService } from "ngx-toastr";
import { forkJoin, of, from,throwError } from "rxjs/index";
import { map, mergeMap, tap,catchError } from "rxjs/internal/operators";
import { Component, OnInit, ViewChild } from '@angular/core';
import {
  Order, OrderApproval, OrderApprovalHistoryList, OrderApprovalListItem,
  OrderDraftFile
} from "../../../../models/order/Order";
import { OrderType } from "../../../../models/order/OrderType";
import { OrderTypeService } from "../../../../services/order-type.service";
import { OrderService } from "../../../../services/order.service";
import { ProcessComponent } from "../order-process";
import { ApprovalType } from "../../../../models/order/ApprovalType";
import { DropzoneConfigInterface } from "ngx-dropzone-wrapper";
import { CdpReporterResourceService } from "@reinform-cdp/reporter-resource";
import { ApprovalListEditComponent } from "../approval-list-edit/approval-list-edit.component";
import { EmailGroup } from "../../../../models/order/EmailGroup";
import { OrderStatus } from "../../../../models/order/OrderStatus";
import { UserService } from "../../../../services/user.service";
import { ApprovalDuration } from "../../../../models/order/ApprovalDuration";
import { ApprovalNode, isRoot, processApprovalList } from "../../../../services/approval-util.service";
import { OrderNsiService } from '../../../../services/order-nsi.service';
import { FileExtService } from "../../../../services/file-ext.service";

@Component({
  selector: 'mggt-order-modify-new',
  templateUrl: './order-edit-new.component.html',
  styleUrls: ['./order-edit.component.scss']
})
export class OrderModifyNewComponent extends ProcessComponent implements OnInit {

  loadingStatus: LoadingStatus;
  success = false;
  task: ITask;
  orderType: OrderType;
  saving: boolean;
  isParallel: boolean;
  minParallelApprovals: number;
  isRecalled: boolean = false;

  orderTypes: OrderType[];
  emailGroups: EmailGroup[];
  orderStatuses: OrderStatus[];
  executives: UserBean[];
  approvers: UserBean[];
  signUsers: UserBean[];
  approvalTypes: ApprovalType[];
  approvalDurations: ApprovalDuration[];

  dzDraftFilesWithoutAttOptions: DropzoneConfigInterface;
  dzDraftFilesAttOptions: DropzoneConfigInterface;

  parallelList: ApprovalNode[];
  declineInfo: any;

  sendingToApproval: boolean;
  sendingToRegistration: boolean;
  creatingDraftFile: boolean;
  creatingDraftFilesWithoutAtt: boolean;
  combiningDraftFile: boolean;
  dataShow: boolean = false;
    filePathForWord:string;
    showButton: boolean = false;

  attach: ExFileType[] = [];
  draftFilesWithoutAtt: ExFileType[] = [];

  startLock: boolean = true;
  onDeletedFileNoApp: boolean = false;
  onDeletedFileApp: boolean = true;
  onDeletedCreateFileApp: boolean = false;

  @ViewChild(ApprovalListEditComponent)
  appovalListEdit: ApprovalListEditComponent;

  constructor(private transition: Transition, private orderTypeService: OrderTypeService,
    private activeTaskService: ActiveTaskService, private authorizationService: AuthorizationService,
    private activityResourceService: ActivityResourceService, private userService: UserService,
    private orderNsiService: OrderNsiService,
    private fileService:FileExtService,
    toastr: ToastrService, orderService: OrderService, fileResourceService: FileResourceService, nsiResourceService: NsiResourceService,
    session: SessionStorage, reporterResourceService: CdpReporterResourceService) {
    super(toastr, orderService, fileResourceService, nsiResourceService, session, reporterResourceService);
  }

  ngOnInit() {
    this.loadingStatus = LoadingStatus.LOADING;

    this.isParallel = this.transition.params()['isParallel'];

    this.minParallelApprovals = this.transition.params()['minParallelApprovals'];

    this.activeTaskService.getTask().pipe(
      mergeMap(task => {
        this.task = task;
        return this.getEntityIdVar(task);
      }),
      mergeMap(id => {
        return this.orderService.get(id);
      }),
      mergeMap(order => {
        this.order = order;
        this.origOrder = angular.copy(order);
        return this.loadDictionaries();
      }),
      mergeMap(() => {
        return from(this.activityResourceService.getTasksHistory({ 
          processVariables: [
            { "name": "EntityIdVar", "value": this.order.documentID, "operation": "equals", "type": "string" }
          ], 
          taskDefinitionKey: 'ApproveDecision',
          size: 1000000, 
          includeProcessVariables: true }));
      }),
      mergeMap((response) => {
        const approveFinishDate = response.data[0] ? response.data[0].endTime : new Date();

        const lastCycle: OrderApprovalHistoryList = this.order.approvalHistory ? _.last(this.order.approvalHistory.approvalCycle) : null;
        this.isRecalled = this.order.returnToEditing||this.order.returnToEditingSeq; //&& lastCycle ? this.hasRecalledStatus(lastCycle) : false;
        this.declineInfo = this.getDeclineInfo(this.order, lastCycle, approveFinishDate);
        this.parallelList = lastCycle ? processApprovalList(lastCycle.agreed).parallel : [];

        if (!this.order.approval || !this.order.approval.approvalCycle || !this.order.approval.approvalCycle.approvalCycleNum) {
          if (lastCycle) {
            return this.initNewCycle(lastCycle);
          } else {
            this.order.approval = new OrderApproval(1);
            return of('');
          }
        } else {
          return of('');
        }
      })
    ).subscribe(() => {
      this.order.approval.approvalCycle.agreed.forEach((a) => {
        if (!a.approvalTime) {
          const approvalType = this.approvalTypes.find(at => at.approvalTypeCode === a.approvalTypeCode);
          a.approvalTime = approvalType.approvalDuration;
        }
      });

      if(this.order.attachWord){
        this.attach = [OrderDraftFile.toExFileType(this.order.attachWord)];
      }
      else if (this.order.attach) {
        this.attach = [OrderDraftFile.toExFileType(this.order.attach)];
      }

      if(this.order.draftFilesWithoutAttWord&&!this.order.draftFiles){
        this.draftFilesWithoutAtt = [OrderDraftFile.toExFileType(this.order.draftFilesWithoutAttWord)];
        this.showButton = true;
      }
      else if (this.order.draftFilesWithoutAtt) {
        this.draftFilesWithoutAtt = [OrderDraftFile.toExFileType(this.order.draftFilesWithoutAtt)];
        let fileExtension = this.draftFilesWithoutAtt[0].nameFile.split('.');
        if (fileExtension[fileExtension.length-1] != 'pdf') {
          this.showButton = true;
        }
      }

      this.loadingStatus = LoadingStatus.SUCCESS;
      this.success = true;

      from(this.fileResourceService.getFolderinfo(this.order.folderID)).subscribe(result => {
        this.filePathForWord = result.path;
      });

    }, err => {
      console.log(err);
      this.loadingStatus = LoadingStatus.ERROR;
    });
  }


  private hasRecalledStatus(lastCycle: OrderApprovalHistoryList): boolean {
    return lastCycle.agreed.some(agree => {
      if (agree.approvalNote) {
        return agree.approvalNote.indexOf('Отозвано инициатором РД') > -1;
      } else {
        return false;
      }
    });
  }

  private initNewCycle(lastCycle: OrderApprovalHistoryList): Observable<any> {
    return forkJoin(
      lastCycle.agreed.map((agreed) => {
        return this.nsiResourceService.ldapUser(agreed.agreedBy.accountName);
      })
    ).pipe(
      map((users: UserBean[]) => {
        let departmentsMap = _.reduce(users, (map, user) => {
          map[user.accountName] = user.departmentFullName;
          return map;
        }, {});
        const nextApprovalCycle = angular.copy(lastCycle);
        nextApprovalCycle.approvalCycleNum = '' + (this.order.approvalHistory.approvalCycle.length + 1);
        nextApprovalCycle.approvalCycleDate = this.declineInfo.approvalFactDate;
        nextApprovalCycle.agreed = [];
        nextApprovalCycle.agreed = this.copyOrderApprovalListItems(lastCycle.agreed, departmentsMap);
        this.order.approval = { approvalCycle: nextApprovalCycle };
        return of('');
      })
    );
  }

  private copyOrderApprovalListItems(agreed: OrderApprovalListItem[], departmentsMap: any): OrderApprovalListItem[] {
    let result = [];
    let mainApprovals = agreed.filter(a => isRoot(a.approvalNum));
    let approvalNum = 1;
    mainApprovals.forEach(mainApproval => {
      result.push(this.copyOrderApprovalListItem(mainApproval, null, approvalNum++, departmentsMap[mainApproval.agreedBy.accountName]));
      //MGGT-1714
      // let addedApprovals = agreed.filter(a => !isRoot(a.approvalNum) && a.approvalNum.startsWith(mainApproval.approvalNum));
      // result = result.concat(addedApprovals.map(a => this.copyOrderApprovalListItem(a, mainApproval, approvalNum++, departmentsMap)));
    });
    return result;
  }

  private copyOrderApprovalListItem(agreed: OrderApprovalListItem, parent: OrderApprovalListItem, approvalNum: number, department: string) {
    let result = new OrderApprovalListItem();
    let approvalType = _.find(this.approvalTypes, (type: ApprovalType) => type.approvalTypeCode === agreed.approvalTypeCode && type.department === department);
    if (!approvalType) {
      approvalType = _.find(this.approvalTypes, (type: ApprovalType) => type.approvalTypeCode === agreed.approvalTypeCode && !type.department);
    }
    let approvalTime = approvalType ? approvalType.approvalDuration : null;
    _.extend(result, {
      approvalNum: '' + approvalNum,
      agreedBy: agreed.agreedBy,
      approvalType: agreed.approvalType,
      approvalTypeCode: agreed.approvalTypeCode,
      approvalTime: approvalTime,
      approvalTerm: agreed.approvalTerm || parent.approvalTerm,
      Parallel: agreed.Parallel,
      Signer: agreed.Signer
    });
    return result;
  }

  private getDeclineInfo(order: Order, lastCycle: OrderApprovalHistoryList, approveFinishDate) {
    if (this.isParallel) {
      return {
        user: null,
        approvalType: null,
        approvalFactDate: approveFinishDate,
        approvalNote: null,
        files: null,
      }
    }

    const notAgreedOrApproved = lastCycle ? _.find(lastCycle.agreed, (agreedItem: OrderApprovalListItem) => {
      return isRoot(agreedItem.approvalNum) && (agreedItem.approvalResult === 'Не согласовано' || agreedItem.approvalResult === 'Не утверждено');
    }) : null;

    const lastSigningPaper = order.SigningOnPaper ? _.last(order.SigningOnPaper) : null;
    if (notAgreedOrApproved) {
      return {
        user: notAgreedOrApproved.agreedBy.fioFull + ' (' + notAgreedOrApproved.agreedBy.post + ')',
        approvalType: notAgreedOrApproved.approvalType,
        approvalFactDate: notAgreedOrApproved.approvalFactDate,
        approvalNote: notAgreedOrApproved.approvalNote,
        files: notAgreedOrApproved.fileApproval
      }
    } else if (lastSigningPaper) {
      const head = order.head;
      return {
        user: head ? head.fio + ' (' + head.post + ')' : null,
        approvalType: null,
        approvalFactDate: lastSigningPaper.signingFactDate,
        approvalNote: lastSigningPaper.noteSigning.noteText,
        files: lastSigningPaper.File
      }
    }
  }

  signingElectronicallyChanged() {
    this.appovalListEdit.headChanged();
  }

  loadDictionaries() {
    return forkJoin([
      this.orderTypeService.geOrderTypes(),
      this.nsiResourceService.departments(),
      this.nsiResourceService.get('EmailGroups'),
      this.nsiResourceService.get('mggt_order_OrderStatuses'),
      this.nsiResourceService.get('mggt_order_ApprovalType'),
      this.nsiResourceService.get('mggt_order_AppDuration')
    ]).pipe(
      mergeMap((result: [OrderType[], DepartmentBean[], EmailGroup[], OrderStatus[], ApprovalType[], ApprovalDuration[]]) => {
        this.orderTypes = result[0];
        this.orderType = this.orderTypes.find(_ => {
          return _.code === this.order.documentTypeCode;
        });

        this.emailGroups = result[2].filter(emailGroup => {
          return emailGroup.sendSystem && emailGroup.sendSystem.find(sendSystem => {
            return sendSystem.code === 'SDO_ORDER';
          });
        });
        this.orderStatuses = result[3];
        this.approvalTypes = result[4];
        this.approvalDurations = result[5];

        return this.userService.getSign(this.orderType);
      }),
      mergeMap((sign) => {
        this.signUsers = sign;

        return this.userService.getExecutives(this.orderType);
      }),
      mergeMap(executives => {
        const signAccounts = this.signUsers.map((u) => u.accountName);

        this.executives = executives.filter((e) => signAccounts.indexOf(e.accountName) !== -1);
        
        return this.userService.getApprovers(this.orderType);
      }),
      tap(approvers => {
        this.approvers = approvers;
      })
    )
  }

  saveAndCreateDraftFile() {
    if (!this.validate()) {
      return;
    }
    this.creatingDraftFile = true;
    this.updateOrder().pipe(
      mergeMap(() => {
        const templateCode = this.order.signingElectronically ? this.orderType.codeTemplateSingElectron : this.orderType.codeTemplate;
        return this.generateDraftFilesWithoutAtt(templateCode);
      })
    ).subscribe((result: OrderDraftFile) => {
      this.creatingDraftFile = false;
      this.order.draftFiles = result;
    }, () => {
      this.creatingDraftFile = false;
    });
  }

  checkDoubleAgreed() {
    let items = this.order.approval.approvalCycle.agreed.slice(0),
      testItem;

    while (items.length) {
      testItem = items.shift();
      if (_.find(items, (item: OrderApprovalListItem) => testItem.agreedBy.accountName === item.agreedBy.accountName)) {
        return testItem;
      }
    }
    return null;

  }

  validate() {
    if (!this.order.documentTypeCode || !this.order.documentYear || !this.order.documentContent ||
      !this.order.executor || !this.order.mailList || this.order.executor.length === 0 || this.order.mailList.length === 0) {
      this.toastr.error('Не заполнены обязательные поля');
      return false;
    }

    if (!this.order.head) {
      this.toastr.error('Не заполнен руководитель, утверждающий документ');
      return false;
    }

    if (this.hasNotFilledApprover()) {
      this.toastr.error('Не заполнен список или срок согласования');
      return false;
    }

    const double = this.checkDoubleAgreed();
    if (double) {
      this.toastr.error(`В листе согласования повторяется согласующий ${double.agreedBy.fioFull}`);
      return false;
    }
    return true;
  }

  hasNotFilledApprover() {
    return this.order.approval.approvalCycle.agreed.some((a) => {
      if (!a.approvalType) {
        return true;
      }

      if (!a.agreedBy) {
        return true;
      } else if (!a.agreedBy.accountName) {
        return true;
      }

      return false;
    });
  }

  saveDocument() {
    if (!this.validate()) {
      return;
    }
    this.saving = true;
    this.updateOrder().subscribe(() => {
      this.saving = false;
    }, error => {
      this.toastr.error(error);
      this.saving = false;
    });
  }

  saveAndSendToApproval() {
    // if (this.startLock) {
    //   this.toastr.error('Не найден файл проект приказа');
    //   return;
    // }

    if (!this.validate()) {
      return;
    }
    if (!this.order.draftFiles) {
      this.toastr.error('Не найден файл проект приказа');
      return;
    }
    if (!this.order.draftFilesWithoutAttWord) {
      this.toastr.error('Файл "Проект РД без приложений" был переформирован. Нужно сформировать заново файл "Проект РД"');
      return;
    }
    if(!!this.order.attach&&this.order.attach.draftFilesDate>this.order.draftFiles.draftFilesDate){
      this.toastr.error('Были обновлены файлы приложения. Нужно сформировать заново файл "Проект РД"');
      return;
    }
    this.order.signingElectronically = this.order.signingElectronically.toString() === 'true';
    this.order.PublishingSite = String(this.order.PublishingSite) === 'true';
    if (!this.order.signingElectronically
        && (!this.order.approval.approvalCycle.agreed || this.order.approval.approvalCycle.agreed.length === 0)) {
      this.toastr.error('Для отправки на согласование необходимо выбрать согласующих');
      return false;
    }
    if (this.order.approval && this.order.approval.approvalCycle && this.order.approval.approvalCycle.agreed) {
      let agreed = this.order.approval.approvalCycle.agreed;
      for (let i = 0; i < agreed.length; i++) {
        if (!agreed[i].approvalTypeCode) {
          this.toastr.error(`Не заполнен список согласования, не указан тип согласования у согласующего ${agreed[i].approvalNum}`);
          return;
        }
        if (!agreed[i].agreedBy) {
          this.toastr.error(`Не заполнен список согласования, не выбран согласующий ${agreed[i].approvalNum}`);
          return;
        }
        // if (!agreed[i].agreedBy) {
        //   this.toastr.error(`Не заполнен список согласования, не выбран срок согласования у ${agreed[i].approvalNum}`);
        //   return;
        // }
        if(!agreed[i].approvalTerm||!agreed[i].approvalTerm.duration){
          this.toastr.error(`Не заполнен список согласования, не выбран срок согласования у ${agreed[i].approvalNum}`);
          // this.toastr.error('Не заполнен список или срок согласования');
          return;
        }
      }
      let parallelApproversCount = agreed.filter(a => a.Parallel).length;
      if (this.minParallelApprovals && parallelApproversCount < this.minParallelApprovals) {
        this.toastr.error(`В блоке "Параллельное согласование" должны быть выбраны минимум ${this.minParallelApprovals} согласующих`);
        return;
      } else if (parallelApproversCount > 0 && parallelApproversCount < 2) {
        this.toastr.error('В блоке "Параллельное согласование" должны быть выбраны минимум 2 согласующих');
        return;
      }
    }
    if (this.order.signingElectronically && !this.hasApproval()) {
      this.toastr.error('Не указано утверждающее лицо');
      return;
    }
    if (!this.order.signingElectronically && !_.some(this.order.approval.approvalCycle.agreed, (item) => {
      return item.agreedBy.accountName === this.order.head.accountName && item.Signer === true
          && _.some(['agreed', 'assent'], i => item.approvalTypeCode === i);
    })) {
      this.toastr.error('Не указано утверждающее лицо');
      return;
    }
    this.order.activeApprover = this.order.approval.approvalCycle.agreed[0].agreedBy.fioShort;
    this.order.returnToEditing=false;
    this.order.returnToEditingSeq=false;
    this.sendingToApproval = true;    
    this.orderNsiService.fillApprovalPlanDates(this.order.approval.approvalCycle.agreed).pipe(
      mergeMap(() => {
        return this.updateOrder()
      }),
      mergeMap(() => {
        let hasParallelApprovals = this.order.approval.approvalCycle.agreed.some(a => a.Parallel);
        return this.activityResourceService.finishTask(parseInt(this.task.id), [
          { "name": "ElApprovalVar", "value": true },
          { "name": "RDPublishingSite", "value": this.order.PublishingSite },
          { name: 'IsParallel', value: hasParallelApprovals }
        ])
      })
    ).subscribe(() => {
      this.sendingToApproval = false;
      this.toastr.success('Документ отправлен на согласование.');
      window.location.href = '/main/#/app/tasks';
    }, () => {
      this.sendingToApproval = false;
    });   
  }

  hasApproval() {
    if (!this.order.approval || !this.order.approval.approvalCycle || !this.order.approval.approvalCycle.agreed) {
      return false;
    }
    return _.some(this.order.approval.approvalCycle.agreed, (a) => {
      return a.approvalTypeCode === ApprovalType.approval;
    })
  }

  isApplication() {
    return this.orderType.application;
  }

  createDraftFilesWithoutAtt() {
      from(this.fileResourceService.getFolderinfo(this.order.folderID)).subscribe(result => {
          this.filePathForWord = result.path;
          this.creatingDraftFilesWithoutAtt = true;
          const templateCode = this.order.signingElectronically ? this.orderType.codeTemplateSingElectron : this.orderType.codeTemplate;
          this.generateDraftFilesWithoutAtt(templateCode).pipe(mergeMap((fileType) => {
            return this.updateOrderPart(ord => {
              ord.draftFilesWithoutAtt = fileType;
            }).pipe(map(()=>{
              this.order.draftFilesWithoutAtt = fileType;
              this.origOrder.draftFilesWithoutAtt=fileType;
              return fileType;
            }));
          }),tap(result => {
            this.order.draftFilesWithoutAtt = result;
            this.draftFilesWithoutAtt = [OrderDraftFile.toExFileType(result)];
          }),mergeMap(()=>{
            return this.activityResourceService.getProcessDefinitions({ key: 'sdord_updateAccessRules', latest: true });
          }),mergeMap((response) => {
            const processes = response.data;
            if (processes.length <= 0) {
              return throwError('Процесс sdord_updateAccessRules не найден');
            } else {
              return of(processes[0]);
            }
          }),
          mergeMap((process: IProcessDefinition) => {
            return this.activityResourceService.initProcess({
              processDefinitionId: process.id,
              variables: [{
                name: 'EntityIdVar',
                value: this.order.documentID
              }, {
                name: 'FileIdVar',
                value: this.draftFilesWithoutAtt[0].idFile
              }, {
                name: 'PrincipalIdVar',
                value: this.session.login()
              }]
            });
          }),catchError(error => {
            console.log(error);
            this.creatingDraftFilesWithoutAtt = false;  
            return throwError(error);
          })).subscribe(()=>{
            this.creatingDraftFilesWithoutAtt = false;
          });
      });
      this.showButton = true;
    this.onDeletedFileNoApp = false;
  }

  onAttachDelete = () => {
    this.deleteAttach().subscribe(() => {
      this.attach = [];
      delete this.order.attach;
      delete this.order.attachWord;
      delete this.origOrder.attach;
      delete this.origOrder.attachWord;
    });
  };

  changeAttach = () => {
    const attach = this.attach[0];
    if (attach) {
      this.order.attach = OrderDraftFile.fromExFileType(attach);
    } else {
      // this.order.attach = null;
    }
  };

  onDraftFilesWithoutAttDelete = () => {
    this.deleteDraftFilesWithoutAtt().subscribe(() => {
      this.draftFilesWithoutAtt = [];
      delete this.order.draftFilesWithoutAtt;
      delete this.order.draftFilesWithoutAttWord;
      delete this.origOrder.draftFilesWithoutAtt;
      delete this.origOrder.draftFilesWithoutAttWord;
      // this.createDraftFilesWithoutAtt();// todo to get MGGT-2337 
    });
  };

  changeDraftFilesWithoutAtt = () => {
    const draftFilesWithoutAtt = this.draftFilesWithoutAtt[0];
    if (draftFilesWithoutAtt) {
      this.order.draftFilesWithoutAtt = OrderDraftFile.fromExFileType(draftFilesWithoutAtt);
      from(this.activityResourceService.getProcessDefinitions({ key: 'sdord_updateAccessRules', latest: true })).pipe(
        mergeMap((response) => {
          const processes = response.data;
          if (processes.length <= 0) {
            return throwError('Процесс sdord_updateAccessRules не найден');
          } else {
            return of(processes[0]);
          }
        }),
        mergeMap((process: IProcessDefinition) => {
          return this.activityResourceService.initProcess({
            processDefinitionId: process.id,
            variables: [{
              name: 'EntityIdVar',
              value: this.order.documentID
            }, {
              name: 'FileIdVar',
              value: this.draftFilesWithoutAtt[0].idFile
            }, {
              name: 'PrincipalIdVar',
              value: this.session.login()
            }]
          });
        })
      ).subscribe();
      // this.fileService.updateAccess(this.order.draftFilesWithoutAtt.draftFilesID,
      //   this.session.login()).subscribe();
      this.onDeletedFileNoApp = false;
    } else {
      // this.order.draftFilesWithoutAtt = null;
      this.onDeletedFileNoApp = true;
    }
    this.creatingDraftFilesWithoutAtt = false;
    this.showButton = true;
  };

  onDraftFilesDelete = () => {
    this.deleteDraftFiles().subscribe(() => {
      delete this.order.draftFiles;
      delete this.origOrder.draftFiles;
      delete this.order.draftFilesWithoutAttWord;
      delete this.origOrder.draftFilesWithoutAttWord;
      if(this.order.draftFilesWithoutAtt){
        this.draftFilesWithoutAtt = [OrderDraftFile.toExFileType(this.order.draftFilesWithoutAtt)];
        this.showButton=true;
      }      
    });
    this.onDeletedCreateFileApp = true;
  };

  combineDraftFile() {
    this.combiningDraftFile = true;
    this.createOrderDraftFile().pipe(mergeMap((fileType) => {
      let draftFilesWithoutAtt = this.order.draftFilesWithoutAtt;
      let draftFilesWithoutAttWord = this.order.draftFilesWithoutAttWord;
      return this.updateOrderPart(ord => {
        ord.draftFiles = fileType;
        ord.draftFilesWithoutAtt=draftFilesWithoutAtt;
        ord.draftFilesWithoutAttWord=draftFilesWithoutAttWord;
      }).pipe(map(()=>{
        this.order.draftFiles = fileType;
        this.origOrder.draftFiles=fileType;
        this.origOrder.draftFilesWithoutAtt=draftFilesWithoutAtt;
        this.origOrder.draftFilesWithoutAttWord=draftFilesWithoutAttWord;
        return fileType;
      }));
    })).subscribe((fileType) => {
      this.draftFilesWithoutAtt = [OrderDraftFile.toExFileType(this.order.draftFilesWithoutAtt)];
      this.showButton = false;
      // this.order.draftFiles = fileType;
      this.combiningDraftFile = false;
      this.onDeletedCreateFileApp = false;
      this.startLock = false;
    }, () => {
      this.combiningDraftFile = false;
    });
  }

  headChanged() {
    this.appovalListEdit.headChanged();
  }

}
