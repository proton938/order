import * as angular from 'angular';
import * as _ from 'lodash';
import { FileResourceService } from '@reinform-cdp/file-resource';
import { ActiveTaskService, ActivityResourceService, ITask } from '@reinform-cdp/bpm-components';
import { Observable } from 'rxjs/Rx';
import { DepartmentBean, NsiResourceService, UserBean } from '@reinform-cdp/nsi-resource';
import { AuthorizationService, SessionStorage } from '@reinform-cdp/security';
import { ToastrService } from 'ngx-toastr';
import { LoadingStatus } from '@reinform-cdp/widgets';
import { forkJoin, from, of } from 'rxjs/index';
import { mergeMap, tap, map } from 'rxjs/internal/operators';
import { Component, OnInit, ViewChild } from '@angular/core';
import {
  Order, OrderApproval, OrderApprovalHistoryList, OrderApprovalListItem,
  OrderDraftFile
} from '../../../../models/order/Order';
import { OrderType } from '../../../../models/order/OrderType';
import { OrderTypeService } from '../../../../services/order-type.service';
import { OrderService } from '../../../../services/order.service';
import { ProcessOldComponent } from '../order-process-old';
import { ApprovalType } from '../../../../models/order/ApprovalType';
import { DropzoneConfigInterface } from 'ngx-dropzone-wrapper';
import { CdpReporterResourceService, CdpReporterPreviewService, PdfService } from '@reinform-cdp/reporter-resource';
import { ApprovalListEditOldComponent } from '../approval-list-edit/approval-list-edit-old.component';
import { EmailGroup } from '../../../../models/order/EmailGroup';
import { OrderStatus } from '../../../../models/order/OrderStatus';
import { User } from '../../../../models/order/User';
import { UserService } from '../../../../services/user.service';
import { formatAs } from '../../../../services/date-util.service';
import { OrderNsiService } from '../../../../services/order-nsi.service';
import { ApprovalNode, isRoot, processApprovalList } from "../../../../services/approval-util.service";


@Component({
  selector: 'mggt-order-modify',
  templateUrl: './order-edit.component.html',
  styleUrls: ['./order-edit.component.scss']
})
export class OrderModifyComponent extends ProcessOldComponent implements OnInit {

  loadingStatus: LoadingStatus;
  success = false;
  task: ITask;
  orderType: OrderType;

  orderTypes: OrderType[];
  emailGroups: EmailGroup[];
  orderStatuses: OrderStatus[];
  signUsers: UserBean[];
  executives: UserBean[];
  approvers: UserBean[];
  approvalTypes: ApprovalType[];

  dzDraftFilesWithoutAttOptions: DropzoneConfigInterface;
  dzDraftFilesAttOptions: DropzoneConfigInterface;

  declineInfo: any;

  sendingToApproval: boolean;
  sendingToRegistration: boolean;
  creatingDraftFile: boolean;
  creatingDraftFilesWithoutAtt: boolean;
  combiningDraftFile: boolean;
  dataShow: boolean = false;
  isRecalled: boolean = false;

  attach: any[] = [];
  draftFilesWithoutAtt: any[] = [];

  @ViewChild(ApprovalListEditOldComponent)
  approvalListEdit: ApprovalListEditOldComponent;

  constructor(public orderTypeService: OrderTypeService,
    private activeTaskService: ActiveTaskService, private authorizationService: AuthorizationService,
    private activityResourceService: ActivityResourceService, private userService: UserService,
    toastr: ToastrService, orderService: OrderService, fileResourceService: FileResourceService, nsiResourceService: NsiResourceService,
    session: SessionStorage, reporterResourceService: CdpReporterResourceService, private reporterPreviewService: CdpReporterPreviewService,
    private orderNsiService: OrderNsiService,
    public pdfService: PdfService) {
    super(toastr, orderService, fileResourceService, nsiResourceService, session, reporterResourceService, orderTypeService, pdfService);
  }

  ngOnInit() {
    this.loadingStatus = LoadingStatus.LOADING;
    this.activeTaskService.getTask().pipe(
      mergeMap(task => {
        this.task = task;
        return this.getEntityIdVar(task);
      }),
      mergeMap(id => {
        return this.orderService.get(id);
      }),
      mergeMap(order => {
        this.order = order;
        this.origOrder = angular.copy(order);
        return this.loadDictionaries();
      }),
      mergeMap(() => {
        const lastCycle = this.order.approvalHistory ? _.last(this.order.approvalHistory.approvalCycle) : null;
        this.isRecalled = this.order.returnToEditing && lastCycle ? this.hasRecalledStatus(lastCycle) : false;
        this.declineInfo = this.getDeclineInfo(this.order, lastCycle);

        if (lastCycle) {
          return this.initNewCycle(lastCycle);
        } else {
          return of(null);
        }
      }),
      tap((nextApprovalCycle) => {
        if (nextApprovalCycle) {
          this.order.approval = { approvalCycle: nextApprovalCycle };
        } else {
          this.order.approval = new OrderApproval(this.order.approvalHistory.approvalCycle.length + 1)
        }
      })
    ).subscribe(() => {
      if (this.order.attach) {
        this.attach = [{
          nameFile: this.order.attach.draftFilesName,
          idFile: this.order.attach.draftFilesID,
          sizeFile: this.order.attach.draftFilesSize,
          dateFile: this.order.attach.draftFilesDate,
          signed: this.order.attach.draftFilesSigned,
          mimeType: this.order.attach.draftFilesMimeType,
          typeFile: this.order.attach.draftFilesType
        }];
      }

      if (this.order.draftFilesWithoutAtt) {
        this.draftFilesWithoutAtt = [{
          nameFile: this.order.draftFilesWithoutAtt.draftFilesName,
          idFile: this.order.draftFilesWithoutAtt.draftFilesID,
          sizeFile: this.order.draftFilesWithoutAtt.draftFilesSize,
          dateFile: this.order.draftFilesWithoutAtt.draftFilesDate,
          signed: this.order.draftFilesWithoutAtt.draftFilesSigned,
          mimeType: this.order.draftFilesWithoutAtt.draftFilesMimeType,
          typeFile: this.order.draftFilesWithoutAtt.draftFilesType
        }];
      }

      this.loadingStatus = LoadingStatus.SUCCESS;
      this.success = true;
    }, () => {
      this.loadingStatus = LoadingStatus.ERROR;
    });
  }

  private hasRecalledStatus(lastCycle: OrderApprovalHistoryList): boolean {
    return lastCycle.agreed.some(agree => {
      if (agree.approvalNote) {
        return agree.approvalNote.indexOf('Отозвано инициатором РД') > -1;
      } else {
        return false;
      }
    });
  }

  private initNewCycle(lastCycle: OrderApprovalHistoryList): Observable<any> {
    return forkJoin(
      lastCycle.agreed.map((agreed) => {
        return this.nsiResourceService.ldapUser(agreed.agreedBy.accountName);
      })
    ).pipe(
      map(users => {
        let usersMap = _.reduce(users, (map, user) => {
          map[user.accountName] = User.fromUserBean(user);
          return map;
        }, {});
        const nextApprovalCycle = angular.copy(lastCycle);
        nextApprovalCycle.approvalCycleNum = '' + (this.order.approvalHistory.approvalCycle.length + 1);
        nextApprovalCycle.approvalCycleDate = this.declineInfo.approvalFactDate;
        // //MGGT-1714
        // let mainApprovals = lastCycle.agreed.filter(a => isRoot(a.approvalNum));
        nextApprovalCycle.agreed = _.map(lastCycle.agreed, agreed => {
          let result = new OrderApprovalListItem();
          let user = usersMap[agreed.agreedBy.accountName];
          let approvalType = _.find(this.approvalTypes, (type: ApprovalType) => type.approvalTypeCode === agreed.approvalTypeCode && type.department === user.department);
          if (!approvalType) {
            approvalType = _.find(this.approvalTypes, (type: ApprovalType) => type.approvalTypeCode === agreed.approvalTypeCode && !type.department);
          }

          _.extend(result, {
            approvalNum: agreed.approvalNum,
            agreedBy: agreed.agreedBy,
            approvalType: agreed.approvalType,
            approvalTypeCode: agreed.approvalTypeCode,
            approvalTime: agreed.approvalTime,
          });
          return result;
        });

        console.log('nextApprovalCycle', nextApprovalCycle);

        return nextApprovalCycle;
      })
    );
  }

  private getDeclineInfo(order: Order, lastCycle: OrderApprovalHistoryList) {
    const notAgreedOrApproved = lastCycle ? _.find(lastCycle.agreed, (agreedItem: OrderApprovalListItem) => {
      return agreedItem.approvalResult === 'Не согласовано' || agreedItem.approvalResult === 'Не утверждено';
    }) : null;

    const lastSigningPaper = order.SigningOnPaper ? _.last(order.SigningOnPaper) : null;
    if (notAgreedOrApproved) {
      return {
        user: notAgreedOrApproved.agreedBy.fioFull + ' (' + notAgreedOrApproved.agreedBy.post + ')',
        approvalType: notAgreedOrApproved.approvalType,
        approvalFactDate: notAgreedOrApproved.approvalFactDate,
        approvalNote: notAgreedOrApproved.approvalNote,
        files: notAgreedOrApproved.fileApproval
      }
    } else if (lastSigningPaper) {
      const head = order.head;
      return {
        user: head ? head.fio + ' (' + head.post + ')' : null,
        approvalType: null,
        approvalFactDate: lastSigningPaper.signingFactDate,
        approvalNote: lastSigningPaper.noteSigning.noteText,
        files: lastSigningPaper.File
      }
    }
  }

  loadDictionaries() {
    return forkJoin([
      this.orderTypeService.geOrderTypes(),
      this.nsiResourceService.departments(),
      this.nsiResourceService.get('EmailGroups'),
      this.nsiResourceService.get('mggt_order_OrderStatuses'),
      this.nsiResourceService.get('mggt_order_ApprovalType')
    ]).pipe(
      mergeMap((result: [OrderType[], DepartmentBean[], EmailGroup[], OrderStatus[], ApprovalType[]]) => {
        this.orderTypes = result[0];
        this.orderType = this.orderTypes.find(_ => {
          return _.code === this.order.documentTypeCode;
        });

        this.emailGroups = result[2].filter(emailGroup => {
          return emailGroup.sendSystem && emailGroup.sendSystem.find(sendSystem => {
            return sendSystem.code === 'SDO_ORDER';
          });
        });
        this.orderStatuses = result[3];
        this.approvalTypes = result[4];

        return this.userService.getSign(this.orderType);
      }),
      mergeMap((sign) => {
        this.signUsers = sign;

        return this.userService.getExecutives(this.orderType);
      }),
      mergeMap(executives => {
        const signAccounts = this.signUsers.map((u) => u.accountName);

        this.executives = executives.filter((e) => signAccounts.indexOf(e.accountName) !== -1);
        
        return this.userService.getApprovers(this.orderType);
      }),
      tap(approvers => {
        this.approvers = approvers;
      })
    )
  }

  saveAndCreateDraftFile() {
    if (!this.validate()) {
      return;
    }
    this.creatingDraftFile = true;
    this.updateOrder().pipe(
      mergeMap(() => {
        return this.generateDraftFile();
      })
    ).subscribe((result: OrderDraftFile) => {
      this.creatingDraftFile = false;
      this.order.draftFiles = result;
    }, () => {
      this.creatingDraftFile = false;
    });
  }

  checkDoubleAgreed() {
    let items = this.order.approval.approvalCycle.agreed.slice(0),
      testItem;

    while (items.length) {
      testItem = items.shift();
      if (_.find(items, (item: OrderApprovalListItem) => (testItem.agreedBy && testItem.agreedBy.accountName === item.agreedBy.accountName))) {
        return testItem;
      }
    }
    return null;

  }

  validate() {
    if (!this.order.documentTypeCode || !this.order.documentYear || !this.order.documentContent ||
      !this.order.executor || !this.order.mailList || this.order.executor.length === 0 || this.order.mailList.length === 0) {
      this.toastr.error('Не заполнены обязательные поля');
      return false;
    }

    if (!this.order.head) {
      this.toastr.error('Не заполнен руководитель, утверждающий документ');
      return false;
    }

    if (this.hasNotFilledApprover()) {
      this.toastr.error('Не заполнен список или срок согласования');
      return false;
    }

    const double = this.checkDoubleAgreed();
    if (double) {
      this.toastr.error(`В листе согласования повторяется согласующий ${double.agreedBy.fioFull}`);
      return false;
    }

    return true;
  }

  hasNotFilledApprover() {
    return this.order.approval.approvalCycle.agreed.some((a) => {
      if (!a.approvalType) {
        return true;
      }

      if (!a.agreedBy) {
        return true;
      } else if (!a.agreedBy.accountName) {
        return true;
      }

      return false;
    });
  }

  saveDocument() {
    if (!this.validate()) {
      return;
    }
    this.updateOrder().subscribe(() => {
    });
  }

  saveAndSendToApproval() {
    if (!this.validate()) {
      return;
    }
    if (!this.order.draftFiles) {
      this.toastr.error('Не найден файл проект приказа');
      return;
    }
    this.order.signingElectronically = this.order.signingElectronically.toString() === 'true';
    this.order.PublishingSite = String(this.order.PublishingSite) === 'true';
    if (!this.order.signingElectronically && (!this.order.approval.approvalCycle.agreed || this.order.approval.approvalCycle.agreed.length === 0)) {
      this.toastr.error('Для отправки на согласование необходимо выбрать согласующих');
      return false;
    }
    if (this.order.approval && this.order.approval.approvalCycle && this.order.approval.approvalCycle.agreed) {
      let agreed = this.order.approval.approvalCycle.agreed;
      for (let i = 0; i < agreed.length; i++) {
        if (!agreed[i].approvalTypeCode || !agreed[i].agreedBy) {
          this.toastr.error('Не заполнен список согласования');
          return;
        }
      }
        if (!this.order.draftFiles) {
            this.toastr.error('Не найден файл проект приказа');
            return;
        }
        this.order.signingElectronically = this.order.signingElectronically.toString() === 'true';
        this.order.PublishingSite = String(this.order.PublishingSite) === 'true';
        if (!this.order.signingElectronically && (!this.order.approval.approvalCycle.agreed || this.order.approval.approvalCycle.agreed.length === 0)) {
            this.toastr.error('Для отправки на согласование необходимо выбрать согласующих');
            return false;
        }
        if (this.order.approval && this.order.approval.approvalCycle && this.order.approval.approvalCycle.agreed) {
            let agreed = this.order.approval.approvalCycle.agreed;
            for (let i = 0; i < agreed.length; i++) {
                if (!agreed[i].approvalTypeCode || !agreed[i].agreedBy) {
                    this.toastr.error('Не заполнен список согласования');
                    return;
                }
            }
        }
        if (this.order.signingElectronically && !this.hasApproval()) {
            this.toastr.error('Не указано утверждающее лицо');
            return;
        }
        if (!this.order.signingElectronically && !_.some(this.order.approval.approvalCycle.agreed, (item) => {
            return item.agreedBy.accountName === this.order.head.accountName;
        })) {
            this.toastr.error('Необходимо добавить руководителя в лист согласования');
            return;
        }
        // this.order.activeApprover = this.order.approval.approvalCycle.agreed[0].agreedBy.fioShort;
        this.sendingToApproval = true;

        this.fillApprovalPlanDates().pipe(
            mergeMap(() => {
                return this.updateOrder()
            }),
            mergeMap(() => {
                return this.activityResourceService.finishTask(parseInt(this.task.id), [{
                    'name': 'ElApprovalVar',
                    'value': true
                }, {
                    'name': 'RDPublishingSite',
                    'value': this.order.PublishingSite
                }])
            })
        ).subscribe(() => {
            this.sendingToApproval = false;
            this.toastr.success('Документ отправлен на согласование.');
            window.location.href = '/main/#/app/tasks';
        }, () => {
            this.sendingToApproval = false;
        });
    }    
  }

    private fillApprovalPlanDates(): Observable<any> {
        return forkJoin(
            this.fillApprovalPlanDate(this.order.approval.approvalCycle.agreed[0])
        );
    }

    private fillApprovalPlanDate(agreed: OrderApprovalListItem): Observable<any> {
        const duration = agreed.approvalTime;
        const now = new Date();
        if (!duration) {
            return of('');
        }
        return this.orderNsiService.addDuration(now, duration).pipe(
            tap(result => {
                const date = new Date(result);
                agreed.approvalPlanDate = new Date(date.getFullYear(), date.getMonth(), date.getDate(), 23, 59, 59, 0);
            })
        );
    }

  hasApproval() {
    if (!this.order.approval || !this.order.approval.approvalCycle || !this.order.approval.approvalCycle.agreed) {
      return false;
    }
    return _.some(this.order.approval.approvalCycle.agreed, (a) => {
      return a.approvalTypeCode === ApprovalType.approval;
    })
  }

  isApplication() {
    return this.orderType.application;
  }

  createDraftFilesWithoutAtt() {
    this.creatingDraftFilesWithoutAtt = true;
    this.generateDraftFile().subscribe(result => {
      this.order.draftFilesWithoutAtt = result;
      this.draftFilesWithoutAtt = [{
        nameFile: this.order.draftFilesWithoutAtt.draftFilesName,
        idFile: this.order.draftFilesWithoutAtt.draftFilesID,
        sizeFile: this.order.draftFilesWithoutAtt.draftFilesSize,
        dateFile: this.order.draftFilesWithoutAtt.draftFilesDate,
        signed: this.order.draftFilesWithoutAtt.draftFilesSigned,
        mimeType: this.order.draftFilesWithoutAtt.draftFilesMimeType,
        typeFile: this.order.draftFilesWithoutAtt.draftFilesType
      }];
      this.order.draftFilesWithoutAttWord = _.cloneDeep(this.order.draftFilesWithoutAtt);
      this.order.draftFilesWithoutAttWord.draftFilesDate = this.order.draftFilesWithoutAtt.draftFilesDate;
      this.creatingDraftFilesWithoutAtt = false;
    }, () => {
      this.creatingDraftFilesWithoutAtt = false;
    });
  }

  generateDraftFile(): Observable<OrderDraftFile> {
    const document: any = _.clone(this.order);
    document.documentDate = formatAs(Date.now(), 'dd.MM.yyyy');
    document.documentNumber = this.order.approvedDocumentNumber;


    let templateCode = this.orderType.codeTemplate;

    if (this.order.signingElectronically) {
      templateCode = this.orderType.codeTemplateSingElectron;
    }

    let request: any = {
      options: {
        jsonSourceDescr: '',
        onProcess: 'Сформировать проект РД',
        placeholderCode: '',
        strictCheckMode: true,
        xwpfResultDocumentDescr: 'Проект РД.docx',
        xwpfTemplateDocumentDescr: ''
      },
      jsonTxt: JSON.stringify({ document: this.order }),
      rootJsonPath: '$',
      nsiTemplate: {
        templateCode: templateCode,
      },
      filenetDestination: {
        fileName: 'Проект РД без приложения.docx',
        fileType: 'defaul',
        folderGuid: this.order.folderID,
        fileAttrs: [
          { attrName: 'docEntityID', attrValues: [this.order.documentID] },
          { attrName: 'docSourceReference', attrValues: ['UI'] },
        ]
      }
    };
    return from(this.reporterResourceService.nsi2Filenet(request)).pipe(
      map(result => {
        return OrderDraftFile.create(result.versionSeriesGuid, result.fileName, result.fileSize.toString(), false, new Date(), result.fileType, result.mimeType);
      })
    );
  }

  deleteAttach = () => {
    this.attach = [];
    from(this.fileResourceService.deleteFile(this.order.attach.draftFilesID)).subscribe(
      () => {
        this.order.attach = null;
      }
    )
  }

  changeAttach = () => {
    const attach = this.attach[0];
    if (attach) {
      this.order.attach = {
        draftFilesName: attach.nameFile,
        draftFilesID: attach.idFile,
        draftFilesSize: attach.sizeFile,
        draftFilesDate: attach.dateFile,
        draftFilesSigned: attach.signed,
        draftFilesMimeType: attach.mimeType,
        draftFilesType: attach.typeFile
      };
    } else {
      this.order.attach = null;
    }
  }

  deleteDraftFilesWithoutAtt = () => {
    this.draftFilesWithoutAtt = [];
    from(this.fileResourceService.deleteFile(this.order.draftFilesWithoutAtt.draftFilesID)).subscribe(
      () => {
        this.order.draftFilesWithoutAtt = null;
      },
      () => {
        this.order.draftFilesWithoutAtt = null;
      },
    )
  }

  changeDraftFilesWithoutAtt = () => {
    const draftFilesWithoutAtt = this.draftFilesWithoutAtt[0];
    if (draftFilesWithoutAtt) {
      this.order.draftFilesWithoutAtt = {
        draftFilesName: draftFilesWithoutAtt.nameFile,
        draftFilesID: draftFilesWithoutAtt.idFile,
        draftFilesSize: draftFilesWithoutAtt.sizeFile,
        draftFilesDate: draftFilesWithoutAtt.dateFile,
        draftFilesSigned: draftFilesWithoutAtt.signed,
        draftFilesMimeType: draftFilesWithoutAtt.mimeType,
        draftFilesType: draftFilesWithoutAtt.typeFile
      };
      this.order.draftFilesWithoutAttWord = _.cloneDeep(this.order.draftFilesWithoutAtt);
      this.order.draftFilesWithoutAttWord.draftFilesDate = this.order.draftFilesWithoutAtt.draftFilesDate;
    } else {
      this.order.draftFilesWithoutAtt = null;
    }
  }

  deleteDraftFiles = () => {
    from(this.fileResourceService.deleteFile(this.order.draftFiles.draftFilesID)).subscribe(
      () => { this.order.draftFiles = undefined },
      () => { this.order.draftFiles = undefined },
    )
  }

  combineDraftFile() {
    this.combiningDraftFile = true;
    this.createOrderDraftFile().subscribe((fileType) => {
      this.order.draftFiles = fileType;
      this.combiningDraftFile = false;
    }, () => {
      this.combiningDraftFile = false;
    });
  }

  headChanged() {
    setTimeout(() => {
      if (this.approvalListEdit) {
        this.approvalListEdit.executivesChanged();
        this.approvalListEdit.calcNotUsed();
        this.approvalListEdit.sortApprovers();
      }
    }, 0);
  }


}
