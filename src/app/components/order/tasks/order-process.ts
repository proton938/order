import { CdpReporterResourceService } from "@reinform-cdp/reporter-resource";
import { FileResourceService, IFileInFolderInfo } from "@reinform-cdp/file-resource";
import { Observable, forkJoin, from, of, throwError } from "rxjs";
import { catchError, map, mergeMap, tap } from "rxjs/internal/operators";
import { SessionStorage } from "@reinform-cdp/security";
import { NsiResourceService } from "@reinform-cdp/nsi-resource";
import * as angular from "angular";
import { ToastrService } from "ngx-toastr";
import { ITask } from "@reinform-cdp/bpm-components";
import {
  Ds, Order, OrderApprovalHistory, OrderApprovalHistoryList, OrderApprovalListItem,
  OrderApprovalListItemSignature, OrderApprover, OrderDraftFile
} from "../../../models/order/Order";
import { OrderService } from "../../../services/order.service";
import { formatDate } from "@angular/common";
import { FileType } from "../../../models/order/FileType";
import { ApprovalType } from "../../../models/order/ApprovalType";
import { OrderStatus } from "../../../models/order/OrderStatus";
import * as _ from "lodash";
import { formatAs } from "../../../services/date-util.service";
import { OrderType } from "../../../models/order/OrderType";
import { ExFileType } from "@reinform-cdp/widgets";

export class ProcessComponent {

  public order: Order;
  public origOrder: Order;

  constructor(public toastr: ToastrService, public orderService: OrderService, public fileResourceService: FileResourceService,
    public nsiResourceService: NsiResourceService, public session: SessionStorage,
    public reporterResourceService: CdpReporterResourceService) {

  }

  getEntityIdVar(task: ITask): Observable<string> {
    let _EntityIdVar = task.variables.find(v => {
      return v.name === 'EntityIdVar';
    });
    if (_EntityIdVar) {
      return of(<string>_EntityIdVar.value);
    } else {
      this.toastr.error('Отсутствует переменная задачи\'EntityIdVar\'');
      return throwError('Отсутствует переменная задачи\'EntityIdVar\'');
    }
  }

  updateOrder(updateOrig: boolean = false): Observable<any> {
    return this.getOrigOrder(updateOrig).pipe(
        mergeMap(order => {
            return this.orderService.update(this.order.documentID, this.orderService.diff(order, this.order));
        }),
        tap(order => {
            this.order = order;
            this.origOrder = angular.copy(order);
        })
    );
  }

  approve(order: Order, agreed: OrderApprovalListItem, taskId, approve: boolean, approvalParams: { approvalNote: string, agreedDs?: OrderApprovalListItemSignature }, closeCycle: boolean): Observable<any> {
    if (!order.approval || !order.approval.approvalCycle || !order.approval.approvalCycle.agreed || order.approval.approvalCycle.agreed.length === 0) {
      return throwError("Лист согласования не заполнен.");
    }
    if (!agreed) {
      return throwError("Некорректный индекс согласующего.");
    }
    if (!approve && !approvalParams.approvalNote) {
      return throwError("Для отрицательного решения необходимо заполнить комментарий.");
    }

    /*if (approve && !approvalParams.agreedDs && (agreed.approvalTypeCode === ApprovalType.agreed || agreed.approvalTypeCode === ApprovalType.approval)) {
        return throwError("Для положительного решения необходимо заполнить информацию об электронной подписи.");
    }*/

    if (approve && agreed.approvalTypeCode === ApprovalType.approval && order.signingElectronically &&
      !order.documentNumber && order.ds
      && order.files && this.order.files.length && this.order.files[0].fileSigned) {
      return throwError("Утверждаемый документ должен быть зарегистрирован и подписан.");
    }

    return from(this.nsiResourceService.get('mggt_order_ApprovalType')).pipe(
      mergeMap((approvalTypes: ApprovalType[]) => {
        let approvalType = approvalTypes.find(at => at.approvalTypeCode === agreed.approvalTypeCode);
        agreed.approvalResult = approve ? approvalType.buttonYes : approvalType.buttonNo;

        agreed.approvalNote = approvalParams.approvalNote;
        agreed.agreedDs = approvalParams.agreedDs || null;
        agreed.approvalFactDate = new Date();

        return this.fillUserInfo(agreed);
      }),
      mergeMap(() => {
        if (closeCycle) {
          return this.closeCycle(order);
        } else {
          return of('');
        }
      })
    );
  }

  fillUserInfo(agreed: OrderApprovalListItem): Observable<any> {
    let plannedAgreedBy = agreed.agreedBy.accountName;
    let login = this.session.login();
    if (login !== plannedAgreedBy) {
      return this.getOrderApprover(login).pipe(
        tap(approver => agreed.factAgreedBy = approver),
        mergeMap(() => this.fillAgreedBy(agreed))
      )
    } else {
      return this.fillAgreedBy(agreed);
    }
  }

  fillAgreedBy(agreed: OrderApprovalListItem): Observable<any> {
    if (!agreed.agreedBy.fioShort) {
      return this.getOrderApprover(agreed.agreedBy.accountName).pipe(
        tap(approver => {
          agreed.agreedBy = approver;
        })
      )
    } else {
      return of('');
    }
  }

  getOrderApprover(login: string) {
    return from(this.nsiResourceService.ldapUser(login)).pipe(
      map(userBean => {
        return OrderApprover.fromUserBean(userBean);
      })
    );

  }

  copyDraftFileWithoutAtt(targetFileName: string): Observable<FileType> {
    return from(this.fileResourceService.copyFile(this.order.draftFilesWithoutAtt.draftFilesID, targetFileName)).pipe(
      map((fileType) => {
        return FileType.create(fileType.versionSeriesGuid, fileType.fileName, fileType.fileSize.toString(),
          false, new Date(), fileType.fileType, fileType.mimeType);
      })
    );
  }

  copyDraftFile(targetFileName: string): Observable<FileType> {
    return from(this.fileResourceService.copyFile(this.order.draftFiles.draftFilesID, targetFileName)).pipe(
      map((fileType) => {
        return FileType.create(fileType.versionSeriesGuid, fileType.fileName, fileType.fileSize.toString(),
          false, new Date(), fileType.fileType, fileType.mimeType);
      })
    );
  }

  closeCycle(order: Order): Observable<any> {
    order.approvalHistory = order.approvalHistory || new OrderApprovalHistory();
    let approvalCycle = <OrderApprovalHistoryList>angular.copy(order.approval.approvalCycle);

    let draftFile = order.draftFiles;
    let fileName = `${this.getFileName(draftFile.draftFilesName)}-согласование-${formatDate(new Date(), 'yyyy-MM-dd HH:mm:ss', 'en')}.${this.getFileExtension(draftFile.draftFilesName)}`;

    return this.copyDraftFile(fileName).pipe(
      mergeMap((fileType) => {
        approvalCycle.approvalCycleFile = approvalCycle.approvalCycleFile || [];
        approvalCycle.approvalCycleFile.push(fileType);

        order.approvalHistory.approvalCycle = order.approvalHistory.approvalCycle || [];
        order.approvalHistory.approvalCycle.push(approvalCycle);
        order.approval.approvalCycle = null;

        if (order.draftFiles.draftFilesSigned) {
          // TODO: signature, delete sign
          // this.fileResourceService.delete
          order.draftFiles.draftFilesSigned = false;
        }
        return of('');
      })
    )
  }

  getFileName(fullFileName: string): string {
    let dotIndex = fullFileName.lastIndexOf(".");
    if (dotIndex < 0) {
      return fullFileName;
    } else {
      return fullFileName.substring(0, dotIndex);
    }
  }

  getFileExtension(fullFileName: string): string {
    let dotIndex = fullFileName.lastIndexOf(".");
    if (dotIndex < 0) {
      return "";
    } else {
      return fullFileName.substring(dotIndex + 1, fullFileName.length);
    }
  }

  generateDraftFileRegInfo(targetFileName: string): Observable<FileType> {
    const document: any = _.clone(this.order);
    document.documentDate = formatDate(Date.now(), 'dd.MM.yyyy', 'en');

    let request: any = {
      options: {
        jsonSourceDescr: "",
        onProcess: "Зарегистрировать проект РД",
        placeholderCode: "",
        strictCheckMode: true,
        xwpfResultDocumentDescr: targetFileName + '.docx',
        xwpfTemplateDocumentDescr: ""
      },
      jsonTxt: JSON.stringify({ document: document }),
      rootJsonPath: "$",
      nsiTemplate: {
        templateCode: 'AdmOrderPWithoutSign'
      },
      filenetDestination: {
        fileName: targetFileName + '.docx',
        fileType: 'MkaDocOther',
        folderGuid: document.folderID,
        fileAttrs: [
          { attrName: 'docEntityID', attrValues: [document.documentID] },
          { attrName: 'docSourceReference', attrValues: ['UI'] },
        ]
      }
    };

    return from(this.reporterResourceService.nsi2Filenet(request)).pipe(
      mergeMap(result => {
        return this.convertFileTypeToPdf(result.versionSeriesGuid, result.fileName);
      }),
      map(result => {
        return FileType.create(result.versionSeriesGuid, result.fileName, result.fileSize.toString(), false, new Date(), result.fileType, result.mimeType);
      })
    );
  }

  updateDocumentNumberBarcodeAndDs(ds?: Ds): Observable<any> {
    return this.generateDocumentNumber().pipe(
      mergeMap(() => {
        return this.generateBarcode();
      }),
      mergeMap(() => {
        this.order.ds = ds;
        return this.updateOrder();
      })
    )
  }

  cleanupOrderFiles(): Observable<any> {
    let fileIds = [];
    if (this.order.signingElectronically && this.order.files) {
      fileIds = this.order.files.map(file => file.fileID);
    } else if (!this.order.signingElectronically && this.order.draftFilesRegInfo) {
      fileIds = [this.order.draftFilesRegInfo.fileID];
    }
    return this.deleteFiles(fileIds);
  }

  setOrderFile(file: FileType) {
    let observable: Observable<any>;
    if (this.order.signingElectronically) {
      observable = this.addOrderFile(file);
    } else {
      this.order.draftFilesRegInfo = file;
      observable = of('');
    }
    return observable
  }

  registerOrder(ds?: Ds): Observable<any> {
    if (!this.order.draftFiles || !this.order.draftFiles.draftFilesID) {
      this.toastr.error("Файл проекта приказа не найден.");
      return throwError("Файл проекта приказа не найден.");
    }

    //Обновляем документ
    return this.checkLinkedDocumentsRegistration().pipe(
      catchError(error => {
        this.toastr.error(error);
        return throwError(error);
      }),
      mergeMap(() => {
        return this.updateDocumentNumberBarcodeAndDs(ds);
      }),
      //TODO: generate file by template
      mergeMap(() => {
        return this.cleanupOrderFiles();
      }),
      mergeMap(_ => {
        let fileName = this.getOrderFileName(this.order, true);
        return this.generateDraftFileRegInfo(fileName);
      }),
      mergeMap(file => {
        return this.setOrderFile(file)
      })
    )
  }

  registerOrderNew(orderType: OrderType, ds?: Ds): Observable<FileType> {
    if (!this.order.draftFiles || !this.order.draftFiles.draftFilesID) {
      this.toastr.error("Файл проекта приказа не найден.");
      return throwError("Файл проекта приказа не найден.");
    }

    let draftFilesWithoutAttFileName;
    let draftFilesWithoutAttPdf;
    //Обновляем документ
    return this.checkLinkedDocumentsRegistration().pipe(
      catchError(error => {
        this.toastr.error(error);
        return throwError(error);
      }),
      mergeMap(() => {
        if (!this.order.documentDate) {
          this.order.documentDate = new Date();
        }

        return this.updateDocumentNumberBarcodeAndDs(ds);
      }),
      mergeMap(() => {
        const file = ExFileType.create(
          this.order.draftFilesWithoutAttWord.draftFilesID,
          this.order.draftFilesWithoutAttWord.draftFilesName,
          +this.order.draftFilesWithoutAttWord.draftFilesSize,
          false,
          new Date(this.order.draftFilesWithoutAttWord.draftFilesDate).getTime(),
          this.order.draftFilesWithoutAttWord.draftFilesType,
          this.order.draftFilesWithoutAttWord.draftFilesMimeType
        );

        draftFilesWithoutAttFileName = `${orderType.name.split(' ')[0]} № ${this.order.documentNumber} от ${formatDate(this.order.documentDate, 'dd.MM.yyyy', 'en')} рег. данные.docx`;
        return this.applyOrderStampNew(file, draftFilesWithoutAttFileName, orderType, { ds: ds });
      }),
      mergeMap((id) => {
        return this.convertFileTypeToPdf(id, draftFilesWithoutAttFileName);
      }),
      mergeMap((draftFilePdf: IFileInFolderInfo) => {
        draftFilesWithoutAttPdf = FileType.create(
          draftFilePdf.versionSeriesGuid, draftFilePdf.fileName, draftFilePdf.fileSize.toString(), false,
          new Date(draftFilePdf.dateCreated), draftFilePdf.fileType, draftFilePdf.mimeType
        );

        if (!this.order.attach||!this.order.attach.draftFilesID) {
          const fileName = `${orderType.name.split(' ')[0]} № ${this.order.documentNumber} от ${formatDate(this.order.documentDate, 'dd.MM.yyyy', 'en')}.pdf`;
          return from(this.fileResourceService.getFolderContent(this.order.folderID,false)).pipe(
            mergeMap(fileList=>{
              let drFile = fileList.find(item=>item.fileName==fileName);
              if(!drFile)return of(null);
              return this.fileResourceService.deleteFile(drFile.versionSeriesGuid);
            }),
            mergeMap(()=>{
              return from(this.fileResourceService.copyFile(draftFilesWithoutAttPdf.fileID, fileName)).pipe(
                mergeMap((fileType) => {
                  return of(
                    FileType.create(fileType.versionSeriesGuid, fileType.fileName, fileType.fileSize.toString(),
                      false, new Date(), fileType.fileType, fileType.mimeType)
                  );
                })
              );
            })
          );         
        }
        return this.convertToPdfIfNecessary(this.order.attach)
          .pipe(
            mergeMap((attachPdf: OrderDraftFile) => {
              let request: any = {
                filenetDestination: {
                  fileAttrs: [
                    { attrName: 'docEntityID', attrValues: [this.order.documentID] },
                    { attrName: 'docSourceReference', attrValues: ['UI'] }
                  ],
                  fileName: `${orderType.name.split(' ')[0]} № ${this.order.documentNumber} от ${formatDate(this.order.documentDate, 'dd.MM.yyyy', 'en')}.pdf`,
                  fileType: 'MkaDocOther',
                  folderGuid: this.order.folderID,
                },
                versionSeriesGuids: [draftFilesWithoutAttPdf.fileID, attachPdf.draftFilesID]
              };

              return this.reporterResourceService.pdfConcatenateFiles(request)
            }),
            mergeMap((result: any) => {
              return of(FileType.create(result.versionSeriesGuid, result.fileName, result.fileSize.toString(), false,
                new Date(result.dateCreated), result.fileType, result.mimeType));
            })
          );
      }),
      mergeMap((orderFile: FileType) => {
        return of(orderFile);
      })
    )
  }

  convertToPdfIfNecessary(file: OrderDraftFile): Observable<OrderDraftFile> {
    if (file.draftFilesMimeType === 'application/pdf') {
      return of(file);
    } else {
      let request: any = {
        filenetDestination: {
          fileAttrs: [
            { attrName: 'docEntityID', attrValues: [this.order.documentID] },
            { attrName: 'docSourceReference', attrValues: ['UI'] }
          ],
          fileName: this.getFileName(file.draftFilesName) + '.pdf',
          fileType: 'MkaDocOther',
          folderGuid: this.order.folderID
        },
        wordVersionSeriesGuid: file.draftFilesID
      };
      return from(this.reporterResourceService.word2pdf(request)).pipe(
        map((response: IFileInFolderInfo) => {
          return OrderDraftFile.fromFileInFolderInfo(response);
        })
      )
    }
  }

  applyOrderStampNew(file: ExFileType, fileName: string, orderType: OrderType, signInfo: { ds: Ds }): Observable<string> {
    return from(this.reporterResourceService.filenet2Filenet(<any>{
      jsonTxt: JSON.stringify(this.order),
      filenetTemplate: {
        templateVersionSeriesGuid: file.idFile
      },
      filenetDestination: {
        reportFileName: file.nameFile,
        reportFileType: file.typeFile,
        reportFolderGuid: this.order.folderID,
        fileName: fileName,
        fileType: file.typeFile,
        folderGuid: this.order.folderID
      },
      rootJsonPath: "$",
      options: {
        onProcess: file.nameFile.replace('.docx', ''),
        placeholderCode: 'sn',
        strictCheckMode: true,
        xwpfResultDocumentDescr: fileName,
        xwpfTemplateDocumentDescr: ""
      }
    })).pipe(
      map(response => response.versionSeriesGuid)
    );
  }

  getOrderFileName(order, removeExt?: boolean) {
    let orderType = order.documentTypeValue;
    orderType = orderType.indexOf(" ") > -1 ? orderType.substring(0, orderType.indexOf(" ")) : orderType;
    return `${orderType} № ${order.documentNumber} от ${formatDate(order.documentDate, 'dd.MM.yyyy', 'en')}${!removeExt ? '.pdf' : ''}`
  }

  checkLinkedDocumentsRegistration(): Observable<any> {
    if (!this.order.linkedDocument || !this.order.linkedDocument.length) {
      return of('');
    }
    return forkJoin(
      this.order.linkedDocument.map(doc => this.orderService.get(doc.linkedDocumentID))
    ).pipe(
      mergeMap((docs: Order[]) => {
        let doc = docs.find(doc => !doc.documentNumber || !doc.documentDate);
        if (doc) {
          return throwError("Связанный документ " + doc.documentID + " не зарегистрирован.")
        } else {
          return of('');
        }
      })
    )
  }

  addOrderFile(file: FileType): Observable<any> {
    this.order.files = [file];
    if (this.order.documentStatusCode === OrderStatus.REGISTER) {
      this.order.documentStatusCode = OrderStatus.REGISTERSCAN;
      return this.processDocumentStatus();
    } else {
      return of('');
    }
  }

  generateDocumentNumber(): Observable<any> {
    if (this.order.documentNumber) {
      return of('');
    } else {
      return this.orderService.nextNumber(this.order.documentTypeCode, this.order.documentYear).pipe(
        tap(number => this.order.documentNumber = number)
      )
    }
  }

  generateBarcode(): Observable<any> {
    if (this.order.barcode) {
      return of('');
    } else {
      return this.orderService.nextBarcode().pipe(
        tap(barcode => this.order.barcode = barcode)
      )
    }
  }

  processDocumentStatus(): Observable<any> {
    let statusCode = this.order.documentStatusCode;
    if (!statusCode) {
      if (this.order.documentNumber) {
        statusCode = OrderStatus.REGISTER;
      } else {
        statusCode = OrderStatus.PROJECT;
      }
    }
    return from(this.nsiResourceService.getBy('mggt_order_OrderStatuses', {
      nickAttr: 'code',
      values: [statusCode]
    })).pipe(
      tap((statuses: OrderStatus[]) => {
        if (statuses.length) {
          let status = statuses[0];
          this.order.documentStatusCode = status.code;
          this.order.documentStatusValue = status.name;
          this.order.documentStatusColor = status.color;
        }
      })
    )
  }

  deleteFiles(ids: string[]): Observable<any> {
    if (!ids || !ids.length) {
      return of('');
    }
    return forkJoin(ids.map(id => this.fileResourceService.deleteFile(id)));
  }

  createOrderDraftFile(): Observable<OrderDraftFile> {
    return this.convertDraftFilesWithoutAtt().pipe(
      mergeMap(() => {
        return this.order.draftFiles && this.order.draftFiles.draftFilesID
            ? this.deleteFiles([this.order.draftFiles.draftFilesID])
            : of('');
      }),      
      mergeMap(() => {
        if (!this.order.attach || !this.order.attach.draftFilesID) {
          return from(this.fileResourceService.getFolderContent(this.order.folderID,false)).pipe(
            mergeMap(fileList=>{
              let drFile = fileList.find(item=>item.fileName=='Проект РД.pdf');
              if(!drFile)return of(null);
              return this.fileResourceService.deleteFile(drFile.versionSeriesGuid);
            }),
            mergeMap(()=>{
              return this.copyDraftFileWithoutAtt('Проект РД.pdf').pipe(
                mergeMap((file: FileType) => {
                  return of(OrderDraftFile.fromFileType(file));
                })
              );
            })
          );
        } else {
          return this.convertAttach().pipe(
            mergeMap(() => {
              let request: any = {
                filenetDestination: {
                  fileAttrs: [
                    { attrName: 'docEntityID', attrValues: [this.order.documentID] },
                    { attrName: 'docSourceReference', attrValues: ['UI'] }
                  ],
                  fileName: 'Проект РД.pdf',
                  fileType: 'MkaDocOther',
                  folderGuid: this.order.folderID,
                },
                versionSeriesGuids: [this.order.draftFilesWithoutAtt.draftFilesID, this.order.attach.draftFilesID]
              };
              return this.reporterResourceService.pdfConcatenateFiles(request)
            }),
            map((response: IFileInFolderInfo) => {
              return OrderDraftFile.fromFileInFolderInfo(response);
            })
          );
        }
      })
    );
  }

  private convertDraftFilesWithoutAtt(): Observable<any> {
    if (this.needToConvertToPdf(this.order.draftFilesWithoutAtt)) {
      this.order.draftFilesWithoutAttWord = this.order.draftFilesWithoutAtt;
      return this.convertToPdf(this.order.draftFilesWithoutAtt).pipe(
        tap(draftFilesWithoutAtt => {
          this.order.draftFilesWithoutAtt = draftFilesWithoutAtt;
        })
      )
    } else {
      return of('');
    }
  }

  private convertAttach(): Observable<any> {
    if (this.needToConvertToPdf(this.order.attach)) {
      this.order.attachWord = this.order.attach;
      return this.convertToPdf(this.order.attach).pipe(
        tap(attach => {
          this.order.attach = attach;
        })
      )
    } else {
      return of('');
    }
  }

  private needToConvertToPdf(file: OrderDraftFile): boolean {
    return file.draftFilesMimeType !== 'application/pdf';
  }

  private convertToPdf(file: OrderDraftFile): Observable<OrderDraftFile> {
    let request: any = {
      filenetDestination: {
        fileAttrs: [
          { attrName: 'docEntityID', attrValues: [this.order.documentID] },
          { attrName: 'docSourceReference', attrValues: ['UI'] }
        ],
        fileName: this.getFileName(file.draftFilesName) + '.pdf',
        fileType: 'MkaDocOther',
        folderGuid: this.order.folderID
      },
      wordVersionSeriesGuid: file.draftFilesID
    };
    return from(this.reporterResourceService.word2pdf(request)).pipe(
      map((response: IFileInFolderInfo) => {
        return OrderDraftFile.fromFileInFolderInfo(response);
      })
    )
  }

  convertFileTypeToPdf(fileId: string, fileName: string): Observable<IFileInFolderInfo> {
    let request: any = {
      filenetDestination: {
        fileAttrs: [
          { attrName: 'docEntityID', attrValues: [this.order.documentID] },
          { attrName: 'docSourceReference', attrValues: ['UI'] }
        ],
        fileName: this.getFileName(fileName) + '.pdf',
        fileType: 'MkaDocOther',
        folderGuid: this.order.folderID
      },
      wordVersionSeriesGuid: fileId
    };
    return from(this.reporterResourceService.word2pdf(request));
  }

  generateDraftFilesWithoutAtt(templateCode: string): Observable<OrderDraftFile> {
    const document: any = _.clone(this.order);
    document.documentDate = formatAs(Date.now(), 'dd.MM.yyyy');
    document.documentNumber = this.order.approvedDocumentNumber;
    let request: any = {
      options: {
        jsonSourceDescr: "",
        onProcess: "Сформировать проект РД",
        placeholderCode: "",
        strictCheckMode: true,
        xwpfResultDocumentDescr: "Проект РД  без приложения.docx",
        xwpfTemplateDocumentDescr: ""
      },
      jsonTxt: JSON.stringify({ document: this.order }),
      rootJsonPath: "$",
      nsiTemplate: {
        templateCode: templateCode,
      },
      filenetDestination: {
        fileName: 'Проект РД  без приложения.docx',
        fileType: 'defaul',
        folderGuid: this.order.folderID,
        fileAttrs: [
          { attrName: 'docEntityID', attrValues: [this.order.documentID] },
          { attrName: 'docSourceReference', attrValues: ['UI'] },
        ]
      }
    };
    return from(this.reporterResourceService.nsi2Filenet(request)).pipe(
      map(result => {
        return OrderDraftFile.create(result.versionSeriesGuid, result.fileName, result.fileSize.toString(), false, new Date(), result.fileType, result.mimeType);
      })
    );
  }

  deleteDraftFilesWithoutAtt() {
    let deleteDraftFilesWithoutAttObserv = !!this.order.draftFilesWithoutAtt&&!!this.order.draftFilesWithoutAtt.draftFilesID?
      from(this.fileResourceService.deleteFile(this.order.draftFilesWithoutAtt.draftFilesID)):of(null);
    // return from(this.fileResourceService.deleteFile(this.order.draftFilesWithoutAtt.draftFilesID)).pipe(
    return deleteDraftFilesWithoutAttObserv.pipe(
      mergeMap(() => {
        let result: Observable<any>;
        if (this.order.draftFilesWithoutAttWord) {
          result = from(this.fileResourceService.deleteFile(this.order.draftFilesWithoutAttWord.draftFilesID));
        } else {
          result = of('');
        }
        return result;
      }),
      mergeMap(() => {
        return this.updateOrderPart(ord => {
          ord.draftFilesWithoutAtt = null;
          ord.draftFilesWithoutAttWord = null;
        })
      })
    );
  };

  deleteAttach() {
    let deleteAttachObserv = !!this.order.attach&&!!this.order.attach.draftFilesID?
      from(this.fileResourceService.deleteFile(this.order.attach.draftFilesID)):of(null);
    return deleteAttachObserv.pipe(
      mergeMap(() => {
        let result: Observable<any>;
        if (this.order.attachWord) {
          result = from(this.fileResourceService.deleteFile(this.order.attachWord.draftFilesID));
        } else {
          result = of({});
        }
        return result;
      }),
      mergeMap(() => {
        return this.updateOrderPart(ord => {
          ord.attach = null;
          ord.attachWord = null;
        });
      })
    );
  };

  deleteDraftFiles() {
    return from(this.fileResourceService.deleteFile(this.order.draftFiles.draftFilesID)).pipe(
      mergeMap(() => {
        if(!!this.order.draftFilesWithoutAtt&&!!this.order.draftFilesWithoutAttWord)
          return from(this.fileResourceService.deleteFile(this.order.draftFilesWithoutAtt.draftFilesID));
        return of(null);
      }),
      mergeMap(() => {
        return this.updateOrderPart(ord => {
          ord.draftFiles = null;
          if(!!ord.draftFilesWithoutAttWord){
            ord.draftFilesWithoutAtt=ord.draftFilesWithoutAttWord;
            ord.draftFilesWithoutAttWord=null;
          }          
        })
      })
    );
  };

  getTaskOwner(task): string {
    let r: string;
    // Определяем реального владельца задачи
    if (!!task && !!task.owner) {
      r = task.owner; // Владелец делегированной задачи
    } else if (!!task && !!task.assignee && task.assignee !== this.session.login()) {
      r = task.assignee; // Задача делегирована, но только взята в работу (не перезагружена)
    } else r = this.session.login();
    return r;
  }

  getOrigOrder(update: boolean = false): Observable<Order> {
      if (update) {
          return this.orderService.get(this.order.documentID).pipe(tap(order => this.origOrder = order));
      }
      return of(this.origOrder);
  }

  updateOrderPart(callback?: (model: Order) => any): Observable<any> {
    return this.getOrigOrder(true).pipe(
      mergeMap(order => {
        const orderNext = angular.copy(order);
        if (callback) {
          callback(orderNext);
        }
        return this.orderService.update(this.order.documentID, this.orderService.diff(order, orderNext));
      }),
      tap(order => {
        callback(this.order);
        callback(this.origOrder);
        // this.order = order;
        // this.origOrder = angular.copy(order);
      })
    )
  }
}
