import * as _ from "lodash";
import { from } from "rxjs/index";
import { AlertService } from "@reinform-cdp/widgets";
import { BsModalRef } from "ngx-bootstrap";
import { Component, Input, OnInit } from '@angular/core';
import { AddedOrderApprovalListItem } from "../approval-list-add/approval-list-add.component";

@Component({
  selector: 'order-modal-content',
  templateUrl: './add-approvers.modal.html',
  styleUrls: ['./add-approvers.modal.scss']
})
export class AddApproversModalComponent implements OnInit {

  agreed: AddedOrderApprovalListItem[];
  @Input() documentTypeCode: string;
  @Input() except: string[];
  @Input() dueDate: Date;
  @Input() approvalNum: string;

  submit: boolean = false;

  constructor(private bsModalRef: BsModalRef, private alertService: AlertService) {
  }

  ngOnInit() {
    this.agreed = [];
  }

  isValid() {
    return !_.some(this.agreed, _ => {
      return !_.agreedBy;
    })
  }

  add() {
    this.submit = true;
    this.bsModalRef.hide();
  }

  cancel() {
    if (this.agreed.length > 0) {
      from(this.alertService.confirm({
        message: 'Добавленные данные не сохранятся. Продолжить без сохранения?',
        type: 'default',
        okButtonText: 'Да',
        windowClass: 'zindex'
      })).subscribe(_ => {
        this.bsModalRef.hide();
      })
    } else {
      this.bsModalRef.hide();
    }
  }

}
