import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {OrderApprover} from "../../../../models/order/Order";

export class AddedOrderApprovalListItem {
    approvalNum: string;
    approvalTime: string;
    agreedBy: OrderApprover;
    approvalType: string;
    approvalTypeCode: string;
    note: string;
}

@Component({
    selector: 'mggt-approval-list-add',
    templateUrl: './approval-list-add.component.html'
})
export class ApprovalListAddComponent implements OnInit {

    @Input() agreed: AddedOrderApprovalListItem[];
    @Output() agreedChanged: EventEmitter<any> = new EventEmitter<any>();
    @Input() documentTypeCode: string;
    @Input() except: string[];
    @Input() systemCode: string;
    @Input() dueDate: Date;
    @Input() approvalNum: string;

    // constructor(@Inject('$localStorage') public $localStorage: any) {
    constructor() {
    }

    ngOnInit() {
    }

}
