import * as _ from "lodash";
import { mergeMap } from "rxjs/internal/operators";
import { NsiResourceService, UserBean } from "@reinform-cdp/nsi-resource";
import { forkJoin } from "rxjs/index";
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { OrderApprover } from "../../../../../models/order/Order";
import { ApprovalType } from "../../../../../models/order/ApprovalType";
import { OrderTypeService } from "../../../../../services/order-type.service";
import { UserService } from "../../../../../services/user.service";
import { OrderType } from "../../../../../models/order/OrderType";
import { OrderNsiService } from "../../../../../services/order-nsi.service";
import { AddedOrderApprovalListItem } from "../approval-list-add.component";

@Component({
  selector: 'mggt-desktop-approval-list-add',
  templateUrl: './desktop.component.html',
  styleUrls: ['./desctop.component.scss']
})
export class DesktopApprovalListAddComponent implements OnInit {

  @Input() agreed: AddedOrderApprovalListItem[];
  @Output() agreedChanged: EventEmitter<any> = new EventEmitter<any>();
  @Input() documentTypeCode: string;
  @Input() except: string[];
  @Input() systemCode: string;
  @Input() dueDate: Date;
  @Input() approvalNum: string;

  allApprovers: OrderApprover[] = [];
  approvers: OrderApprover[] = [];
  approvalTypes: ApprovalType[] = [];
  signUsers: UserBean[];
  orderType:OrderType;

  assentApprovers: OrderApprover[];
  assentApproversNotUsed: OrderApprover[];
  agreedApprovers: OrderApprover[];
  agreedApproversNotUsed: OrderApprover[];

  constructor(private orderTypeService: OrderTypeService, private nsiResourceService: NsiResourceService,
    private userService: UserService, private orderNsiService: OrderNsiService) {
  }

  ngOnInit() {
    forkJoin([
      this.orderTypeService.geOrderTypes(),
      this.nsiResourceService.get('mggt_order_ApprovalType'),
    ]).pipe(
      mergeMap((result: [OrderType[], ApprovalType[]]) => {
        this.orderType = result[0].find(type => type.code === this.documentTypeCode);

        let approvalTypes = result[1];
        for (let i = 0; i < approvalTypes.length; i++) {
          if (!this.approvalTypes.find(type => type.approvalTypeCode === approvalTypes[i].approvalTypeCode)) {
            this.approvalTypes.push(approvalTypes[i]);
          }
        }

        _.remove(this.approvalTypes, at => {
          return at.approvalTypeCode === ApprovalType.approval;
        });
        return this.userService.getSign(this.orderType);
      })).pipe(mergeMap((sign) => {
        this.signUsers = sign;
        return this.userService.getApprovers(this.orderType);
      })
    ).subscribe((approvers: UserBean[]) => {
      this.allApprovers = _.chain(approvers).filter(a => {
        return _.indexOf(this.except, a.accountName) < 0;
      }).map(a => OrderApprover.fromUserBean(a)).value();
      const signAccounts = this.signUsers.map((u) => u.accountName);
      this.assentApprovers = this.allApprovers;
      this.agreedApprovers = this.assentApprovers.filter((e) => signAccounts.indexOf(e.accountName) !== -1);
      this.calcNotUsed();
    })
  }

  setNumeration() {
    _.each(this.agreed, (item, index) => {
      item.approvalNum = `${this.approvalNum}.${index + 1}`;
    })
  }

  swapApprovals(ind1: number, ind2: number) {
    let app = this.agreed[ind1];
    this.agreed[ind1] = this.agreed[ind2];
    this.agreed[ind2] = app;
    this.setNumeration();
    this.agreedChanged.emit();
  }

  approverChanged() {
    this.calcNotUsed();
  }
  calcNotUsed() {
    const signAccounts = this.signUsers.map((u) => u.accountName);
    const usedAccounts = this.agreed.filter((a) => (!!a.agreedBy)).map((a) => (a.agreedBy.accountName));

    this.assentApproversNotUsed = (this.assentApprovers || [])
        .filter((a) => (usedAccounts.indexOf(a.accountName) === -1));
    this.agreedApproversNotUsed = (this.assentApproversNotUsed || [])
        .filter((e) => signAccounts.indexOf(e.accountName) !== -1);
  }

  // getAprovers() {
  //   this.approvers = this.allApprovers.filter(a => {
  //     return !this.agreed.some((ag) => {
  //       if (!ag.agreedBy) {
  //         return false;
  //       }
  //       return ag.agreedBy.accountName === a.accountName;
  //     });
  //   });
  // }

  delApproval(ind: number) {
    this.agreed.splice(ind, 1);
    // this.getAprovers();
    this.approverChanged();
    this.setNumeration();
    this.agreedChanged.emit();
  }

  addApproval() {
    let agreed = new AddedOrderApprovalListItem();
    let approvalType = _.find(this.approvalTypes, (at: ApprovalType) => {
      if (!this.systemCode) {
        return at.approvalTypeCode === ApprovalType.assent;
      }
      return at.approvalTypeCode === ApprovalType.agreed;
    });
    agreed.approvalType = approvalType.approvalType;
    agreed.approvalTypeCode = approvalType.approvalTypeCode;
    this.orderNsiService.toLocalDateTime(this.dueDate).pipe(
      mergeMap(value => {
        return this.orderNsiService.calcDuration(value);
      }),
      mergeMap(value => {
        return this.orderNsiService.divideTimeBy(value, 2);//this.agreed.length + 1);
      })
    ).subscribe(approvalTime => {
      agreed.approvalTime = approvalTime;
      this.agreed.push(agreed);
      this.approverChanged();
      // this.getAprovers();
      this.setNumeration();
    });
    this.agreedChanged.emit();
  }

  approvalTypeChanged(ind: number) {
    let approvalType = _.find(this.approvalTypes, (at: ApprovalType) => {
      return at.approvalTypeCode === this.agreed[ind].approvalTypeCode
    });
    if (approvalType) {
      this.agreed[ind].approvalType = approvalType.approvalType;
    } else {
      this.agreed[ind].approvalType = null;
    }
    if(!!this.agreed[ind].approvalTypeCode){
      let validationList;
      validationList = this.assentApprovers;
      if(approvalType.approvalTypeCode=='agreed')
        validationList = this.agreedApprovers;

      if (this.agreed[ind].agreedBy) {
        const exists = validationList.find((a) => a.accountName === this.agreed[ind].agreedBy.accountName);

        if (!exists) {
          this.agreed[ind].agreedBy = null;
          this.calcNotUsed();
        }
      }
    }
    
    this.setNumeration();
    this.agreedChanged.emit();
  }

  getApprovalTypes(index) {
    let current = this.agreed[index];
    let hasApproval = _.some(this.agreed, (a) => {
      return a.approvalTypeCode === ApprovalType.approval;
    });
    return !hasApproval || current.approvalTypeCode === ApprovalType.approval ? this.approvalTypes : _.filter(this.approvalTypes, (o: ApprovalType) => {
      return o.approvalTypeCode !== ApprovalType.approval;
    });
  }

}
