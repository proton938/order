import { CdpReporterResourceService, PdfService } from "@reinform-cdp/reporter-resource";
import { formatDate } from "@angular/common";
import { NsiResourceService } from "@reinform-cdp/nsi-resource";
import * as _ from "lodash";
import { catchError, map, mergeMap, tap } from "rxjs/internal/operators";
import { FileResourceService, IFileInFolderInfo, IFileType } from "@reinform-cdp/file-resource";
import { from, of, throwError } from "rxjs/index";
import { BsModalService } from "ngx-bootstrap";
import * as angular from "angular";
import { Component, OnInit } from '@angular/core';
import { Ds, Order, OrderApprovalListItem, OrderDraftFile, OrderApprover } from "../../../../models/order/Order";
import { ExFileType, LoadingStatus } from "@reinform-cdp/widgets";
import { ICertificateInfoEx, SessionStorage, SignService } from "@reinform-cdp/security";
import { ActiveTaskService, ActivityResourceService, ITask, CdpBpmHistoryResourceService } from "@reinform-cdp/bpm-components";
import { OrderType } from "../../../../models/order/OrderType";
import { ProcessOldComponent } from "../order-process-old";
import { ToastrService } from "ngx-toastr";
import { OrderService } from "../../../../services/order.service";
import { Observable } from "rxjs/Rx";
import { OrderTypeService } from "../../../../services/order-type.service";
import { ApprovalType } from "../../../../models/order/ApprovalType";
import { RegisterDocumentService } from "../../../../services/register-document.service";
import { RedirectToTaskModalComponent, RedirectToTaskSettings } from "../redirect-to-task/redirect-to-task.modal";
import { FileType } from "../../../../models/order/FileType";
import { CancelDocumentService } from "../../../../services/cancel-document.service";
import * as moment_ from "moment";
import { DropzoneConfigInterface } from "ngx-dropzone-wrapper";
import { AddApproversModalComponent } from "../add-approvers/add-approvers.modal";
import { OrderActivitiService } from "../../../../services/order-activiti.service";
import { IWordReporterResult } from "@reinform-cdp/reporter-resource/dist/models/IWordReporterResult";
import {HelperService} from "../../../../services/helper.service";

const moment = moment_;

@Component({
    selector: 'mggt-order-approve',
    templateUrl: './order-approve.component.html',
    styleUrls: ['./order-approve.component.scss']
})
export class OrderApproveComponent extends ProcessOldComponent implements OnInit {

    loadingStatus: LoadingStatus;
    success = false;
    task: ITask;
    order: Order;
    origOrder: Order;
    orderType: OrderType;
    showDocumentText: boolean;

    loopCounter: number;
    lastApprovalListItem: boolean;
    approvalState: boolean;

    approvalNote: string;

    sendingBackToWork: boolean;
    agreeing: boolean;
    approving: boolean;
    addingApprovals: boolean = false;
    approvalTypes: ApprovalType[];
    approvalType: ApprovalType;

    openDraftSignDialog: () => void;
    openMobileDraftSignDialog: () => void;

    openSignDialog: () => void;
    openMobileSignDialog: () => void;
    orderFile: ExFileType = new ExFileType();
    isMobile: boolean = false;

    checkSign: string;

    dropzoneConfig: DropzoneConfigInterface;

    constructor(private activeTaskService: ActiveTaskService,
        public helper: HelperService,
        public orderTypeService: OrderTypeService,
        private activityResourceService: ActivityResourceService,
        private registerDocumentService: RegisterDocumentService,
        private modalService: BsModalService,
        private cancelDocumentService: CancelDocumentService,
        private orderActivitiService: OrderActivitiService,
        private signService: SignService,
        public pdfService: PdfService,
        toastr: ToastrService,
        orderService: OrderService,
        fileResourceService: FileResourceService,
        nsiResourceService: NsiResourceService,
        sessionStorage: SessionStorage,
        reporterResourceService: CdpReporterResourceService,
        public historyResourceService: CdpBpmHistoryResourceService) {
        super(toastr, orderService, fileResourceService, nsiResourceService, sessionStorage, reporterResourceService, orderTypeService, pdfService);
    }

    ngOnInit() {
        this.helper.beforeUnload.init();
        this.loadingStatus = LoadingStatus.LOADING;
        this.activeTaskService.getTask().pipe(
            mergeMap(task => {
                this.task = task;
                return this.getEntityIdVar(task);
            }),
            mergeMap(id => {
                return this.orderService.get(id);
            }),
            mergeMap(order => {
                this.order = order;
                this.origOrder = angular.copy(order);

                // const loopCounterVar = this.task.variables.find(v => {
                //     return v.name === 'loopCounter';
                // });

                // if (!loopCounterVar) {
                //     this.toastr.error('Не удалось определить номер согласующего в листе согласования.');
                //     this.loadingStatus = LoadingStatus.ERROR;
                //     return;
                // }
                // this.loopCounter = <number>loopCounterVar.value;
                let taskOwnerLogin: string;
                // Определяем реального владельца задачи
                if (!!this.task && !!this.task.owner) {
                    taskOwnerLogin = this.task.owner; // Владелец делегированной задачи
                } else if (!!this.task && !!this.task.assignee && this.task.assignee !== this.session.login()) {
                    taskOwnerLogin = this.task.assignee; // Задача делегирована, но только взята в работу (не перезагружена)
                } else taskOwnerLogin = this.session.login();
                this.loopCounter = this.order.approval.approvalCycle.agreed.findIndex((agree) => {
                    return agree.agreedBy.accountName === taskOwnerLogin && !agree.approvalResult;
                    //this.session.login();
                });
                if (this.loopCounter === undefined) {
                    this.toastr.error('Не удалось определить номер согласующего в листе согласования.');
                    this.loadingStatus = LoadingStatus.ERROR;

                    return throwError('Не удалось определить номер согласующего в листе согласования.');
                }
                this.approvalState = this.order.approval.approvalCycle.agreed[this.loopCounter].approvalTypeCode === ApprovalType.approval;
                this.lastApprovalListItem = this.loopCounter === this.order.approval.approvalCycle.agreed.length - 1;

                // if (!this.order.activeApprover) {
                //     this.order.activeApprover = this.session.fioShort();
                // }

                if (!this.order.documentDate)
                    this.order.documentDate = new Date();

                let login = this.session.login();

                if (!this.order.approval.approvalCycle.agreed[this.loopCounter].approvalPlanDate) {
                    this.order.approval.approvalCycle.agreed[this.loopCounter].approvalPlanDate = this.task.dueDate;
                    return this.updateOrder().pipe(
                        mergeMap(() => {
                            return this.loadDictionaries();
                        })
                    );
                } else {
                    return this.loadDictionaries();
                }
            }),
            mergeMap(() => {
                return this.signService.checkSign();
            }),
            mergeMap(res => {
                this.checkSign = res;
                return this.nsiResourceService.getDictFromCache('mggt_order_ApprovalType');
            })
        ).subscribe((response) => {
            this.approvalTypes = response.value;
            // .find(agreed => agreed.agreedBy.accountName === this.session.login());
            // берем запись по процессу а не ищем по логину
            const approval = this.order.approval.approvalCycle.agreed[this.loopCounter];
            this.approvalType = this.approvalTypes.find(type => type.approvalTypeCode === approval.approvalTypeCode);
            this.initDropzone();
            this.updateCurrentAgree();
            this.loadingStatus = LoadingStatus.SUCCESS;
            this.success = true;
        }, (error) => {
            console.error(error);
            this.loadingStatus = LoadingStatus.ERROR;
        });
    }

    ngOnDestroy() {
        this.helper.beforeUnload.destroy();
    }

    updateCurrentAgree() {
        let currentAgree = this.order.approval.approvalCycle.agreed[this.loopCounter];
        if (currentAgree.agreedBy.accountName == this.session.login()) {
            currentAgree.factAgreedBy=null;
            return;
        }
        // currentAgree.agreedBy =
        currentAgree.factAgreedBy = OrderApprover.fromUserBean({
            post: this.session.post(),
            displayName: this.session.fullName(),
            accountName: this.session.login(),
            fioShort: this.session.fioShort(),
            iofShort: this.session.iofShort(),
            telephoneNumber: this.session.telephoneNumber()
        });
    }

    loadDictionaries(): Observable<any> {
        return this.orderTypeService.geOrderTypes().pipe(
            tap(orderTypes => {
                this.orderType = orderTypes.find(val => val.code === this.order.documentTypeCode);
                // TODO: check OASI_ADMORDER_OGS
                this.showDocumentText = this.orderType.accessAction === 'OASI_ADMORDER_OGS';
            })
        )
    }

    initDropzone() {
        let component = this;
        this.dropzoneConfig = {
            autoProcessQueue: true,
            parallelUploads: 1,
            params: {
                fileType: 'MkaDocOther',
                docSourceReference: 'UI'
            },
            init: function () {
                this.on("processing", function (file) {
                    this.options.params.folderGuid = component.order.folderID;
                    this.options.params.docEntityID = component.order.documentID;
                });
                this.on("error", function (file, errorMessage) {
                    component.toastr.warning(errorMessage.message ? errorMessage.message : errorMessage, 'Ошибка');
                    this.removeFile(file);
                });
                this.on("success", function (file, guid) {
                    let agreed = component.order.approval.approvalCycle.agreed[component.loopCounter];
                    agreed.fileApproval = agreed.fileApproval || [];
                    const fileType: FileType = FileType.create(guid, file.name, file.size.toString(), false, new Date(), file.type, 'MkaDocOther');
                    agreed.fileApproval.push(fileType);
                    this.removeFile(file);
                });
            }
        }
    }

    sendBackToWork() {
        if (!this.approvalNote) {
            this.toastr.error('Не указан комментарий');
            return;
        }
        this.helper.beforeUnload.start();
        this.sendingBackToWork = true;
        this.approve(this.order, this.loopCounter, this.task.id, false, {
            approvalNote: this.approvalNote
        }).pipe(
            mergeMap(() => {
                return from(this.activityResourceService.finishTask(parseInt(this.task.id), [
                    { name: 'ApprovalApprovedVar', value: false }
                ]));
            })
        ).subscribe(() => {
            this.helper.beforeUnload.stop();
            this.sendingBackToWork = false;
            this.toastr.success('Документ отправлен на доработку.');
            window.location.href = '/main/#/app/tasks';
        }, () => {
            this.helper.beforeUnload.stop();
            this.sendingBackToWork = false;
        });
    }

    agreeAndSendForwardHandler() {
        this.helper.beforeUnload.start();
        this.agreeing = true;
        if (this.lastApprovalListItem) {
            this.checkProcess().pipe(
                mergeMap(() => {
                    return this.registerDocumentService.checkLinkedDocuments(this.order);
                })
            ).subscribe(() => {
                this.agreeAndSendForward();
            }, err => {
                this.helper.beforeUnload.stop();
                this.agreeing = false;
                if (err.documentID && err.documentTypeValue) {
                    this.redirectToTask({ documentID: err.documentID, documentTypeValue: err.documentTypeValue });
                }
            });
        }
        else {
            this.agreeAndSendForward();
        }
    }

    agreeAndSendForward() {
        this.agreeing = true;
        switch (this.order.approval.approvalCycle.agreed[this.loopCounter].approvalTypeCode) {
            case 'assent':
                this.checkProcess().pipe(
                    mergeMap(() => {
                        return this.approve(this.order, this.loopCounter, this.task.id, true, {
                            approvalNote: this.approvalNote
                        });
                    }),
                    mergeMap(() => {
                        if (this.lastApprovalListItem) {
                            // this.order.activeApprover = null;
                            return this.updateOrder().pipe(
                                mergeMap(() => {
                                    return this.registerOrder()
                                }),
                                mergeMap((file) => {
                                    this.order.draftFilesRegInfo = file;

                                    return this.updateOrder();
                                }),
                                mergeMap(() => {
                                    return this.orderService.send(this.order.documentID, false);
                                })
                            );
                        } else {
                            // this.order.activeApprover = this.getNextApprover().agreedBy.fioShort;
                            return this.updateOrder();
                        }
                    }),
                    mergeMap(() => {
                        return from(this.activityResourceService.finishTask(parseInt(this.task.id), [
                            { name: 'ApprovalApprovedVar', value: true }
                        ]));
                    })
                ).subscribe((result: any) => {
                    this.helper.beforeUnload.stop();
                    this.agreeing = false;
                    this.toastr.success('Электронный документ успешно согласован.');
                    window.location.href = '/main/#/app/tasks';
                }, () => {
                    this.agreeing = false;
                    this.helper.beforeUnload.stop();
                });
                break;
            default:
                if (this.isMobile) this.openMobileDraftSignDialog();
                else this.openDraftSignDialog();
                break;
        }
    }

    afterDraftSign = function (result: any) {
        if (result.error) {
            this.helper.beforeUnload.stop();
            this.agreeing = false;
            return;
        }
        let certEx: ICertificateInfoEx = result.result.certEx;
        this.order.draftFiles.draftFilesSigned = true;
        if (result.result && result.result.isDss) {
            this.order.draftFiles.draftFilesSigned = false;
        }
        // if (this.lastApprovalListItem) {
        //     this.order.activeApprover = null;
        // } else {
        //     this.order.activeApprover = this.getNextApprover().agreedBy.fioShort;
        // }
        this.checkProcess().pipe(
            mergeMap(() => {
                return this.updateOrder();
            }),
            mergeMap(() => {
                return this.approve(this.order, this.loopCounter, this.task.id, true, {
                    approvalNote: this.approvalNote,
                    agreedDs: {
                        agreedDsLastName: certEx.lastName,
                        agreedDsName: certEx.firstName,
                        agreedDsPosition: certEx.position,
                        agreedDsCN: certEx.serialNumber,
                        agreedDsFrom: moment(certEx.from, 'DD.MM.YYYY HH:mm:ss').toDate(),
                        agreedDsTo: moment(certEx.till, 'DD.MM.YYYY HH:mm:ss').toDate()
                    }
                })
            }),
            mergeMap(() => {
                if (this.lastApprovalListItem) {
                    return this.updateOrder().pipe(
                        mergeMap(() => {
                            return this.registerOrder()
                        }),
                        mergeMap((file) => {
                            this.order.draftFilesRegInfo = file;
                            return this.updateOrder();
                        })
                    );
                } else {
                    return this.orderService.send(this.order.documentID, false);
                }
            }),
            mergeMap(() => {
                return from(this.activityResourceService.finishTask(parseInt(this.task.id), [{
                    "name": "ApprovalApprovedVar",
                    "value": true
                }]));
            })
        ).subscribe((result: any) => {
            this.helper.beforeUnload.stop();
            this.agreeing = false;
            this.toastr.success('Электронный документ успешно согласован.');
            window.location.href = '/main/#/app/tasks';
        }, error => {
            this.helper.beforeUnload.stop();
            console.log(error);
            this.agreeing = false;
        });
    }.bind(this);

    beforeSign = function (fileInfo: ExFileType, folderGuid, certEx) {
        return this._beforeSign(fileInfo, folderGuid, certEx);
    }.bind(this);

    _beforeSign(fileInfo: ExFileType, folderGuid, certEx) {
        let ds = {
            dsLastName: certEx.lastName,
            dsName: certEx.firstName,
            dsPosition: certEx.position,
            dsCN: certEx.serialNumber,
            dsFrom: moment(certEx.from, 'DD.MM.YYYY HH:mm:ss').toDate(),
            dsTo: moment(certEx.till, 'DD.MM.YYYY HH:mm:ss').toDate()
        };

        if (!this.order.documentDate) {
            this.order.documentDate = new Date();
        }

        let draftFilesWithoutAttPdf: FileType;

        return this.checkProcess().pipe(
            mergeMap(() => {
                return this.updateDocumentNumberBarcodeAndDs(ds);
            }),
            mergeMap(_ => {
                const file = ExFileType.create(
                    this.order.draftFilesWithoutAttWord.draftFilesID,
                    this.order.draftFilesWithoutAttWord.draftFilesName,
                    +this.order.draftFilesWithoutAttWord.draftFilesSize,
                    false,
                    new Date(this.order.draftFilesWithoutAttWord.draftFilesDate).getTime(),
                    this.order.draftFilesWithoutAttWord.draftFilesType,
                    this.order.draftFilesWithoutAttWord.draftFilesMimeType
                );

                return this.applyOrderStamp(file, { ds: ds });
            }),
            mergeMap((id) => {
                return this.convertFileTypeToPdf(id, this.order.draftFilesWithoutAttWord.draftFilesName);
            }),
            mergeMap((draftFilePdf: IFileInFolderInfo) => {
                draftFilesWithoutAttPdf = FileType.create(
                    draftFilePdf.versionSeriesGuid, draftFilePdf.fileName, draftFilePdf.fileSize.toString(), false,
                    new Date(draftFilePdf.dateCreated), draftFilePdf.fileType, draftFilePdf.mimeType
                );

                if (!this.order.attachWord) {
                    const fileName = `${this.orderType.name.split(' ')[0]} № ${this.order.documentNumber} от ${formatDate(this.order.documentDate, 'dd.MM.yyyy', 'en')}.pdf`;

                    return this.copyFile(draftFilesWithoutAttPdf.fileID, fileName).pipe(
                        mergeMap((fileType) => {
                            return of(fileType);
                        })
                    );
                }

                return this.convertToPdfIfNecessary(this.order.attachWord)
                    .pipe(
                        mergeMap((attachPdf: OrderDraftFile) => {
                            let request: any = {
                                filenetDestination: {
                                    fileAttrs: [
                                        { attrName: 'docEntityID', attrValues: [this.order.documentID] },
                                        { attrName: 'docSourceReference', attrValues: ['UI'] }
                                    ],
                                    fileName: `${this.orderType.name.split(' ')[0]} № ${this.order.documentNumber} от ${formatDate(this.order.documentDate, 'dd.MM.yyyy', 'en')}.pdf`,
                                    fileType: 'MkaDocOther',
                                    folderGuid: this.order.folderID,
                                },
                                versionSeriesGuids: [draftFilesWithoutAttPdf.fileID, attachPdf.draftFilesID]
                            };

                            return this.reporterResourceService.pdfConcatenateFiles(request)
                        }),
                        mergeMap((result: any) => {
                            return of(FileType.create(result.versionSeriesGuid, result.fileName, result.fileSize.toString(), false,
                                new Date(result.dateCreated), result.fileType, result.mimeType));
                        })
                    );
            }),
            mergeMap((orderFile: FileType) => {
                let exFile = ExFileType.create(
                    orderFile.fileID,
                    orderFile.fileName,
                    +orderFile.fileSize,
                    orderFile.fileSigned,
                    new Date(orderFile.fileDate).getTime(),
                    orderFile.fileType,
                    orderFile.mimeType
                );
                _.extend(this.orderFile, exFile);

                return this.setOrderFile(orderFile).pipe(
                    map(_ => exFile)
                );
            })
        ).toPromise();
    }

    generateOrderFile(fileName: string): Observable<ExFileType> {
        let docxFileName = fileName.substring(0, fileName.lastIndexOf(".")) + '.docx';
        let request: any = {
            options: {
                jsonSourceDescr: "",
                onProcess: "Сформировать проект РД",
                placeholderCode: "",
                strictCheckMode: true,
                xwpfResultDocumentDescr: docxFileName,
                xwpfTemplateDocumentDescr: ""
            },
            jsonTxt: JSON.stringify({ document: this.order }),
            rootJsonPath: "$",
            nsiTemplate: {
                templateCode: 'AdmOrderPWithSign'
            },
            filenetDestination: {
                fileName: docxFileName,
                fileType: 'default',
                folderGuid: this.order.folderID,
                fileAttrs: [
                    { attrName: 'docEntityID', attrValues: [this.order.documentID] },
                    { attrName: 'docSourceReference', attrValues: ['UI'] },
                ]
            }
        };
        return from(this.reporterResourceService.nsi2Filenet(request)).pipe(
            map((response: IWordReporterResult) => {
                return ExFileType.create(response.versionSeriesGuid, response.fileName, response.fileSize, false,
                    new Date().getTime(), response.fileType, response.mimeType);
            })
        );
    }

    afterSign = function (result: any) {
        if (result.error) {
            this.helper.beforeUnload.stop();
            console.error(result);
            this.approving = false;
            return;
        }
        let canceledDocument = this.order.canceledDocument;
        let certEx: ICertificateInfoEx = result.result.certEx;
        this.orderFile.signed = true;

        _.find(this.order.files, (f: FileType) => f.fileID === this.orderFile.idFile).fileSigned = true;

        // if (this.lastApprovalListItem) {
        //     this.order.activeApprover = null;
        // }
        // else {
        //     this.order.activeApprover = this.getNextApprover().agreedBy.fioShort;
        // }
        this.updateOrder().pipe(
            mergeMap(() => {
                return this.approve(this.order, this.loopCounter, this.task.id, true, {
                    approvalNote: this.approvalNote,
                    agreedDs: {
                        agreedDsLastName: certEx.lastName,
                        agreedDsName: certEx.firstName,
                        agreedDsPosition: certEx.position,
                        agreedDsCN: certEx.serialNumber,
                        agreedDsFrom: moment(certEx.from, 'DD.MM.YYYY HH:mm:ss').toDate(),
                        agreedDsTo: moment(certEx.till, 'DD.MM.YYYY HH:mm:ss').toDate()
                    }
                })
            }),
            mergeMap(() => {
                return this.cancelDocumentService.cancelDocument(canceledDocument, this.order.documentDate);
            }),
            mergeMap(() => {
                return from(this.activityResourceService.finishTask(parseInt(this.task.id), [{
                    "name": "ApprovalApprovedVar",
                    "value": true
                }]));
            })
        ).subscribe(() => {
            this.helper.beforeUnload.stop();
            this.approving = false;
            this.toastr.success('Электронный документ успешно подписан.');
            window.location.href = '/main/#/app/tasks';
        }, (err) => {
            this.helper.beforeUnload.stop();
            console.error(err);
            if (err.documentID && err.documentTypeValue) {
                this.redirectToTask({
                    documentID: err.documentID,
                    documentTypeValue: err.documentTypeValue
                });
            }
            this.approving = false;
        });
    }.bind(this);

    approveAndSendForward() {
        this.helper.beforeUnload.start();
        this.approving = true;
        let canceledDocument = this.order.canceledDocument;
        this.checkProcess().pipe(
            mergeMap(() => {
                return this.registerDocumentService.checkLinkedDocuments(this.order);
            }),
            mergeMap(() => {
                return this.cancelDocumentService.checkCanceledDocument(canceledDocument);
            }),
            mergeMap(() => {
                return this.checkLinkedDocumentsRegistration();
            }),
            catchError(error => {
                this.toastr.error(error);
                return throwError(error);
            }),
            // mergeMap(() => {
            //   if (!this.order.documentDate) {
            //     this.order.documentDate = new Date();
            //   }
            //   return this.updateDocumentNumberBarcodeAndDs();
            // }),
            // mergeMap(() => {
            //   let fileName = this.getOrderFileName(this.order);
            //   return this.generateOrderFile(fileName)
            // }),
            // tap(file => {
            //   _.extend(this.orderFile, file);
            // })
        ).subscribe(() => {
            if (this.isMobile) this.openMobileSignDialog();
            else this.openSignDialog();
        }, () => {
            this.helper.beforeUnload.stop();
            this.approving = false;
        });
    }

    getNextApprover() {
        let agreed = this.order.approval.approvalCycle.agreed;
        let approvalNum = agreed[this.loopCounter].approvalNum;
        let nextApprover = agreed.filter(a => !a.approvalFactDate)
            .filter(a => a.approvalNum.startsWith(approvalNum + '.'))
            .find(a => true);
        if (!nextApprover) {
            let nextApprovalNum = '' + (parseInt(approvalNum) + 1);
            nextApprover = agreed.find(a => a.approvalNum === nextApprovalNum);
        }
        if (!nextApprover) {
            nextApprover = agreed.filter(a => !a.approvalFactDate)
                .find(a => true);
        }
        return nextApprover;
    }

    deleteApprovalFile = function (fileInfo: IFileType) {
        console.log(fileInfo);
        from(this.fileResourceService.deleteFile(fileInfo.idFile)).subscribe(
            () => {
                let index = this.order.approval.approvalCycle.agreed[this.loopCounter].fileApproval.findIndex(file => file.fileID === fileInfo.idFile);
                this.order.approval.approvalCycle.agreed[this.loopCounter].fileApproval.splice(index, 1);
            }
        )
    }.bind(this);

    addApproval() {
        this.helper.beforeUnload.start();
        let approval = this.order.approval.approvalCycle.agreed[this.loopCounter];
        const options = {
            initialState: {
                systemCode: this.order.systemCode,
                documentTypeCode: this.order.documentTypeCode,
                dueDate: this.task.dueDate,
                except: [] /*_.map(this.order.approval.approvalCycle.agreed, a => {
        // 			return a.agreedBy.accountName;
        // 		});*/,
                approvalNum: approval.approvalNum
            },
            'class': 'modal-lg'
        };
        const ref = this.modalService.show(AddApproversModalComponent, options);
        this.modalService.onHide.pipe(
            mergeMap(() => {
                return this.checkProcess();
            }),
            mergeMap(() => {
                let component = <AddApproversModalComponent>ref.content;
                if (component.submit) {
                    this.addingApprovals = true;
                    let agreed = this.order.approval.approvalCycle.agreed[this.loopCounter];
                    agreed.added = agreed.added || [];
                    component.agreed.forEach((added,index) => {
                        if(!added.approvalNum)
                            added.approvalNum=approval.approvalNum+'.'+(index+1).toString();
                        agreed.added.push({
                            additionalAgreed: {
                                accountName: added.agreedBy.accountName,
                                fio: added.agreedBy.fioFull,
                                iofShort: added.agreedBy.iofShort
                            },
                            addedNote: added.note
                        });
                    });
                    let newApprovers: OrderApprovalListItem[] = component.agreed.map(added => {
                        return {
                            approvalNum: added.approvalNum,
                            approvalTime: added.approvalTime,
                            agreedBy: added.agreedBy,
                            approvalType: added.approvalType,
                            approvalTypeCode: added.approvalTypeCode
                        }
                    });
                    newApprovers.reverse().forEach(newApprover => {
                        this.order.approval.approvalCycle.agreed.splice(this.loopCounter, 0, newApprover)
                    });
                    return this.updateOrder().pipe(
                        mergeMap(() => {
                            return this.orderActivitiService.addApprovers(this.task.id, this.order.approval.approvalCycle.agreed);
                        }),
                        tap(() => {
                            this.helper.beforeUnload.stop();
                            window.location.href = '/main/#/app/tasks';
                        })
                    );
                } else {
                    return throwError('cancelled');
                }
            })
        ).subscribe(() => {
            this.helper.beforeUnload.stop();
            this.addingApprovals = false;
        }, () => {
            this.helper.beforeUnload.stop();
            this.addingApprovals = false;
        });
    }

    private redirectToTask(linked: RedirectToTaskSettings) {
        const options = {
            initialState: {
                linked: linked,
            },
            'class': 'modal-lg'
        };
        const ref = this.modalService.show(RedirectToTaskModalComponent, options);
        this.modalService.onHide.pipe(
            mergeMap(() => {
                return of('');

            })
        );
    }

    private checkProcess(): Observable<any> {
        return this.historyResourceService.getHistoricTaskInstance(this.task.id).pipe(
            mergeMap((response) => {
                return this.historyResourceService.getHistoricProcessInstance(response.processInstanceId);
            }),
            mergeMap((response) => {
                if (response.endTime) {
                    this.toastr.warning('Отозвано инициатором РД');
                    window.location.href = '/main/#/app/tasks';
                    return throwError('Отозвано инициатором РД');
                }
                return of('');
            })
        );
    }
}
