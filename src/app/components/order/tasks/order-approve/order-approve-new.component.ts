import { CdpReporterResourceService } from "@reinform-cdp/reporter-resource";
import { NsiResourceService } from "@reinform-cdp/nsi-resource";
import * as _ from "lodash";
import { catchError, map, mergeMap, tap } from "rxjs/internal/operators";
import { FileResourceService, IFileInFolderInfo, IFileType } from "@reinform-cdp/file-resource";
import { from, of, throwError, combineLatest } from "rxjs/index";
import { BsModalService } from "ngx-bootstrap";
import * as angular from "angular";
import { Component, OnInit } from '@angular/core';
import { Ds, Order, OrderApprovalListItem, OrderApprover } from "../../../../models/order/Order";
import { ExFileType, LoadingStatus } from "@reinform-cdp/widgets";
import { ICertificateInfoEx, SessionStorage, SignService } from "@reinform-cdp/security";
import { ActiveTaskService, ActivityResourceService, ITask, CdpBpmHistoryResourceService } from "@reinform-cdp/bpm-components";
import { OrderType } from "../../../../models/order/OrderType";
import { ProcessComponent } from "../order-process";
import { ToastrService } from "ngx-toastr";
import { OrderService } from "../../../../services/order.service";
import { Observable } from "rxjs/Rx";
import { OrderTypeService } from "../../../../services/order-type.service";
import { ApprovalType } from "../../../../models/order/ApprovalType";
import { RegisterDocumentService } from "../../../../services/register-document.service";
import { RedirectToTaskModalComponent, RedirectToTaskSettings } from "../redirect-to-task/redirect-to-task.modal";
import { FileType } from "../../../../models/order/FileType";
import { CancelDocumentService } from "../../../../services/cancel-document.service";
import * as moment_ from "moment";
import { DropzoneConfigInterface } from "ngx-dropzone-wrapper";
import { AddApproversModalComponent } from "../add-approvers/add-approvers.modal";
import { OrderActivitiService } from "../../../../services/order-activiti.service";
import { IWordReporterResult } from "@reinform-cdp/reporter-resource/dist/models/IWordReporterResult";
import {HelperService} from "../../../../services/helper.service";

const moment = moment_;

@Component({
  selector: 'mggt-order-approve-new',
  templateUrl: './order-approve-new.component.html',
  styleUrls: ['./order-approve.component.scss']
})
export class OrderApproveNewComponent extends ProcessComponent implements OnInit {

  loadingStatus: LoadingStatus;
  success = false;
  task: ITask;
  order: Order;
  origOrder: Order;
  fileApproval: FileType[] = [];
  orderType: OrderType;
  showDocumentText: boolean;

  taskOwnerLogin: string;
  lastApprovalListItem: boolean;
  approvalState: boolean;

  isParallel: boolean;
  isMainApprover: boolean;
  needToCloseCycle: boolean;

  approvalNote: string = '';

  showApprovalList: boolean = false;

  sendingBackToWork: boolean;
  agreeing: boolean;
  approving: boolean;
  addingApprovals: boolean = false;
  approvalTypes: ApprovalType[];
  approvalType: ApprovalType;

  isAllowedAddingApprovals: boolean = true;

  openDraftSignDialog: () => void;
  openMobileDraftSignDialog: () => void;

  openSignDialog: () => void;
  openMobileSignDialog: () => void;
  orderFile: ExFileType = new ExFileType();
  isMobile: boolean = false;
  approvalDepth: number = 0;

  checkSign: string;

  dropzoneConfig: DropzoneConfigInterface;
  isNegotiationProjectSign: boolean = false;

  constructor(public activeTaskService: ActiveTaskService,
    public helper: HelperService,
    private orderTypeService: OrderTypeService,
    public activityResourceService: ActivityResourceService,
    private registerDocumentService: RegisterDocumentService,
    private modalService: BsModalService,
    private cancelDocumentService: CancelDocumentService,
    private orderActivitiService: OrderActivitiService,
    public signService: SignService,
    toastr: ToastrService,
    orderService: OrderService,
    fileResourceService: FileResourceService,
    nsiResourceService: NsiResourceService,
    sessionStorage: SessionStorage,
    reporterResourceService: CdpReporterResourceService,
    public historyResourceService: CdpBpmHistoryResourceService) {
    super(toastr, orderService, fileResourceService, nsiResourceService, sessionStorage, reporterResourceService);
  }

  ngOnInit() {
    this.helper.beforeUnload.init();
    this.loadingStatus = LoadingStatus.LOADING;
    this.activeTaskService.getTask().pipe(
      mergeMap(task => {
        this.task = task;

        // Определяем реального владельца задачи
        if (!!this.task && !!this.task.owner) {
            this.taskOwnerLogin = this.task.owner; // Владелец делегированной задачи
        } else if (!!this.task && !!this.task.assignee && this.task.assignee !== this.session.login()) {
            this.taskOwnerLogin = this.task.assignee; // Задача делегирована, но только взята в работу (не перезагружена)
        } else {
            this.taskOwnerLogin = this.session.login();
        }

        this.isNegotiationProjectSign = 'sdordNegotiationProjectSign'.includes(task.formKey);
        return this.getEntityIdVar(task);
      }),
      mergeMap(id => {
        return this.orderService.get(id);
      }),
      mergeMap(order => {
        this.order = order;
        this.origOrder = angular.copy(order);

        let agreed = OrderApproveNewComponent.getCurrentAgreed(this.order, this.taskOwnerLogin);

        if (agreed === undefined) {
          this.toastr.error('Не удалось определить номер согласующего в листе согласования.');
          this.loadingStatus = LoadingStatus.ERROR;

          return throwError('Не удалось определить номер согласующего в листе согласования.');
        }

        this.fileApproval = agreed.fileApproval || [];

        this.approvalState = agreed.approvalTypeCode === ApprovalType.approval;
        this.approvalDepth = agreed.approvalNum.split('.').length;
        this.lastApprovalListItem = agreed === _.last(this.order.approval.approvalCycle.agreed);

        this.isMainApprover = this.approvalDepth === 1;
        this.isParallel = agreed.Parallel;
        this.needToCloseCycle = this.isMainApprover && !this.isParallel;

        // if (!this.order.activeApprover) {
        //   this.order.activeApprover = this.session.fioShort();
        // }

        if (!this.order.documentDate) {
          this.order.documentDate = new Date();
        }

        if (!agreed.approvalPlanDate) {
          agreed.approvalPlanDate = this.task.dueDate;
          return this.updateOrder().pipe(
            mergeMap(() => {

              return this.loadDictionaries();
            })
          );
        } else {
          return this.loadDictionaries();
        }
      }),
      mergeMap(() => {
        let agreed = OrderApproveNewComponent.getCurrentAgreed(this.order, this.taskOwnerLogin);
        if (agreed.added && agreed.added.length > 0) {
          this.isAllowedAddingApprovals = false;
        }

        return this.signService.checkSign();
      }),
      mergeMap(res => {
        this.checkSign = res;
        return this.nsiResourceService.getDictFromCache('mggt_order_ApprovalType');
      })
    ).subscribe((response) => {
      this.approvalTypes = response.value;
      this.order.approval.approvalCycle.agreed.forEach((a) => {
        if (!a.approvalTime && a.approvalTypeCode) {
          const approvalType = this.approvalTypes.find(at => at.approvalTypeCode === a.approvalTypeCode);
          a.approvalTime = approvalType.approvalDuration;
        }
      });

      // .find(agreed => agreed.agreedBy.accountName === this.session.login());
      // берем запись по процессу а не ищем по логину
      const agreed =  OrderApproveNewComponent.getCurrentAgreed(this.order, this.taskOwnerLogin);
      this.approvalType = this.approvalTypes.find(type => type.approvalTypeCode === agreed.approvalTypeCode);
      this.initDropzone();
      this.loadingStatus = LoadingStatus.SUCCESS;
      this.success = true;
    }, (error) => {
      console.error(error);
      this.loadingStatus = LoadingStatus.ERROR;
    });
  }

  static getCurrentAgreed(order: Order, taskOwnerLogin: string): OrderApprovalListItem {
    return order.approval.approvalCycle.agreed.find(agree => {
      return agree.agreedBy.accountName === taskOwnerLogin && !agree.approvalResult;
    });
  }

  ngOnDestroy() {
    this.helper.beforeUnload.destroy();
  }

  static updateCurrentAgree(order: Order, taskOwnerLogin: string, session: SessionStorage) {
    let agree = OrderApproveNewComponent.getCurrentAgreed(order, taskOwnerLogin);
    if (agree.agreedBy.accountName == session.login()) {
      agree.factAgreedBy=null;
      return;
    }
    // currentAgree.agreedBy =
    agree.factAgreedBy = OrderApprover.fromUserBean({
      post: session.post(),
      displayName: session.fullName(),
      accountName: session.login(),
      fioShort: session.fioShort(),
      iofShort: session.iofShort(),
      telephoneNumber: session.telephoneNumber()
    });
  }

  loadDictionaries(): Observable<any> {
    return this.orderTypeService.geOrderTypes().pipe(
      tap(orderTypes => {
        this.orderType = orderTypes.find(val => val.code === this.order.documentTypeCode);
        // TODO: check OASI_ADMORDER_OGS
        this.showDocumentText = this.orderType.accessAction === 'OASI_ADMORDER_OGS';
      })
    )
  }

  initDropzone() {
    let component = this;
    this.dropzoneConfig = {
      autoProcessQueue: true,
      parallelUploads: 1,
      params: {
        fileType: 'MkaDocOther',
        docSourceReference: 'UI'
      },
      init: function () {
        this.on("processing", function (file) {
          this.options.params.folderGuid = component.order.folderID;
          this.options.params.docEntityID = component.order.documentID;
        });
        this.on("error", function (file, errorMessage) {
          component.toastr.warning(errorMessage.message ? errorMessage.message : errorMessage, 'Ошибка');
          this.removeFile(file);
        });
        this.on("success", function (file, guid) {
          const fileType: FileType = FileType.create(guid, file.name, file.size.toString(), false, new Date(), file.type, 'MkaDocOther');
          component.fileApproval.push(fileType);
          this.removeFile(file);
        });
      }
    }
  }

  sendBackToWork() {
    this.helper.beforeUnload.start();
    if (!this.approvalNote) {
      this.toastr.error('Не указан комментарий');
      return;
    }
    this.sendingBackToWork = true;
    let ApprovalDurations;
    this.checkProcess().pipe(
      mergeMap(() => this.refreshAndUpdateOrder((order) => {
          ApprovalDurations = order.approval.approvalCycle.agreed.map((agreed) => (agreed.approvalTime)).join(',');

          OrderApproveNewComponent.updateCurrentAgree(order, this.taskOwnerLogin, this.session);
          let agreed = OrderApproveNewComponent.getCurrentAgreed(order, this.taskOwnerLogin);
          agreed.fileApproval = this.fileApproval;
          return this.approve(order, agreed, this.task.id, false, {
              approvalNote: this.approvalNote
          }, this.needToCloseCycle).pipe(map(_ => order));
      })),
      mergeMap((order) => {
        this.order = order;
        this.origOrder = angular.copy(order);

        const params: any = [
          { name: 'AddApproval', value: false },
        ];

        if (this.isNegotiationProjectSign) {
          params.push({ name: 'IsApproved', value: false });
        }

        if (!this.isNegotiationProjectSign && !this.isParallel) {
          params.push({ name: 'ApprovalApprovedVar', value: false });
          params.push({ name: 'ApprovalDurations', value: ApprovalDurations });
        }

        return from(this.activityResourceService.finishTask(parseInt(this.task.id), params));
      })
    ).subscribe(
      () => {
        this.helper.beforeUnload.stop();
        this.sendingBackToWork = false;
        this.toastr.success('Документ отправлен на доработку.');
        window.location.href = '/main/#/app/tasks';
      },
      (err) => {
        this.helper.beforeUnload.stop();
        console.log('subscribe err', err);
        this.sendingBackToWork = false;
      });
  }

  agreeAndSendForwardHandler() {
    this.helper.beforeUnload.start();
    this.agreeing = true;
    if (this.lastApprovalListItem) {
      this.checkProcess().pipe(
        mergeMap(() => {
          return this.registerDocumentService.checkLinkedDocuments(this.order);
        })
      ).subscribe(
        () => {
          this.agreeAndSendForward();
        },
        (err) => {
          this.helper.beforeUnload.stop();
          this.agreeing = false;
          if (err.documentID && err.documentTypeValue) {
            this.redirectToTask({ documentID: err.documentID, documentTypeValue: err.documentTypeValue });
          }
        });
    } else {
      this.agreeAndSendForward();
    }
  }

  agreeAndSendForward() {
    this.agreeing = true;
    switch (this.approvalType.approvalTypeCode) {
      case 'assent':
        this.checkProcess().pipe(
          mergeMap(() => {
            let func = (order: Order) => {
              OrderApproveNewComponent.updateCurrentAgree(order, this.taskOwnerLogin, this.session);
              let agreed = OrderApproveNewComponent.getCurrentAgreed(order, this.taskOwnerLogin);
              agreed.fileApproval = this.fileApproval;
              return this.approve(order, agreed, this.task.id, true, {
                  approvalNote: this.approvalNote
              }, false).pipe(map(_ => order));
            };
            if (this.lastApprovalListItem) {
              // this.order.activeApprover = null;
              return this.refreshAndUpdateOrder(func).pipe(
                mergeMap((order) => {
                  this.order = order;
                  this.origOrder = angular.copy(order);

                  return this.registerOrder().pipe(
                    mergeMap(() => {
                        return this.orderService.send(this.order.documentID, false);
                    }),
                    map(() => order)
                  );
                })
              )
            } else {
              // this.order.activeApprover = this.getNextApprover().agreedBy.fioShort;
                return this.refreshAndUpdateOrder(func)
            }
          }),
          mergeMap((order: Order) => {
            this.order = order;
            this.origOrder = angular.copy(order);

            const params: any = [
              { name: 'AddApproval', value: false },
            ];

            if (this.isNegotiationProjectSign) {
              params.push({ name: 'IsApproved', value: true });
            }

            const ApprovalDurations = this.order.approval.approvalCycle.agreed.map((agreed) => (agreed.approvalTime)).join(',');
            if (!this.isNegotiationProjectSign && !this.isParallel) {
              params.push({ name: 'ApprovalApprovedVar', value: true });
              params.push({ name: 'ApprovalDurations', value: ApprovalDurations });
            }

            return from(this.activityResourceService.finishTask(parseInt(this.task.id), params));
          })
        ).subscribe((result: any) => {
          this.helper.beforeUnload.stop();
          this.agreeing = false;
          this.toastr.success('Документ успешно согласован.');
          window.location.href = '/main/#/app/tasks';
        }, (e) => {
          this.helper.beforeUnload.stop();
          console.log(e);
          this.agreeing = false;
        });
        break;
      default:
        if (this.isMobile) this.openMobileDraftSignDialog();
        else this.openDraftSignDialog();
        break;
    }
  }

  afterDraftSign = function (result: any) {
    if (result.error) {
      this.helper.beforeUnload.stop();
      this.agreeing = false;
      return;
    }
    let certEx: ICertificateInfoEx = result.result.certEx;
    // исправление ошибки при повторном подписании после dss.
    /*if (result.result && result.result.isDss) {
        this.order.draftFiles.draftFilesSigned = false;
    } else {
        this.order.draftFiles.draftFilesSigned = true;
    }*/

    // if (this.lastApprovalListItem) {
    //   this.order.activeApprover = null;
    // } else {
    //   this.order.activeApprover = this.getNextApprover().agreedBy.fioShort;
    // }

    this.checkProcess().pipe(
      mergeMap(() => this.refreshAndUpdateOrder(order => {
        order.draftFiles.draftFilesSigned = true;
        OrderApproveNewComponent.updateCurrentAgree(order, this.taskOwnerLogin, this.session);
        let agreed = OrderApproveNewComponent.getCurrentAgreed(order, this.taskOwnerLogin);
        agreed.fileApproval = this.fileApproval;
        return this.approve(order, agreed, this.task.id, true, {
            approvalNote: this.approvalNote,
            agreedDs: {
                agreedDsLastName: certEx.lastName,
                agreedDsName: certEx.firstName,
                agreedDsPosition: certEx.position,
                agreedDsCN: certEx.serialNumber,
                agreedDsFrom: moment(certEx.from, 'DD.MM.YYYY HH:mm:ss').toDate(),
                agreedDsTo: moment(certEx.till, 'DD.MM.YYYY HH:mm:ss').toDate()
            }
        }, false).pipe(map(_ => order));
      })),
      mergeMap((order) => {
        this.order = order;
        this.origOrder = angular.copy(order);

        if (this.lastApprovalListItem) {
          return this.registerOrder();
        } else {
          return this.orderService.send(this.order.documentID, false);
        }
      }),
      mergeMap(() => {
        const params: any = [
          { name: 'AddApproval', value: false },
        ];

        if (this.isNegotiationProjectSign) {
          params.push({ name: 'IsApproved', value: true });
        }

        const ApprovalDurations = this.order.approval.approvalCycle.agreed.map((agreed) => (agreed.approvalTime)).join(',');
        if (!this.isNegotiationProjectSign && !this.isParallel) {
          params.push({ name: 'ApprovalApprovedVar', value: true });
          params.push({ name: 'ApprovalDurations', value: ApprovalDurations });
        }

        return from(this.activityResourceService.finishTask(parseInt(this.task.id), params));
      })
    ).subscribe((result: any) => {
      this.helper.beforeUnload.stop();
      this.agreeing = false;
      this.toastr.success('Документ успешно согласован.');
      window.location.href = '/main/#/app/tasks';
    }, error => {
      this.helper.beforeUnload.stop();
      console.log(error);
      this.agreeing = false;
    });
  }.bind(this);

  beforeSign = function (fileInfo: ExFileType, folderGuid, certEx) {
    let ds = {
      dsLastName: certEx.lastName,
      dsName: certEx.firstName,
      dsPosition: certEx.position,
      dsCN: certEx.serialNumber,
      dsFrom: moment(certEx.from, 'DD.MM.YYYY HH:mm:ss').toDate(),
      dsTo: moment(certEx.till, 'DD.MM.YYYY HH:mm:ss').toDate()
    };

    return this.checkProcess().pipe(
      mergeMap(() => {
        return this.updateDocumentNumberBarcodeAndDs(ds);
      }),
      mergeMap(_ => {
        return this.applyOrderStamp(this.orderFile, { ds: ds });
      }),
      mergeMap(id => {
        return this.convertFileTypeToPdf(id, fileInfo.nameFile);
      }),
      mergeMap((response: IFileInFolderInfo) => {
        let exFile = ExFileType.create(response.versionSeriesGuid, response.fileName, response.fileSize, false,
          new Date(response.dateCreated).getTime(), response.fileType, response.mimeType);
        let file = FileType.create(response.versionSeriesGuid, response.fileName, response.fileSize.toString(), false,
          new Date(response.dateCreated), response.fileType, response.mimeType);
        _.extend(this.orderFile, exFile);
        return this.setOrderFile(file).pipe(
          map(_ => exFile)
        );
      })
    ).toPromise();
  }.bind(this);

  generateOrderFile(fileName: string): Observable<ExFileType> {
    let docxFileName = fileName.substring(0, fileName.lastIndexOf(".")) + '.docx';
    let request: any = {
      options: {
        jsonSourceDescr: "",
        onProcess: "Сформировать проект РД",
        placeholderCode: "",
        strictCheckMode: true,
        xwpfResultDocumentDescr: docxFileName,
        xwpfTemplateDocumentDescr: ""
      },
      jsonTxt: JSON.stringify({ document: this.order }),
      rootJsonPath: "$",
      nsiTemplate: {
        templateCode: 'AdmOrderPWithSign'
      },
      filenetDestination: {
        fileName: docxFileName,
        fileType: 'default',
        folderGuid: this.order.folderID,
        fileAttrs: [
          { attrName: 'docEntityID', attrValues: [this.order.documentID] },
          { attrName: 'docSourceReference', attrValues: ['UI'] },
        ]
      }
    };
    return from(this.reporterResourceService.nsi2Filenet(request)).pipe(
      map((response: IWordReporterResult) => {
        return ExFileType.create(response.versionSeriesGuid, response.fileName, response.fileSize, false,
          new Date().getTime(), response.fileType, response.mimeType);
      })
    );
  }

  applyOrderStamp(file: ExFileType, signInfo: { ds: Ds }): Observable<String> {
    return from(this.reporterResourceService.filenet2Filenet(<any>{
      jsonTxt: JSON.stringify(signInfo),
      filenetTemplate: {
        templateVersionSeriesGuid: file.idFile
      },
      filenetDestination: {
        reportFileName: file.nameFile,
        reportFileType: file.typeFile,
        reportFolderGuid: this.order.folderID,
        fileName: file.nameFile,
        fileType: file.typeFile,
        folderGuid: this.order.folderID
      },
      rootJsonPath: "$",
      options: {
        onProcess: file.nameFile.replace('.docx', ''),
        placeholderCode: 'sn',
        strictCheckMode: true,
        xwpfResultDocumentDescr: file.nameFile,
        xwpfTemplateDocumentDescr: ""
      }
    })).pipe(
      map(response => response.versionSeriesGuid)
    );
  }

  afterSign = function (result: any) {
    if (result.error) {
      console.error(result);
      this.helper.beforeUnload.stop();
      this.approving = false;
      return;
    }
    let certEx: ICertificateInfoEx = result.result.certEx;

    if (result.result && result.result.isDss) {
        this.orderFile.signed = false;
    } else {
        this.orderFile.signed = true;
    }
    this.refreshAndUpdateOrder(order => {
      // if (this.lastApprovalListItem) {
      //   this.order.activeApprover = null;
      // }
      // else {
      //   this.order.activeApprover = this.getNextApprover().agreedBy.fioShort;
      // }
      _.find(order.files, (f: FileType) => f.fileID === this.orderFile.idFile).fileSigned = true;
      OrderApproveNewComponent.updateCurrentAgree(order, this.taskOwnerLogin, this.session);
      let agreed = OrderApproveNewComponent.getCurrentAgreed(order, this.taskOwnerLogin);
      agreed.fileApproval = this.fileApproval;
      return this.approve(order, agreed, this.task.id, true, {
        approvalNote: this.approvalNote,
        agreedDs: {
          agreedDsLastName: certEx.lastName,
          agreedDsName: certEx.firstName,
          agreedDsPosition: certEx.position,
          agreedDsCN: certEx.serialNumber,
          agreedDsFrom: moment(certEx.from, 'DD.MM.YYYY HH:mm:ss').toDate(),
          agreedDsTo: moment(certEx.till, 'DD.MM.YYYY HH:mm:ss').toDate()
        }
      }, this.needToCloseCycle).pipe(map(_ => order))
    }).pipe(
      mergeMap((order) => {
        this.order = order;
        this.origOrder = angular.copy(order);

        let canceledDocument = this.order.canceledDocument;
        return this.cancelDocumentService.cancelDocument(canceledDocument, this.order.documentDate);
      }),
      mergeMap(() => {
        const params: any[] = [[
          { name: 'AddApproval', value: false },
        ]];

        if (this.isNegotiationProjectSign) {
          params.push({ name: 'IsApproved', value: true });
        }

        if (!this.isNegotiationProjectSign) {
          params.push({ name: 'ApprovalApprovedVar', value: true });
        }
        return from(this.activityResourceService.finishTask(parseInt(this.task.id), params));
      })
    ).subscribe(() => {
      this.helper.beforeUnload.stop();
      this.approving = false;
      this.toastr.success('Электронный документ успешно создан и подписан');
      window.location.href = '/main/#/app/tasks';
    }, (err) => {
      this.helper.beforeUnload.stop();
      console.error(err);
      if (err.documentID && err.documentTypeValue) {
        this.redirectToTask({
          documentID: err.documentID,
          documentTypeValue: err.documentTypeValue
        });
      }
      this.approving = false;
    });
  }.bind(this);

  approveAndSendForward() {
    this.approving = true;
    this.helper.beforeUnload.start();
    let canceledDocument = this.order.canceledDocument;
    this.checkProcess().pipe(
      mergeMap(() => {
        return this.registerDocumentService.checkLinkedDocuments(this.order);
      }),
      mergeMap(() => {
        return this.cancelDocumentService.checkCanceledDocument(canceledDocument);
      }),
      mergeMap(() => {
        return this.checkLinkedDocumentsRegistration();
      }),
      catchError(error => {
        this.toastr.error(error);
        return throwError(error);
      }),
      mergeMap(() => {
        return this.updateDocumentNumberBarcodeAndDs();
      }),
      mergeMap(() => {
        let fileName = this.getOrderFileName(this.order);
        return this.generateOrderFile(fileName)
      }),
      tap(file => {
        _.extend(this.orderFile, file);
      })
    ).subscribe(() => {
      if (this.isMobile) this.openMobileSignDialog();
      else this.openSignDialog();
    }, () => {
      this.helper.beforeUnload.stop();
      this.approving = false;
    });
  }

  static getNextApprover(order: Order, taskOwnerLogin) {
    let agreed = OrderApproveNewComponent.getCurrentAgreed(order, taskOwnerLogin);
    let approvalNum = agreed.approvalNum;
    let nextApprover = order.approval.approvalCycle.agreed.filter(a => !a.approvalFactDate)
      .filter(a => a.approvalNum.startsWith(approvalNum + '.'))
      .find(a => true);
    if (!nextApprover) {
      let nextApprovalNum = '' + (parseInt(approvalNum) + 1);
      nextApprover = order.approval.approvalCycle.agreed.find(a => a.approvalNum === nextApprovalNum);
    }
    if (!nextApprover) {
      nextApprover = order.approval.approvalCycle.agreed.filter(a => !a.approvalFactDate)
        .find(a => true);
    }
    return nextApprover;
  }

  deleteApprovalFile = function (fileInfo: IFileType) {
    from(this.fileResourceService.deleteFile(fileInfo.idFile)).subscribe(
      () => {
        let index = this.fileApproval.findIndex(file => file.fileID === fileInfo.idFile);
        this.fileApproval.splice(index, 1);
      }
    )
  }.bind(this);

  addApproval() {
    this.helper.beforeUnload.start();
    let agreed = OrderApproveNewComponent.getCurrentAgreed(this.order, this.taskOwnerLogin);
    const options = {
      initialState: {
        systemCode: this.order.systemCode,
        documentTypeCode: this.order.documentTypeCode,
        dueDate: this.task.dueDate,
        except: _.map(this.order.approval.approvalCycle.agreed, (a) => (a.agreedBy.accountName)),
        approvalNum: agreed.approvalNum
      },
      ignoreBackdropClick: true,
      'class': 'modal-xl modal-lg'
    };
    const ref = this.modalService.show(AddApproversModalComponent, options);
    this.modalService.onHide.pipe(
      mergeMap(() => {
        return this.checkProcess();
      }),
      mergeMap(() => {
        let component = <AddApproversModalComponent>ref.content;
        if (component.submit) {
          this.addingApprovals = true;
          component.agreed.forEach((added, index) => {
            if(!added.approvalNum) {
              added.approvalNum = agreed.approvalNum + '.' + (index + 1).toString();
            }
          });
          let newApprovers: OrderApprovalListItem[] = component.agreed.map((added) => {
            return {
              approvalNum: added.approvalNum,
              approvalTime: added.approvalTime,
              agreedBy: added.agreedBy,
              approvalType: added.approvalType,
              approvalTypeCode: added.approvalTypeCode,
              Parallel: false,
              Signer: false,
            }
          });
          return this.refreshAndUpdateOrder(order => {
            let agreed = OrderApproveNewComponent.getCurrentAgreed(order, this.taskOwnerLogin);
            agreed.added = agreed.added || [];
            component.agreed.forEach((added,index) => {
              agreed.added.push({
                additionalAgreed: {
                  accountName: added.agreedBy.accountName,
                  fio: added.agreedBy.fioFull,
                  iofShort: added.agreedBy.iofShort
                },
                addedNote: added.note
              });
            });
            let index = order.approval.approvalCycle.agreed.indexOf(agreed);
            newApprovers.reverse().forEach(newApprover => {
                order.approval.approvalCycle.agreed.splice(index, 0, newApprover)
            });
            return of(order);
          }).pipe(
            mergeMap((order) => {
              this.order = order;
              this.origOrder = angular.copy(order);

              // return this.orderActivitiService.addApprovers(this.task.id, this.order.approval.approvalCycle.agreed);
              const ApprovalUsers = newApprovers.map((agreed) => (agreed.agreedBy.accountName)).join(',');
              const ApprovalDurations = newApprovers.map((agreed) => (agreed.approvalTime)).join(',');
              const params = [
                { name: 'AddApproval', value: true },
                { name: 'ApprovalUsers', value: ApprovalUsers },
                { name: 'ApprovalDurations', value: ApprovalDurations },
              ];

              if (!this.isNegotiationProjectSign && !this.isParallel) {
                params.push({ name: 'IsParallel', value: true });
                params.push({ name: 'ApprovalApprovedVar', value: false });
              }

              return from(this.activityResourceService.finishTask(parseInt(this.task.id), params));
            }),
            tap(() => {
              this.helper.beforeUnload.stop();
              window.location.href = '/main/#/app/tasks';
            })
          );
        } else {
          return throwError('cancelled');
        }
      })
    ).subscribe(() => {
      this.helper.beforeUnload.stop();
      this.addingApprovals = false;
    }, () => {
      this.helper.beforeUnload.stop();
      this.addingApprovals = false;
    });
  }

  private redirectToTask(linked: RedirectToTaskSettings) {
    const options = {
      initialState: {
        linked: linked,
      },
      'class': 'modal-lg'
    };
    const ref = this.modalService.show(RedirectToTaskModalComponent, options);
    this.modalService.onHide.pipe(
      mergeMap(() => {
        return of('');

      })
    );
  }

  private checkProcess(): Observable<any> {
    // this.orderService.get(this.order.documentID).subscribe(document=>{
    //   if(!!document.returnToEditing||!!document.returnToEditingSeq){}
    // },error=>{
    //   console.log(error);
    //   this.toastr.error('Не удалось получить документ');
    //   this.sendingToApproval = false;
    // });
    return this.historyResourceService.getHistoricTaskInstance(this.task.id).pipe(
      mergeMap((response) => {
        // console.log(response);
        return this.historyResourceService.getHistoricProcessInstance(response.processInstanceId);
      }),
      mergeMap((response) => {
        console.log(response);
        if (response.endTime) {
          this.toastr.warning('Отозвано инициатором РД');
          window.location.href = '/main/#/app/tasks';
          return throwError('Отозвано инициатором РД');
        }
        return of('');
      })
    );
  }

  hasAgreed(): boolean {
    return this.helper.hasAgreed(this.order);
  }

  get isAgreeBtnDisabledBtn() {
    let r = this.approving || this.sendingBackToWork || this.addingApprovals || this.approvalNote !== '';
    if (!r) {
      r = !!this.fileApproval && !!this.fileApproval.length;
    }
    return r;
  }

  refreshAndUpdateOrder(func: (order: Order) => Observable<Order>): Observable<Order> {
    return this.orderService.get(this.order.documentID).pipe(
      mergeMap(order => {
        let origOrder = angular.copy(order);
        return combineLatest(of(origOrder), func(order));
      }),
      mergeMap(([origOrder, order]) => {
          return this.orderService.update(this.order.documentID, this.orderService.diff(origOrder, order));
      })
    );
  }
}
