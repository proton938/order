import {Component, Input, OnInit} from '@angular/core';
import {Order} from "../../../../models/order/Order";
import {StateService} from "@uirouter/core";

@Component({
    selector: 'mggt-order-info',
    templateUrl: './order-info.component.html',
    styleUrls: ['./order-info.component.scss']
})
export class OrderInfoComponent implements OnInit {

    @Input() order: Order;
    @Input() showDocumentText: boolean;

    taskDescription: string = "";
    isCollapsed: boolean = true;

    constructor(public $state: StateService) {
    }

    ngOnInit() {
    }

}
