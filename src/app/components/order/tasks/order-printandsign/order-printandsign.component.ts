import * as angular from "angular";
import * as _ from "lodash";
import {FileResourceService} from "@reinform-cdp/file-resource";
import {ActiveTaskService, ActivityResourceService, ITask} from "@reinform-cdp/bpm-components";
import {NsiResourceService} from "@reinform-cdp/nsi-resource";
import {AuthorizationService, SessionStorage} from "@reinform-cdp/security";
import {ToastrService} from "ngx-toastr";
import {LoadingStatus} from "@reinform-cdp/widgets";
import {mergeMap, tap} from "rxjs/internal/operators";
import {Component, OnInit} from '@angular/core';
import {OrderSigningOnPaper} from "../../../../models/order/Order";
import {OrderTypeService} from "../../../../services/order-type.service";
import {OrderService} from "../../../../services/order.service";
import {ProcessComponent} from "../order-process";
import {CdpReporterResourceService} from "@reinform-cdp/reporter-resource";
import {CancelDocumentService} from "../../../../services/cancel-document.service";
import {HelperService} from "../../../../services/helper.service";

@Component({
    selector: 'mggt-order-printandsign',
    templateUrl: './order-printandsign.component.html',
    styleUrls: ['./order-printandsign.component.scss']
})
export class OrderPrintandsignComponent extends ProcessComponent implements OnInit {

    loadingStatus: LoadingStatus;
    success = false;
    task: ITask;

    signingFactDate: Date;
    noteText: string;

    sendingToRegistration: boolean;
    sendingBackToWork: boolean;

    constructor(private orderTypeService: OrderTypeService,
                public helper: HelperService,
                private activeTaskService: ActiveTaskService, private authorizationService: AuthorizationService,
                private activityResourceService: ActivityResourceService,
                private sessionStorage: SessionStorage,
                private cancelDocumentService: CancelDocumentService,
                toastr: ToastrService, orderService: OrderService, fileResourceService: FileResourceService, nsiResourceService: NsiResourceService, session: SessionStorage,
                reporterResourceService: CdpReporterResourceService) {
        super(toastr, orderService, fileResourceService, nsiResourceService, session, reporterResourceService);
    }

    ngOnInit() {
        this.helper.beforeUnload.init();
        this.loadingStatus = LoadingStatus.LOADING;
        this.signingFactDate = new Date();

        this.activeTaskService.getTask().pipe(
            mergeMap(task => {
                this.task = task;
                return this.getEntityIdVar(task);
            }),
            mergeMap(id => {
                return this.orderService.get(id);
            }),
            tap(order => {
                this.order = order;
                this.origOrder = angular.copy(order);
            })
        ).subscribe(() => {
            this.loadingStatus = LoadingStatus.SUCCESS;
            this.success = true;
        }, () => {
            this.loadingStatus = LoadingStatus.ERROR;
        });
    }

    ngOnDestroy() {
        this.helper.beforeUnload.destroy();
    }

    getSigninigOnPaper() {
        if (!this.order.SigningOnPaper) {
            this.order.SigningOnPaper = [];
        }
        let result = _.find(this.order.SigningOnPaper, (s: OrderSigningOnPaper) => {
            return s.taskId === this.task.id;
        });
        if (!result) {
            result = new OrderSigningOnPaper(this.task.id);
            this.order.SigningOnPaper.push(result);
        }
        return result;
    }

    signed() {
        this.helper.beforeUnload.start();
        let signinigOnPaper = this.getSigninigOnPaper();
        signinigOnPaper.signingResult = true;
        signinigOnPaper.signingFactDate = this.signingFactDate;
        signinigOnPaper.noteSigning = {
            noteText: this.noteText,
            noteAuthor: this.sessionStorage.login(),
            noteAuthorFIO: this.sessionStorage.name(),
            noteDate: new Date()
        };
        this.sendingToRegistration = true;

        let canceledDocument = this.order.canceledDocument;
        this.cancelDocumentService.checkCanceledDocument(canceledDocument).pipe(
            mergeMap(() => {
                return this.updateOrder();
            }),
            mergeMap(() => {
                return this.cancelDocumentService.cancelDocument(canceledDocument, this.order.documentDate);
            }),
            mergeMap(() => {
                return this.activityResourceService.finishTask(parseInt(this.task.id), [{
                    "name": "ApprovedByBossVar",
                    "value": true
                }])
            })
        ).subscribe(() => {
            this.helper.beforeUnload.stop();
            this.sendingToRegistration = false;
            this.toastr.success('Документ отправлен на сканирование.');
            window.location.href = '/main/#/app/tasks';
        }, () => {
            this.helper.beforeUnload.stop();
            this.sendingToRegistration = false;
        });
    }

    sendBackToWork() {
        if (!this.noteText) {
            this.toastr.error('Не указан комментарий');
            return;
        }
        let signinigOnPaper = this.getSigninigOnPaper();
        signinigOnPaper.signingResult = false;
        signinigOnPaper.signingFactDate = this.signingFactDate;
        signinigOnPaper.noteSigning = {
            noteText: this.noteText,
            noteAuthor: this.sessionStorage.login(),
            noteAuthorFIO: this.sessionStorage.name(),
            noteDate: new Date()
        };
        this.sendingBackToWork = true;
        this.updateOrder().pipe(
            mergeMap(() => {
                return this.closeCycle(this.order);
            }),
            mergeMap(() => {
                return this.activityResourceService.finishTask(parseInt(this.task.id), [{
                    "name": "ApprovedByBossVar",
                    "value": false
                }])
            })
        ).subscribe(() => {
            this.sendingBackToWork = false;
            this.toastr.success('Документ отправлен на доработку.');
            window.location.href = '/main/#/app/tasks';
        }, () => {
            this.sendingBackToWork = false;
        });
    }

    getApprovalReportUrl() {
        // TODO: reporter
        // return this.reportResource.getApprovalReportUrl(this.order.documentID);
    }

}
