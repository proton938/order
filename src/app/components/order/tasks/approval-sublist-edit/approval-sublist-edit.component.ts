import { UserBean } from "@reinform-cdp/nsi-resource";
import { Component, EventEmitter, Input, OnInit, Output, OnChanges } from '@angular/core';
import { ApprovalTerm, OrderApprovalListItem, OrderApprover } from "../../../../models/order/Order";
import { ApprovalType } from "../../../../models/order/ApprovalType";
import { ApprovalDuration } from "../../../../models/order/ApprovalDuration";
import { ApprovalNode } from "../../../../services/approval-util.service";

export class ApprovalListEditDicts {
  approvalTypes: ApprovalType[];
  executives: UserBean[];
  approvers: UserBean[];
  signUsers: UserBean[];
  approvalDurations: ApprovalDuration[];
}

@Component({
  selector: 'mggt-approval-sublist-edit',
  templateUrl: './approval-sublist-edit.component.html',
  styleUrls: ['./approval-sublist-edit.component.scss']
})
export class ApprovalSublistEditComponent implements OnInit, OnChanges {

  @Input() list: ApprovalNode[];
  @Input() canAdd: () => boolean = () => true;
  @Input() canDelete: (agreed: OrderApprovalListItem) => boolean = () => true;
  @Input() canSwap: (ind1: number, ind2: number) => boolean = () => true;
  @Input() hasApproval: () => boolean;

  @Input() head: OrderApprover;
  @Input() headOnly: boolean = false;
  @Input() signUsers?: OrderApprover[] = [];
  @Input() approvers: OrderApprover[];
  @Input() executives: OrderApprover[];

  assentApprovers: OrderApprover[];
  agreedApprovers: OrderApprover[];

  signAccounts: string[] = [];

  @Input() approvalTypes: ApprovalType[];
  @Input() approvalTypesWithoutApproval: ApprovalType[];
  @Input() defaultApprovalType: ApprovalType;
  @Input() disableApprovalType: boolean;
  @Input() hideApprovalType: boolean;

  @Input() approvalDurations: ApprovalDuration[];
  @Input() editApprovalTerm: boolean;

  @Input() defaultDuration: string;
  

  @Output() approverAdded: EventEmitter<OrderApprovalListItem> = new EventEmitter<OrderApprovalListItem>();
  @Output() approverDeleted: EventEmitter<OrderApprovalListItem> = new EventEmitter<OrderApprovalListItem>();
  @Output() approvalTypeChanged: EventEmitter<OrderApprovalListItem> = new EventEmitter<OrderApprovalListItem>();
  @Output() approverChanged: EventEmitter<OrderApprovalListItem> = new EventEmitter<OrderApprovalListItem>();
  @Output() approvalDurationChanged: EventEmitter<OrderApprovalListItem> = new EventEmitter<OrderApprovalListItem>();
  @Output() approversSwapped: EventEmitter<any> = new EventEmitter<any>();

  constructor() {
  }

  ngOnInit() {
    this.approvalTypesWithoutApproval = this.approvalTypes.filter(at => at.approvalTypeCode !== ApprovalType.approval);

    this.signAccounts = this.signUsers.map((u) => u.accountName);

    this.assentApprovers = this.approvers;
    this.agreedApprovers = this.approvers.filter((e) => this.signAccounts.indexOf(e.accountName) !== -1);
  }

  ngOnChanges(changes) {
    if (changes.signUsers) {
      this.signAccounts = changes.signUsers.currentValue.map((u) => u.accountName);
    }

    if (changes.approvers) {
      this.assentApprovers = changes.approvers.currentValue;
      this.agreedApprovers = changes.approvers.currentValue.filter((e) => this.signAccounts.indexOf(e.accountName) !== -1);
    }
  }

  onApproverAdded(item: ApprovalNode) {
    if (this.defaultApprovalType) {
      item.val.approvalType = this.defaultApprovalType.approvalType;
      item.val.approvalTypeCode = this.defaultApprovalType.approvalTypeCode;
    }
    this.approverAdded.emit(item.val);
  }

  onApproverDeleted(item: ApprovalNode) {
    this.approverDeleted.emit(item.val);
  }

  onApprovalTypeChanged(item: ApprovalNode | OrderApprovalListItem) {
    // @ts-ignore
    let agreed = item.val || item;
    let approvalType = this.approvalTypes.find(at => at.approvalTypeCode === agreed.approvalTypeCode);
    agreed.approvalType = approvalType && approvalType.approvalType;
    this.approvalTypeChanged.emit(agreed);
  }

  onApproverChanged(item: OrderApprovalListItem) {
    let agreed = item;
    this.approverChanged.emit(agreed);
  }

  onApprovalDurationChanged(item: OrderApprovalListItem) {
    let agreed = item;
    let approvalDuration = this.approvalDurations.find(d => d.Code === agreed.approvalTerm.code);
    agreed.approvalTerm = ApprovalTerm.fromApprovalDuration(approvalDuration);
    this.approvalDurationChanged.emit(agreed);
  }

  onApproversSwapped() {
    this.approversSwapped.emit(null);
  }

  getApprovalTypes(agreed) {
    let hasApproval = this.hasApproval();
    return !hasApproval || agreed.approvalTypeCode === ApprovalType.approval ? this.approvalTypes :
      this.approvalTypesWithoutApproval;
  }

  calcNotUsed() {
    // const usedAccounts = this.order.approval.approvalCycle.agreed.filter((a) => (!!a.agreedBy)).map((a) => (a.agreedBy.accountName));
    // this.assentApproversNotUsed = this.assentApprovers.filter((a) => (usedAccounts.indexOf(a.accountName) === -1));
  }

  getApprovers(agreed) {
    // if (agreed.Signer) {
    //   if (this.head) {
    //     return [this.head];
    //   } else {
    //     return [];
    //   }
    // }

    if (agreed.approvalTypeCode === ApprovalType.agreed) {
      if (this.signUsers) {
        const signAccounts = this.signUsers.map((u) => u.accountName);

        return this.approvers.filter((e) => signAccounts.indexOf(e.accountName) !== -1);
      } else {
        return this.approvers;
      }
    }

    if (agreed.approvalTypeCode === ApprovalType.assent) {
      return this.approvers;
    }

    return [];
  }

}
