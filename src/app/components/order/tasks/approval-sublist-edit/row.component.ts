import { Component, EventEmitter, Input, OnInit, Output, ViewContainerRef, ViewChild } from '@angular/core';
import { ApprovalType } from '../../../../models/order/ApprovalType';
import { ApprovalNode } from '../../../../services/approval-util.service';
import { OrderApprovalListItem, OrderApprover, ApprovalTerm } from '../../../../models/order/Order';
import { ApprovalDuration } from '../../../../models/order/ApprovalDuration';
import { find } from 'lodash';

@Component({
  selector: 'mggt-approval-sublist-edit-row',
  templateUrl: './row.component.html'
})
export class ApprovalSublistEditRowComponent implements OnInit {

  @Input() list: ApprovalNode[];
  @Input() level: number;
  @Input() approvalDurations: ApprovalDuration[];
  @Input() disableApprovalType: boolean;
  @Input() head: OrderApprover;
  @Input() headOnly: boolean = false;
  @Input() hideApprovalType: boolean;
  @Input() editApprovalTerm: boolean;
  @Input() defaultDuration: string = 'OneDay';
  @Input() getApprovalTypes: (item: OrderApprovalListItem) => ApprovalType[] = () => [];
  @Input() getApprovers: (item: OrderApprovalListItem) => OrderApprover[] = () => [];
  @Input() agreedApprovers: OrderApprover[];
  @Input() assentApprovers: OrderApprover[];
  @Input() canAdd: () => boolean = () => true;
  @Input() canDelete: (agreed: OrderApprovalListItem) => boolean = () => true;
  @Input() canSwap: (ind1: number, ind2: number) => boolean = () => true;

  @Output() approverAdded: EventEmitter<ApprovalNode> = new EventEmitter<ApprovalNode>();
  @Output() approverDeleted: EventEmitter<ApprovalNode> = new EventEmitter<ApprovalNode>();
  @Output() approvalTypeChanged: EventEmitter<ApprovalNode> = new EventEmitter<ApprovalNode>();
  @Output() approverChanged: EventEmitter<ApprovalNode> = new EventEmitter<ApprovalNode>();
  @Output() approvalDurationChanged: EventEmitter<ApprovalNode> = new EventEmitter<ApprovalNode>();
  @Output() approversSwapped: EventEmitter<any> = new EventEmitter<any>();

  @ViewChild('template') template;

  opennedItemIndexes: number[] = [];

  constructor(private viewContainerRef: ViewContainerRef) {
  }

  ngOnInit() {
    this.viewContainerRef.createEmbeddedView(this.template);
  }

  toggle(itemIndex) {
    if (this.opennedItemIndexes.indexOf(itemIndex) === -1) {
      this.opennedItemIndexes.push(itemIndex);
    } else {
      this.opennedItemIndexes = this.opennedItemIndexes.filter((idx) => (idx !== itemIndex));
    }
  }

  isShowAdditionalApprovals(idx) {
    return this.opennedItemIndexes.indexOf(idx) !== -1;
  }

  addApproval() {

    let item = new ApprovalNode(new OrderApprovalListItem());

    if (this.editApprovalTerm) {
      const defaultDuration = this.approvalDurations.find((r) => (r.Code === this.defaultDuration));
      item.val.approvalTerm = ApprovalTerm.fromApprovalDuration(defaultDuration);
    }

    if (this.head && this.headOnly) {
      if (this.head.fio && !this.head.fioFull) { this.head.fioFull  = this.head.fio; }
      item.val.agreedBy = this.head;
    }

    this.list.push(item);
    this.approverAdded.emit(item);
  }

  onChildApproverAdded(item: ApprovalNode) {
    this.approverAdded.emit(item);
  }

  delApproval(ind: number) {
    let agreed = this.list[ind];
    this.list.splice(ind, 1);
    this.approverDeleted.emit(agreed);
  }

  onChildApproverDeleted(item: ApprovalNode) {
    this.approverDeleted.emit(item);
  }

  onApprovalTypeChanged(item: ApprovalNode) {
    this.approvalTypeChanged.emit(item);
  }

  onApproverChanged(item: ApprovalNode) {
    this.approverChanged.emit(item);
  }

  onApprovalDurationChanged(item: ApprovalNode) {
    this.approvalDurationChanged.emit(item);
  }

  swapApprovals(ind1: number, ind2: number) {
    let app = this.list[ind1];
    this.list[ind1] = this.list[ind2];
    this.list[ind2] = app;
    this.approversSwapped.emit(null);
  }

  onApproversSwapped() {
    this.approversSwapped.emit(null);
  }

  get isAddDisabled(): boolean {
    let r = false;
    if (this.headOnly) r = !this.head;
    return r;
  }

  get isHeadOnly(): boolean {
    return !!this.head && this.headOnly;
  }

}
