import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProcessScanComponent } from './process-scan.component';

describe('ProcessScanComponent', () => {
  let component: ProcessScanComponent;
  let fixture: ComponentFixture<ProcessScanComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProcessScanComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProcessScanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
