import * as angular from "angular";
import { ToastrService } from "ngx-toastr";
import * as _ from "lodash";
import { Observable } from "rxjs/Rx";
import { mergeMap, tap, catchError } from "rxjs/internal/operators";
import { from, of, throwError } from "rxjs/index";
import { ActiveTaskService, ActivityResourceService, ITask } from "@reinform-cdp/bpm-components";
import { LoadingStatus } from "@reinform-cdp/widgets";
import { Component, OnInit } from '@angular/core';
import { Order } from "../../../../models/order/Order";
import { OrderService } from "../../../../services/order.service";
import { FileResourceService } from "@reinform-cdp/file-resource";
import { DropzoneConfigInterface } from "ngx-dropzone-wrapper";
import {FileType} from "../../../../models/order/FileType";
import { ProcessComponent } from "../order-process";
import { SessionStorage } from "@reinform-cdp/security";
import { NsiResourceService } from "@reinform-cdp/nsi-resource";
import { CdpReporterResourceService } from "@reinform-cdp/reporter-resource";
import {HelperService} from "../../../../services/helper.service";

@Component({
    selector: 'mggt-process-scan',
    templateUrl: './process-scan.component.html',
    styleUrls: ['./process-scan.component.scss']
})
export class ProcessScanComponent extends ProcessComponent implements OnInit {

    loadingStatus: LoadingStatus;
    success = false;
    task: ITask;
    order: Order;
    dropzoneConfig: DropzoneConfigInterface;

    submitting: boolean;

    constructor(private activeTaskService: ActiveTaskService,
        public helper: HelperService,
        private activityResourceService: ActivityResourceService,
        public orderService: OrderService,
        public toastr: ToastrService,
        private fileService: FileResourceService,
        session: SessionStorage,
        nsiResourceService: NsiResourceService,
        reporterResourceService: CdpReporterResourceService,

    ) {
        super(toastr, orderService, fileService, nsiResourceService, session, reporterResourceService);
    }

    ngOnInit() {
        this.helper.beforeUnload.init();
        this.loadingStatus = LoadingStatus.LOADING;

        this.activeTaskService.getTask().pipe(
            mergeMap(task => {
                this.task = task;
                return this.getEntityIdVar(task);
            }),
            mergeMap(id => {
                return this.orderService.get(id);
            }),
            tap(order => {
                this.order = order;
                this.origOrder = angular.copy(order);
                this.initDropzone();
            })
        ).subscribe(() => {
            this.loadingStatus = LoadingStatus.SUCCESS;
            this.success = true;
        }, () => {
            this.loadingStatus = LoadingStatus.ERROR;
        });
    }

    ngOnDestroy() {
        this.helper.beforeUnload.destroy();
    }

    initDropzone() {
        let component = this;
        this.dropzoneConfig = {
            autoProcessQueue: true,
            parallelUploads: 1,
            dictDefaultMessage: 'Загрузить файл. <br/> Допустимые расширения: doc, docx, xls, xslx, pdf',
            params: {
                folderGuid: this.order.folderID,
                fileType: 'MkaDocOther',
                docEntityID: this.order.documentID,
                docSourceReference: 'UI'
            },
            accept: (file, done) => {
                this.deleteFiles(this.order.files ? this.order.files.map(file => file.fileID) : []).subscribe(() => {
                    done();
                });
            },
            init: function () {
                this.on("success", function (file, guid) {
                    this.removeFile(file);
                    const fileType: FileType = FileType.create(guid, file.name, file.size.toString(), false, new Date(), file.type, 'MkaDocOther');
                    component.order.files = component.order.files || [];
                    component.order.files.push(fileType);

                });
                this.on('error', function (file, errorMessage) {
                    this.toastr.warning(errorMessage.message ? errorMessage.message : errorMessage, 'Ошибка');
                    this.removeFile(file);
                });
            }
        }
    }

    execute() {
        this.helper.beforeUnload.start();
        // if((!this.order.privacy||!this.order.privacy.code)&&
        //     (!this.order.files||!this.order.files.length)){
        //         this.toastr.warning("Не приложен файл Отсканированная копия!");
        //         return;
        //     }
        if(!this.order.files||!this.order.files.length){
            this.toastr.warning("Не приложен файл Отсканированная копия!");
            return;
        }
        this.submitting = true;
        const canceledDocument = this.order.canceledDocument;

        this.updateOrder().pipe(
            mergeMap(() => {
                return this.activityResourceService.finishTask(parseInt(this.task.id), [{
                    "name": "RDPublishingSite",
                    "value": false,
                }]);
            }),
            mergeMap(() => {
                return canceledDocument ?
                    this.orderService.send(canceledDocument.canceledDocumentID, false) : of ('');
            })
        ).subscribe(() => {
            this.helper.beforeUnload.stop();
            this.submitting = false;
            window.location.href = '/main/#/app/tasks';
        }, () => {
            this.helper.beforeUnload.stop();
            this.submitting = false;
        })


    }

    deleteDocument({fileInfo}) {
        let ind = this.order.files.findIndex(el => el.fileID === fileInfo.idFile);
        this.order.files.splice(ind, 1);
    };

}
