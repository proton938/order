import { Component, Input, OnInit } from '@angular/core';
import { ApprovalTerm, Order, OrderApproval, OrderApprovalListItem, OrderApprover } from "../../../../models/order/Order";
import { ApprovalListEditDicts } from "../approval-sublist-edit/approval-sublist-edit.component";
import { ApprovalListAddComponent } from "../approval-list-add/approval-list-add.component";
import { ApprovalType } from "../../../../models/order/ApprovalType";
import { User } from "../../../../models/order/User";
import {ApprovalNode, getApprovalsNodes, isRoot} from "../../../../services/approval-util.service";
import {OrderService} from "../../../../services/order.service";

@Component({
  selector: 'mggt-approval-list-edit',
  templateUrl: './approval-list-edit.component.html'
})
export class ApprovalListEditComponent implements OnInit {

  @Input() order: Order;
  parallelApprovers: ApprovalNode[] = [];
  serialApprovers: ApprovalNode[] = [];
  signers: ApprovalNode[] = [];

  @Input() enableDoubleAgree: boolean;
  @Input() dicts: ApprovalListEditDicts;

  defaultDuration: any = {
    parallel: 'OneDay',
    sequential: 'OneDay'
  };

  approversFullList: OrderApprover[];
  approvers: OrderApprover[];

  executivesFullList: OrderApprover[];
  executives: OrderApprover[];

  users: { [key: string]: User } = {};

  approvalTypes: ApprovalType[];
  approvalApproveType: ApprovalType;
  defaultApprovalType: ApprovalType;

  parallelApprovalDurationCode: string;
  signDurationCode: string;

  constructor(public orderService: OrderService) {
  }

  ngOnInit() {
    if (!this.order.approval) {
      this.initApprovalList();
    }

    if (!this.order.approval.approvalCycle.agreed) {
      this.order.approval.approvalCycle.agreed = [];
    }

    this.approvalTypes = [
      this.dicts.approvalTypes.find(oat => oat.approvalTypeCode === ApprovalType.assent),
      this.dicts.approvalTypes.find(oat => oat.approvalTypeCode === ApprovalType.agreed)
    ];
    this.approvalApproveType = this.dicts.approvalTypes.find(oat => oat.approvalTypeCode === ApprovalType.approval);

    let defaultApprovalTypeCode = this.order.systemCode ? ApprovalType.agreed : ApprovalType.assent;
    this.defaultApprovalType = this.dicts.approvalTypes.find(at => at.approvalTypeCode === defaultApprovalTypeCode);

    this.order.approval.approvalCycle.agreed.filter(a => !a.approvalTime).forEach(a => this.personSetTime(a));
    this.order.approval.approvalCycle.agreed.filter(a => !a.approvalTerm).forEach(a => a.approvalTerm = new ApprovalTerm());

    this.executivesFullList = [];
    this.dicts.executives.forEach(userBean => {
      this.users[userBean.accountName] = User.fromUserBean(userBean);
      this.executivesFullList.push(OrderApprover.fromUserBean(userBean));
    });
    if (this.enableDoubleAgree) {
      this.executives = this.executivesFullList;
    }
    else {
      this.updateApproversAndExecutives();
    }

    this.approversFullList = [];
    this.dicts.approvers.forEach(userBean => {
      this.users[userBean.accountName] = User.fromUserBean(userBean);
      this.approversFullList.push(OrderApprover.fromUserBean(userBean));
    });
    if (this.enableDoubleAgree) {
      this.approvers = this.approversFullList;
    }
    else {
      this.updateApproversAndExecutives();
    }
    this.initDefaultDurations();
    this.initSublists();
    this.setNumeration();
    if (this.parallelApprovers.length) {
      let duration = this.parallelApprovers
        .map(a => a.val.approvalTerm.duration)
        .reduce((a, b) => Math.max(a, b), 0);
      let approvalDuration = this.dicts.approvalDurations.find(d => parseInt(d.Duration) === duration);
      if (approvalDuration) {
        this.parallelApprovalDurationCode = approvalDuration.Code;
      }
    }
    if (this.signers.length) {
      let duration = this.signers
        .map(a => a.val.approvalTerm.duration)
        .reduce((a, b) => Math.max(a, b), 0);
      let approvalDuration = this.dicts.approvalDurations.find(d => parseInt(d.Duration) === duration);
      if (approvalDuration) {
        this.signDurationCode = approvalDuration.Code;
      }
    }
    this.calcApprovalTermAll();
  }

  initDefaultDurations() {
    let r: any = { parallel: [], sequential: [] };
    this.dicts.approvalDurations.forEach((item: any) => {
      if (item.DefaultParallel) r.parallel.push(item.Code);
      if (item.DefaultSequential) r.sequential.push(item.Code);
    });
    if (r.parallel.length === 1) this.defaultDuration.parallel = r.parallel[0];
    if (r.sequential.length === 1) this.defaultDuration.sequential = r.sequential[0];
  }

  initSublists() {
    this.parallelApprovers = getApprovalsNodes(this.getApprovals(this.order.approval.approvalCycle.agreed, a => !a.Signer && a.Parallel));
    this.serialApprovers = getApprovalsNodes(this.getApprovals(this.order.approval.approvalCycle.agreed, a => !a.Signer && !a.Parallel));
    this.signers = getApprovalsNodes(this.getApprovals(this.order.approval.approvalCycle.agreed, a => a.Signer));
  }

  private getApprovals(agreed: OrderApprovalListItem[], predicate: (a: OrderApprovalListItem) => boolean): OrderApprovalListItem[] {
    let result = [];
    agreed.filter(predicate).filter(a => isRoot(a.approvalNum)).forEach(rootApproval => {
      let leafApprovals = agreed.filter(a => !isRoot(a.approvalNum) && a.approvalNum.startsWith(rootApproval.approvalNum))
      result = result.concat(leafApprovals);
      result.push(rootApproval);
    });
    return result;
  }

  hasApproval() {
    return this.order.approval.approvalCycle.agreed.some(a => a.approvalTypeCode === ApprovalType.approval);
  }

  canAddParallel() {
    return !this.parallelApprovers.some(a => !a.val.agreedBy || !a.val.approvalTypeCode);
  }

  canAddSerial() {
    return !this.serialApprovers.some(a => !a.val.agreedBy || !a.val.approvalTypeCode || !a.val.approvalTerm);
  }

  canAddSigner() {
    return !this.signers.length;
  }

  parallelApprovalDurationCodeChanged() {
    if (this.parallelApprovalDurationCode) {
      let duration = this.dicts.approvalDurations.find(ad => ad.Code === this.parallelApprovalDurationCode);
      if(!this.parallelApprovers.length){
        let item = new ApprovalNode(new OrderApprovalListItem());
        if(this.defaultApprovalType){
          item.val.approvalType = this.defaultApprovalType.approvalType;
          item.val.approvalTypeCode = this.defaultApprovalType.approvalTypeCode;
        }   
        item.val.Parallel = true;
        item.val.Signer = false;      
        item.val.approvalTerm = ApprovalTerm.fromApprovalDuration(duration);  
        this.parallelApprovers.push(item);        
        this.updateApproversAndExecutives();
        this.sortApprovalsAndSetNumeration(); 
      } 
      else this.parallelApprovers.forEach(a => a.val.approvalTerm = ApprovalTerm.fromApprovalDuration(duration));
    } else {
      this.parallelApprovers.forEach(a => a.val.approvalTerm = null);
    }
    this.calcApprovalTermAll()
  }

  parallelApproverAdded(agreed: OrderApprovalListItem) {
    if(!this.parallelApprovalDurationCode){
      this.parallelApprovalDurationCode = this.defaultDuration.parallel;
      this.parallelApprovalDurationCodeChanged();
    }
    else {//if (this.parallelApprovalDurationCode) {
      let duration = this.dicts.approvalDurations.find(ad => ad.Code === this.parallelApprovalDurationCode);
      agreed.approvalTerm = ApprovalTerm.fromApprovalDuration(duration);
    }
    agreed.Parallel = true;
    agreed.Signer = false;
    this.updateApproversAndExecutives();
    this.sortApprovalsAndSetNumeration();

  }

  serialApproverAdded(agreed: OrderApprovalListItem) {
    agreed.Parallel = false;
    agreed.Signer = false;
    this.updateApproversAndExecutives();
    this.sortApprovalsAndSetNumeration();

    this.calcApprovalTermAll()
  }

  signDurationCodeChanged() {
    if (this.signDurationCode) {
      let duration = this.dicts.approvalDurations.find(ad => ad.Code === this.signDurationCode);
      this.signers.forEach(a => a.val.approvalTerm = ApprovalTerm.fromApprovalDuration(duration));
      this.orderService.signDurationCode = true;
    } else {
      this.signers.forEach(a => a.val.approvalTerm = null);
      this.orderService.signDurationCode = false;
    }
    this.calcApprovalTermAll();
  }

  signerAdded(agreed: OrderApprovalListItem) {
    if (this.signDurationCode) {
      let duration = this.dicts.approvalDurations.find(ad => ad.Code === this.signDurationCode);
      agreed.approvalTerm = ApprovalTerm.fromApprovalDuration(duration);
    }
    agreed.Signer = true;

    this.updateApproversAndExecutives();
    this.sortApprovalsAndSetNumeration();
  }

  approverDeleted() {
    if(!!this.parallelApprovalDurationCode&&!this.parallelApprovers.length){
      this.parallelApprovalDurationCode=null;
      this.parallelApprovalDurationCodeChanged();
    }
    this.updateApproversAndExecutives();
    this.sortApprovalsAndSetNumeration();
    this.setHead();
    this.calcApprovalTermAll();
  }

  approverChanged(agreed: OrderApprovalListItem) {
    this.personSetTime(agreed);
    this.updateApproversAndExecutives();
    this.sortApprovalsAndSetNumeration();
    this.setHead();
  }

  approvalTypeChanged(agreed: OrderApprovalListItem) {
    if(!!agreed.approvalTypeCode){
      let validationList=this.approversFullList;
      if(agreed.approvalTypeCode=='agreed'){
        let signAccounts = this.dicts.signUsers.map((u) => u.accountName);
        validationList = this.approversFullList.filter((e) => signAccounts.indexOf(e.accountName) !== -1);
      }        
      console.log(validationList);
      if (agreed.agreedBy) {
        const exists = validationList.find((a) => a.accountName === agreed.agreedBy.accountName);
        if (!exists) {
          agreed.agreedBy = null;
        }
      }
    }
    this.personSetTime(agreed);
    this.updateApproversAndExecutives()
    this.sortApprovalsAndSetNumeration();
    this.setHead();
  }

  approvalDurationChanged() {
    this.calcApprovalTermAll();
  }

  approversSwapped() {
    this.sortApprovalsAndSetNumeration();
  }

  headChanged() {
    if (this.order.head && this.order.signingElectronically) {
      let item = new OrderApprovalListItem();
      item.approvalTypeCode = this.approvalApproveType.approvalTypeCode;
      item.approvalType = this.approvalApproveType.approvalType;
      item.agreedBy = OrderApprover.fromUser(this.order.head);
      item.Signer = true;

      this.personSetTime(item);
      
      this.signers = [new ApprovalNode(item)];
    } else {
      this.signers = [];
    }
    this.signDurationCode = null;

    this.parallelApprovers.forEach((a) => {
      if (a.val.agreedBy && a.val.agreedBy.accountName === this.order.head.accountName) {
        a.val.agreedBy = null;
      }
    });
    this.serialApprovers.forEach((a) => {
      if (a.val.agreedBy && a.val.agreedBy.accountName === this.order.head.accountName) {
        a.val.agreedBy = null;
      }
    });

    this.sortApprovalsAndSetNumeration();
  }

  initApprovalList() {
    let historyLength = this.order.approvalHistory && this.order.approvalHistory.approvalCycle ?
      this.order.approvalHistory.approvalCycle.length : 0;
    this.order.approval = new OrderApproval(historyLength + 1);
  }

  setNumeration() {
    this.setNodesNumeration(this.parallelApprovers);
    this.setNodesNumeration(this.serialApprovers, this.parallelApprovers.length);
    this.setNodesNumeration(this.signers, this.parallelApprovers.length + this.serialApprovers.length);
  }

  setNodesNumeration(nodes: ApprovalNode[], offset: number = 0, prefix: string = null) {
    nodes.forEach((node, index) => {

      let approvalNum = '' + (index + offset + 1);
      if (prefix) {
        approvalNum = prefix + '.' + approvalNum;
      }
      node.val.approvalNum = approvalNum;
      if (node.items && node.items.length) {
        this.setNodesNumeration(node.items, 0, approvalNum);
      }
    });
  }

  sortApprovalsAndSetNumeration() {
    this.setNumeration();
    let result: OrderApprovalListItem[] = [];
    this.addNodes(this.parallelApprovers, result);
    this.addNodes(this.serialApprovers, result);
    this.addNodes(this.signers, result);

    this.order.approval.approvalCycle.agreed = result;
  }

  addNodes(nodes: ApprovalNode[], array: OrderApprovalListItem[]) {
    nodes.forEach(node => {
      if (node.items) {
        this.addNodes(node.items, array);
      }
      array.push(node.val);
    })
  }

  isSigningElectronically() {
    return this.order.signingElectronically && this.order.signingElectronically.toString() === 'true';
  }

  getOrderApprover(user: User): OrderApprover {
    return OrderApprover.processOrderApprover({
      post: user.post,
      fio: user.fio,
      fioFull: user.fio || (<any>user.fioFull),
      accountName: user.accountName,
      fioShort: user.fioShort,
      iofShort: null,
      phone: user.phoneNumber
    });
  }

  updateApproversAndExecutives() {
    if (this.enableDoubleAgree) {
      return;
    }
    if (this.approversFullList) {
      this.approvers = this.approversFullList.filter(a => {
        return !this.order.approval.approvalCycle.agreed || !this.order.approval.approvalCycle.agreed.find(agreed => {
          return agreed.agreedBy && agreed.agreedBy.accountName === a.accountName;
        });
      });
    }

    if (this.executivesFullList) {
      this.executives = this.executivesFullList.filter(a => {
        return !this.order.approval.approvalCycle.agreed || !this.order.approval.approvalCycle.agreed.find(agreed => {
          return agreed.agreedBy && agreed.agreedBy.accountName === a.accountName;
        });
      });
    }
  }

  setHead() {
    const signingElectronically = this.isSigningElectronically();
    if (signingElectronically) {
      const approver = this.order.approval.approvalCycle.agreed.find(a => a.approvalTypeCode === ApprovalType.approval);
      if (approver) {
        let user = this.users[approver.agreedBy.accountName];
        let department = user ? user.department : null;
        this.order.head = ApprovalListEditComponent.getUser(approver.agreedBy, department);
      } else {
        this.order.head = null;
      }
    }
  }

  personSetTime(agreed: OrderApprovalListItem) {
    if (!agreed.agreedBy) return;
    let user = this.users[agreed.agreedBy.accountName];
    let department = user ? user.department : null;
    let approvalType = this.dicts.approvalTypes.find(at => at.approvalTypeCode === agreed.approvalTypeCode && at.department === department);
    if (!approvalType) {
      approvalType = this.dicts.approvalTypes.find(at => at.approvalTypeCode === agreed.approvalTypeCode && !at.department);
    }
    agreed.approvalTime = approvalType && approvalType.approvalDuration;
  }

  calcApprovalTermAll() {
    let serialApprovalTermNotSet = this.serialApprovers.some(a => !a.val.approvalTerm);
    let parallelDuration = 0;
    let serialDuration = 0;
    let signDuration = 0;

    if (this.parallelApprovalDurationCode) {
      parallelDuration = parseInt(this.dicts.approvalDurations.find(d => d.Code === this.parallelApprovalDurationCode).Duration);
    }

    serialDuration = this.serialApprovers.filter(a => a.val.approvalTerm.duration)
      .map(a => a.val.approvalTerm.duration).reduce((a, b) => a + b, 0);

    if (this.signDurationCode) {
      signDuration = parseInt(this.dicts.approvalDurations.find(d => d.Code === this.signDurationCode).Duration);
    }

    if (parallelDuration + serialDuration + signDuration > 0) {
      this.order.approval.approvalCycle.approvalTermAll = { duration: parallelDuration + serialDuration + signDuration };
    } else {
      this.order.approval.approvalCycle.approvalTermAll = null;
    }
  }

  static getUser(approver: OrderApprover, department: string): User {
    if (!approver) {
      return null;
    }
    return {
      post: approver.post,
      fio: approver.fioFull,
      accountName: approver.accountName,
      email: null,
      phoneNumber: approver.phone,
      iofShort: approver.iofShort,
      department: department
    }
  }

}
