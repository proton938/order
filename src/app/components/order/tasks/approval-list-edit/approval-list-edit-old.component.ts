import * as _ from 'lodash';
import { UserBean } from '@reinform-cdp/nsi-resource';
import * as angular from 'angular';
import { Component, Input, OnInit } from '@angular/core';
import { Order, OrderApprover } from '../../../../models/order/Order';
import { ApprovalType } from '../../../../models/order/ApprovalType';
import { User } from '../../../../models/order/User';
import { OrderApproval } from '../../../../models/order/Order';
import { OrderApprovalListItem } from '../../../../models/order/Order';

export class ApprovalListEditDicts {
  approvalTypes: ApprovalType[];
  approvers: UserBean[];
}
@Component({
  selector: 'mggt-approval-list-edit-old',
  templateUrl: './approval-list-edit-old.component.html'
})
export class ApprovalListEditOldComponent implements OnInit {

  @Input() order: Order;
  @Input() signingElectronically: boolean;
  @Input() approvalTypes: ApprovalType[];
  @Input() signUsers: UserBean[];
  @Input() approvers: UserBean[];
  @Input() executive: UserBean;

  approvalTypesAll: ApprovalType[] = [];

  assentApprovers: OrderApprover[];
  assentApproversNotUsed: OrderApprover[];
  agreedApprovers: OrderApprover[];
  agreedApproversNotUsed: OrderApprover[];

  approversUsers: OrderApprover[];
  users: { [key: string]: User } = {};
  executiveUsers: { [key: string]: User } = {};

  constructor() {
  }

  ngOnInit() {
    this.order.approval = this.order.approval || new OrderApproval(1);

    if (!this.order.approval.approvalCycle.agreed) {
      this.order.approval.approvalCycle.agreed = [];
    }
    this.order.approval.approvalCycle.agreed.forEach((a) => {
      a.agreedBy = OrderApprover.processOrderApprover(a.agreedBy);
    });

    this.approvers.forEach((userBean) => {
      this.users[userBean.accountName] = User.fromUserBean(userBean);
    });

    this.approvalTypesAll = angular.copy(this.approvalTypes);

    this.approvalTypes = this.approvalTypes.filter((at) => (at.approvalTypeCode !== 'approval'));

    this.executivesChanged();

    const signAccounts = this.signUsers.map((u) => u.accountName);
    this.assentApprovers = this.approvers.map(OrderApprover.fromUserBean);
    this.agreedApprovers = this.assentApprovers.filter((e) => signAccounts.indexOf(e.accountName) !== -1);

    this.calcNotUsed();
  }

  addApproval() {
    const agreed = new OrderApprovalListItem();
    agreed.approvalNum = (this.order.approval.approvalCycle.agreed.length + 1).toString();

    this.order.approval.approvalCycle.agreed.push(agreed);
    this.sortApprovers();
  }

  executivesChanged() {
    this.executiveUsers = {};

    if (!this.executive) {
      return;
    }

    this.users[this.executive.accountName] = User.fromUserBean(this.executive);
    this.executiveUsers[this.executive.accountName] = User.fromUserBean(this.executive);

    const alreadyAdded = this.order.approval.approvalCycle.agreed.some((a) => {
      if (a.agreedBy && a.agreedBy.accountName === this.executive.accountName) {
        return true;
      }

      return false;
    });

    const hasApproval = this.order.approval.approvalCycle.agreed.some((a) => {
      return a.approvalTypeCode === ApprovalType.approval;
    });

    if (!alreadyAdded) {
      if (this.signingElectronically) {
        if (!hasApproval) {
          const agreed = new OrderApprovalListItem();
          agreed.agreedBy = OrderApprover.processOrderApprover(OrderApprover.fromUser(this.order.head));
          agreed.approvalTypeCode = ApprovalType.approval;
          agreed.approvalType = 'Утверждение';
          this.personSetTime(agreed);

          this.order.approval.approvalCycle.agreed.push(agreed);
        }
      } else {
        const agreed = new OrderApprovalListItem();
        agreed.agreedBy = OrderApprover.processOrderApprover(OrderApprover.fromUser(this.order.head));

        this.order.approval.approvalCycle.agreed.push(agreed);
      }
    }

    if (this.signingElectronically) {
      this.order.approval.approvalCycle.agreed.forEach((a) => {
        if (alreadyAdded) {
          if (a.approvalTypeCode === 'approval') {
            if (a.agreedBy && a.agreedBy.accountName !== this.executive.accountName) {
              a.agreedBy = null;
              a.approvalTypeCode = null;
              a.approvalType = null;
            }
          }

          if (a.agreedBy && a.agreedBy.accountName === this.executive.accountName) {
            a.approvalTypeCode = 'approval';
            a.approvalType = 'Утверждение';
          }
        } else {
          if (a.approvalTypeCode === 'approval') {
            if (a.agreedBy && a.agreedBy.accountName !== this.executive.accountName) {
              a.agreedBy = OrderApprover.processOrderApprover(OrderApprover.fromUser(this.order.head));
            }
          }
        }
      });
    }

    this.order.approval.approvalCycle.agreed.forEach((a) => {
      this.personSetTime(a);
    });
  }

  calcNotUsed() {
    const signAccounts = this.signUsers.map((u) => u.accountName);
    const usedAccounts = this.order.approval.approvalCycle.agreed.filter((a) => (!!a.agreedBy)).map((a) => (a.agreedBy.accountName));

    this.assentApproversNotUsed = this.assentApprovers.filter((a) => (usedAccounts.indexOf(a.accountName) === -1));
    this.agreedApproversNotUsed = this.assentApproversNotUsed.filter((e) => signAccounts.indexOf(e.accountName) !== -1);
  }

  approverChanged(agreed: OrderApprovalListItem) {
    agreed.agreedBy = _.clone(agreed.agreedBy);
    this.personSetTime(agreed);
    this.sortApprovers();
    this.calcNotUsed();
  }

  sortApprovers() {
    this.order.approval.approvalCycle.agreed = this.order.approval.approvalCycle.agreed.sort((a: OrderApprovalListItem, b: OrderApprovalListItem) => {
      if (a.agreedBy && !!this.executiveUsers[a.agreedBy.accountName]) {
        return 1;
      }

      if (b.agreedBy && !!this.executiveUsers[b.agreedBy.accountName]) {
        return -1;
      }

      return +a.approvalNum > +b.approvalNum ? 1 : -1;
    });

    this.setNumeration();
  }

  personSetTime(agreed: OrderApprovalListItem) {
    if (!agreed.agreedBy) {
      return;
    }

    const user = this.users[agreed.agreedBy.accountName];
    const department = user ? user.department : null;
    let approvalType = this.approvalTypesAll.find(at => at.approvalTypeCode === agreed.approvalTypeCode && at.department === department);

    if (!approvalType) {
      approvalType = this.approvalTypesAll.find(at => at.approvalTypeCode === agreed.approvalTypeCode && !at.department);
    }

    agreed.approvalTime = approvalType && approvalType.approvalDuration;
  }

  approvalTypeChanged(ind: number) {
    if (!this.order.approval.approvalCycle.agreed[ind].approvalTypeCode) {
      this.order.approval.approvalCycle.agreed[ind].approvalType = null;
      this.order.approval.approvalCycle.agreed[ind].approvalTypeCode = null;
      return;
    }

    const approvalType = _.find(this.approvalTypes, (at: any) => {
      return at.approvalTypeCode === this.order.approval.approvalCycle.agreed[ind].approvalTypeCode;
    });

    this.order.approval.approvalCycle.agreed[ind].approvalType = approvalType.approvalType;
    this.order.approval.approvalCycle.agreed[ind].approvalTypeCode = approvalType.approvalTypeCode;

    if(!!this.order.approval.approvalCycle.agreed[ind].approvalTypeCode){
      let validationList;

      validationList = this.assentApprovers;
      if(approvalType.approvalTypeCode=='agreed')
        validationList = this.agreedApprovers;

      if (this.order.approval.approvalCycle.agreed[ind].agreedBy) {
        const exists = validationList.find((a) => a.accountName === this.order.approval.approvalCycle.agreed[ind].agreedBy.accountName);

        if (!exists) {
          this.order.approval.approvalCycle.agreed[ind].agreedBy = null;
          this.calcNotUsed();
        }
      }
    }   
    this.personSetTime(this.order.approval.approvalCycle.agreed[ind]);
  }

  setNumeration() {
    _.each(this.order.approval.approvalCycle.agreed, (item, index) => {
      item.approvalNum = '' + (index + 1);
    });
  }

  swapApprovals(ind1: number, ind2: number) {
    const agreed = this.order.approval.approvalCycle.agreed;
    const approver = agreed[ind1];
    agreed[ind1] = agreed[ind2];
    agreed[ind2] = approver;

    this.setNumeration();
    this.sortApprovers();
  }

  delApproval(ind: number) {
    this.order.approval.approvalCycle.agreed.splice(ind, 1);
    this.setNumeration();
    this.calcNotUsed();
  }

}
