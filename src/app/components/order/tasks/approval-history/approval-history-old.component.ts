import { Component, Input, OnInit } from '@angular/core';
import { OrderApprovalHistory, OrderApprovalHistoryList } from '../../../../models/order/Order';
import * as angular from 'angular';

@Component({
  selector: 'mggt-approval-history-old',
  templateUrl: './approval-history-old.component.html'
})
export class ApprovalHistoryOldComponent implements OnInit {
  @Input() history: OrderApprovalHistory;
  // cycleShow: boolean[] = [];
  historyList:OrderApprovalHistoryList[]=[];

  constructor() { }

  ngOnInit() {
    if (this.history) {
      this.historyList = angular.copy(this.history.approvalCycle).reverse();
      // .sort((a,b)=>{
      //   return a.approvalCycleDate>b.approvalCycleDate?-1:1;
      // });
    //   this.history.approvalCycle.forEach(() => {
    //     this.cycleShow.push(false);
    //   });
    }
  }
}
