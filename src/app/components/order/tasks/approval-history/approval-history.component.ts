import { Component, Input, OnInit } from '@angular/core';
import { OrderApprovalHistory } from '../../../../models/order/Order';

@Component({
  selector: 'mggt-approval-history',
  templateUrl: './approval-history.component.html',
  styleUrls: ['./approval-history.component.scss']
})
export class ApprovalHistoryComponent implements OnInit {
  @Input() history: OrderApprovalHistory;
  cycleShow: boolean[] = [];

  constructor() { }

  ngOnInit() {
    if (this.history && this.history.approvalCycle) {
      this.history.approvalCycle.forEach(() => {
        this.cycleShow.push(false);
      });
    }
  }
}
