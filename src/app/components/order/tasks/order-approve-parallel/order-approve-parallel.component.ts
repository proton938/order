import { CdpReporterResourceService } from "@reinform-cdp/reporter-resource";
import { NsiResourceService } from "@reinform-cdp/nsi-resource";
import * as _ from "lodash";
import { FileResourceService } from "@reinform-cdp/file-resource";
import { BsModalService } from "ngx-bootstrap";
import { Component} from '@angular/core';
import { ActiveTaskService, ActivityResourceService, CdpBpmHistoryResourceService } from "@reinform-cdp/bpm-components";
import { ToastrService } from "ngx-toastr";
import { OrderService } from "../../../../services/order.service";
import { OrderTypeService } from "../../../../services/order-type.service";
import { RegisterDocumentService } from "../../../../services/register-document.service";
import { CancelDocumentService } from "../../../../services/cancel-document.service";
import { OrderActivitiService } from "../../../../services/order-activiti.service";
import { OrderApproveNewComponent } from "../order-approve/order-approve-new.component";
import { SessionStorage, SignService } from "@reinform-cdp/security";
import {HelperService} from "../../../../services/helper.service";

@Component({
  selector: 'mggt-order-approve-parallel',
  templateUrl: './order-approve-parallel.component.html',
  styleUrls: ['./order-approve-parallel.component.scss']
})
export class OrderApproveParallelComponent extends OrderApproveNewComponent {
  constructor(activeTaskService: ActiveTaskService,
    helper: HelperService,
    orderTypeService: OrderTypeService,
    activityResourceService: ActivityResourceService,
    registerDocumentService: RegisterDocumentService,
    modalService: BsModalService,
    cancelDocumentService: CancelDocumentService,
    orderActivitiService: OrderActivitiService,
    signService: SignService,
    toastr: ToastrService,
    orderService: OrderService,
    fileResourceService: FileResourceService,
    nsiResourceService: NsiResourceService,
    sessionStorage: SessionStorage,
    reporterResourceService: CdpReporterResourceService,
    historyResourceService: CdpBpmHistoryResourceService) {
    super(activeTaskService, helper, orderTypeService, activityResourceService, registerDocumentService, modalService, cancelDocumentService, orderActivitiService, signService, toastr, orderService, fileResourceService, nsiResourceService, sessionStorage, reporterResourceService, historyResourceService);
  }

  preSendToWork() {
    return this.approvalNote
        ? this.sendBackToWork()
        : this.helper.alert.message({
          message: 'Для отправки документа на доработку необходимо добавить комментарий'
        }).then(res=> {}).catch(err => {});
  }
}
