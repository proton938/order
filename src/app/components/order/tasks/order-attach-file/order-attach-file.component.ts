import { ExFileType, LoadingStatus } from "@reinform-cdp/widgets";
import * as angular from "angular";
import * as _ from "lodash";
import { FileResourceService } from "@reinform-cdp/file-resource";
import { ActiveTaskService, ActivityResourceService, ITask } from "@reinform-cdp/bpm-components";
import { Observable } from "rxjs/Rx";
import { DepartmentBean, NsiResourceService, UserBean } from "@reinform-cdp/nsi-resource";
import { AuthorizationService, SessionStorage } from "@reinform-cdp/security";
import { ToastrService } from "ngx-toastr";
import { forkJoin,from } from "rxjs/index";
import { mergeMap, tap,map } from "rxjs/internal/operators";
import { Component, OnInit, ViewChild } from '@angular/core';
import { OrderApprovalListItem, OrderDraftFile, OrderApproval, OrderApprover, ApprovalTerm } from "../../../../models/order/Order";
import { OrderType } from "../../../../models/order/OrderType";
import { OrderTypeService } from "../../../../services/order-type.service";
import { OrderService } from "../../../../services/order.service";
import { ProcessComponent } from "../order-process";
import { ApprovalType } from "../../../../models/order/ApprovalType";
import { DropzoneConfigInterface } from "ngx-dropzone-wrapper";
import { CdpReporterResourceService } from "@reinform-cdp/reporter-resource";
import { ApprovalListEditComponent } from "../approval-list-edit/approval-list-edit.component";
import { EmailGroup } from "../../../../models/order/EmailGroup";
import { OrderStatus } from "../../../../models/order/OrderStatus";
import { UserService } from "../../../../services/user.service";
import { ApprovalDuration } from "../../../../models/order/ApprovalDuration";
import { OrderNsiService } from "../../../../services/order-nsi.service";
import {ApprovalNode, getApprovalsNodes, isRoot} from "../../../../services/approval-util.service";
import { User } from "../../../../models/order/User";
import { formatAs } from '../../../../services/date-util.service';
import {HelperService} from "../../../../services/helper.service";

@Component({
  selector: 'mggt-order-attach-file',
  templateUrl: './order-attach-file.component.html',
  styleUrls: ['./order-attach-file.component.scss']
})
export class OrderAttachFileComponent extends ProcessComponent implements OnInit {

  loadingStatus: LoadingStatus;
  success = false;
  task: ITask;
  account: any = {};
  orderType: OrderType;

  orderTypes: OrderType[];
  approvers: UserBean[];
  approvalTypes: ApprovalType[];
  approvalDurations: ApprovalDuration[];

  defaultApprovalType: ApprovalType;
  parallelApprovers:ApprovalNode[] = [];
  parallelApprovalDurationCode: string;
  approversFullList: OrderApprover[];
  approverItems: OrderApprover[];
  users: { [key: string]: User } = {};

  saving: boolean;
  sendingToApproval: boolean;
  creatingDraftFiles:boolean;

  draftFiles:any[]=[];
  

  constructor(private orderTypeService: OrderTypeService,
    public helper: HelperService,
    private activeTaskService: ActiveTaskService, private authorizationService: AuthorizationService,
    private activityResourceService: ActivityResourceService,
    private userService: UserService, private orderNsiService: OrderNsiService,
    toastr: ToastrService, orderService: OrderService, fileResourceService: FileResourceService,
    nsiResourceService: NsiResourceService, session: SessionStorage,
    reporterResourceService: CdpReporterResourceService) {
    super(toastr, orderService, fileResourceService, nsiResourceService, session, reporterResourceService);
  }

  ngOnInit() {
    this.helper.beforeUnload.init();
    this.loadingStatus = LoadingStatus.LOADING;
    
    this.account.accountName = this.session.login();
    this.account.fio = this.session.name();
    this.account.post = this.session.post();

    this.activeTaskService.getTask().pipe(
      mergeMap(task => {
        this.task = task;
        return this.getEntityIdVar(task);
      }),
      mergeMap(id => {
        return this.orderService.get(id);
      }),
      mergeMap(order => {
        this.order = order;
        this.origOrder = angular.copy(order);

        return this.loadDictionaries();
      })
    ).subscribe(() => {
      if (!this.order.approval) {
        this.initApprovalList();
      }
      if (!this.order.approval.approvalCycle.agreed) {
        this.order.approval.approvalCycle.agreed = [];
      }
      let defaultApprovalTypeCode = ApprovalType.assent;
      this.defaultApprovalType = this.approvalTypes.find(at => at.approvalTypeCode === defaultApprovalTypeCode);

      this.order.approval.approvalCycle.agreed.filter(a => !a.approvalTime).forEach(a => this.personSetTime(a));
      this.order.approval.approvalCycle.agreed.filter(a => !a.approvalTerm).forEach(a => a.approvalTerm = new ApprovalTerm());
      this.approversFullList = [];
      this.approvers.forEach(userBean => {
        this.users[userBean.accountName] = User.fromUserBean(userBean);
        this.approversFullList.push(OrderApprover.fromUserBean(userBean));
      });
      this.updateApproversAndExecutives();
      this.initSublists();
      this.setNumeration();
      if (this.parallelApprovers.length) {
        let duration = this.parallelApprovers
          .map(a => a.val.approvalTerm.duration)
          .reduce((a, b) => Math.max(a, b), 0);
        let approvalDuration = this.approvalDurations.find(d => parseInt(d.Duration) === duration);
        if (approvalDuration) {
          this.parallelApprovalDurationCode = approvalDuration.Code;
        }
      }

      if (this.order.draftFiles) {
        this.draftFiles = [{
            nameFile: this.order.draftFiles.draftFilesName,
            idFile: this.order.draftFiles.draftFilesID,
            sizeFile: this.order.draftFiles.draftFilesSize,
            dateFile: this.order.draftFiles.draftFilesDate,
            signed: this.order.draftFiles.draftFilesSigned,
            mimeType: this.order.draftFiles.draftFilesMimeType,
            typeFile: this.order.draftFiles.draftFilesType
        }];
      }

      this.loadingStatus = LoadingStatus.SUCCESS;
      this.success = true;
    }, () => {
      this.loadingStatus = LoadingStatus.ERROR;
    });
  }

  ngOnDestroy() {
    this.helper.beforeUnload.destroy();
  }

  loadDictionaries() {
    return forkJoin([
      this.orderTypeService.geOrderTypes(),
      this.nsiResourceService.get('mggt_order_ApprovalType'),
      this.nsiResourceService.get('mggt_order_AppDuration')
    ]).pipe(
      mergeMap((result: [OrderType[],  ApprovalType[], ApprovalDuration[]]) => {//DepartmentBean[],EmailGroup[], OrderStatus[], 
        this.orderTypes = result[0];
        this.orderType = this.orderTypes.find(_ => {
          return _.code === this.order.documentTypeCode;
        });
        this.approvalTypes = result[1].filter(
          item=>item.approvalTypeCode==ApprovalType.assent);
        this.approvalDurations = result[2];
        
        return this.userService.getApprovers(this.orderType);
      }),
      tap(approvers => {
        this.approvers = approvers;
      })
    )
  }

  deleteDraft(fileInfo: any) {
    from(this.fileResourceService.deleteFile(fileInfo.idFile)).subscribe(() => {
      this.order.draftFiles = null;
    });    
  }

  changeDraftFiles() {
      if (this.draftFiles.length > 0) {
          this.order.draftFiles = OrderDraftFile.create(
            this.draftFiles[0].idFile,
            this.draftFiles[0].nameFile,
            this.draftFiles[0].sizeFile,
            this.draftFiles[0].signed,
            this.draftFiles[0].dateFile,
            this.draftFiles[0].typeFile,
            this.draftFiles[0].mimeType
          );
      } else {
          this.order.draftFiles = null;
      }
  }
  generateDraftFile() {
    const document: any = _.clone(this.order);
    // document.documentDate = formatAs(Date.now(), 'dd.MM.yyyy');
    // document.documentNumber = this.order.approvedDocumentNumber;


    let templateCode = 'AdmOrderPWithoutSignCorrections';

    let request: any = {
      options: {
        jsonSourceDescr: '',
        onProcess: 'Сформировать проект РД',
        placeholderCode: '',
        strictCheckMode: true,
        xwpfResultDocumentDescr: 'Проект РД.docx',
        xwpfTemplateDocumentDescr: ''
      },
      jsonTxt: JSON.stringify({ document: this.order }),
      rootJsonPath: '$',
      nsiTemplate: {
        templateCode: templateCode,
      },
      filenetDestination: {
        fileName: '' +
            'Проект РД.docx',
        fileType: 'defaul',
        folderGuid: this.order.folderID,
        fileAttrs: [
          { attrName: 'docEntityID', attrValues: [this.order.documentID] },
          { attrName: 'docSourceReference', attrValues: ['UI'] },
        ]
      }
    };
    this.creatingDraftFiles=true;
    from(this.reporterResourceService.nsi2Filenet(request)).pipe(
      map(result => {
        return OrderDraftFile.create(result.versionSeriesGuid, result.fileName, result.fileSize.toString(), false, new Date(), result.fileType, result.mimeType);
      })
    ).subscribe(result=>{
      this.toastr.success('Файл сформирован');
      this.order.draftFiles=result;
      this.draftFiles = [{
        nameFile: this.order.draftFiles.draftFilesName,
        idFile: this.order.draftFiles.draftFilesID,
        sizeFile: this.order.draftFiles.draftFilesSize,
        dateFile: this.order.draftFiles.draftFilesDate,
        signed: this.order.draftFiles.draftFilesSigned,
        mimeType: this.order.draftFiles.draftFilesMimeType,
        typeFile: this.order.draftFiles.draftFilesType
      }];
      this.creatingDraftFiles=false;
    },error=>{
      this.creatingDraftFiles=false;
      console.log(error);
      this.toastr.error('Не удалось сформировать файл');
    });
  }

  initSublists() {
    this.parallelApprovers = getApprovalsNodes(this.getApprovals(this.order.approval.approvalCycle.agreed, a => !a.Signer && a.Parallel));
  }
  private getApprovals(agreed: OrderApprovalListItem[], predicate: (a: OrderApprovalListItem) => boolean): OrderApprovalListItem[] {
    let result = [];
    agreed.filter(predicate).filter(a => isRoot(a.approvalNum)).forEach(rootApproval => {
      let leafApprovals = agreed.filter(a => !isRoot(a.approvalNum) && a.approvalNum.startsWith(rootApproval.approvalNum))
      result = result.concat(leafApprovals);
      result.push(rootApproval);
    });
    return result;
  }
  
  personSetTime(agreed: OrderApprovalListItem) {
    if (!agreed.agreedBy) return;
    let user = this.users[agreed.agreedBy.accountName];
    let department = user ? user.department : null;
    let approvalType = this.approvalTypes.find(at => at.approvalTypeCode === agreed.approvalTypeCode && at.department === department);
    if (!approvalType) {
      approvalType = this.approvalTypes.find(at => at.approvalTypeCode === agreed.approvalTypeCode && !at.department);
    }
    agreed.approvalTime = approvalType && approvalType.approvalDuration;
  }  

  initApprovalList() {
    let historyLength = this.order.approvalHistory && this.order.approvalHistory.approvalCycle ?
      this.order.approvalHistory.approvalCycle.length : 0;
    this.order.approval = new OrderApproval(historyLength + 1);
  }

  parallelApprovalDurationCodeChanged() {
    if (this.parallelApprovalDurationCode) {
      let duration = this.approvalDurations.find(ad => ad.Code === this.parallelApprovalDurationCode);
      this.parallelApprovers.forEach(a => a.val.approvalTerm = ApprovalTerm.fromApprovalDuration(duration));
      
    } else {
      this.parallelApprovers.forEach(a => a.val.approvalTerm = null);
      
    }
    this.calcApprovalTermAll();
  }
  calcApprovalTermAll() {  
    if (this.parallelApprovalDurationCode) {
      let parallelDuration = parseInt(this.approvalDurations.find(d => d.Code === this.parallelApprovalDurationCode).Duration);
      this.order.approval.approvalCycle.approvalTermAll = { duration: parallelDuration };
    }else this.order.approval.approvalCycle.approvalTermAll = null;
  }
  canAddParallel() {
    return !this.parallelApprovers.some(a => !a.val.agreedBy || !a.val.approvalTypeCode);
  }
  hasApproval() {
    return this.order.approval.approvalCycle.agreed.some(a => a.approvalTypeCode === ApprovalType.approval);
  }
  parallelApproverAdded(agreed: OrderApprovalListItem) {
    if (this.parallelApprovalDurationCode) {
      let duration = this.approvalDurations.find(ad => ad.Code === this.parallelApprovalDurationCode);
      agreed.approvalTerm = ApprovalTerm.fromApprovalDuration(duration);
    }
    agreed.Parallel = true;
    agreed.Signer = false;
    this.updateApproversAndExecutives();
    this.sortApprovalsAndSetNumeration();
  }
  sortApprovalsAndSetNumeration() {
    this.setNumeration();
    let result: OrderApprovalListItem[] = [];
    this.addNodes(this.parallelApprovers, result);   

    this.order.approval.approvalCycle.agreed = result;
  }
  setNumeration() {
    this.setNodesNumeration(this.parallelApprovers);
  }
  setNodesNumeration(nodes: ApprovalNode[], offset: number = 0, prefix: string = null) {
    nodes.forEach((node, index) => {

      let approvalNum = '' + (index + offset + 1);
      if (prefix) {
        approvalNum = prefix + '.' + approvalNum;
      }
      node.val.approvalNum = approvalNum;
      if (node.items && node.items.length) {
        this.setNodesNumeration(node.items, 0, approvalNum);
      }
    });
  }
  addNodes(nodes: ApprovalNode[], array: OrderApprovalListItem[]) {
    nodes.forEach(node => {
      if (node.items) {
        this.addNodes(node.items, array);
      }
      array.push(node.val);
    })
  }
  updateApproversAndExecutives() {
    if (this.approversFullList) {
      this.approverItems = this.approversFullList.filter(a => {
        return !this.order.approval.approvalCycle.agreed || !this.order.approval.approvalCycle.agreed.find(agreed => {
          return agreed.agreedBy && agreed.agreedBy.accountName === a.accountName;
        });
      });
    }
  }
  approverDeleted() {
    this.updateApproversAndExecutives();
    this.sortApprovalsAndSetNumeration();
    this.calcApprovalTermAll();
  }

  approverChanged(agreed: OrderApprovalListItem) {
    this.personSetTime(agreed);
    this.updateApproversAndExecutives();
    this.sortApprovalsAndSetNumeration();
  }

  approvalTypeChanged(agreed: OrderApprovalListItem) {
    this.personSetTime(agreed);
    this.sortApprovalsAndSetNumeration();
  }

  approversSwapped() {
    this.sortApprovalsAndSetNumeration();
  }
  

  validate() {
    if(!this.order.draftFiles){
      this.toastr.error('Поле Проект РД является обязательным!');
      return false;
    }
    if (!this.order.approval.approvalCycle.agreed.length||this.hasNotFilledApprover()) {
      this.toastr.error('Не заполнен список участников');
      return false;
    }

    const double = this.checkDoubleAgreed();
    if (double) {
      this.toastr.error(`В списоке участников повторяется участник ${double.agreedBy.fioFull}`);
      return false;
    }
    return true;
  }

  hasNotFilledApprover() {
    return this.order.approval.approvalCycle.agreed.some((a) => {
      if (!a.approvalType) {
        return true;
      }

      if (!a.agreedBy) {
        return true;
      } else if (!a.agreedBy.accountName) {
        return true;
      }

      return false;
    });
  }

  checkDoubleAgreed() {
    let items = this.order.approval.approvalCycle.agreed.slice(0),
      testItem;

    while (items.length) {
      testItem = items.shift();
      if (items.find(item => testItem.approvalTypeCode === item.approvalTypeCode
        && testItem.agreedBy.accountName === item.agreedBy.accountName)) {
        return testItem;
      }
    }
    return null;
  }

  saveDocument() {
    if (!this.validate()) {
      return;
    }

    console.log('order', this.order);

    this.saving = true;
    this.updateOrder().subscribe(() => {
      this.saving = false;
    }, () => {
      this.saving = false;
    });
  }

  saveAndSendToApproval() {
    if (!this.validate()) {
      return;
    }

    if (this.order.approval && this.order.approval.approvalCycle && this.order.approval.approvalCycle.agreed) {
      let agreed = this.order.approval.approvalCycle.agreed;

      const isNotValid = agreed.some((a) => {
        const isNotValid = !a.approvalTypeCode || !a.agreedBy || !a.approvalTerm || !a.approvalTerm.duration;

        return isNotValid;
      });

      if (isNotValid) {
        this.toastr.error('Не заполнен список участников');
        return;
      }
      let parallelApproversCount = agreed.filter(a => a.Parallel).length;
      if (parallelApproversCount > 0 && parallelApproversCount < 2) {
        this.toastr.error('Необходимо указать минимум 2 участников');
        return;
      }
    }
    // this.order.activeApprover = this.order.approval.approvalCycle.agreed[0].agreedBy.fioShort;

    this.helper.beforeUnload.start();
    this.sendingToApproval = true;
    this.orderNsiService.fillApprovalPlanDates(this.order.approval.approvalCycle.agreed).pipe(
      mergeMap(() => {
        return this.updateOrder();
      }),
      mergeMap(() => {
        return this.activityResourceService.finishTask(parseInt(this.task.id), [])
      })
    ).subscribe(() => {
      this.helper.beforeUnload.stop();
      this.sendingToApproval = false;
      this.toastr.success('Документ отправлен на согласование.');
      window.location.href = '/main/#/app/tasks';
    }, () => {
      this.helper.beforeUnload.stop();
      this.sendingToApproval = false;
    });
  }

  // private fillApprovalPlanDates(): Observable<any> {
  //   return forkJoin(
  //     this.order.approval.approvalCycle.agreed.map(a => this.fillApprovalPlanDate(a))
  //   )
  // }

  // private fillApprovalPlanDate(agreed: OrderApprovalListItem): Observable<any> {
  //   let duration = agreed.approvalTerm.duration;
  //   let now = new Date();
  //   return this.orderNsiService.addDuration(now, 'P' + duration + 'D').pipe(
  //     tap(result => {
  //       let date = new Date(result);
  //       agreed.approvalPlanDate = new Date(date.getFullYear(), date.getMonth(), date.getDate(), 23, 59, 59, 0);
  //     })
  //   )
  // }

  isValidApproval = (): boolean => {
    let valid: boolean = true;
    if (this.order.approval && this.order.approval.approvalCycle && this.order.approval.approvalCycle.agreed) {
      valid = !this.order.approval.approvalCycle.agreed.some(agreed => !(agreed.agreedBy && agreed.agreedBy.accountName));
    }
    return valid;
  }

}
