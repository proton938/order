import { CdpReporterResourceService } from '@reinform-cdp/reporter-resource';
import { NsiResourceService } from '@reinform-cdp/nsi-resource';
import { SessionStorage } from '@reinform-cdp/security';
import { from } from 'rxjs';
import * as angular from 'angular';
import * as _ from 'lodash';
import { FileResourceService } from '@reinform-cdp/file-resource';
import { ActiveTaskService, ActivityResourceService, ITask } from '@reinform-cdp/bpm-components';
import { ToastrService } from 'ngx-toastr';
import { LoadingStatus } from '@reinform-cdp/widgets';
import { mergeMap, tap } from 'rxjs/internal/operators';
import { Component, OnInit } from '@angular/core';
import { OrderService } from '../../../../services/order.service';
import { ProcessComponent } from '../order-process';
import {User} from '../../../../models/order/User';
import {HelperService} from "../../../../services/helper.service";

@Component({
    selector: 'mggt-order-review-inner',
    templateUrl: './order-review-inner.component.html'
})
export class OrderReviewInnerComponent extends ProcessComponent implements OnInit {

    loadingStatus: LoadingStatus;
    success = false;
    task: ITask;
    comment: string = '';

    submitting: boolean;
    isShowCreateInstructionBtn: boolean = true;

    constructor(private activeTaskService: ActiveTaskService,
                public helper: HelperService,
                private activityResourceService: ActivityResourceService,
                toastr: ToastrService, orderService: OrderService, fileResourceService: FileResourceService,
                nsiResourceService: NsiResourceService, session: SessionStorage, reporterResourceService: CdpReporterResourceService) {
        super(toastr, orderService, fileResourceService, nsiResourceService, session, reporterResourceService);
    }

    ngOnInit() {
        this.helper.beforeUnload.init();
        this.loadingStatus = LoadingStatus.LOADING;

        this.activeTaskService.getTask().pipe(
            mergeMap(task => {
                this.task = task;
                return this.getEntityIdVar(task);
            }),
            mergeMap(id => {
                return this.orderService.get(id);
            }),
            tap(order => {
                this.order = order;
                this.origOrder = angular.copy(order);
                // TODO #MGGT-2311 - temporary hidden
                // this.isShowCreateInstructionBtn = this.order.documentStatusCode === 'REGISTERMAIL';
            })
        ).subscribe(() => {
            this.loadingStatus = LoadingStatus.SUCCESS;
            this.success = true;
        }, () => {
            this.loadingStatus = LoadingStatus.ERROR;
        });
    }

    ngOnDestroy() {
        this.helper.beforeUnload.destroy();
    }

    execute() {
        this.helper.beforeUnload.start();
        this.submitting = true;

        const taskOwner = this.getTaskOwner(this.task);
        const reviewInner = _.find(this.order.reviewInner, i => {
            return i.reviewBy && i.reviewBy.accountName === taskOwner;
        });
        if (reviewInner) {
            if (taskOwner !== this.session.login()) {
                reviewInner.factReviewBy = User.fromUserBean( {
                    post: this.session.post(),
                    displayName: this.session.fullName(),
                    accountName: this.session.login(),
                    fioShort: this.session.fioShort(),
                    iofShort: this.session.iofShort(),
                    telephoneNumber: this.session.telephoneNumber()
                })
            }
            reviewInner.reviewFactDate = new Date();
            reviewInner.reviewNote = this.comment;
        }
        this.updateOrder().pipe(
            mergeMap(() => {
                return from(this.activityResourceService.finishTask(parseInt(this.task.id), []));
            })
        ).subscribe(res => {
            this.helper.beforeUnload.stop();
            this.submitting = false;
            window.location.href = '/main/#/app/tasks';
        }, () => {
            this.helper.beforeUnload.stop();
            this.submitting = false;
        });
    }

}
