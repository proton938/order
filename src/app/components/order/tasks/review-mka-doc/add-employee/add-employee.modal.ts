import {ToastrService} from 'ngx-toastr';
import {Component, OnInit} from '@angular/core';
import {BsModalRef} from "ngx-bootstrap";
import {NsiResourceService} from "@reinform-cdp/nsi-resource";
import {AlertService} from "@reinform-cdp/widgets";
import {catchError, mergeMap, tap} from "rxjs/internal/operators";
import {Observable} from "rxjs/Rx";
import {from, throwError} from "rxjs/index";
import {ActivityProcessHistoryManager, ActivityResourceService} from "@reinform-cdp/bpm-components";

@Component({
  selector: 'add-employee-modal',
  templateUrl: './add-employee.modal.html'
})
export class AddEmployeeModalComponent implements OnInit {

  theme: string = '';
  submit: boolean;
  employee: any;

  constructor(private bsModalRef: BsModalRef) {
  }

  ngOnInit() {
    this.submit = false;
  }

  create() {
    this.submit = true;
    this.bsModalRef.hide();
  }

  cancel() {
    this.submit = false;
    this.bsModalRef.hide();
  }

}
