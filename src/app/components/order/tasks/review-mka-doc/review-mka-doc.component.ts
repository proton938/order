import * as angular from "angular";
import * as _ from "lodash";
import { BsModalService } from "ngx-bootstrap";
import { StateService, or } from '@uirouter/core';
import { FileResourceService } from "@reinform-cdp/file-resource";
import { ActiveTaskService, ActivityResourceService, ITask } from "@reinform-cdp/bpm-components";
import { Observable } from "rxjs/Rx";
import { DepartmentBean, NsiResourceService, UserBean } from "@reinform-cdp/nsi-resource";
import { AuthorizationService, SessionStorage } from "@reinform-cdp/security";
import { ToastrService } from "ngx-toastr";
import { LoadingStatus } from "@reinform-cdp/widgets";
import { forkJoin, from, of, throwError, EMPTY } from "rxjs/index";
import { mergeMap, tap, map, catchError } from "rxjs/internal/operators";
import { Component, OnInit, ViewChild } from '@angular/core';
import {
    Order, OrderApproval, OrderApprovalHistoryList, OrderApprovalListItem,
    OrderDraftFile
} from "../../../../models/order/Order";
import { OrderType } from "../../../../models/order/OrderType";
import { OrderTypeService } from "../../../../services/order-type.service";
import { OrderService } from "../../../../services/order.service";
import { ProcessComponent } from "../order-process";
import { ApprovalType } from "../../../../models/order/ApprovalType";
import { DropzoneConfigInterface } from "ngx-dropzone-wrapper";
import { CdpReporterResourceService, CdpReporterPreviewService } from "@reinform-cdp/reporter-resource";
import { ApprovalListEditComponent } from "../approval-list-edit/approval-list-edit.component";
import { EmailGroup } from "../../../../models/order/EmailGroup";
import { OrderStatus } from "../../../../models/order/OrderStatus";
import { User } from "../../../../models/order/User";
import { UserService } from "../../../../services/user.service";
import { formatAs } from "../../../../services/date-util.service";
import { BlockUI, NgBlockUI } from "ng-block-ui";
import { DocumentUser } from "../../../../models/core/DocumentUser";
import { AddEmployeeModalComponent } from "./add-employee/add-employee.modal";
import { SearchExtDataItem, SolarResourceService } from "@reinform-cdp/search-resource";
import {HelperService} from "../../../../services/helper.service";

@Component({
    selector: 'mggt-order-review-mka-doc',
    templateUrl: './review-mka-doc.component.html'
})
export class ReviewMkaDocComponent extends ProcessComponent implements OnInit {

    loadingStatus: LoadingStatus;
    success = false;
    task: ITask;
    orderType: OrderType;

    reviewNote: string = '';
    currentUser:any = {};
    listEmpl: any;
    processId: string;

    sentForReviewExpanded: boolean = false;
    internalInstructionsExpanded: boolean = false;
    assignments: any[] = [];

    constructor(private orderTypeService: OrderTypeService,
        public helper: HelperService,
        private activeTaskService: ActiveTaskService, private authorizationService: AuthorizationService,
        private activityResourceService: ActivityResourceService, private userService: UserService,
        toastr: ToastrService, orderService: OrderService, fileResourceService: FileResourceService, nsiResourceService: NsiResourceService,
        session: SessionStorage, reporterResourceService: CdpReporterResourceService, private reporterPreviewService: CdpReporterPreviewService,
        public $state: StateService, private modalService: BsModalService, private solarResourceService: SolarResourceService) {
        super(toastr, orderService, fileResourceService, nsiResourceService, session, reporterResourceService);
    }

    ngOnInit() {
        this.helper.beforeUnload.init();
        this.loadingStatus = LoadingStatus.LOADING;

        this.currentUser.accountName = this.session.login();
        this.currentUser.fio = this.session.fullName();
        this.currentUser.post = this.session.post() || undefined;

        let id;

        this.activeTaskService.getTask().pipe(
            mergeMap(task => {
                this.task = task;
                return this.getEntityIdVar(task);
            }),
            mergeMap(resp => {
                id = resp;
                return this.orderService.get(id)
            }),
            mergeMap(order => {
                this.order = order;
                this.origOrder = angular.copy(order);
                this.order.review = this.order.review || [];
                return from(this.solarResourceService.searchExt({
                    page: 0,
                    pageSize: 10000,
                    fields: [
                      new SearchExtDataItem('orderMKAID', id),
                      new SearchExtDataItem('docTypeCode', 'INSTRUCTION')
                    ]
                  })).pipe(tap(response => {
                    this.assignments = response.docs || [];
                  }))
            }),
            mergeMap(() => {
                const review = _.find(this.order.review, f => f.reviewBy && f.reviewBy.accountName === this.session.login());
                if (review && !review.reviewPlanDate) {
                    review.reviewPlanDate = this.task.dueDate;
                    return this.updateOrder();
                }
                return of('');
            })
        ).subscribe(() => {
            this.loadingStatus = LoadingStatus.SUCCESS;
            this.success = true;
        }, () => {
            this.loadingStatus = LoadingStatus.ERROR;
        });
    }

    ngOnDestroy() {
        this.helper.beforeUnload.destroy();
    }

    addEmpl() {
        this.helper.beforeUnload.start();
        this.addEmployee().pipe(
            mergeMap(response => {
                this.listEmpl = response;
                return this.getProcessId('sdordmka_reviewMkaDocumentHand')
            }), mergeMap(response => {
                this.processId = response.data[0].id;
                return this.orderService.get(this.order.documentID);
            }), mergeMap(response => {
                this.order = response;
                this.origOrder = angular.copy(this.order);
                this.order.review = this.order.review || [];
                if (this.listEmpl.length > 0) {
                    this.listEmpl.forEach(l => {
                        let reviewBy = {
                            accountName: l.accountName,
                            post: l.post || undefined,
                            fio: l.displayName
                        }

                        let newReview: any = {
                            sentReviewBy: this.currentUser,
                            reviewBy
                        }
                        this.order.review.push(newReview);

                        this.activityResourceService.initProcess({
                            processDefinitionId: this.processId,
                            variables: [
                                { 'name': 'EntityIdVar', 'value': this.order.documentID },
                                { 'name': 'sdoUserVar', 'value': l.accountName }
                            ]
                        })
                    })
                }
                return this.updateOrder();
            }), tap(response => {
            }), catchError(error => {
                console.log(error);
                return throwError(error);
            }), catchError(error => {
                return EMPTY;
            })
        ).subscribe(() => {
            this.helper.beforeUnload.stop();
        }, error => {
            this.helper.beforeUnload.stop();
        });
    }

    createInstruction() {
        let host = `${window.location.protocol}//${window.location.host}`;
        let backUrl = encodeURIComponent(window.location.href);
        window.location.href = `${host}/sdo/instruction/#/app/instruction/instruction/new?field=OrderID&id=${this.order.documentID}&backUrl=${backUrl}`;
    }

    finishTask() {
        this.helper.beforeUnload.start();
        this.orderService.get(this.order.documentID).pipe(
            mergeMap((order: Order) => {
                this.order = order;
                this.origOrder = angular.copy(this.order);
                this.order.review = this.order.review || [];

				let review =  this.order.review.find(r => (r.reviewBy && r.reviewBy.accountName === this.currentUser.accountName) && !r.factReviewBy);
				if (review) {
                    review.reviewFactDate = new Date();
                    review.reviewNote = this.reviewNote;
				} else {
					review = {
						reviewFactDate: new Date(),
						reviewNote: this.reviewNote,
						factReviewBy: this.currentUser
					};
					this.order.review.push(review);
				}
			
               
                return this.updateOrder();
            }), mergeMap(response => {
                return this.activityResourceService.finishTask(parseInt(this.task.id));
            }), tap(response => {
                this.toastr.success('Задача успешно завершена!');
                window.location.href = '/main/#/app/tasks';
            }), catchError(error => {
                this.toastr.error('Ошибка при завершении задачи!');
                console.log(error);
                return throwError(error);
            })
        ).subscribe(response => {
            this.helper.beforeUnload.stop();
        }, error => {
            this.helper.beforeUnload.stop();
            console.log(error);
        })
    }

    getProcessId(name: string): Observable<any> {
        return from(this.activityResourceService.getProcessDefinitions({ key: name, latest: true })).pipe(
            map(id => {
                return id;
            })
        )
    }

    addEmployee(): Observable<string> {
        return Observable.create(subscriber => {
            const options = {
                initialState: {},
                'class': 'modal-md'
            };
            const ref = this.modalService.show(AddEmployeeModalComponent, options);
            let subscr = this.modalService.onHide.subscribe(() => {
                let component = <AddEmployeeModalComponent>ref.content;

                if (component.submit) {
                    subscriber.next(component.employee);
                } else {
                    subscriber.error('canceled');
                }
                subscr.unsubscribe();
            });
        });
    }
}
