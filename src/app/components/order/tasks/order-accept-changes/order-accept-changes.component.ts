import { CdpReporterResourceService } from "@reinform-cdp/reporter-resource";
import { NsiResourceService } from "@reinform-cdp/nsi-resource";
import * as _ from "lodash";
import { FileResourceService } from "@reinform-cdp/file-resource";
import { Component,OnInit} from '@angular/core';
import { ActiveTaskService, ActivityResourceService,ITask } from "@reinform-cdp/bpm-components";
import { ToastrService } from "ngx-toastr";
import { OrderService } from "../../../../services/order.service";
import { SessionStorage } from "@reinform-cdp/security";
import { mergeMap } from "rxjs/internal/operators";
import { from, of, throwError } from "rxjs";
import { LoadingStatus } from "@reinform-cdp/widgets";
import * as angular from 'angular';
import { Observable } from "rxjs/Rx";
import { ProcessComponent } from "../order-process";
import { DomSanitizer } from '@angular/platform-browser';
import {HelperService} from "../../../../services/helper.service";

@Component({
  selector: 'mggt-order-accept-changes',
  templateUrl: './order-accept-changes.component.html',
})
export class OrderAcceptChangesComponent extends ProcessComponent implements OnInit {

  success = false;
  task: ITask;
  
  draftPath:string;
  loadingStatus: LoadingStatus;
  sendingBackToWork: boolean;
  agreeing: boolean;
  showApprovalList: boolean = false;
  constructor(public domSanitizationService: DomSanitizer,
    public helper: HelperService,
    private activeTaskService: ActiveTaskService,
    private activityResourceService: ActivityResourceService,
    toastr: ToastrService,
    orderService: OrderService,
    fileResourceService: FileResourceService,
    nsiResourceService: NsiResourceService,
    sessionStorage: SessionStorage,
    reporterResourceService: CdpReporterResourceService) {
      super(toastr, orderService, fileResourceService, nsiResourceService, sessionStorage, reporterResourceService);
  }
  ngOnInit() {
    this.helper.beforeUnload.init();
    this.loadingStatus = LoadingStatus.LOADING;
    this.activeTaskService.getTask().pipe(
      mergeMap(task => {
        this.task = task;
        return this.getEntityIdVar(task);
      }),
      mergeMap(id => {
        return this.orderService.get(id);
      }),
      mergeMap(order => {
        this.order = order;
        this.origOrder = angular.copy(order);

        return from(this.fileResourceService.getFolderinfo(this.order.folderID).then(folderInfo=>{
          let domainName=document.domain;
          if(!!this.order.draftFiles.draftFilesName)
            this.draftPath=`ms-word:ofe|u|https://${domainName}/alfresco/aos${folderInfo.path}/${this.order.draftFiles.draftFilesName}`;
        }));
      })
    ).subscribe((response) => {
      this.loadingStatus = LoadingStatus.SUCCESS;
      this.success = true;
    }, (error) => {
      console.error(error);
      this.loadingStatus = LoadingStatus.ERROR;
    });
  }

  ngOnDestroy() {
    this.helper.beforeUnload.destroy();
  }

  processDocument() {
    this.helper.beforeUnload.start();
    this.agreeing = true;    
    from(this.activityResourceService.finishTask(parseInt(this.task.id), [])).subscribe((result: any) => {
      this.helper.beforeUnload.stop();
      this.agreeing = false;
      this.toastr.success('Документ успешно согласован.');
      window.location.href = '/main/#/app/tasks';
    }, (e) => {
      this.helper.beforeUnload.stop();
      console.log(e);
      this.agreeing = false;
    });
  } 

  processDocumentOld() {
    this.agreeing = true;    
    from(this.activityResourceService.finishTask(parseInt(this.task.id), 
    [{ name: 'IsApproved', value: true }])).subscribe((result: any) => {
      this.agreeing = false;
      this.toastr.success('Документ успешно согласован.');
      window.location.href = '/main/#/app/tasks';
    }, (e) => {
      console.log(e);
      this.agreeing = false;
    });
  } 
  
  sendBackToWork() {
    this.sendingBackToWork = true;
    this.closeCycle(this.order).pipe(
      mergeMap(() => {
        return this.updateOrder();
      }),
      mergeMap(() => {
        return from(this.activityResourceService.finishTask(parseInt(this.task.id), 
        [{ name: 'IsApproved', value: false }]));
      })
    ).subscribe(
      () => {
        console.log('subscribe');
        this.sendingBackToWork = false;
        this.toastr.success('Документ отправлен на доработку.');
        window.location.href = '/main/#/app/tasks';
      },
      (err) => {
        console.log('subscribe err', err);
        this.sendingBackToWork = false;
      });
  }
  
}
