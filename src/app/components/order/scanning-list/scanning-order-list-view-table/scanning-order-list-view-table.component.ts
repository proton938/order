import {PlatformConfig} from "@reinform-cdp/core";
import {BsModalService} from "ngx-bootstrap";
import {ToastrService} from 'ngx-toastr';
import {Component} from '@angular/core';
import {OrderListViewTableComponent} from "../../list/order-list-view-table/order-list-view-table.component";
import {UploadFileModalComponent} from "../../card/upload-file/upload-file.modal";
import {OrderService} from "../../../../services/order.service";
import {mergeMap} from "rxjs/internal/operators";
import {of, throwError} from "rxjs/index";
import {StateService} from "@uirouter/core";
import {CdpSolrResourceService} from "@reinform-cdp/search-resource";

@Component({
    selector: 'mggt-scanning-order-list-view-table',
    templateUrl: './scanning-order-list-view-table.component.html',
    styleUrls: ['./scanning-order-list-view-table.component.scss']
})
export class ScanningOrderListViewTableComponent extends OrderListViewTableComponent {

    constructor(platformConfig: PlatformConfig, private modalService: BsModalService, private orderService: OrderService, public $state: StateService, public cdpSolrResourceService: CdpSolrResourceService, public toastr: ToastrService) {
        super(platformConfig, $state, cdpSolrResourceService, toastr);
    }

    showUploadWindow(id: string) {
        this.orderService.get(id).pipe(
            mergeMap(order => {
                const options = {
                    initialState: {
                        order: order
                    },
                    'class': 'modal-lg'
                };
                const ref = this.modalService.show(UploadFileModalComponent, options);
                return this.modalService.onHide.pipe(
                    mergeMap(() => {
                        let component = <UploadFileModalComponent> ref.content;
                        if (component.submit) {
                            return of('');
                        } else {
                            return throwError('cancelled');
                        }

                    })
                );
            })
        ).subscribe(() => {
            this.search(this.sorting);
        });
    }


}
