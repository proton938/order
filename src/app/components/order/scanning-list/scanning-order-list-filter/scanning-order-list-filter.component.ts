import {NsiResourceService} from "@reinform-cdp/nsi-resource";
import {Component} from '@angular/core';
import {OrderListFilterComponent} from "../../list/order-list-filter/order-list-filter.component";

@Component({
  selector: 'mggt-scanning-order-list-filter',
  templateUrl: './scanning-order-list-filter.component.html',
  styleUrls: ['./scanning-order-list-filter.component.scss']
})
export class ScanningOrderListFilterComponent extends OrderListFilterComponent {

    constructor(nsiResourceService: NsiResourceService) {
      super(nsiResourceService);
    }

  ngOnInit() {
  }

}
