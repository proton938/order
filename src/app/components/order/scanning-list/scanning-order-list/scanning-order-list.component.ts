import {BsModalService} from "ngx-bootstrap";
import {Component, ViewChild} from '@angular/core';
import {OrderListComponent} from "../../list/order-list/order-list.component";
import {StateService} from "@uirouter/core";
import {ScanningOrderListViewTableComponent} from "../scanning-order-list-view-table/scanning-order-list-view-table.component";
import {AttachOriginalsModalComponent} from "../../card/attach-originals/attach-originals.modal";

@Component({
    selector: 'mggt-scanning-order-list',
    templateUrl: './scanning-order-list.component.html',
    styleUrls: ['./scanning-order-list.component.scss']
})
export class ScanningOrderListComponent extends OrderListComponent {

    @ViewChild(ScanningOrderListViewTableComponent)
    tableView: ScanningOrderListViewTableComponent;

    constructor($state: StateService, private modalService: BsModalService) {
        super($state);
    }

    initFilter() {
    }

    search() {
        if (this.tableView) {
            this.tableView.search(this.sorting)
        }
    }

    showAttachOriginalsWindow() {
        const options = {
            initialState: {},
            'class': 'modal-lg'
        };
        const ref = this.modalService.show(AttachOriginalsModalComponent, options);
        return this.modalService.onHide.subscribe(() => {
        }, () => {
        });
    }

}
