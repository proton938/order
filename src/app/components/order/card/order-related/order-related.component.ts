import {map, mergeMap, tap,} from "rxjs/internal/operators";
import * as _ from "lodash";
import {ToastrService} from "ngx-toastr";
import {Component, Input, OnInit} from '@angular/core';
import {Order} from "../../../../models/order/Order";
import {LoadingStatus} from "@reinform-cdp/widgets";
import {AuthorizationService} from "@reinform-cdp/security";
import {NsiResourceService} from "@reinform-cdp/nsi-resource";
import {forkJoin, of} from "rxjs/index";
import {OrderTypeService} from "../../../../services/order-type.service";
import {OrderStatus} from "../../../../models/order/OrderStatus";
import {Observable} from "rxjs/Rx";
import {StateService} from "@uirouter/core";
import {CdpSolrResourceService} from "@reinform-cdp/search-resource";
import {OrderService} from "../../../../services/order.service";

export class RelatedDoc {
    id: string;
    type: string;
    order?: Order;
}

@Component({
    selector: 'mggt-order-related',
    templateUrl: './order-related.component.html',
    styleUrls: ['./order-related.component.scss']
})
export class OrderRelatedComponent implements OnInit {

    @Input() order: Order;
    relatedDocuments: RelatedDoc[] = [];
    rdStatus: any;
    rdTypes: any;
    rdLinkTypes: any;

    loadingStatus: LoadingStatus = LoadingStatus.LOADING;
    success = false;

    constructor(public $state: StateService,
                private toastr: ToastrService,
                private authorizationService: AuthorizationService,
                private nsiResourceService: NsiResourceService,
                private orderTypeService: OrderTypeService,
                private orderService: OrderService,
                private cdpSolrResourceService: CdpSolrResourceService) {
    }

    ngOnInit() {
        this.loadDictionaries().pipe(
            mergeMap(() => {
                return this.getRelatedDocuments(this.order, [this.order.documentID]);
            }),
            tap(orders => {
                console.log(orders);
                this.relatedDocuments = orders.sort((a, b) => {
                    if (a.order.documentDate > b.order.documentDate) return 1;
                    else if (a.order.documentDate < b.order.documentDate) return 0;
                    return Number(a.order.documentNumber > b.order.documentNumber);
                });

            })
        ).subscribe(() => {
            this.loadingStatus = LoadingStatus.SUCCESS;
            this.success = true;
        }, () => {
            this.loadingStatus = LoadingStatus.ERROR;
        });
    }

    getRelatedDocuments(order: Order, except?: string[], orderMap?: { [key: string]: Order }): Observable<RelatedDoc[]> {
        orderMap = orderMap || {};
        return this.getRelatedDocumentsIds(order).pipe(
            mergeMap((docs: RelatedDoc[]) => {
                docs = docs.filter(doc => !orderMap[doc.id] && except.indexOf(doc.id) < 0);
                if (!docs || !docs.length) {
                    return of([])
                }
                return forkJoin(docs.map(doc => this.orderService.get(doc.id))).pipe(
                    mergeMap(orders => {
                        orders.forEach(order => {
                            orderMap[order.documentID] = order;
                            docs.find(doc => doc.id === order.documentID).order = order;
                        });
                        return (orders.length ? forkJoin(orders.map(order => this.getRelatedDocuments(order, except, orderMap))) : of([])).pipe(
                            map((relatedDocs: RelatedDoc[][]) => {
                                return docs.concat(_.flatten(relatedDocs));
                            })
                        )
                    })
                );
            })
        );
    }

    getRelatedDocumentsIds(order: Order): Observable<RelatedDoc[]> {
        let ids = [];
        ids.push(of(
            [].concat(this.order.canceledDocument ? [
                {id: this.order.canceledDocument.canceledDocumentID, type: 'cancellation'}
            ] : [])
                .concat(this.order.updatedDocument ? this.order.updatedDocument.map(doc => {
                    return {id: doc.updatedDocumentID, type: 'correction'};
                }) : [])
                .concat(this.order.linkedDocument ? this.order.linkedDocument.map(doc => {
                    return {id: doc.linkedDocumentID, type: doc.linkedDocumentType}
                }) : [])
        ));
        ids.push(this.cdpSolrResourceService.query({
            types: ['SDO_ORDER_ORDER'],
            query: `canceledDocumentID:(${order.documentID})`
        }).pipe(
            map(result =>
                result.docs ? result.docs.map((doc: any) => {
                    return {id: doc.sys_documentId, type: 'cancellation'}
                }) : []
            )
        ));
        ids.push(this.cdpSolrResourceService.query({
            types: ['SDO_ORDER_ORDER'],
            query: `updatedDocumentsID:(${order.documentID})`
        }).pipe(
            map(result =>
                result.docs ? result.docs.map((doc: any) => {
                    return {id: doc.sys_documentId, type: 'correction'}
                }) : []
            )
        ));
        return forkJoin(ids).pipe(
            map((docs: RelatedDoc[][]) => {
                return _.flatten(docs);
            })
        );
    }

    loadDictionaries(): Observable<any> {
        return forkJoin([
            this.nsiResourceService.get('mggt_order_OrderStatuses'),
            this.orderTypeService.geOrderTypes(),
            this.nsiResourceService.get('mggt_order_OrderLinkTypes')
        ]).pipe(
            tap((values: any[]) => {
                this.rdStatus = _.reduce(values[0], (obj, status: OrderStatus) => {
                    obj[status.code] = status.statusAnalytic;
                    return obj;
                }, {});
                this.rdTypes = values[1];
                this.rdLinkTypes = _.reduce(values[2], (obj, type: any) => {
                    obj[type.LinkTypeCode] = type.LinkTypeName;
                    return obj;
                }, {});
            })
        );
    }

}
