import { map, mergeMap, tap, } from "rxjs/internal/operators";
import * as _ from "lodash";
import { ToastrService } from "ngx-toastr";
import { Component } from '@angular/core';
import { Order } from "../../../../../models/order/Order";
import { AuthorizationService } from "@reinform-cdp/security";
import { NsiResourceService } from "@reinform-cdp/nsi-resource";
import { OrderTypeService } from "../../../../../services/order-type.service";
import { StateService } from "@uirouter/core";
import { CdpSolrResourceService } from "@reinform-cdp/search-resource";
import { OrderService } from "../../../../../services/order.service";
import { OrderRelatedComponent } from "../order-related.component";

export class RelatedDoc {
  id: string;
  type: string;
  order?: Order;
}

@Component({
  selector: 'mggt-mobile-order-related',
  templateUrl: './order-related.component.html',
  styleUrls: ['./order-related.component.scss']
})
export class MobileOrderRelatedComponent extends OrderRelatedComponent {
  constructor(public $state: StateService,
    toastr: ToastrService,
    authorizationService: AuthorizationService,
    nsiResourceService: NsiResourceService,
    orderTypeService: OrderTypeService,
    orderService: OrderService,
    cdpSolrResourceService: CdpSolrResourceService) {
    super($state, toastr, authorizationService, nsiResourceService, orderTypeService, orderService, cdpSolrResourceService);
  }
}
