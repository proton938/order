import {Component, Input, OnInit} from '@angular/core';
import {forkJoin, of} from "rxjs/index";
import {Observable} from "rxjs/Rx";
import {map, mergeMap, tap,} from "rxjs/internal/operators";
import * as _ from "lodash";
import {LoadingStatus} from "@reinform-cdp/widgets";
import {NsiResourceService} from "@reinform-cdp/nsi-resource";
import {OrderStatus} from "../../../../models/order/OrderStatus";

@Component({
    selector: 'mggt-order-inner-assignments',
    templateUrl: './order-inner-assignments.component.html',
    styleUrls: ['./order-inner-assignments.component.scss']
})
export class OrderInnerAssignmentsComponent implements OnInit {

    @Input() assignments: any;
    statuses: any;
    priorities: any;

    loadingStatus: LoadingStatus = LoadingStatus.LOADING;
    success: boolean = false;

    constructor(private nsiResourceService: NsiResourceService) {

    }

    ngOnInit() {
        forkJoin([
            this.nsiResourceService.get('InstructionStatus'),
            this.nsiResourceService.get('InstructionPriority')
        ]).subscribe((response: any) => {
            this.statuses = response[0];
            this.priorities = response[1];

            this.loadingStatus = LoadingStatus.SUCCESS;
            this.success = true;

            this.statuses.forEach(st => this.statuses[st.name] = st.color);
            this.priorities.forEach(pr => this.priorities[pr.name] = pr.color);
        }, () => {
            this.loadingStatus = LoadingStatus.ERROR;
        });
    }

}
