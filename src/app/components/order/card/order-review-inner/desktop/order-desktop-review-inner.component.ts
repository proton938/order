import * as angular from "angular";
import {Component, Input, OnInit} from '@angular/core';
import {Order, ReviewInner} from "../../../../../models/order/Order";
import {DocumentUser} from "../../../../../models/core/DocumentUser";

@Component({
    selector: 'mggt-order-desktop-review-inner',
    templateUrl: './order-desktop-review-inner.component.html'
})
export class OrderDesktopReiewInnerComponent implements OnInit {

    @Input() order: Order;

    reviewInner: ReviewInner[] = [];

    constructor() {
    }

    ngOnInit() {
        this.reviewInner = angular.copy(this.order.reviewInner).filter(f =>{
            return f.reviewBy && f.reviewBy.fio;
        });        
    }

}
