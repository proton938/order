import * as angular from "angular";
import {Component, Input, OnInit} from '@angular/core';
import {Order, ReviewInner} from "../../../../../models/order/Order";
import {DocumentUser} from "../../../../../models/core/DocumentUser";
import { OrderDesktopReiewInnerComponent } from "../desktop/order-desktop-review-inner.component";

@Component({
    selector: 'mggt-order-mobile-review-inner',
    templateUrl: './order-mobile-review-inner.component.html'
})
export class OrderMobileReiewInnerComponent extends OrderDesktopReiewInnerComponent implements OnInit {

    constructor() {
        super();
    }

}
