import * as angular from "angular";
import {Component, Input, OnInit,Inject} from '@angular/core';
import {Order, ReviewInner} from "../../../../models/order/Order";
import {DocumentUser} from "../../../../models/core/DocumentUser";

@Component({
    selector: 'mggt-order-card-review-inner',
    templateUrl: './order-review-inner.component.html'
})
export class OrderCardReiewInnerComponent implements OnInit {

    @Input() order: Order;

    // reviewInner: ReviewInner[] = [];

    constructor(@Inject('$localStorage') public $localStorage: any) { }

    ngOnInit() {
    //     this.reviewInner = angular.copy(this.order.reviewInner).filter(f =>{
    //         return f.reviewBy && f.reviewBy.fio;
    //     });        
    }

}
