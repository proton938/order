import { PlatformConfig } from '@reinform-cdp/core';
import {ToastrService} from "ngx-toastr";
import * as angular from "angular";
import * as _ from "lodash";
import {map, mergeMap, tap} from "rxjs/internal/operators";
import {forkJoin, from, of, throwError} from "rxjs";
import {Observable} from "rxjs/Rx";
import {NsiResourceService} from "@reinform-cdp/nsi-resource";
import {ActivityResourceService, IProcessDefinition, ITaskVariable} from "@reinform-cdp/bpm-components";
import {SessionStorage} from "@reinform-cdp/security";
import {LoadingStatus} from "@reinform-cdp/widgets";
import {Component, OnInit} from '@angular/core';
import {Order, OrderProcess} from "../../../../models/order/Order";
import {OrderType} from "../../../../models/order/OrderType";
import {OrderTheme} from "../../../../models/order/OrderTheme";
import {StateService} from "@uirouter/core";
import {OrderService} from "../../../../services/order.service";
import {OrderTypeService} from "../../../../services/order-type.service";
import {UserService} from "../../../../services/user.service";

@Component({
    selector: 'mggt-order-initiate-edit-orders',
    templateUrl: './order-initiate-edit-orders.component.html',
    styleUrls: ['./order-initiate-edit-orders.component.scss']
})
export class OrderInitiateEditOrdersComponent implements OnInit {

    loadingStatus: LoadingStatus;
    success = false;
    orderTypes: OrderType[];
    themes: OrderTheme[];
    availableYears: number[];
    availableThemes: OrderTheme[] = [];

    saving: boolean = false;
    order: Order;

    constructor(private $state: StateService,
                private session: SessionStorage,
                private toastr: ToastrService,
                private orderService: OrderService,
                private orderTypeService: OrderTypeService,
                private activityResourceService: ActivityResourceService,
                private userService: UserService,
                private nsiRestService: NsiResourceService,
                private platformConfig: PlatformConfig) {
    }

    ngOnInit() {
        this.order = new Order();
        this.order.documentYear = new Date().getFullYear();
        this.loadingStatus = LoadingStatus.LOADING;
        this.loadDictionaries().subscribe(() => {
            this.loadingStatus = LoadingStatus.SUCCESS;
            this.success = true;
        }, () => {
            this.loadingStatus = LoadingStatus.ERROR;
        });
    }

    loadDictionaries(): Observable<any> {
        this.availableYears = _.range(2016, 2028);
        return forkJoin([
            this.orderTypeService.geOrderTypes(),
            this.nsiRestService.get('mggt_order_OrderThemes')
        ]).pipe(
            tap(values => {
                this.orderTypes = values[0].filter(ot => !ot.orderMKA);
                this.themes = values[1];
                this.documentTypeChanged();
            })
        );
    }

    validate() {
        if (!this.order.documentTypeCode || !this.order.documentYear || !this.order.documentContent || !this.order.documentThemeCode) {
            this.toastr.error('Не заполнены обязательные поля');
            return false;
        }
        return true;
    }

    save() {
        if (!this.validate()) {
            return;
        }
        this.saving = true;
        let key = 'sdord_coEditOrders';
        this.orderService.create(this.order).pipe(
            mergeMap(order => {
                this.order = order;
                return from(this.activityResourceService.getProcessDefinitions({key: key, latest: true}));
            }),
            mergeMap(response => {
                let processes = response.data;
                if (processes.length <= 0) {
                    return throwError("Процесс " + key + " не найден");
                } else {
                    return of(processes[0]);
                }
            }),
            mergeMap((process: IProcessDefinition) => {
                return this.activityResourceService.initProcess({
                    processDefinitionId: process.id,
                    variables: this.getInitProcessProperties(this.order)
                });
            }),
            mergeMap(id => {
                let newOrder = angular.copy(this.order);
                newOrder.bpmProcess = new OrderProcess();
                newOrder.bpmProcess.bpmProcessId = id.toString();
                return this.orderService.update(this.order.documentID, this.orderService.diff(this.order, newOrder)).pipe(
                    mergeMap(() => {
                        return this.getTaskId(id);
                    })
                );
            })
        ).subscribe((taskId) => {
            this.saving = false;
            window.location.href = this.$state.href('app.execution.orderattachDocCreateApprovalList',
                {system: 'rd', taskId: taskId, systemCode: this.platformConfig.systemCode.toUpperCase()},
                {absolute: true});
        });
    }

    getTaskId(processId: any): Observable<string> {
        return from(this.activityResourceService.getTasks({processInstanceId: processId})).pipe(
            map(result => {
                return result.data[0].id;
            })
        );
    }

    documentTypeChanged() {
        this.order.documentThemeCode = null;
        if (this.order.documentTypeCode) {
            this.availableThemes = _.filter(this.themes, (th: OrderTheme) => {
                return th.docCode === this.order.documentTypeCode;
            })
        } else {
            this.availableThemes = [];
        }
    }

    themeChanged() {
        if (this.order.documentThemeCode) {
            this.order.documentThemeValue = _.find(this.availableThemes, (th: OrderTheme) => {
                return th.themeCode === this.order.documentThemeCode;
            }).themeName;
        } else {
            delete this.order.documentThemeValue;
        }
    }

    getInitProcessProperties(order: Order): ITaskVariable[] {
        // const orderType = _.find(this.orderTypes, (_: OrderType) => {
        //     return _.code === order.documentTypeCode;
        // });
        return [{
            "name": "EntityIdVar",
            "value": order.documentID
        }, {
            "name": "EntityDescriptionVar",
            "value": order.documentTypeValue + " " + order.documentContent
        }, {
            "name": "ResponsibleVar",
            "value": this.session.login()
        }];
    }

    cancel() {
        console.log('cancel operation');
        window.history.back();
    }

}
