import {BsModalRef} from "ngx-bootstrap";
import {Component, OnInit} from '@angular/core';

@Component({
    selector: 'order-modal-content',
    templateUrl: './attach-originals.modal.html'
})

export class AttachOriginalsModalComponent implements OnInit {

    submit: boolean = false;

    constructor(private bsModalRef: BsModalRef) {
    }

    ngOnInit() {
    }

    save() {
        this.submit = true;
        this.bsModalRef.hide();
    }

    cancel() {
        this.bsModalRef.hide();
    }

}
