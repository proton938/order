import * as angular from "angular";
import {Component, Input, OnInit} from '@angular/core';
import {Order} from "../../../../models/order/Order";
import {DocumentUser} from "../../../../models/core/DocumentUser";

@Component({
    selector: 'mggt-order-review',
    templateUrl: './order-review.component.html'
})
export class OrderReiewComponent implements OnInit {

    @Input() order: Order;

    reviewBy: DocumentUser[] = [];
    factReviewBy: DocumentUser[] = [];

    constructor() {
    }

    ngOnInit() {
        this.reviewBy = angular.copy(this.order.review).filter(f =>{
            return f.reviewBy && f.reviewBy.fio;
        });
        this.factReviewBy = angular.copy(this.order.review).filter(f => {
            return f.factReviewBy && f.factReviewBy.fio;
        });
    }

}
