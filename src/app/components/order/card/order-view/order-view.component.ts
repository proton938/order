import * as angular from 'angular';
import { FileResourceService } from '@reinform-cdp/file-resource';
import { BsModalService } from 'ngx-bootstrap';
import * as _ from 'lodash';
import { StateService, Transition } from '@uirouter/core';
import { ToastrService } from 'ngx-toastr';
import { Component,  Input, OnInit,  Inject } from '@angular/core';
import { Order } from '../../../../models/order/Order';
import { AlertService, LoadingStatus } from '@reinform-cdp/widgets';
import { OrderType } from '../../../../models/order/OrderType';
import { AuthorizationService } from '@reinform-cdp/security';
import { OrderService } from '../../../../services/order.service';
import { NsiResourceService } from '@reinform-cdp/nsi-resource';
import { ActivityProcessHistoryManager, ActivityResourceService } from '@reinform-cdp/bpm-components';
import { OrderTypeService } from '../../../../services/order-type.service';
import { catchError, mergeMap, tap } from 'rxjs/internal/operators';
import { Observable, from, of, throwError } from 'rxjs';
import { OrderStatus } from '../../../../models/order/OrderStatus';

import { ServiceInformationModalComponent } from '../service-information/service-information.modal';
import { ExtDocumentService } from '../../../../services/ext-document.service';
import { CdpSolrResourceService } from '@reinform-cdp/search-resource';
import { AttachFileModalComponent } from '../../../../components/common/attach-file-modal/attach-file-modal.component';
import { FileType } from '../../../../models/order/FileType';
import {HelperService} from "../../../../services/helper.service";

@Component({
  selector: 'mggt-order-view',
  templateUrl: './order-view.component.html',
  styleUrls: ['./order-view.component.scss']
})
export class OrderViewComponent implements OnInit {

  loadingStatus: LoadingStatus;
  success = false;

  @Input() order: Order;
  orderType: OrderType;
  isSendDisabled: boolean;
  isUploadDisabled: boolean;
  modal: any;
  cancelingDocument: Order;
  extDocLink: string;

  hasAccessRight: boolean = true;
  isSending: boolean = false;
  isUpdating: boolean = false;
  isDeleted: boolean = false;

  canDeleteFile: boolean = false;
  canDelete: boolean = false;

  copyFiles: FileType[];

  constructor(public $state: StateService,
    private transition: Transition,
    private fileResourceService: FileResourceService,
    private orderService: OrderService,
    private alertService: AlertService,
    private authorizationService: AuthorizationService,
    private modalService: BsModalService,
    private extDocumentService: ExtDocumentService,
    private toastr: ToastrService,
    private nsiResourceService: NsiResourceService,
    private orderTypeService: OrderTypeService,
    private processHistoryManager: ActivityProcessHistoryManager,
    private activityResourceService: ActivityResourceService,
    private cdpSolrResourceService: CdpSolrResourceService,
    private helper: HelperService,
    @Inject('$localStorage') public $localStorage: any) {
  }

  ngOnInit() {
    if (!this.authorizationService.check('ORDER_DOCUMENT_CARD')) {
      this.loadingStatus = LoadingStatus.ERROR;
      return;
    }

    this.loadingStatus = LoadingStatus.LOADING;

    if (_.isEmpty(this.order.canceledDocument)) {
      delete this.order.canceledDocument;
    }

    this.isSendDisabled = this.order.documentStatusCode !== 'REGISTERSCAN';

    this.isUploadDisabled = !this.order.documentID || !this.order.documentNumber;

    this.isDeleted = this.order.documentStatusCode === 'DELETED';

    this.canDeleteFile = this.authorizationService.check('SDO_ORDER_ORDER_ADD_SCAN');
    this.copyFiles = angular.copy(this.order.files);

    return this.orderTypeService.geOrderTypes().pipe(
      mergeMap((orderTypes: OrderType[]) => {
        this.orderType = _.find(orderTypes, (type: OrderType) => type.code === this.order.documentTypeCode);
        if (this.orderType.accessEI) {
          //TODO: OASI_ADMORDER_CARD_EI
          this.hasAccessRight = this.authorizationService.check('OASI_ADMORDER_CARD_EI');
        }
        return this.loadCancelingDocument();
      }),
      mergeMap(() => {
        return this.activityResourceService.queryTasks({
          active: true,
          processInstanceVariables: [{
            name: 'EntityIdVar',
            value: this.order.documentID,
            operation: 'equals',
            type: 'string'
          }]
        })
      }),
      mergeMap((tasksStatus) => {
        let editDoc = _.some(tasksStatus.data, x => {
          //console.log(x);
          return x.formKey === 'EditDoc';
        });

        this.canDelete = this.order.documentStatusCode === 'PROJECT' || (this.order.documentStatusCode === 'APPROVAL' && editDoc);
        return this.extDocumentService.getLink(this.order);
      }),
      tap((link) => {
        this.extDocLink = link;
      })
    ).subscribe(() => {
      this.loadingStatus = LoadingStatus.SUCCESS;
      this.success = true;
    }, () => {
      this.loadingStatus = LoadingStatus.ERROR;
    });

  }

  loadCancelingDocument(): Observable<any> {
    if (this.order.documentStatusCode === OrderStatus.CANCELED) {
      return from(this.cdpSolrResourceService.query({
        types: ['SDO_ORDER_ORDER'],
        query: `canceledDocumentID: ${this.order.documentID}`
      })).pipe(
        mergeMap(result => {
          if (result.docs && result.docs.length) {
            let doc = <any>result.docs[0];
            return this.orderService.get(doc.sys_documentId)
          }
          return of(null);
        }),
        tap(order => {
          this.cancelingDocument = order;
        })
      );
    } else {
      return of('');
    }

  }

  openServiceInformation() {
    const options = {
      initialState: {
        order: this.order
      },
      'class': 'modal-lg'
    };
    const ref = this.modalService.show(ServiceInformationModalComponent, options);
    this.modalService.onHide.subscribe(() => {
    });
  }

  sendMails() {
    this.isUpdating = true;
    this.isSending = true;

    this.orderService.mail(this.order.documentID).pipe(
      tap(() => {
        this.toastr.success('Рассылка успешно осуществлена');
        this.$state.reload();
      }),
      catchError(error => {
        this.toastr.warning('Рассылка завершилась с ошибкой');
        this.$state.reload();
        return throwError(error);
      })
    ).subscribe(() => {
      this.isUpdating = false;
      this.isSending = false;
    }, () => {
      this.isUpdating = false;
      this.isSending = false;
    });
  }

  deleteDocument(fileInfo: any) {
    this.showAttachScanModal();
  };

  showAttachScanModal(): void {
    this.isUpdating = true;

    this.modalService.show(AttachFileModalComponent, {
      class: 'modal-lg',
      keyboard: false,
      backdrop: 'static',
      initialState: {
        folderId: this.order.folderID,
        saveCallback: (file: FileType) => {
          this.fileResourceService.deleteFile(this.copyFiles[0].fileID)
            .then(() => {
              return this.orderService.replaceFile(this.order, file).toPromise();
            })
            .then(() => {
              this.order.files = [file];
              this.copyFiles = angular.copy(this.order.files);
              return this.orderService.sendToDocumentsDB(this.order.documentID).toPromise();
            })
            .then(() => {
              this.isUpdating = false;
            })
            .catch((error) => {
              console.log(error);
              this.isUpdating = false;
            });
        },
        cancelCallback: (file: FileType) => {
          this.order.files = angular.copy(this.copyFiles);

          if (file) {
            this.fileResourceService.deleteFile(file.fileID)
          }
          this.isUpdating = false;
        }
      }
    });
  }
}
