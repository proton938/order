import {Component, OnInit, Input, AfterViewInit} from '@angular/core';
import {BlockUI, NgBlockUI} from 'ng-block-ui';
import * as JSONEditor from 'jsoneditor';
import {copy} from 'angular';
import {ToastrService} from 'ngx-toastr';
// import * as jsonpatch from 'fast-json-patch';
import {StateService} from '@uirouter/core';
import { OrderService } from '../../../../services/order.service';
import { DateFormatService } from '../../../../services/date-format.service';

@Component({
  selector: 'order-json-editor',
  templateUrl: './json-editor.component.html'
})
export class OrderJsonEditorComponent implements AfterViewInit {
  @Input()document: any;
  documentEdited: any;
  container: any;
  options: any;
  editor: any;
  @BlockUI('card-editor') blockUI: NgBlockUI;

  
  constructor(public orderService: OrderService,
    private dateFormatService: DateFormatService,
    private toastrService: ToastrService,
    private stateService: StateService) {
  }

  ngAfterViewInit() {
    this.container = document.getElementById('jsoneditor');
    this.options = {};
    this.editor = new JSONEditor(this.container, this.options);
    this.documentEdited = this.dateFormatService.formatOrderDates(this.document);
    this.editor.set(this.documentEdited);
  }

  save() {
    this.blockUI.start();
    this.documentEdited = this.editor.get();
    this.orderService.update(this.document.documentID, 
      this.orderService.diff(this.document, this.documentEdited)).subscribe(order => {
        this.blockUI.stop();
        this.document = order;
        this.documentEdited = this.dateFormatService.formatOrderDates(this.document);
        this.editor.set(this.documentEdited);
        this.toastrService.success('Документ успешно сохранен!');
      }, error => {
        this.toastrService.error('Ошибка при сохранении документа!');
        this.blockUI.stop();
      });
  }  
}
