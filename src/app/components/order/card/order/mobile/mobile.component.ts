
import { PlatformConfig } from '@reinform-cdp/core';
import * as _ from "lodash";
import { BsModalService } from "ngx-bootstrap";
import { FileResourceService } from "@reinform-cdp/file-resource";
import { StateService, Transition } from '@uirouter/core';
import { Component, Inject } from '@angular/core';
import { NsiResourceService } from "@reinform-cdp/nsi-resource";
import { OrderService } from "../../../../../services/order.service";
import { OrderTypeService } from "../../../../../services/order-type.service";
import { BreadcrumbsService } from "@reinform-cdp/skeleton";
import { SolarResourceService } from "@reinform-cdp/search-resource";
import { mergeMap } from "rxjs/internal/operators";
import { from } from "rxjs/index";
import { AlertService } from "@reinform-cdp/widgets";
import { ActivityProcessHistoryManager, ActivityResourceService } from "@reinform-cdp/bpm-components";
import { DesktopOrderComponent } from '../desktop/desktop.component';
import { ToastrService } from "ngx-toastr";
import { SessionStorage } from '@reinform-cdp/security';
import {LinkService} from "../../../../../services/link.service";

@Component({
  selector: 'mggt-mobile-order',
  templateUrl: './mobile.component.html',
})
export class MobileOrderComponent extends DesktopOrderComponent {

  hideOvers: boolean = true;
  isUpdating: boolean = false;

  constructor($state: StateService,
    transition: Transition,
    orderService: OrderService,
    orderTypeService: OrderTypeService,
    nsiResourceService: NsiResourceService,
    solarResourceService: SolarResourceService,
    breadcrumbsService: BreadcrumbsService,
    platformConfig: PlatformConfig,
    alertService: AlertService,
    processHistoryManager: ActivityProcessHistoryManager,
    @Inject('$localStorage') public $localStorage: any,
    activityResourceService: ActivityResourceService,
    fileResourceService: FileResourceService,
    modalService: BsModalService,
    toastr: ToastrService,
    session: SessionStorage,
    linkService: LinkService) {
    super($state,
      transition,
      orderService,
      orderTypeService,
      nsiResourceService,
      solarResourceService,
      breadcrumbsService,
      platformConfig,
      $localStorage,
      activityResourceService,
      alertService,
      processHistoryManager,
      toastr,
      fileResourceService,
      modalService,
      session,
      linkService);
  }

  onTabSelected(index: number) {
    if (this.activeTab === index) {
      this.hideOvers = !this.hideOvers;
    } else {
      this.hideOvers = true;
    }
    this.activeTab = index;
  }

  delete() {
    this.isUpdating = true;

    from(this.alertService.confirm({
      message: 'После удаления документ будет невозможно востановить',
      okButtonText: 'Удалить',
      type: 'danger'
    })).pipe(
      mergeMap(() => {
        return this.orderService.markAsDeleted(this.order);
      }),
      mergeMap(() => {
        return this.processHistoryManager.deleteProcess(this.order.bpmProcess.bpmProcessId);
      })
    ).subscribe(() => {
      this.toastr.success("Документ удален");
      if (this.orderType.orderMKA) {
        this.$state.go('app.order.mka-showcase')
      } else {
        this.$state.go('app.order.showcase');
      }
      this.isUpdating = false;
    }, () => {
      this.isUpdating = false;
    });
  }

  editCard() { }
}
