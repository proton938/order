import { PlatformConfig } from '@reinform-cdp/core';
import { FileResourceService } from "@reinform-cdp/file-resource";
import { ICdpBpmProcessManagerConfig, ActivityResourceService, ActivityProcessHistoryManager, QueryProcessInstance, IProcessDefinition } from "@reinform-cdp/bpm-components";
import * as _ from "lodash";
import { ToastrService } from "ngx-toastr";
import { BsModalService } from "ngx-bootstrap";
import { StateService, Transition } from '@uirouter/core';
import { Component, OnInit, Inject } from '@angular/core';
import { LoadingStatus, AlertService } from "@reinform-cdp/widgets";
import { NsiResourceService } from "@reinform-cdp/nsi-resource";
import { Order } from "../../../../../models/order/Order";
import { Observable } from "rxjs/Rx";
import { of, from, throwError, forkJoin } from "rxjs/index";
import { OrderService } from "../../../../../services/order.service";
import { OrderStatus } from "../../../../../models/order/OrderStatus";
import { OrderType } from "../../../../../models/order/OrderType";
import { OrderTypeService } from "../../../../../services/order-type.service";
import { map, mergeMap, tap, catchError } from "rxjs/internal/operators";
import { UploadFileModalComponent } from "../../upload-file/upload-file.modal";
import { SearchExtDataItem, SolarResourceService } from "@reinform-cdp/search-resource";
import { BreadcrumbsService } from "@reinform-cdp/skeleton";
import { SessionStorage } from '@reinform-cdp/security';
import { OrderApprovalHistory, OrderApprovalHistoryList } from '../../../../../models/order/Order';
import { FileType } from "../../../../../models/order/FileType";
import { BlockUI, NgBlockUI } from "ng-block-ui";
import * as angular from 'angular';
import { formatDate } from '@angular/common';
import { error } from 'protractor';
import {LinkService} from "../../../../../services/link.service";

@Component({
  selector: 'mggt-desktop-order',
  templateUrl: './desktop.component.html',
})
export class DesktopOrderComponent implements OnInit {

  loadingStatus: LoadingStatus;
  success = false;

  order: Order;
  origOrder: Order;
  orderType: OrderType;
  orderStatuses: { [key: string]: string } = {};
  editingOrder: boolean;
  assignments: any;
  canDelete: boolean = false;

  coordinators: QueryProcessInstance[] = [];

  activeTab: number = 0;

  activeAssignees: string[] = [];
  activeOwners: string[] = [];

  processManagerConfig: ICdpBpmProcessManagerConfig;

  instructions: any[] = [];

  isUpdating: boolean = false;
  isDeleted: boolean = false;
  isRecalling: boolean = false;
  isUploadDisabled: boolean;
  isRecallDisabled: boolean;
  isNegotiationProject: boolean = true;
  isShowCreateInstructionBtn: boolean = false;

  showHistory: boolean = false;
  showJson: boolean = false;
  showReviewInner: boolean = false;
  showInstructions: boolean = false;

  @BlockUI('desktopOrder') blockUI: NgBlockUI;

  constructor(
    public $state: StateService,
    private transition: Transition,
    public orderService: OrderService,
    private orderTypeService: OrderTypeService,
    private nsiResourceService: NsiResourceService,
    private solarResourceService: SolarResourceService,
    private breadcrumbsService: BreadcrumbsService,
    private platformConfig: PlatformConfig,
    @Inject('$localStorage') public $localStorage: any,
    public activityResourceService: ActivityResourceService,
    public alertService: AlertService,
    public processHistoryManager: ActivityProcessHistoryManager,
    public toastr: ToastrService,
    public fileResourceService: FileResourceService,
    public modalService: BsModalService,
    public session: SessionStorage,
    private linkService: LinkService) { }

  ngOnInit() {
    let id: string = this.transition.params()['id'];

    this.processManagerConfig = {
      sysName: this.platformConfig.systemCode.toLowerCase(),
      linkVarValue: id
    };
    this.showHistory = this.session.hasPermission('SDO_ORDER_ORDER_HISTORY');
    this.showJson = this.session.hasPermission('SDO_ORDER_ORDER_JSON');
    
    this.loadingStatus = LoadingStatus.LOADING;
    this.editingOrder = this.transition.params()['editingOrder'];
    this.nsiResourceService.get('mggt_order_OrderStatuses').then(result => {
      this.orderStatuses = _.reduce(result, (obj, status: OrderStatus) => {
        obj[status.code] = status.statusAnalytic;
        return obj;
      }, {});
    });

    let observable: Observable<Order>;
    if (!id) {
      observable = this.newOrder();
      this.editingOrder = true;
    } else {

      observable = this.orderService.get(id).pipe(
        mergeMap(order => {
          return this.getOrderType(order.documentTypeCode).pipe(
            map(orderType => {
              this.orderType = orderType;
              return order;
            })
          )
        }),
        mergeMap(order => {
          this.updateBreadcrumbs();
          if (this.orderType.orderMKA) {
            return from(this.solarResourceService.searchExt({
              page: 0,
              pageSize: 10000,
              fields: [
                new SearchExtDataItem('orderMKAID', id),
                new SearchExtDataItem('docTypeCode', 'INSTRUCTION')
              ]
            })).pipe(map(response => {
              this.assignments = response.docs || [];
              return order;
            })
            );
          }
          else {
            return of(order);
          }
        })
      );
    }

    observable.subscribe(order => {
      this.order = order;
      this.origOrder = angular.copy(order);
      this.isUploadDisabled = !this.order.documentID || !this.order.documentNumber;
      this.isDeleted = this.order.documentStatusCode === "DELETED";

      this.isShowCreateInstructionBtn = this.order.documentStatusCode === 'REGISTERMAIL';
      this.updateInstructionList();

      this.showReviewInner = (!this.order.receivedFromMKA||!this.order.receivedFromMKA.FromMKA)&&
        !!this.order.reviewInner&&this.order.reviewInner.some(item=>
          !!item.reviewBy&&!!item.reviewBy.accountName);

      this.activityResourceService.queryTasks({
        active: true,
        processInstanceVariables: [{
          name: "EntityIdVar",
          value: this.order.documentID,
          operation: "equals",
          type: "string"
        }]
      }).then((tasksStatus) => {
        let editDoc = _.some(tasksStatus.data, x => {
          return /^EditDoc(New|Paral)?$/.test(x.formKey);
        });

        this.canDelete = this.order.documentStatusCode === "PROJECT" || (this.order.documentStatusCode === "APPROVAL" && editDoc);

        this.isNegotiationProject = _.some(tasksStatus.data, x => {
          const isNegotiation = x.formKey.indexOf('NegotiationProjectOrderParal') > -1 || x.formKey.indexOf('NegotiationProjectOrderNew') > -1 || x.formKey.indexOf('NegotiationProjectSign') > -1;
          const isResponsibleExecutor = !!this.order.responsibleExecutor&&this.session.login() === this.order.responsibleExecutor.accountName;
          return isNegotiation && isResponsibleExecutor;
        });

        this.activeAssignees = _.map(tasksStatus.data, x => x.assignee);
        this.activeOwners = _.map(tasksStatus.data, x => x.owner ? x.owner : x.assignee);

        this.loadingStatus = LoadingStatus.SUCCESS;
        this.success = true;
      });
    })
  }

  deleteCard() {
    this.isUpdating = true;

    from(this.alertService.confirm({
      message: 'После удаления документ будет невозможно востановить',
      okButtonText: 'Удалить',
      type: 'danger'
    })).pipe(
      mergeMap(() => {
        return this.orderService.markAsDeleted(this.order);
      }),
      mergeMap((order) => {
        this.order = order;
        this.isDeleted = this.order.documentStatusCode === "DELETED";

        return this.processHistoryManager.deleteProcess(this.order.bpmProcess.bpmProcessId);
      })
    ).subscribe(() => {
      this.toastr.success("Документ удален");
      
      this.isUpdating = false;
    }, () => {
      this.isUpdating = false;
    });
  }

  private deleteOrderFile(): Observable<any> {
    return from(this.fileResourceService.deleteFile(this.order.files[0].fileID)).pipe(
      mergeMap(() => {
        return this.orderService.deleteFiles(this.order);
      })
    );
  }

  openUploadDocument(ignoreExistedFiles: boolean = false) {
    if (!ignoreExistedFiles && _.size(this.order.files)) {
      from(this.alertService.confirm({
        message: 'Для добавления нового файла, сначала необходимо удалить уже существующий файл',
        okButtonText: 'Удалить',
        type: 'danger'
      })).pipe(
        mergeMap(() => {
          return this.deleteOrderFile();
        })
      ).subscribe((order) => {
        this.order = order;
        this.showUploadWindow();
      });
      return;
    }
    this.showUploadWindow();
  }

  private showUploadWindow() {
    const options = {
      initialState: {
        order: this.order,
      },
      'class': 'modal-lg'
    };
    const ref = this.modalService.show(UploadFileModalComponent, options);
    this.modalService.onHide.pipe(
      mergeMap(() => {
        let component = <UploadFileModalComponent>ref.content;
        if (component.submit) {
          this.order = component.newOrder;
          return this.orderService.sendToDocumentsDB(this.order.documentID);
        } else {
          return throwError('cancelled');
        }

      })
    ).subscribe(() => {
      // this.$state.reload();
    });
  }

  private getOrderType(code): Observable<OrderType> {
    return this.orderTypeService.geOrderTypes().pipe(
      map(orderTypes => {
        return _.find(orderTypes, (type: OrderType) => type.code === code);
      })
    )
  }

  newOrder(): Observable<Order> {
    let order = <Order>{
      documentID: "",
      bpmProcess: null,
      documentTypeCode: "",
      documentTypeValue: "",
      documentYear: (new Date()).getFullYear(), // current year
      documentNumber: null,
      documentDate: null,
      documentExpireDate: null,
      canceledDocument: null,
      documentStatusCode: "NEW",
      documentStatusValue: "Новый",
      documentStatusColor: "warning",
      documentContent: "",
      address: "",
      approvedDocumentNumber: "",
      createDateTime: null,
      createAuthor: null,
      barcode: null,
      folderID: "",
      files: [],
      head: null,
      executor: [],
      registar: null,
      mailList: [],
      mailDateTime: null,
      psoInputID: null,
      psoOutputID: [],
      draftFiles: null,
      signingElectronically: false,
      approval: null,
      approvalHistory: null,
      SigningOnPaper: null,
      draftFilesWithoutAtt: null,
      attach: null,
      documentThemeCode: '1223 ',
      documentThemeValue: null,
      additionalMaterials: [],
      draftFilesRegInfo: null,
      ds: null
    };
    return of(order);
  }

  copyOrder(): Observable<Order> {
    this.loadingStatus = LoadingStatus.LOADING;
    let fieldsToCopy = _.pick(this.order, [
      'documentTypeCode',
      'documentTypeValue',
      'documentYear',
      'documentContent',
      'head',
      'executor',
      'registar',
      'mailList'
    ]);
    let newOrder: Order = _.extend(new Order(), {
      documentID: "",
      documentTypeCode: "",
      documentTypeValue: "",
      documentYear: (new Date()).getFullYear(), // current year
      documentNumber: null,
      documentDate: "",
      documentExpireDate: null,
      canceledDocument: null,
      documentStatusCode: "NEW",
      documentStatusValue: "Новый",
      documentStatusColor: "warning",
      documentContent: "",
      address: "",
      approvedDocumentNumber: "",
      createDateTime: null,
      createAuthor: null,
      barcode: null,
      folderID: "",
      files: [],
      head: null,
      executor: [],
      registar: null,
      mailList: [],
      mailDateTime: null,
      psoInputID: null,
      psoOutputID: []
    }, fieldsToCopy);
    return of(newOrder);
  }

  createRelated() {
    this.$state.go('app.order.initiate', {
      linkedDocumentID: this.order.documentID,
      linkedDocumentType: this.order.documentTypeValue
    });
  }

  editOrder(edit: boolean) {
    this.editingOrder = edit;
  }

  createCopy() {
    this.copyOrder().subscribe(() => this.editOrder(true));
  }

  updateBreadcrumbs() {
    if (this.orderType.orderMKA) {
      this.breadcrumbsService.setBreadcrumbsChain([{
        title: 'Распорядительного документа МКА',
        url: this.$state.href('app.order.mka-showcase', {})
      }, {
        title: 'Распорядительный документ МКА',
        url: null
      }]);
    } else {
      this.breadcrumbsService.setBreadcrumbsChain([{
        title: 'Распорядительные документы',
        url: this.$state.href('app.order.showcase', {})
      }, {
        title: 'Распорядительный документ',
        url: null
      }]);
    }
  }

  isActive(index: number): boolean {
    return index === this.activeTab;
  }

  activate(index: number): boolean {
    if (this.activeTab === index) {
      return false;
    } else {
      this.activeTab = index;
      return true;
    }
  }

  recall(): void {
    from(this.alertService.confirm({
      message: 'Отозвать РД с согласования?',
      okButtonText: 'Да',
      type: 'danger'
    }).catch(reject=>{
      console.log("Recall is cancelled");
      throw {
        isCanceledRecall:true,
        message:'Recall is cancelled'
      };
    })).pipe(
      tap(() => {
        this.isRecalling = true;
        this.blockUI.start();
      }),
      mergeMap(() => {
        return this.closeCycle();
      }),
      mergeMap(()=>{
        return this.activityResourceService.queryTasks({
          active: true,
          processInstanceVariables: [{
            name: "EntityIdVar",
            value: this.order.documentID,
            operation: "equals",
            type: "string"
          }]
        });
      }),
      mergeMap((tasksStatus) => {
        _.some(tasksStatus.data, x => {
          if(x.formKey.indexOf('NegotiationProjectOrderParal') > -1)
            this.order.returnToEditing=true;
          if(x.formKey.indexOf('NegotiationProjectOrderNew') > -1 || x.formKey.indexOf('NegotiationProjectSign') > -1)
            this.order.returnToEditingSeq=true;
          return this.order.returnToEditing||this.order.returnToEditingSeq
        });
        return this.updateOrder();
      }),
      mergeMap((order: Order) => {
        this.origOrder = order;
        
        return this.activityResourceService.queryProcessInstances({
          variables: [{
            name: 'EntityIdVar',
            value: this.order.documentID,
            operation: 'equals',
            type: 'string'
          }]
        });
      }),
      mergeMap((processInstances) => {
        this.coordinators = processInstances.data.filter(instance => instance.processDefinitionKey === 'sdord_coordinationOrders_ID');
        const instances = processInstances.data.filter(instance => instance.processDefinitionKey !== 'sdord_coordinationOrders_ID');
        if(!instances.length)return of(null);
        return forkJoin(instances.map(instance => this.activityResourceService.deleteProcess(instance.id)));
      }),
      mergeMap(() => {
        if(!this.coordinators.length)return of(null);
        return forkJoin(this.coordinators.map(coordinator => this.activityResourceService.deleteProcess(coordinator.id)));
      }),
      mergeMap(() => {
        return this.activityResourceService.getProcessDefinitions({ key: 'sdord_coordinationOrders_ID', latest: true });
      }),
      mergeMap((response) => {
        const processes = response.data;
        if (processes.length <= 0) {
          return throwError('Процесс sdord_coordinationOrders_ID не найден');
        } else {
          return of(processes[0]);
        }
      }),
      mergeMap((process: IProcessDefinition) => {
        return this.activityResourceService.initProcessForced({
          processDefinitionId: process.id,
          variables: [{
            name: 'EntityIdVar',
            value: this.order.documentID
          }, {
            name: 'EntityDescriptionVar',
            value: this.order.documentTypeValue + ' ' + this.order.documentContent
          }, {
            name: 'ContractorVar',
            value: this.session.login()
          }]
        });
      }),
      mergeMap((bpmProcessId) => {
        this.order.bpmProcess.bpmProcessId = bpmProcessId;

        return this.updateOrder();
      }),
      mergeMap(() => {
        return this.orderService.mailReturnToEditNotifications(this.order.documentID, this.activeAssignees);
      }),
      catchError((error) => {
        console.log(error);        
        if(!!error&&!!error.isCanceledRecall)return throwError(error);
        this.toastr.error('Распорядительный документ не удалось вернуть на доработку');
        this.isRecalling = false;
        this.blockUI.stop();
        return throwError(error);
      })
    ).subscribe(() => {
      this.toastr.success('Распорядительный документ возвращен на доработку');
      this.isRecalling = false;
      this.blockUI.stop();
      setTimeout(() => {
        window.location.reload();
      }, 1000);
      // window.location.reload();
      // window.location.href = '/main/#/app/tasks';
    },error=>{
      this.isRecalling = false;
      this.blockUI.stop();
    });
  }

  closeCycle(): Observable<string> {
    // this.order.returnToEditing = true;
    if(!this.order.approval.approvalCycle||!this.order.approval.approvalCycle.agreed)
      return of('');
    let fio:string='';
    if(!!this.order.responsibleExecutor&&!!this.order.responsibleExecutor.iofShort){
      let iof = this.order.responsibleExecutor.iofShort.split('.');
      let lastName = iof.splice(iof.length-1)[0].trim()+' ';
      fio=lastName + iof.join('.')+'.';
    }
    
    
    this.order.approval.approvalCycle.agreed = this.order.approval.approvalCycle.agreed.map(agree => {
      if (this.activeOwners.includes(agree.agreedBy.accountName) && !agree.approvalResult) {
        agree.approvalResult = agree.approvalTypeCode === 'approval' ? 'Не утверждено' : 'Не согласовано';
        agree.approvalNote = 'Отозвано инициатором '+fio;
        return agree;
      }
      return agree;
    });
    this.order.approvalHistory = this.order.approvalHistory || new OrderApprovalHistory();
    const approvalCycle = <OrderApprovalHistoryList>angular.copy(this.order.approval.approvalCycle);
    const draftFile = this.order.draftFiles;
    const fileName = `${this.getFileName(draftFile.draftFilesName)}-согласование-${formatDate(new Date(), 'yyyy-MM-dd HH:mm:ss', 'en')}.${this.getFileExtension(draftFile.draftFilesName)}`;

    return this.copyDraftFile(fileName).pipe(
      mergeMap((fileType) => {
        approvalCycle.approvalCycleFile = approvalCycle.approvalCycleFile || [];
        approvalCycle.approvalCycleFile.push(fileType);

        this.order.approvalHistory.approvalCycle = this.order.approvalHistory.approvalCycle || [];
        this.order.approvalHistory.approvalCycle.push(approvalCycle);
        this.order.approval.approvalCycle = null;

        if (this.order.draftFiles.draftFilesSigned) {
          this.order.draftFiles.draftFilesSigned = false;
        }
        return of('');
      })
    );
  }

  getFileName(fullFileName: string): string {
    let dotIndex = fullFileName.lastIndexOf('.');
    if (dotIndex < 0) {
      return fullFileName;
    } else {
      return fullFileName.substring(0, dotIndex);
    }
  }

  getFileExtension(fullFileName: string): string {
    let dotIndex = fullFileName.lastIndexOf('.');
    if (dotIndex < 0) {
      return '';
    } else {
      return fullFileName.substring(dotIndex + 1, fullFileName.length);
    }
  }

  copyDraftFile(targetFileName: string): Observable<FileType> {
    return from(this.fileResourceService.copyFile(this.order.draftFiles.draftFilesID, targetFileName)).pipe(
      map((fileType) => {
        return FileType.create(fileType.versionSeriesGuid, fileType.fileName, fileType.fileSize.toString(),
          false, new Date(), fileType.fileType, fileType.mimeType);
      })
    );
  }

  updateOrder(): Observable<any> {
    return this.orderService.update(this.order.documentID, this.orderService.diff(this.origOrder, this.order));
  }

  // Связанные поручения
  updateInstructionList() {
    this.linkService.findFromSolr(this.order.documentID, ['SDO_INSTRUCTION_INSTRUCTION']).subscribe(res => {
      this.instructions = res;
      this.showInstructions = this.instructions.length > 0;
    });
  }
}
