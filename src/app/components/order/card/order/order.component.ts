import {Component, Inject, Input, OnInit} from '@angular/core';

@Component({
  selector: 'mggt-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.scss']
})
export class OrderComponent implements OnInit {
  constructor(@Inject('$localStorage') public $localStorage: any) { }

  ngOnInit() {
  }
}
