import {BsModalRef} from "ngx-bootstrap";
import {Component, Input, OnInit} from '@angular/core';
import {Order} from "../../../../models/order/Order";

@Component({
    selector: 'order-modal-content',
    templateUrl: './service-information.modal.html'
})
export class ServiceInformationModalComponent implements OnInit {

    @Input() order: Order;

    constructor(private bsModalRef: BsModalRef) {
    }

    ngOnInit() {
    }

    cancel() {
        this.bsModalRef.hide();
    }

}
