import { Component, Input, OnInit } from '@angular/core';
import { ToastrService } from "ngx-toastr";
import { CdpReporterPreviewService } from "@reinform-cdp/reporter-resource";
import * as fs from 'file-saver';
import * as _ from 'lodash';
import { ActivityResourceService, IHistoricProcessInstance } from '@reinform-cdp/bpm-components';
import { Order } from "../../../../models/order/Order";
import {ActivityExtService} from "../../../../services/activity-ext.service";
import { LoadingStatus } from '@reinform-cdp/widgets';
import {HelperService} from "../../../../services/helper.service";

@Component({
  selector: 'mggt-order-approvals',
  templateUrl: './order-approvals.component.html',
  styleUrls: ['./order-approvals.component.scss']
})
export class OrderApprovalsComponent implements OnInit {

  @Input() order: Order;
  cycleShow: { [key: string]: boolean } = {};
  loadingStatus: LoadingStatus = LoadingStatus.LOADING;
  loading: boolean = true;
  isOldBP: boolean = false;

  constructor(
    public reporterPreviewService: CdpReporterPreviewService,
    public toastr: ToastrService,
    public activityExt: ActivityExtService,
    public helper: HelperService
  ) {
  }

  ngOnInit() {
      const bpKey = 'sdord_coordinationOrders_ID';
      const borderDate: Date = new Date('2019-12-30T16:58:22');
      this.activityExt.resource.getProcessHistoryByReqNum(this.order.documentID, 'EntityIdVar').then(res => {
          if (res && res.data) {
              const instance: IHistoricProcessInstance = _.find(res.data, i => i.processDefinitionId.indexOf(bpKey) === 0);
              if (instance) {
                  this.activityExt.getProcessInfo(instance.processDefinitionId).subscribe((info: any) => {
                      if (info && info.releaseDateTime) {
                          if (new Date(info.releaseDateTime) < borderDate) this.isOldBP = true;
                      }
                      this.loadingStatus = LoadingStatus.SUCCESS;
                  }, err => {
                      console.log(err);
                      this.loadingStatus = LoadingStatus.SUCCESS;
                  });
              } else {
                  this.loadingStatus = LoadingStatus.SUCCESS;
              }
          } else {
              this.loadingStatus = LoadingStatus.SUCCESS;
          }
      }).catch(e => {
          console.log(e);
          this.loadingStatus = LoadingStatus.SUCCESS;
      });
  }

  savePrintForm() {
    this.reporterPreviewService
      .nsi2Preview({
        options: {
          jsonSourceDescr: "",
          onProcess: 'Лист согласования с основными согласующими',
          placeholderCode: "",
          strictCheckMode: true,
          xwpfResultDocumentDescr: 'Лист согласования с основными согласующими.docx',
          xwpfTemplateDocumentDescr: ""
        },
        jsonTxt: this.helper.toJson({ document: this.order }),
        rootJsonPath: "$",
        nsiTemplate: {
          templateCode: 'AdmOrderLS'
        },
      })
      .then((blob) => {
        fs.saveAs(blob, `Лист согласования с основными согласующими.docx`);
      });
  }

  get isSuccess() {
      return this.loadingStatus === LoadingStatus.SUCCESS;
  }
}
