import {Component, Input, OnInit} from '@angular/core';
import { ToastrService } from "ngx-toastr";
import { CdpReporterPreviewService } from "@reinform-cdp/reporter-resource";
import {Order} from "../../../../../models/order/Order";
import { OrderApprovalsComponent } from '../order-approvals.component';
import { ActivityResourceService } from '@reinform-cdp/bpm-components';
import {ActivityExtService} from "../../../../../services/activity-ext.service";
import {HelperService} from "../../../../../services/helper.service";

@Component({
  selector: 'mggt-mobile-order-approvals',
  templateUrl: './order-approvals.component.html',
  styleUrls: ['./order-approvals.component.scss']
})
export class MobileOrderApprovalsComponent extends OrderApprovalsComponent {
  @Input() order: Order;
  cycleShow: { [key: string]: boolean } = {};

  constructor(
    public reporterPreviewService: CdpReporterPreviewService,
    public toastr: ToastrService,
    public activityExt: ActivityExtService,
    public helper: HelperService
  ) {

    super(reporterPreviewService, toastr, activityExt, helper);
  }
}
