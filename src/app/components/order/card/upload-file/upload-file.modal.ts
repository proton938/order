import {ToastrService} from "ngx-toastr";
import {BsModalRef} from "ngx-bootstrap";
import {Component, Input, OnInit} from '@angular/core';
import {DropzoneConfigInterface} from "ngx-dropzone-wrapper";
import {Order} from "../../../../models/order/Order";
import {OrderService} from "../../../../services/order.service";
import {FileType} from "../../../../models/order/FileType";

@Component({
    selector: 'order-modal-content',
    templateUrl: './upload-file.modal.html',
})
export class UploadFileModalComponent implements OnInit {

    @Input() order: Order;

    newOrder: Order;
    config: DropzoneConfigInterface;

    submit: boolean = false;

    constructor(private bsModalRef: BsModalRef, private orderService: OrderService, private toastr: ToastrService) {
    }

    ngOnInit() {
        let self = this;
        this.config = {
            autoProcessQueue: true,
            acceptedFiles: ".pdf,.doc,.docx",
            maxFiles: 1,
            params: {
                folderGuid: this.order.folderID,
                fileType: 'MkaDocOther',
                docEntityID: this.order.documentID,
                docSourceReference: 'UI'
            },
            init: function () {
                this.on("maxfilesexceeded", function (file, errorMessage) {
                    self.toastr.warning('Можно загрузить всего один файл.', 'Ошибка');
                    this.removeFile(file);
                });
                this.on("error", function (file, errorMessage) {
                    self.toastr.warning(errorMessage, 'Ошибка');
                    this.removeFile(file);
                });
                this.on("success", function (file, id) {
                    console.log(id);
                    this.removeFile(file);
                    self.save(id, file);
                });
            }
        };
    }

    save(guid: string, file: File) {
        const fileType: FileType = FileType.create(guid, file.name, file.size.toString(), false, new Date(), file.type, 'MkaDocOther');
        this.orderService.addFile(this.order, fileType).subscribe((order) => {
            this.newOrder = order;
            this.submit = true;
            this.bsModalRef.hide();
        }, () => {});
    }

    cancel() {
        this.bsModalRef.hide();
    }

}
