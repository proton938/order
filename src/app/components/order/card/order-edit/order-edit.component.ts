import * as _ from "lodash";
import * as angular from "angular";
import {ToastrService} from "ngx-toastr";
import {StateService} from '@uirouter/core';
import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Order, PreviousDocument} from "../../../../models/order/Order";
import {AlertService, LoadingStatus} from "@reinform-cdp/widgets";
import {OrderType} from "../../../../models/order/OrderType";
import {EmailGroup} from "../../../../models/order/EmailGroup";
import {OrderTheme} from "../../../../models/order/OrderTheme";
import {Observable, Subject} from "rxjs/Rx";
import {OrderService} from "../../../../services/order.service";
import {DepartmentBean, NsiResourceService, UserBean} from "@reinform-cdp/nsi-resource";
import {CdpSolrResourceService} from "@reinform-cdp/search-resource";
import {AuthorizationService} from "@reinform-cdp/security";
import {concat, forkJoin, from, of} from "rxjs/index";
import {
    catchError, debounceTime, distinctUntilChanged, filter, map, mergeMap, switchMap,
    tap
} from "rxjs/internal/operators";
import {OrderStatus} from "../../../../models/order/OrderStatus";
import {User} from "../../../../models/order/User";
import {FileResourceService} from '@reinform-cdp/file-resource';
import {CancelType} from "../../../../models/order/CancelType";
import {UpdateType} from "../../../../models/order/UpdateType";
import {OrderTypeService} from "../../../../services/order-type.service";
import {RegisterDocumentService} from "../../../../services/register-document.service";
import {CancelDocumentService} from "../../../../services/cancel-document.service";
import {UserService} from "../../../../services/user.service";

@Component({
    selector: 'mggt-order-edit',
    templateUrl: './order-edit.component.html'
})
export class OrderEditComponent implements OnInit {

    loadingStatus: LoadingStatus;
    success = false;

    @Input() order: Order;
    origOrder: Order;
    @Output() cancel: EventEmitter<any> = new EventEmitter<any>();

    isCanceledDocumentDisabled: boolean;

    orderTypes: OrderType[];
    orderType: OrderType;
    executives: User[];

    executors$: Observable<UserBean[]>;
    executorsLoading = false;
    executorsInput$ = new Subject<string>();

    registrars: User[];
    departments: DepartmentBean[];
    emailGroups: EmailGroup[];
    availableYears: number[];
    themes: OrderTheme[];
    availableThemes: OrderTheme[] = [];
    updatedTypes: string[] = [];

    hasAccessRight: boolean = true;

    selectedEmailGroups: EmailGroup[];

    canceledDocuments$: Observable<CancelType[]>;
    canceledDocumentsLoading = false;
    canceledDocumentsInput$ = new Subject<string>();

    updatedDocuments$: Observable<UpdateType[]>;
    updatedDocumentsLoading = false;
    updatedDocumentsInput$ = new Subject<string>();

    subscribers$: Observable<User[]>;
    subscribersInput$ = new Subject<string>();

    additionalMaterials: any[] = [];

    constructor(private $state: StateService,
                private orderService: OrderService,
                private orderTypeService: OrderTypeService,
                private nsiResourceService: NsiResourceService,
                private cdpSolrResourceService: CdpSolrResourceService,
                private fileResourceService: FileResourceService,
                private alertService: AlertService,
                private toastr: ToastrService,
                private authorizationService: AuthorizationService,
                private cancelDocumentService: CancelDocumentService,
                private userService: UserService,
                private registerDocumentService: RegisterDocumentService) {
    }

    ngOnInit() {
        if (!this.authorizationService.check('ORDER_DOCUMENT_FORM')) {
            this.loadingStatus = LoadingStatus.ERROR;
            return;
        }

        this.loadingStatus = LoadingStatus.LOADING;

        this.availableYears = _.range(2016, 2028);

        this.order = angular.copy(this.order);
        this.origOrder = angular.copy(this.order);

        if (this.order.additionalMaterials) {
            this.additionalMaterials = this.order.additionalMaterials.map(file => ({
                nameFile: file.fileName,
                idFile: file.fileID,
                sizeFile: file.fileSize,
                dateFile: file.fileDate,
                signed: file.fileSize,
                mimeType: file.mimeType,
                typeFile: file.fileType
            }));
        }

        this.isCanceledDocumentDisabled = this.order.documentStatusCode !== 'NEW';

        this.canceledDocuments$ = concat(
            of([]),
            this.canceledDocumentsInput$.pipe(
                debounceTime(300),
                distinctUntilChanged(),
                tap(() => this.canceledDocumentsLoading = true),
                switchMap((term: string) => this.searchCanceledDocuments(term)),
                catchError(() => of([])),
                tap(() => this.canceledDocumentsLoading = false)
            )
        );

        this.updatedDocuments$ = concat(
            of([]),
            this.updatedDocumentsInput$.pipe(
                debounceTime(300),
                distinctUntilChanged(),
                tap(() => this.updatedDocumentsLoading = true),
                switchMap((term: string) => this.searchUpdatedDocuments(term)),
                catchError(() => of([])),
                tap(() => this.updatedDocumentsLoading = false)
            )
        );

        this.subscribers$ = concat(
            of([]),
            this.subscribersInput$.pipe(
                debounceTime(300),
                distinctUntilChanged(),
                filter(term => term && term.length > 1),
                switchMap((term: string) => this.searchSubscribers(term)),
                catchError(() => of([])),
                tap(() => this.updatedDocumentsLoading = false)
            )
        );

        this.executors$ = concat(
            of([]),
            this.executorsInput$.pipe(
                debounceTime(300),
                distinctUntilChanged(),
                filter(term => term && term.length > 2),
                tap(() => this.executorsLoading = true),
                switchMap((term: string) => this.searchExecutors(term)),
                catchError(() => of([])),
                tap(() => this.executorsLoading = false)
            )
        );

        this.loadDictionaries().subscribe(() => {
            this.loadingStatus = LoadingStatus.SUCCESS;
            this.success = true;
        }, () => {
            this.loadingStatus = LoadingStatus.ERROR;
        });
    }

    loadDictionaries(): Observable<any> {
        return forkJoin([
            this.orderTypeService.geOrderTypes(),
            this.nsiResourceService.get('EmailGroups'),
            this.nsiResourceService.searchUsers({group: ['MGGT_ADMORDER_REGISTER']}),
            this.nsiResourceService.get('mggt_order_OrderThemes'),
            this.nsiResourceService.get('mggt_order_OrderStatuses'),
            this.nsiResourceService.departments()
        ]).pipe(
            tap((values: [OrderType[], EmailGroup[], UserBean[], OrderTheme[], OrderStatus[], DepartmentBean[]]) => {
                this.orderTypes = values[0];
                this.orderType = _.find(this.orderTypes, (type: OrderType) => type.code === this.order.documentTypeCode);
                if (this.orderType.accessEI) {
                  //TODO: OASI_ADMORDER_CARD_EI
                  this.hasAccessRight = this.authorizationService.check('OASI_ADMORDER_CARD_EI');
                }
                this.emailGroups = values[1].filter(emailGroup => {
                    return emailGroup.sendSystem && emailGroup.sendSystem.find(sendSystem => {
                        return sendSystem.code === 'MGGT_ORDER';
                    });
                });
                this.registrars = values[2].map(user => User.fromUserBean(user));
                this.themes = values[3];
                this.updatedTypes = values[4]
                    .filter(status => status.statusAnalytic === 'Действующий')
                    .map(status => status.code);
                this.departments = values[5];

                this.documentTypeChanged();
            })
        );
    }

    loadDependentOnOrderType() {
        let orderType = _.find(this.orderTypes, (type: OrderType) => type.code === this.order.documentTypeCode);
        this.userService.getExecutives(orderType).subscribe((executives: UserBean[]) => {
            this.executives = executives.map(user => User.fromUserBean(user));
        })
    }

    documentTypeChanged() {
        if (this.order.documentTypeCode) {
            this.loadDependentOnOrderType();
            this.availableThemes = _.filter(this.themes, (th: OrderTheme) => {
                return th.docCode === this.order.documentTypeCode;
            })
        } else {
            this.availableThemes = [];
        }
    }

    themeChanged() {
        if (this.order.documentThemeCode) {
            this.order.documentThemeValue = _.find(this.availableThemes, (th: OrderTheme) => {
                return th.themeCode === this.order.documentThemeCode;
            }).themeName;
        } else {
            delete this.order.documentThemeValue;
        }
    }

    isThemeDisabled() {
        return this.order.approvedDocumentNumber || this.order.previousDocument;
    }

    searchCanceledDocuments(query): Observable<CancelType[]> {
        return from(this.cdpSolrResourceService.query({
            types: ['SDO_ORDER_ORDER'],
            query: query ? `docName:${query}*` : '',
            pageSize: 20
        })).pipe(
            map(result => {
                return _.map(result.docs, (doc: any) => {
                    return {
                        canceledDocumentID: doc.sys_documentId,
                        canceledDocumentNum: doc.docNumber,
                        canceledDocumentDate: doc.docDate,
                        canceledDocumentType: doc.docType,
                        canceledDocumentTypeCode: doc.docTypeCode,
                        docName: doc.docName
                    };
                });
            })
        );
    }

    searchUpdatedDocuments(query): Observable<UpdateType[]> {
        query = [
            `docStatus:(${this.updatedTypes.join(" OR ")})`,
            query ? `docName:${query}*` : null
        ].filter(obj => obj).join(" AND ");
        return from(this.cdpSolrResourceService.query({
            types: ['SDO_ORDER_ORDER'],
            query: query
        })).pipe(
            map(result => {
                return _.map(result.docs, (doc: any) => {
                    return {
                        updatedDocumentID: doc.sys_documentId,
                        updatedDocumentNum: doc.docNumber,
                        updatedDocumentDate: doc.docDate,
                        updatedDocumentType: doc.docType,
                        updatedDocumentTypeCode: doc.docTypeCode,
                        docName: doc.docName
                    };
                });
            })
        );
    }

    searchSubscribers(query): Observable<User[]> {
        return from(this.nsiResourceService.searchUsers({fio: query})).pipe(
            map(users => users.filter(user => !!user.mail).map(user => User.fromUserBean(user)))
        );
    }

    searchExecutors(query): Observable<User[]> {
        return from(this.nsiResourceService.searchUsers({fio: query})).pipe(
            map(users => users.map(user => User.fromUserBean(user)))
        );
    }

    validate() {
        if (!this.order.documentTypeCode || !this.order.documentYear || !this.order.documentContent ||
            !this.order.executor || !this.order.mailList || this.order.executor.length === 0 || this.order.mailList.length === 0) {
            this.toastr.error('Не заполнены обязательные поля');
            return false;
        }
        return true;
    }

    save() {
        if (!this.validate()) {
            return;
        }
        this.saveOrder(this.order).subscribe(id => {
            this.$state.go('app.order.document', {
                id: id,
                editingOrder: false
            }, {reload: true});
        });
    }

    saveOrder(document: Order): Observable<string> {
        return this.registerDocumentService.fillDocumentDate(this.order).pipe(
            mergeMap(() => {
                if (document.documentID) {
                    let id: string = document.documentID;
                    return this.orderService.update(id, this.orderService.diff(this.origOrder, this.order));
                } else {
                    return this.orderService.create(document);
                }
            }),
            map(order => {
                this.order = order;
                this.origOrder = angular.copy(order);
                return order.documentID;
            })
        )
    }

    cancelEdit() {
        this.cancel.emit();
    }

    isRequiredFieldsFilled() {
        if (!this.order.documentTypeCode) {
            return false;
        }

        if (!this.order.documentYear) {
            return false;
        }

        if (!this.order.documentContent || !this.order.documentContent.trim()) {
            return false;
        }

        return true;
    }

    generateDocumentNumber(): Observable<any> {
        return this.registerDocumentService.registerDocument(this.order).pipe(
            tap(() => {
                _.extend(this.order, {
                    documentStatusCode: 'REGISTER',
                    documentStatusValue: 'Зарегистрирован',
                    documentStatusColor: 'primary'
                });
            })
        )
    }

    registerDocument() {
        if (!this.isRequiredFieldsFilled()) {
            return this.alertService.message({
                message: 'Заполните все обязательные поля, отмеченные звездочкой'
            });
        }

        this.registerDocumentService.fillDocumentDate(this.order).pipe(
            mergeMap(() => {
                return this.cancelDocumentService.checkCanceledDocument(this.order.canceledDocument);
            }),
            mergeMap(() => {
                return this.generateDocumentNumber();
            }),
            mergeMap(() => {
                return this.saveOrder(this.order)
            }),
            mergeMap(() => {
                return this.cancelDocumentService.cancelDocument(this.order.canceledDocument, this.order.documentDate);
            }),
            mergeMap(() => {
                return this.alertService.message({
                    message: 'Документ успешно зарегистрирован, данные сохранены'
                });
            })
        ).subscribe(() => {
            this.$state.go('app.order.document', {
                id: this.order.documentID,
                editingOrder: false
            }, {reload: true});
        })
    }

    findUser(users: User[], login: string) {
        return _.find(users, (user: User) => {
            return user.accountName === login;
        })
    }

    addEmailGroup(emailGroup: EmailGroup) {
        forkJoin(emailGroup.users.filter(login => !this.findUser(this.order.mailList, login))
            .map(login => this.nsiResourceService.ldapUser(login))).pipe(
            map(users => {
                return users.filter(user => !!user.mail).map(user => User.fromUserBean(user))
            })
        ).subscribe((users) => {
            this.order.mailList = this.order.mailList.concat(users);
        })
    }

    deleteAdditionalMaterials = (fileInfo: any) => {
        from(this.fileResourceService.deleteFile(fileInfo.idFile)).subscribe(() => {
            let index = this.order.additionalMaterials.findIndex(mat => mat.fileID === fileInfo.idFile);
            this.order.additionalMaterials.splice(index, 1);
            this.additionalMaterials = this.order.additionalMaterials.map(file => ({
                nameFile: file.fileName,
                idFile: file.fileID,
                sizeFile: file.fileSize,
                dateFile: file.fileDate,
                signed: file.fileSize,
                mimeType: file.mimeType,
                typeFile: file.fileType
            }));
        });
    };

    changeAdditionalMaterials = () => {
        if (this.additionalMaterials.length > 0) {
            this.order.additionalMaterials = this.additionalMaterials.map(file => ({
                fileName: file.nameFile,
                fileID: file.idFile,
                fileSize: file.sizeFile,
                fileDate: file.dateFile,
                fileSigned: file.signed,
                mimeType: file.mimeType,
                fileType: file.typeFile
            }));
        } else {
            this.order.additionalMaterials = []
        }
    };

}
