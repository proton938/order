import * as _ from "lodash";
import { NsiResourceService, UserBean } from "@reinform-cdp/nsi-resource";
import { Observable, Subject } from "rxjs/Rx";
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Filter } from "../order-list/order-list.component";
import { IOrderShowcaseDicts } from "../../../showcase/showcase.component";
import { concat, from, of } from "rxjs/index";
import { catchError, debounceTime, distinctUntilChanged, filter, switchMap, tap } from "rxjs/internal/operators";
import { OrderStatus } from "../../../../models/order/OrderStatus";

@Component({
  selector: 'mggt-order-list-filter',
  templateUrl: './order-list-filter.component.html',
  styleUrls: ['./order-list-filter.component.scss']
})
export class OrderListFilterComponent implements OnInit {

  @Input() filter: Filter;
  @Input() dicts: IOrderShowcaseDicts;
  @Input() isMka?: boolean;

  @Output() onFilterChanged: EventEmitter<Filter> = new EventEmitter<Filter>();

  heads$: Observable<UserBean[]>;
  headsLoading = false;
  headsInput$ = new Subject<string>();

  responsibleExecutors$: Observable<UserBean[]>;
  responsibleExecutorsLoading = false;
  responsibleExecutorsInput$ = new Subject<string>();

  searchOnFilterChange = false;

  constructor(public nsiResourceService: NsiResourceService) {
  }

  ngOnInit() {

    this.heads$ = concat(
      of([]),
      this.headsInput$.pipe(
        debounceTime(300),
        distinctUntilChanged(),
        filter(term => term && term.length > 3),
        tap(() => this.headsLoading = true),
        switchMap((term: string) => this.loadHeads(term)),
        catchError(() => of([])),
        tap(() => this.headsLoading = false)
      )
    );

    this.responsibleExecutors$ = concat(
      of([]),
      this.responsibleExecutorsInput$.pipe(
        debounceTime(300),
        distinctUntilChanged(),
        filter(term => term && term.length > 3),
        tap(() => this.responsibleExecutorsLoading = true),
        switchMap((term: string) => this.loadResponsibleExecutors(term)),
        catchError(() => of([])),
        tap(() => this.responsibleExecutorsLoading = false)
      )
    );

  }

  loadHeads(query: string): Observable<UserBean[]> {
    return from(this.nsiResourceService.searchUsers({
      fio: query,
      group: ['MGGT_ADMORDER_HEAD']
    }))
  }

  loadResponsibleExecutors(query: string): Observable<UserBean[]> {
    return from(this.nsiResourceService.searchUsers({
      fio: query,
      group: ['MGGT_ADMORDER_EDIT']
    }))
  }

  onSelectStatusAnalyticCallback(item: string) {
    this.filter.docStatus = this.filter.docStatus || [];
    let newConditions = this.dicts.orderStatuses.filter(st => st.statusAnalytic === item);
    this.filter.docStatus = _.uniq(this.filter.docStatus.concat(newConditions));
  }

  onRemoveStatusAnalyticCallback(item: { value: string }) {
    this.filter.docStatus = this.filter.docStatus.filter(cond => cond.statusAnalytic !== item.value);
  }

  onSelectConditionCallback(item: OrderStatus) {
    let conditions = [].concat(this.filter.docStatus).concat([item]);
    let notAddedConditions = this.dicts.orderStatuses.filter(st => {
      return st.statusAnalytic === item.statusAnalytic && !_.some(conditions, cond => cond.code === st.code)
    });
    if (notAddedConditions.length === 0) {
      this.filter.statusAnalytic = this.filter.statusAnalytic || [];
      this.filter.statusAnalytic = this.filter.statusAnalytic.concat([item.statusAnalytic]);
    }
  }

  onRemoveConditionCallback(item: { value: OrderStatus }) {
    this.filter.statusAnalytic = this.filter.statusAnalytic.filter(st => st !== item.value.statusAnalytic);
  }

  onSelectExecutorCallback(item) {
    if (!this.filter.responsibleExecutorDep || this.filter.responsibleExecutorDep.length === 0) {
      let dep = this.dicts.departments.find(dep => dep.description === item.departmentCode);
      if (dep) {
        this.filter.responsibleExecutorDep = [dep];
      }
    }
  }

  filterChanged() {
    this.onFilterChanged.emit(this.filter);
  }

  clear() {
    this.filter.clear();
    this.onFilterChanged.emit(this.filter);
  }

}
