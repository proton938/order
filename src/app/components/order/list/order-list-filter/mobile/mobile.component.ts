import * as _ from "lodash";
import {NsiResourceService, UserBean} from "@reinform-cdp/nsi-resource";
import {Observable, Subject} from "rxjs/Rx";
import {Component, EventEmitter, Input, Output} from '@angular/core';
import {Filter} from "../../order-list/order-list.component";
import {IOrderShowcaseDicts} from "../../../../showcase/showcase.component";
import { OrderListFilterComponent } from "../order-list-filter.component";

@Component({
    selector: 'mggt-mobile-order-list-filter',
    templateUrl: './mobile.component.html',
})
export class MobileOrderListFilterComponent extends OrderListFilterComponent {

    @Input() filter: Filter;
    @Input() dicts: IOrderShowcaseDicts;
    @Input() isMka?: boolean;

    @Output() onFilterChanged: EventEmitter<Filter> = new EventEmitter<Filter>();

    heads$: Observable<UserBean[]>;
    headsLoading = false;
    headsInput$ = new Subject<string>();

    responsibleExecutors$: Observable<UserBean[]>;
    responsibleExecutorsLoading = false;
    responsibleExecutorsInput$ = new Subject<string>();

    searchOnFilterChange = true;

    constructor(public nsiResourceService: NsiResourceService) {
      super(nsiResourceService);
    }

    mobileFilterChange() {
      this.filterChanged();
    }
}
