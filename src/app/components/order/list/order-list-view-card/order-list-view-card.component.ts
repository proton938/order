import { Component, Input, OnInit } from '@angular/core';
import { IOrderShowcaseDicts } from "../../../showcase/showcase.component";
import { AuthorizationService } from "@reinform-cdp/security";
import { StateService } from "@uirouter/core";

@Component({
  selector: 'mggt-order-list-view-card',
  templateUrl: './order-list-view-card.component.html',
  styleUrls: ['./order-list-view-card.component.scss']
})
export class OrderListViewCardComponent implements OnInit {

  @Input() doc: any;
  @Input() dicts: IOrderShowcaseDicts;
  @Input() hideLine: boolean;
  @Input() showStatus: boolean;

  hasFileAccess: boolean = false;

  constructor(public $state: StateService, private authorizationService: AuthorizationService) {
  }

  ngOnInit() {
    const documentTypeName = this.doc.documentTypeValueOrd || this.doc.docType;
    let orderType = this.dicts.orderTypes.find(orderType => orderType.name.trim() === documentTypeName);

    if (orderType) {
      if (orderType.accessEI) {
        this.hasFileAccess = this.authorizationService.check(orderType.accessEI);
      } else {
        this.hasFileAccess = true;
      }
    }
  }

  getStatusColor(statusName: string) {
    let status = this.dicts.orderStatuses.find(status => status.name === statusName);
    return status ? status.color : '';
  }

}
