import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Sorting} from "../../../../../models/Sorting";
import { OrderListSortingComponent } from '../order-list-sorting.component';

@Component({
    selector: 'mggt-mobile-order-list-sorting',
    templateUrl: './mobile.component.html',
    styleUrls: ['./mobile.component.scss']
})
export class MobileOrderListSortingComponent extends OrderListSortingComponent {

    @Input() sorting: Sorting;
    @Output() sortingChanged: EventEmitter<any> = new EventEmitter();

    constructor() {
      super()
    }

    ngOnInit() {
      console.info(this.sorting);
    }
}
