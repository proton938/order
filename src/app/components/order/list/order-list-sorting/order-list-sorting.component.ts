import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Sorting} from "../../../../models/Sorting";

@Component({
    selector: 'mggt-order-list-sorting',
    templateUrl: './order-list-sorting.component.html',
    styleUrls: ['./order-list-sorting.component.scss']
})
export class OrderListSortingComponent implements OnInit {

    @Input() sorting: Sorting;
    @Output() sortingChanged: EventEmitter<any> = new EventEmitter();

    constructor() {
    }

    ngOnInit() {
    }

    change(sortingName: string, sortingValue: string, sortingText: string) {
        this.sorting = new Sorting(sortingName, sortingValue, sortingText);
        this.sortingChanged.emit(this.sorting);
    }

    changeValue() {
        if (this.sorting) {
            this.sorting.value = this.sorting.value === 'asc' ? 'desc' : 'asc';
            this.sortingChanged.emit(this.sorting);
        }
    }
}
