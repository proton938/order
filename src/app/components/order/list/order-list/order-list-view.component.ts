import {ToastrService} from 'ngx-toastr';
import {EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FacetedSearchResult, QuerySearchData, CdpSolrResourceService} from "@reinform-cdp/search-resource";
import {Filter} from "./order-list.component";
import {Sorting} from "../../../../models/Sorting";
import {Observable} from "rxjs/Rx";
import {IOrderShowcaseDicts} from "../../../showcase/showcase.component";
import * as moment_ from "moment";
import {formatDate} from "@angular/common";

const moment = moment_;

export abstract class OrderListViewComponent implements OnInit {

    hiddenSorting: string[] = ['docType', 'docNumberSort', 'docContent'];

    @Input() searchRequest: QuerySearchData;
    @Input() searchResult: FacetedSearchResult;
    @Output() searchResultChange: EventEmitter<any> = new EventEmitter();
    @Input() sorting: Sorting;
    @Input() preFilter: string;
    @Input() filter: Filter;
    @Input() common: string;
    @Input() loading: boolean;
    @Output() loadingChange: EventEmitter<any> = new EventEmitter();
    @Input() dicts: IOrderShowcaseDicts;

    constructor(public cdpSolrResourceService: CdpSolrResourceService, public toastr: ToastrService) {
    }

    ngOnInit() {
        this.search(this.sorting);
    }

    search(sorting: Sorting) {
        this.sorting = sorting;
        this.setLoading(true);
        this.doSearch().subscribe(resp => {
            this.searchResult = resp;
            this.searchResultChange.emit(resp);
            this.setLoading(false);
        }, err => {
            this.toastr.error(err.data.exception);
            console.log(err);
            this.setLoading(false);
        });
    }

    private setLoading(loading: boolean) {
        this.loading = loading;
        this.loadingChange.emit(loading);
    }

    abstract doSearch(): Observable<FacetedSearchResult>;

    public getSortingAsString(sorting: Sorting): string {
        let result = sorting.name + ' ' + sorting.value;
        result += ',' + this.hiddenSorting.join(' ' + sorting.value + ',') + ' ' + sorting.value;
        return result;
    }

    public getQuery(common: string): string {
        let result: string[] = [];
        if (this.preFilter && this.preFilter.length) {
            result.push(this.preFilter);
        }
        let filterString = this.filter ? this.filter.asString() : "";
        if (filterString && filterString.length) {
            result.push(filterString);
        }
        let commonSearch = this.buildCommonSearch(common);
        if (commonSearch) {
            result.push(commonSearch);
        }
        return result.length ? result.join(" AND ") : null;
    }

    public setCommon(common: string) {
        this.common = common;
    }

    private buildCommonSearch(common: string) {
        if (!common) {
            return null;
        }
        let items = [];
        let strVal = `(${this.splitSearchText(this.escapeQueryChars(common)).map(item => item).map(item => `*${item}*`).join(" AND ")})`;
        items.push(`docNumber:${strVal}`);
        items.push(`docContent:${strVal}`);
        let dateFilter = this.getDateFilter(common);
        if (dateFilter) {
            items.push(`docDate:${dateFilter}`);
        }
        return `(${items.join(' OR ')})`;
    }

    private getDateFilter(str: string) {
        let dateRange = [];
        if (this.isDate(str)) {
            let date = this.parseDate(str);
            dateRange = [date, date];
        } else if (str.indexOf(",") >= 0) {
            let dateRangeItems = str.split(",");
            if (dateRangeItems.length === 2 && this.isDate(dateRangeItems[0]) && this.isDate(dateRangeItems[1])) {
                dateRange = [this.parseDate(dateRangeItems[0]), this.parseDate(dateRangeItems[1])];
            }
        } else if (this.isSolrDate(str)) {
            let date = new Date(str);
            dateRange = [date, date];
        }
        if (dateRange.length === 2) {
            return `[${this.formatDate(dateRange[0])} TO ${this.formatDate(dateRange[1])}]`;
        } else {
            return null;
        }
    }

    private isDate(str: string) {
        return str.match(/^\d{2}\.\d{2}\.\d{4}$/);
    }

    private parseDate(str: string) {
        return moment(str, 'DD.MM.YYYY').toDate();
    }

    private isSolrDate(str: string) {
        return str.match(/^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}Z$/);
    }

    private formatDate(date: Date) {
        return formatDate(date, 'yyyy-MM-ddTHH:mm:s', 'en') + 'Z';
    }

    private escapeQueryChars(s: string) {
        return s.replace(/([\+\-\!\*\+\&\|\(\)\[\]\{\}\^\~\?\:\;\"])/g, "\\$1");
    }

    private splitSearchText(s: string) {
        return s.replace(/[^\\p0-9A-Za-zа-яА-Я]/gi, " ").split(" ");
    }

}
