import { DepartmentBean, UserBean } from "@reinform-cdp/nsi-resource";
import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { StateService } from "@uirouter/core";
import { FacetedSearchResult, SearchExtData, CdpSolrResourceService } from "@reinform-cdp/search-resource";
import { Sorting } from "../../../../models/Sorting";
import { OrderListViewPlainComponent } from "../order-list-view-plain/order-list-view-plain.component";
import { IOrderShowcaseDicts } from "../../../showcase/showcase.component";
import { OrderType } from "../../../../models/order/OrderType";
import { OrderTheme } from "../../../../models/order/OrderTheme";
import { OrderListViewTableComponent } from "../order-list-view-table/order-list-view-table.component";
import { OrderListViewIndentComponent } from "../order-list-view-indent/order-list-view-indent.component";
import { OrderStatus } from "../../../../models/order/OrderStatus";
import { DateRangeFilter } from "../../../../models/DateRangeFilter";
import { formatDate } from "@angular/common";
import { NSIDocumentType } from "@reinform-cdp/core";

export class Filter {
  docYear: string[];
  docDate: DateRangeFilter = new DateRangeFilter();
  docNumber: string;
  barcode: string;
  docType: OrderType[];
  docContent: string;
  docTheme: OrderTheme[];
  linkDocNumber: string;
  statusAnalytic: string[];
  docStatus: OrderStatus[];
  createDate: DateRangeFilter = new DateRangeFilter();
  accountNameHead: UserBean[];
  responsibleExecutorDep: DepartmentBean[];
  responsibleExecutor: UserBean[];
  privacy:NSIDocumentType[];

  asString(): string {
    let result: string[] = [];
    if (this.docYear && this.docYear.length) {
      result.push(`docYear:(${this.docYear.join(" OR ")})`);
    }
    if (this.docDate.from || this.docDate.to) {
      result.push(`docDate:${this.getDateRangeFilter(this.docDate)}`);
    }
    if (this.docNumber) {
      result.push(`docNumber:(${this.docNumber})`);
    }
    if (this.barcode) {
      result.push(`barcode:(${this.barcode})`);
    }
    if (this.docType && this.docType.length) {
      result.push(`(${this.docType.map(val => `docTypeCode:(\"${val.code}\")`).join(' OR ')})`)
    }
    if (this.privacy && this.privacy.length) {
      result.push(`(${this.privacy.map(val => `privacy:(\"${val.name}\")`).join(' OR ')})`)
    }
    if (this.docContent) {
      result.push(`docContent:(${this.docContent})`);
    }
    if (this.docTheme && this.docTheme.length) {
      result.push(`docThemeCode:(${this.docTheme.map(val => val.themeCode).join(" OR ")})`);
    }
    if (this.linkDocNumber) {
      result.push(`linkDocNumber:(${this.linkDocNumber})`);
    }
    if (this.statusAnalytic && this.statusAnalytic.length) {
      result.push(`statusAnalytic:(${this.statusAnalytic.join(" OR ")})`);
    }
    if (this.docStatus && this.docStatus.length) {
      result.push(`(${this.docStatus.map(val => `docStatus:("${this.escape(val.name)}")`).join(' OR ')})`)
    }
    if (this.createDate.from || this.createDate.to) {
      result.push(`createDate:${this.getDateRangeFilter(this.createDate)}`);
    }
    if (this.accountNameHead && this.accountNameHead.length && (typeof this.accountNameHead !== 'string')) {
      result.push(`accountNameHead:(${this.accountNameHead.map(val => `${val.accountName}`).join(" OR ")})`);
    }
    else if (this.accountNameHead && this.accountNameHead.length) {
      result.push(`head:(*${this.accountNameHead}*)`);
    }
    if (this.responsibleExecutorDep && this.responsibleExecutorDep.length && (typeof this.responsibleExecutorDep !== 'string')) {
      result.push(`(${this.responsibleExecutorDep.map(val => `responsibleExecutorDep:(${this.escape(val.name)})`).join(' OR ')})`)
    }
    else if (this.responsibleExecutorDep && this.responsibleExecutorDep.length) {
      result.push(`responsibleExecutorDep:(*${this.responsibleExecutorDep}*)`);
    }
    if (this.responsibleExecutor && this.responsibleExecutor.length && (typeof this.responsibleExecutor !== 'string')) {
      result.push(`(${this.responsibleExecutor.map(val => `responsibleExecutor:("${val.displayName}")`).join(' OR ')})`);
    }
    else if (this.responsibleExecutor && this.responsibleExecutor.length) {
      result.push(`responsibleExecutor:(*${this.responsibleExecutor}*)`);
    }
    return result.length ? result.join(" AND ") : "";
  }

  escape(str: string) {
    return str.replace(/"/g, "\\\"");
  }

  getDateRangeFilter(value: DateRangeFilter): string {
    let fromDateString = value.from ? `${formatDate(value.from, 'yyyy-MM-dd', 'en')}T00:00:00Z` : '*';
    let toDateString = value.to ? `${formatDate(value.to, 'yyyy-MM-dd', 'en')}T23:59:59Z` : '*';
    return `[${fromDateString} TO ${toDateString}]`;
  }

  count(): number {
    const docYear = this.docYear.length === 0 ? 0 : 1;
    const docDate = this.docDate.isEmpty() ? 0 : 1;
    const docNumber = this.docNumber ? 1 : 0;
    const barcode = this.barcode ? 1 : 0;
    const docType = (!this.docType || this.docType.length === 0) ? 0 : 1;
    const privacy = (!this.privacy || this.privacy.length === 0) ? 0 : 1;
    const docTheme = (!this.docTheme || this.docTheme.length === 0) ? 0 : 1;
    const docContent = (!this.docContent || this.docContent === "") ? 0 : 1;
    const linkDocNumber = (!this.linkDocNumber || this.linkDocNumber === "") ? 0 : 1;
    const statusAnalytic = (!this.statusAnalytic || this.statusAnalytic.length === 0) ? 0 : 1;
    const docStatus = (!this.docStatus || this.docStatus.length === 0) ? 0 : 1;
    const accountNameHead = (!this.accountNameHead || this.accountNameHead.length === 0) ? 0 : 1;
    const responsibleExecutorDep = (!this.responsibleExecutorDep || this.responsibleExecutorDep.length === 0) ? 0 : 1;
    const responsibleExecutor = (!this.responsibleExecutor || this.responsibleExecutor.length === 0) ? 0 : 1;
    const createDate = this.createDate.isEmpty() ? 0 : 1;

    return (docYear + docDate + docNumber + barcode + docType + privacy + docTheme + docContent + linkDocNumber + statusAnalytic + docStatus + accountNameHead + responsibleExecutor + responsibleExecutorDep + createDate);
  }

  clear() {
    this.docYear = [];
    this.docDate = new DateRangeFilter();
    this.docNumber = null;
    this.barcode = null;
    this.docType = [];
    this.privacy=[];
    this.docContent = '';
    this.docTheme = [];
    this.linkDocNumber = '';
    this.statusAnalytic = [];
    this.docStatus = [];
    this.createDate = new DateRangeFilter();
    this.accountNameHead = [];
    this.responsibleExecutorDep = [];
    this.responsibleExecutor = [];
  }

}

@Component({
  selector: 'mggt-order-list',
  templateUrl: './order-list.component.html',
  styleUrls: ['./order-list.component.scss']
})
export class OrderListComponent implements OnInit {

  @Input() dicts: IOrderShowcaseDicts;
  @Input() preFilter: string;
  @Input() sorting: Sorting;
  @Input() forbidCreate: boolean;
  @Input() isMka?: boolean;

  isLoading: boolean;
  searchRequest: SearchExtData;
  searchResult: FacetedSearchResult;
  currentPage: number;
  view: string;
  extSearchCollapsed: boolean = true;
  filter: Filter;
  common: string;
  filtersCount = 0;

  @ViewChild(OrderListViewPlainComponent)
  plainView: OrderListViewPlainComponent;

  @ViewChild(OrderListViewTableComponent)
  tableView: OrderListViewTableComponent;

  @ViewChild(OrderListViewIndentComponent)
  indentView: OrderListViewIndentComponent;

  constructor(public $state: StateService) {
  }

  ngOnInit() {
    this.view = 'list';

    this.searchResult = { facets: {}, docs: [], numFound: 0, pageSize: 0, start: 0 };
    this.searchRequest = new SearchExtData(null, 0, 10, null, null, [], ['SDO_ORDER_ORDER']);
    this.sorting = this.sorting || new Sorting('docDate', 'desc', 'По дате РД');
    this.filter = new Filter();
    this.initFilter();
  }

  public initFilter() {
    let currentYear = (new Date()).getFullYear();
    this.filter.docYear = [currentYear.toString(), (currentYear - 1).toString()];
    if (this.preFilter.indexOf("docStatus") === -1) {
      this.filter.statusAnalytic = ['Действующий'];
      this.filter.docStatus = this.dicts.orderStatuses.filter(st => st.statusAnalytic === 'Действующий');
    } else {
      this.filter.docStatus = this.dicts.orderStatuses.filter(st => st.name === 'На согласовании');
    }

    this.filtersCount = this.filter.count();
  }

  search() {
    if (this.plainView) {
      this.plainView.search(this.sorting)
    }
    if (this.tableView) {
      this.tableView.search(this.sorting)
    }
    if (this.indentView) {
      this.indentView.search(this.sorting)
    }
  }

  sortingChange(sorting: Sorting) {
    this.sorting = sorting;
    this.searchRequest.page = 0;
    this.search();
  }

  filterChange(filter: Filter) {
    if (filter.createDate.to) {
      filter.createDate.to.setHours(23, 59, 59, 999);
    }

    if (filter.docDate.to) {
      filter.docDate.to.setHours(23, 59, 59, 999);
    }

    this.filter = filter;

    this.searchRequest.page = 0;
    this.filtersCount = this.filter.count();

    this.search();
  }

  commonChange(common: string) {
    this.common = common;
    this.searchRequest.page = 0;
    if (this.plainView) {
      this.plainView.setCommon(common);
    }
    if (this.tableView) {
      this.tableView.setCommon(common);
    }
    if (this.indentView) {
      this.indentView.setCommon(common);
    }
    this.search();
  }

  changePage() {
    this.searchRequest.page = this.currentPage - 1;
    this.search();
  }

  isListView() {
    return this.view === 'list';
  }

  isIndentView() {
    return this.view === 'indent';
  }

  isTableView() {
    return this.view === 'table';
  }

}
