import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { StateService } from "@uirouter/core";
import * as _ from 'lodash';
import { FacetedSearchResult, CdpSolrResourceService, SearchExtData } from "@reinform-cdp/search-resource";
import { Sorting } from "../../../../../models/Sorting";
import { OrderListViewPlainComponent } from "../../order-list-view-plain/order-list-view-plain.component";
import { IOrderShowcaseDicts } from "../../../../showcase/showcase.component";

import { Filter, OrderListComponent } from "../order-list.component";

@Component({
  selector: 'mggt-mobile-order-list',
  templateUrl: './mobile.component.html',
  styleUrls: ['./mobile.component.scss']
})
export class MobileOrderListComponent extends OrderListComponent {

  @Input() dicts: IOrderShowcaseDicts;
  @Input() preFilter: string;
  @Input() sorting: Sorting;
  @Input() forbidCreate: boolean;
  @Input() isMka?: boolean;

  isLoading: boolean;
  searchRequest: SearchExtData;
  searchResult: FacetedSearchResult;
  currentPage: number;
  extSearchCollapsed: boolean = true;
  filter: Filter;
  common: string;
  filtersCount = 0;

  isDocsLoaded: boolean = false;
  appendBlock: boolean = false;

  @ViewChild(OrderListViewPlainComponent)
  plainView: OrderListViewPlainComponent;

  constructor(public $state: StateService, public cdpSolrResourceService: CdpSolrResourceService) {
    super($state);
  }

  mobileLoadMore() {
    if (this.isDocsLoaded) {
      return;
    }
    this.appendBlock = true;
    let searchRequest = _.clone(this.searchRequest);
    searchRequest.page = (this.searchResult.docs.length / this.searchResult.pageSize);

    this.cdpSolrResourceService.query(searchRequest).subscribe(response => {
      let loadedDocs: any = response.docs;

      this.searchResult.docs = this.searchResult.docs.concat(loadedDocs);

      this.isDocsLoaded = this.searchResult.docs.length === this.searchResult.numFound;
      this.searchResult.numFound = response.numFound;
      this.searchResult.pageSize = response.pageSize;
      this.appendBlock = false;
    }, err => {
      this.appendBlock = false;
    });
  }
}
