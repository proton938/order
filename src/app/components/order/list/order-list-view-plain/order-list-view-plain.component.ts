import {Component} from '@angular/core';
import {OrderListViewComponent} from "../order-list/order-list-view.component";
import {ToastrService} from "ngx-toastr";
import {FacetedSearchResult, CdpSolrResourceService} from "@reinform-cdp/search-resource";
import {from} from "rxjs/index";
import {Observable} from "rxjs/Rx";

@Component({
    selector: 'mggt-order-list-view-plain',
    templateUrl: './order-list-view-plain.component.html',
    styleUrls: ['./order-list-view-plain.component.scss']
})
export class OrderListViewPlainComponent extends OrderListViewComponent {

    constructor(public cdpSolrResourceService: CdpSolrResourceService, public toastr: ToastrService) {
        super(cdpSolrResourceService, toastr);
    }

    ngOnInit() {
        super.ngOnInit();
    }

    doSearch(): Observable<FacetedSearchResult> {
        this.searchRequest.sort = this.getSortingAsString(this.sorting);
        this.searchRequest.common = this.common;
        this.searchRequest.query = this.getQuery(this.common);
        return from(this.cdpSolrResourceService.query(this.searchRequest));
    }

}
