import {PlatformConfig} from "@reinform-cdp/core";
import {Component, EventEmitter, Output} from '@angular/core';
import {OrderListViewComponent} from "../order-list/order-list-view.component";
import {ToastrService} from "ngx-toastr";
import {FacetedSearchResult, CdpSolrResourceService} from "@reinform-cdp/search-resource";
import {from} from "rxjs/index";
import {Observable} from "rxjs/Rx";
import {Sorting} from "../../../../models/Sorting";
import {StateService} from "@uirouter/core";

@Component({
  selector: 'mggt-order-list-view-table',
  templateUrl: './order-list-view-table.component.html',
  styleUrls: ['./order-list-view-table.component.scss']
})
export class OrderListViewTableComponent extends OrderListViewComponent {

    @Output() onSortingChanged: EventEmitter<Sorting> = new EventEmitter<Sorting>();

    constructor(private platformConfig: PlatformConfig, public $state: StateService, public cdpSolrResourceService: CdpSolrResourceService, public toastr: ToastrService) {
        super(cdpSolrResourceService, toastr);
    }

    ngOnInit() {
        super.ngOnInit();
    }

    doSearch(): Observable<FacetedSearchResult> {
        this.searchRequest.sort = this.getSortingAsString(this.sorting);
        this.searchRequest.common = this.common;
        this.searchRequest.query = this.getQuery(this.common);
        return from(this.cdpSolrResourceService.query(this.searchRequest));
    }

    public sortingChanged(name: string, value: string, text: string) {
      let sorting = new Sorting(name, value, text);
      this.onSortingChanged.emit(sorting)
    }

    public getStatusColor(statusName: string) {
        let status = this.dicts.orderStatuses.find(status => status.name === statusName);
        return status ? status.color : '';
    }

    getFileLink(id) {
        return `/filestore/v1/files/${id}?systemCode=${this.platformConfig.systemCode}`;
    }

}
