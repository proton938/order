import {FacetedSearchResult, SearchResultDocument, CdpSolrResourceService} from "@reinform-cdp/search-resource";
import {Component} from '@angular/core';
import {OrderListViewComponent} from "../order-list/order-list-view.component";
import {ToastrService} from "ngx-toastr";
import {from} from "rxjs/index";
import {Observable, Subscription} from "rxjs/Rx";
import {tap} from "rxjs/internal/operators";

export class SearchSection {
    id: string;
    count: number;
    filter: string;
    expanded: boolean = false;
    loading: boolean = false;
    hasMoreDocs: boolean;
    docs: SearchResultDocument[] = [];
}

@Component({
    selector: 'mggt-order-list-view-indent',
    templateUrl: './order-list-view-indent.component.html',
    styleUrls: ['./order-list-view-indent.component.scss']
})
export class OrderListViewIndentComponent extends OrderListViewComponent {

    searchSections: SearchSection[];

    constructor(public cdpSolrResourceService: CdpSolrResourceService, public toastr: ToastrService) {
        super(cdpSolrResourceService, toastr);
    }

    ngOnInit() {
        super.ngOnInit();
    }

    doSearch(): Observable<FacetedSearchResult> {
        this.searchRequest.sort = this.getSortingAsString(this.sorting);
        this.searchRequest.common = this.common;
        this.searchRequest.query = this.getQuery(this.common);
        this.searchRequest.jsonFacet = "{categories: {type: 'terms', field: 'docStatus', limit: -1, missing: true, numBuckets: true}}";
        return from(this.cdpSolrResourceService.query(this.searchRequest)).pipe(
            tap(resp => {
                this.searchSections = this.convertSections(resp.facets);
            })
        );
    }

    public getQuery(common: string, subquery?: string): string {
        let queryStrings = [super.getQuery(common), subquery];
        return queryStrings.filter(val => val && val.length).join(" AND ");
    }

    fetchDocuments(searchSection: SearchSection) {
        if (!searchSection.docs) {
            searchSection.docs = [];
        }
        searchSection.loading = true;
        return from(this.cdpSolrResourceService.searchExt({
            common: this.common,
            query: this.getQuery(this.common, searchSection.filter),
            pageSize: this.searchRequest.pageSize,
            page: searchSection.docs.length / this.searchRequest.pageSize,
            types: ['SDO_ORDER_ORDER']
        })).pipe(
            tap(result => {
                searchSection.docs = searchSection.docs.concat(result.docs);
                searchSection.hasMoreDocs = result.numFound > searchSection.docs.length;
            })
        ).subscribe(response => {
            searchSection.loading = false;
        }, error => {
            console.log(error);
            searchSection.loading = false;
        });
    }

    convertSections(facets) {
        let result = [];
        if (facets.categories) {
            if (facets.categories.numBuckets > 0) {
                facets.categories.buckets.forEach(bucket => {
                    result.push({
                        id: bucket.val,
                        count: bucket.count,
                        filter: `docStatus:(${bucket.val})`,
                        expanded: false,
                        loading: false,
                        docs: []
                    });
                });
            }
        }
        return result;
    }

    triggerSection(searchSection: SearchSection) {
        if (searchSection.expanded && !searchSection.docs.length) {
            this.fetchDocuments(searchSection);
        }
    }

}
