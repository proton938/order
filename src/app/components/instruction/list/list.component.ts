import {Component, Input, OnInit} from '@angular/core';
import {NsiResourceService} from '@reinform-cdp/nsi-resource';
import {from} from 'rxjs';
import * as _ from 'lodash';
import {formatDate} from '@angular/common';

@Component({
  selector: 'mggt-instruction-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class InstructionListComponent implements OnInit {
  @Input() items: any[] = []; // items from SOLR result
  dicts: any = {};
  loading = true;

  constructor(private nsi: NsiResourceService) {}

  ngOnInit() {
    from(this.nsi.getDictsFromCache([
      'InstructionStatus', 'InstructionPriority'
    ])).subscribe(res => {
      this.dicts = res;
      this.loading = false;
    });
  }

  getColor(code: string, dictName: string): string {
    const dictItem = _.find(this.dicts[dictName], i => i.code === code);
    return dictItem ? dictItem.color : '';
  }

  getAcceptDate(item: any): string {
    let r = '';
    if (item && item.acceptDate && item.acceptDate.length) {
      const lastDateString: string = _.last(item.acceptDate);
      let lastDate: Date = new Date(lastDateString);
      if (lastDateString.indexOf('T') < 0) { lastDate.setHours(0, 0, 0) }
      if (lastDate && lastDate.getFullYear() !== 1900) {
        r = formatDate(lastDate, 'dd.MM.yyyy', 'en') +
          "\n" + formatDate(lastDate, 'HH:mm', 'en');
      }
    }
    return r;
  }
}