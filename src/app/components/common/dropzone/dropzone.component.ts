import {PlatformConfig} from "@reinform-cdp/core";
import * as _ from "lodash";
import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {DropzoneConfigInterface, DropzoneDirective} from "ngx-dropzone-wrapper";


@Component({
    selector: 'mggt-dropzone',
    templateUrl: './dropzone.component.html',
    styleUrls: ['./dropzone.component.scss']
})
export class DropzoneComponent implements OnInit {

    @Input() config: DropzoneConfigInterface;

    @ViewChild(DropzoneDirective) dropzone: DropzoneDirective;
    defaultConfig: DropzoneConfigInterface = {
        autoProcessQueue: false,
        paramName: 'file',
        dictDefaultMessage: 'Загрузить файл',
        headers: {
            'Accept': 'text/plain'
        }
    };

    constructor(private platformConfig: PlatformConfig) {
    }

    ngOnInit() {
        this.defaultConfig.url = `/filestore/v1/files/file?systemCode=${this.platformConfig.systemCode}`
        if (this.config) {
            this.config = _.extend(this.defaultConfig, this.config);
        } else {
            this.config = this.defaultConfig;
        }
    }

}
