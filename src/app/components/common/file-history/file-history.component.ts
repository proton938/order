import {Component, Input, OnInit} from '@angular/core';
import {LoadingStatus} from '@reinform-cdp/widgets';
import {FileExtService} from '../../../services/file-ext.service';
import {NsiResourceService, UserBean} from '@reinform-cdp/nsi-resource';
import * as _ from 'lodash';
import {Observable, from} from 'rxjs';

@Component({
    selector: 'mggt-file-history',
    templateUrl: 'file-history.component.html'
})

export class FileHistoryComponent implements OnInit {

    @Input() fileId: string;
    @Input() systemCode: string = 'sdo';
    @Input() subsystemCode: string = 'order';

    loadingStatus: LoadingStatus = LoadingStatus.LOADING;
    success: boolean;
    updating: boolean;
    users: any = {
        admin: { displayName: 'Администратор' }
    };
    usersLoading: boolean;
    rows: any[] = [];
    lastUpdateDate: Date;

    constructor(private fileExtService: FileExtService,
                private nsi: NsiResourceService) {}

    ngOnInit() {
        if (this.fileId) {
            this.updateHistory()
                .subscribe((res: any) => {
                    this.loadingStatus = LoadingStatus.SUCCESS;
                    this.success =  true;
                }, err => {
                    this.loadingStatus = LoadingStatus.ERROR;
                });
        } else {
            // Нет идентификатора файла
        }
    }

    getUsers() {
        let loginList: string[] = [];
        this.rows.forEach(i => {
            if (i.modifiedByUser) loginList.push(i.modifiedByUser);
            if (i.lockedByUser) loginList.push(i.lockedByUser);
        });
        loginList = _.compact(loginList).filter(i => !this.users[i]);
        if (loginList.length) {
            this.usersLoading = true;
            from(this.nsi.getUsersFromCache(loginList)).subscribe((users: UserBean[]) => {
                if (users && users.length) {
                    users.forEach(user => {
                        if (!this.users[user.accountName]) this.users[user.accountName] = user;
                    })
                }
            });
        }
    }

    updateHistory(): Observable<any> {
        return Observable.create(obs => {
            this.fileExtService
                .getVersionHistory(this.fileId, this.systemCode, this.subsystemCode)
                .subscribe((res: any) => {
                    this.rows = res && res.allVersionHistory ? res.allVersionHistory : [];
                    this.lastUpdateDate = new Date();
                    this.getUsers();
                    obs.next(this.rows);
                    obs.complete();
                }, error => {
                    console.log(error);
                    obs.error(error);
                    obs.complete();
                });
        });
    }

    update() {
        if (this.updating) return;
        this.updating = true;
        this.updateHistory()
            .subscribe((res: any) => {
                this.updating = false;
            }, () => {
                this.updating = false;
            });
    }

    get displayRows() {
        return this.rows.filter(i => i.version !== '1.0');
    }
}
