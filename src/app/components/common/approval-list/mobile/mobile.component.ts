import {PlatformConfig} from "@reinform-cdp/core";
import {Component} from '@angular/core';
import {DesktopApprovalListComponent} from "../desktop/desktop.component";
import {FileResourceService} from "@reinform-cdp/file-resource";
import {NsiResourceService} from "@reinform-cdp/nsi-resource";

import { ApprovalNode, processApprovalList } from "../../../../services/approval-util.service";

@Component({
  selector: 'mggt-mobile-approval-list',
  templateUrl: './mobile.component.html'
})
export class MobileApprovalListComponent extends DesktopApprovalListComponent {

  constructor(fileResourceService: FileResourceService,
              nsiResourseService: NsiResourceService,
              platformConfig: PlatformConfig) {
    super(fileResourceService, nsiResourseService, platformConfig);
  }

  initTrees() {
    let nodeList = processApprovalList(this.copiedList)
    this.parallelList = this.getFlattenItems(nodeList.parallel);
    this.sequentialList = this.getFlattenItems(nodeList.sequential);
    this.signList = this.getFlattenItems(nodeList.signApprovals);
  }


  getFlattenItems(items: ApprovalNode[]): ApprovalNode[] {
    if (!items) {
      return [];
    }

    const flattenList = [];

    items.forEach((item) => {
      flattenList.push(item);
      if (item.items) {
        flattenList.push(...this.getFlattenItems(item.items));
      }
    });

    return flattenList;
  }
}
