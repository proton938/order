import { Component, Inject, Input, OnInit } from '@angular/core';
import { ApprovalType } from "../../../../models/order/ApprovalType";
import { PlatformConfig } from "@reinform-cdp/core";
import { SessionStorage } from '@reinform-cdp/security';
import * as _ from "lodash";
import { ApprovalNode, isUserInsideChildrenItems, isRoot } from "../../../../services/approval-util.service";

@Component({
  selector: '[mggt-approval-list-rows]',
  templateUrl: './rows.component.html'
})
export class ApprovalRowsComponent implements OnInit {

  @Input() list: ApprovalNode[];
  @Input() level: any;
  @Input() mainNumber: any;
  @Input() withAdditional: boolean = false;
  @Input() showAdditional: boolean = true;
  @Input() approvalTypes: ApprovalType[];
  opennedItemIndexes: string[] = [];
  flattenList: ApprovalNode[] = [];

  constructor(@Inject('$localStorage') public $localStorage: any, private platformConfig: PlatformConfig, private session: SessionStorage) {
  }

  ngOnInit() {
    const login = this.session.login();
    this.initList(this.list);

    _.each(this.approvalTypes, at => {
      this.approvalTypes[at.approvalTypeCode] = at;
    });

    this.flattenList = this.getFlattenItems(this.list);

    this.flattenList.forEach((item) => {
      if (isUserInsideChildrenItems(item, login)) {
        this.opennedItemIndexes.push(item.val.approvalNum);
      }
    });

    this.applyVisiblityRules();
  }

  initList(list) {
    list.forEach((item) => {
      item.isHidden = false;
      if (item.items) {
        this.initList(item.items);
      }
    });
  }

  getFlattenItems(items: ApprovalNode[]): ApprovalNode[] {
    if (!items) {
      return [];
    }

    const flattenList = [];

    items.forEach((item) => {
      flattenList.push(item);
      if (item.items) {
        flattenList.push(...this.getFlattenItems(item.items));
      }
    });

    return flattenList;
  }

  applyVisiblityRules() {
    this.flattenList.forEach((i) => {
      i.isHidden = true;
    });

    this.opennedItemIndexes.sort().forEach((approvalNum) => {
      this.flattenList.forEach((i) => {
        if (isRoot(i.val.approvalNum)) {
          i.isHidden = false;
          return;
        }

        const isFirstLevelChild = i.val.approvalNum.startsWith(approvalNum + '.') &&
            i.val.approvalNum.split('.').length === approvalNum.split('.').length + 1;

        if (isFirstLevelChild) {
          i.isHidden = false;
        }

      });
    });

    if (this.opennedItemIndexes.length === 0) {
      this.flattenList.forEach((i) => {
        if (isRoot(i.val.approvalNum)) {
          i.isHidden = false;
        } else {
          i.isHidden = true;
        }
      });
    }
  }

  toggle(approvalNum) {
    if (this.opennedItemIndexes.indexOf(approvalNum) === -1) {
      this.opennedItemIndexes.push(approvalNum);
    } else {
      this.opennedItemIndexes = this.opennedItemIndexes.filter((a) => (a.indexOf(approvalNum) !== 0));
    }


    this.applyVisiblityRules();
  }

  isShowAdditionalApprovals(approvalNum) {
    return this.opennedItemIndexes.indexOf(approvalNum) !== -1;
  }

  getFileLink(id) {
    return `/filestore/v1/files/${id}?systemCode=${this.platformConfig.systemCode}`;
  }

  getApprovalResultColor(approvalTypeCode: string, approvalResult: string) {
    if (this.approvalTypes) {
      let approvalType = this.approvalTypes[approvalTypeCode];
      if (approvalType) {
        if (approvalResult === approvalType.buttonYes) {
          return 'label-' + approvalType.buttonYesColor;
        } else if (approvalResult === approvalType.buttonNo) {
          return 'label-' + approvalType.buttonNoColor;
        }
      }
    }
  }
}
