import { Component, Input, OnInit } from '@angular/core';
import { OrderApprovalList, OrderApprovalListItem } from "../../../../models/order/Order";
import { ApprovalType } from "../../../../models/order/ApprovalType";
import { FileResourceService } from "@reinform-cdp/file-resource";
import { NsiResourceService } from "@reinform-cdp/nsi-resource";
import { PlatformConfig } from "@reinform-cdp/core";
import { from } from "rxjs/index";
import { copy } from "angular";
import { ApprovalNode, processApprovalList } from "../../../../services/approval-util.service";

@Component({
  selector: 'mggt-desktop-approval-list',
  templateUrl: './desktop.component.html'
})
export class DesktopApprovalListComponent implements OnInit {

  @Input() list: OrderApprovalList;
  @Input()hideTitle:boolean=false;

  parallelList: ApprovalNode[];
  sequentialList: ApprovalNode[];
  signList: ApprovalNode[];

  approvalTypes: ApprovalType[];
  copiedList: OrderApprovalListItem[] = [];

  constructor(private fileResourceService: FileResourceService,
    private nsiResourseService: NsiResourceService,
    private platformConfig: PlatformConfig) {
  }

  ngOnInit() {
    this.copiedList = copy(this.list.agreed);

    from(this.nsiResourseService.get('mggt_order_ApprovalType')).subscribe((approvalTypes: ApprovalType[]) => {
      this.approvalTypes = approvalTypes;
      this.initTrees();
    });
  }

  initTrees() {
    let nodeList = processApprovalList(this.copiedList)
    this.parallelList = nodeList.parallel;
    this.sequentialList = nodeList.sequential;
    this.signList = nodeList.signApprovals;
  }

  getFileLink(id) {
    return `/filestore/v1/files/${id}?systemCode=${this.platformConfig.systemCode}`;
  }

  getApprovalResultColor(approvalTypeCode: string, approvalResult: string) {
    if (this.approvalTypes) {
      let approvalType = this.approvalTypes[approvalTypeCode];
      if (approvalType) {
        if (approvalResult === approvalType.buttonYes) {
          return 'label-' + approvalType.buttonYesColor;
        } else if (approvalResult === approvalType.buttonNo) {
          return 'label-' + approvalType.buttonNoColor;
        }
      }
    }
  }

}
