import * as _ from "lodash";
import { Component, Input, OnInit } from '@angular/core';
import { OrderApprovalList } from '../../../../../models/order/Order';
import { ApprovalType } from '../../../../../models/order/ApprovalType';
import { FileResourceService } from '@reinform-cdp/file-resource';
import { NsiResourceService } from '@reinform-cdp/nsi-resource';
import { PlatformConfig } from '@reinform-cdp/core';
import { from } from 'rxjs';
import { copy } from 'angular';

@Component({
    selector: 'mggt-desktop-approval-list-old',
    templateUrl: './desktop.component.html'
})
export class DesktopApprovalListOldComponent implements OnInit {

    @Input() list: OrderApprovalList;
    approvalTree = [];
    approvalTypes: { [key: string]: ApprovalType };
    copiedList = [];

    constructor(private fileResourceService: FileResourceService,
                private nsiResourseService: NsiResourceService,
                private platformConfig: PlatformConfig) {
    }

    ngOnInit() {
        this.copiedList = copy(this.list.agreed)
        from(this.nsiResourseService.get('mggt_order_ApprovalType')).subscribe((approvalTypes: ApprovalType[]) => {
            this.approvalTypes = {};
            _.each(approvalTypes, at => {
                this.approvalTypes[at.approvalTypeCode] = at;
            });
            this.initTree();
        });
    }

    initTree() {
        this.approvalTree = this.getApprovals();
    }

    getApprovals(parentApprovalNum = '', depth = 0) {
        const currentLevel = this.copiedList.filter((item) => (
            item.approvalNum.split('.').length === depth + 1 &&
            item.approvalNum.startsWith(parentApprovalNum) &&
            item.approvalNum !== parentApprovalNum
        ));

        currentLevel.forEach(item => {
            if (item.added) {
                item.items = this.getApprovals(item.approvalNum, depth + 1);
            }
        });

        return currentLevel;
    }

    getFileLink(id) {
        return `/filestore/v1/files/${id}?systemCode=${this.platformConfig.systemCode.toUpperCase()}`;
    }

    getApprovalResultColor(approvalTypeCode: string, approvalResult: string) {
        if (this.approvalTypes) {
            let approvalType = this.approvalTypes[approvalTypeCode];
            if (approvalType) {
                if (approvalResult === approvalType.buttonYes) {
                    return 'label-' + approvalType.buttonYesColor;
                } else if (approvalResult === approvalType.buttonNo) {
                    return 'label-' + approvalType.buttonNoColor;
                }
            }
        }
    }

}
