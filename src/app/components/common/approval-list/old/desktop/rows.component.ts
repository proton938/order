import { Component, Inject, Input, OnInit } from '@angular/core';
import { ApprovalType } from '../../../../../models/order/ApprovalType';
import { PlatformConfig } from '@reinform-cdp/core';
import { SessionStorage } from '@reinform-cdp/security';

@Component({
    selector: '[mggt-approval-list-row-old]',
    templateUrl: './rows.component.html'
})
export class ApprovalRowOldComponent implements OnInit {

    @Input() list: any[];
    @Input() level: any;
    @Input() mainNumber: any;
    @Input() withAdditional: boolean = false;
    @Input() approvalTypes: { [key: string]: ApprovalType };
    opennedItemIndexes: number[] = [];
    constructor(@Inject('$localStorage') public $localStorage: any, private platformConfig: PlatformConfig, private session: SessionStorage) { }

    ngOnInit() {
        const login = this.session.login();
        this.list.forEach((item, idx) => {
            if (this.isUserInsideChildrenItems(item, login)) {
                this.opennedItemIndexes.push(idx);
                return true;
            }

            return false;
        });
    }

    isUserInsideChildrenItems(item, login) {
        if (!item.items) {
            return false;
        }

        return item.items.some((childItem) => {
            if (childItem.agreedBy.accountName === login) {
                return true;
            }

            if (childItem.items) {
                return this.isUserInsideChildrenItems(childItem, login);
            }

            return false;
        });
    }

    toggle(itemIndex) {
        if (this.opennedItemIndexes.indexOf(itemIndex) === -1) {
            this.opennedItemIndexes.push(itemIndex);
        } else {
            this.opennedItemIndexes = this.opennedItemIndexes.filter((idx) => (idx !== itemIndex));
        }
    }

    isShowAdditionalApprovals(idx) {
        return this.opennedItemIndexes.indexOf(idx) !== -1;
    }

    getFileLink(id) {
        return `/filestore/v1/files/${id}?systemCode=${this.platformConfig.systemCode.toUpperCase()}`;
    }

    getApprovalResultColor(approvalTypeCode: string, approvalResult: string) {
        if (this.approvalTypes) {
            let approvalType = this.approvalTypes[approvalTypeCode];
            if (approvalType) {
                if (approvalResult === approvalType.buttonYes) {
                    return 'label-' + approvalType.buttonYesColor;
                } else if (approvalResult === approvalType.buttonNo) {
                    return 'label-' + approvalType.buttonNoColor;
                }
            }
        }
    }
}
