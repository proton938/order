import {Component, Inject, Input, OnInit} from '@angular/core';
import {OrderApprovalList} from '../../../../models/order/Order';

@Component({
    selector: 'mggt-approval-list-old',
    templateUrl: './approval-list-old.component.html'
})
export class ApprovalListOldComponent implements OnInit {

    @Input() list: OrderApprovalList;

    constructor(@Inject('$localStorage') public $localStorage: any) { }

    ngOnInit() {
    }

}
