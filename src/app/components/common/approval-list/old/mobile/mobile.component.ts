import {PlatformConfig} from '@reinform-cdp/core';
import {Component} from '@angular/core';
import {DesktopApprovalListOldComponent} from '../desktop/desktop.component';
import {FileResourceService} from '@reinform-cdp/file-resource';
import {NsiResourceService} from '@reinform-cdp/nsi-resource';

@Component({
    selector: 'mggt-mobile-approval-list-old',
    templateUrl: './mobile.component.html'
})
export class MobileApprovalListOldComponent extends DesktopApprovalListOldComponent {

    constructor(fileResourceService: FileResourceService,
                nsiResourseService: NsiResourceService,
                platformConfig: PlatformConfig) {
        super(fileResourceService, nsiResourseService, platformConfig);
    }

}
