import {Component, Inject, Input, OnInit} from '@angular/core';
import {OrderApprovalList} from "../../../models/order/Order";

@Component({
  selector: 'mggt-approval-list',
  templateUrl: './approval-list.component.html'
})
export class ApprovalListComponent implements OnInit {

  @Input() list: OrderApprovalList;
  @Input()hideTitle:boolean=false;

  constructor(@Inject('$localStorage') public $localStorage: any) { }

  ngOnInit() {
  }

}
