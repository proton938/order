import * as _ from "lodash";
import {SessionStorage} from "@reinform-cdp/security";
import {ActivityResourceService, IActivityArray, ITask} from "@reinform-cdp/bpm-components";
import {FacetedSearchResult} from "@reinform-cdp/search-resource";
import {mergeMap} from "rxjs/internal/operators";
import {Observable} from "rxjs/Rx";
import {ToastrService} from "ngx-toastr";
import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {OrderService} from "../../../services/order.service";
import {forkJoin, from, of, throwError} from "rxjs/index";
import {DropzoneConfigInterface} from "ngx-dropzone-wrapper";
import {FileType} from "../../../models/order/FileType";
import {Order} from "../../../models/order/Order";
import {CdpSolrResourceService} from "@reinform-cdp/search-resource";

@Component({
    selector: 'mggt-not-scanned-attach',
    templateUrl: './not-scanned-attach.component.html'
})
export class NotScannedAttachComponent implements OnInit {

    config: DropzoneConfigInterface;

    @Output() done: EventEmitter<any> = new EventEmitter<any>();

    constructor(private orderService: OrderService, private toastr: ToastrService,
                private cdpSolrResourceService: CdpSolrResourceService, private activityResourceService: ActivityResourceService,
                private session: SessionStorage) {
    }

    ngOnInit() {
        let component = this;
        this.config = {
            autoProcessQueue: true,
            params: {
                fileType: 'MkaDocOther',
                docSourceReference: 'UI'
            },
            accept: (file: File, done: Function) => {
                let fileName: string = file.name;
                let match = /[A-Za-z]*(\d*).*/.exec(fileName);
                if (match[1]) {
                    let barcode: string = match[1];
                    this.getOrderIdByBarcode(barcode).pipe(
                        mergeMap(id => {
                            return this.orderService.get(id);
                        })
                    ).subscribe(order => {
                        file['order'] = order;
                        done()
                    }, error => {
                        done(error);
                    })
                } else {
                    done("Имя файла не соответствует шаблону");
                }
            },
            init: function () {
                this.on("processing", function (file) {
                    let order = file['order'];
                    this.options.params.folderGuid = order.folderID;
                    this.options.params.docEntityID = order.documentID;
                    console.log(this.options);
                });
                this.on('error', function (file, error) {
                    file['observable'] = of('');
                    component.toastr.warning(file.name + ": " + error, 'Ошибка');
                });
                this.on("success", function (file, guid) {
                    let order = file['order'];
                    file['observable'] = component.save(order, guid, file);
                });
                this.on("queuecomplete", function (file, errorMessage) {
                    let dropzone = this;
                    let callback = function () {
                        let successNum = _.filter(dropzone.files, (file: any) => {
                            return file.status === 'success';
                        }).length;
                        let failedNum = _.filter(dropzone.files, (file: any) => {
                            return file.status === 'error';
                        }).length;
                        if (failedNum === 0) {
                            component.toastr.success("Обработано файлов: успешно - " + successNum + ", неуспешно - " + failedNum, "Результат");
                        } else {
                            component.toastr.warning("Обработано файлов: успешно - " + successNum + ", неуспешно - " + failedNum, "Результат");
                        }
                        component.done.emit();
                    };
                    forkJoin(this.files.map(f => f['observable'])).subscribe(callback, callback);
                });
            }
        };
    }

    save(order: Order, guid: string, file: File): Observable<any> {
        const fileType: FileType = FileType.create(guid, file.name, file.size.toString(), false, new Date(), file.type, 'MkaDocOther');
        return this.orderService.addFile(order, fileType).pipe(
            mergeMap(() => {
                return this.activityResourceService.queryTasks({
                    processInstanceVariables: [{
                        name: "EntityIdVar",
                        value: order.documentID,
                        operation: "equals",
                        type: "string"
                    }]
                })
            }),
            mergeMap((tasksStatus: IActivityArray<ITask>) => {
                let scanningTask = _.find(tasksStatus.data, (x: ITask) => {
                    return x.formKey === "ScanRD";
                });
                return scanningTask ? this.finishScanningTask(scanningTask, order) : of('');
            })
        );
    }

    private finishScanningTask(scanningTask: ITask, order: Order): Observable<any> {
        return from(this.activityResourceService.setTaskAssignee(scanningTask.id, this.session.login())).pipe(
            mergeMap(() => {
                return this.activityResourceService.finishTask(parseInt(scanningTask.id), [{
                    "name": "RDPublishingSite",
                    "value": order.PublishingSite
                }])
            }),
            mergeMap(() => {
                // TODO: gpzu
                return of('');
                // if (order.systemCode === 'gpzu' && order.systemParentID
                //     && order.canceledDocument && order.canceledDocument.canceledDocumentNum && order.canceledDocument.canceledDocumentDate) {
                //     return self.orderResource.createIsogdXml({
                //         id: order.documentID
                //     }).$promise.then(file => {
                //         order.isogdXmlGuid = file.fileID;
                //         return self.orderResource.update({id: order.documentID}, {document: order});
                //     }).then(() => {
                //         return self.orderTaskService.getProcessDefinition('sdord_ToIsogd_ID').then((process: oasiBpmRest.IProcessDefinition) => {
                //             let props = [{
                //                 "name": "EntityIdVar",
                //                 "value": order.documentID
                //             }, {
                //                 "name": "EntityDescriptionVar",
                //                 "value": order.documentTypeValue + " " + order.documentContent
                //             }];
                //             return self.orderTaskService.initProcess(process.id, props);
                //         });
                //     });
                // }
            })
        );
    }

    private getOrderIdByBarcode(barcode: string): Observable<string> {
        return from(this.cdpSolrResourceService.query({
            types: ['SDO_ORDER_ORDER'],
            query: `barcode:(${barcode}) AND docStatus:(Зарегистрирован)`,
            pageSize: 1000
        })).pipe(
            mergeMap((searchResult: FacetedSearchResult) => {
                if (searchResult.numFound > 0) {
                    return of(searchResult.docs[0].documentId);
                } else {
                    return throwError("Карточка со штрихкодом " + barcode + " не найдена");
                }
            })
        );
    }

}
