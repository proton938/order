import {AppComponent} from './app.component';
import {OrderComponent} from "./components/order/card/order/order.component";
import {OrderInitiateComponent} from "./components/order/card/order-initiate/order-initiate.component";
import {ShowcaseComponent} from "./components/showcase/showcase.component";
import {OrderPublishComponent} from "./components/order/tasks/order-publish/order-publish.component";
import {ProcessScanComponent} from "./components/order/tasks/process-scan/process-scan.component";
import {OrderPrepareComponent} from "./components/order/tasks/order-prepare/order-prepare.component";
import {OrderPrintandsignComponent} from "./components/order/tasks/order-printandsign/order-printandsign.component";
import {OrderIntroComponent} from "./components/order/tasks/order-intro/order-intro.component";
import {OrderApproveComponent} from "./components/order/tasks/order-approve/order-approve.component";
import {OrderApproveNewComponent} from "./components/order/tasks/order-approve/order-approve-new.component";
import {OrderNegotiationSignComponent} from "./components/order/tasks/order-negotiation-sign/order-negotiation-sign.component";
import {OrderApproveParallelComponent} from "./components/order/tasks/order-approve-parallel/order-approve-parallel.component";
import {OrderApproveDesicionComponent} from "./components/order/tasks/order-approve-desicion/order-approve-desicion.component";
import {OrderModifyNewComponent} from "./components/order/tasks/order-edit/order-edit-new.component";
import {ProcessRegisterComponent} from './components/order/tasks/process-register/process-register.component';
import {ReviewMkaDocComponent} from './components/order/tasks/review-mka-doc/review-mka-doc.component';
import {PageNotFoundComponent} from '@reinform-cdp/skeleton';
import {TaskWrapperComponent} from '@reinform-cdp/bpm-components';
import {OrderAttachFileComponent} from './components/order/tasks/order-attach-file/order-attach-file.component';
import {OrderInitiateEditOrdersComponent} from './components/order/card/order-initiate-edit-orders/order-initiate-edit-orders.component';
import {OrderAssignPermissComponent} from './components/order/tasks/order-assign-permiss/order-assign-permiss.component';
import {OrderApproveDocComponent} from './components/order/tasks/order-approve-doc/order-approve-doc.component';
import {OrderAcceptChangesComponent} from './components/order/tasks/order-accept-changes/order-accept-changes.component';
import {OrderReworkDocComponent} from './components/order/tasks/order-rework-doc/order-rework-doc.component';
import {OrderModifyComponent} from "./components/order/tasks/order-edit/order-edit.component";
import {OrderReviewInnerComponent} from "./components/order/tasks/order-review-inner/order-review-inner.component";
import {CdpDependenciesTreeComponent} from '@reinform-cdp/widgets'

export const statesConfig = [
  {
    name: 'app.versions',
    url: '/versions',
    component: CdpDependenciesTreeComponent
  },
  {
    name: '404',
    url: '/*path',
    component: PageNotFoundComponent
  },
  {
    name: 'app.execution',
    url: '/execution/:system/:taskId?systemCode',
    component: TaskWrapperComponent,
    params: {
      systemCode: 'SDO'
    }
  },
  {
    name: 'app.order',

    url: '/order',
    component: AppComponent
  },
  {
    name: 'app.order.document',
    url: '/document/:id',
    component: OrderComponent,
    params: {
      editingOrder: null,
      useBreadcrumbs: true
    }
  },
  {
    name: 'app.order.initiate',
    url: '/document/initiate',
    component: OrderInitiateComponent,
    params: {
      linkedDocumentID: null,
      linkedDocumentType: null,
      ref: null,
    }
  },
  {
    name: 'app.order.initiateedit',
    url: '/document/initiateedit',
    component: OrderInitiateEditOrdersComponent
  },
  {
    name: 'app.order.showcase',
    url: '/showcase',
    component: ShowcaseComponent,
    params: {
      activeTab: null,
      tabBuilder: 'general',
      orderTypesFilter: 'general',
      suffix: '',
      title: 'Распорядительные документы'
    }
  },
  {
    name: 'app.order.mka-showcase',
    url: '/mka-showcase',
    component: ShowcaseComponent,
    params: {
      activeTab: 'mka',
      tabBuilder: 'mka',
      orderTypesFilter: 'mka',
      suffix: 'mka',
      title: 'Распорядительные документы МКА'
    }
  },
  {
    name: 'app.execution.orderPublish',
    url: '/rdPublishRD',
    component: OrderPublishComponent
  },
  {
    name: 'app.execution.orderPrepare',
    url: '/sdordPrepareRD?systemCode',
    component: OrderPrepareComponent
  },
  {
    name: 'app.execution.orderNegotiate',
    url: '/sdordNegotiationProjectOrder',
    component: OrderApproveComponent
  },
  {
    name: 'app.execution.orderNegotiateNew',
    url: '/sdordNegotiationProjectOrderNew',
    component: OrderApproveNewComponent
  },
  {
    name: 'app.execution.orderNegotiateParal',
    url: '/sdordNegotiationProjectOrderParal',
    component: OrderApproveParallelComponent
  },
  {
    name: 'app.execution.sdordNegotiationProjectSign',
    url: '/sdordNegotiationProjectSign',
    component: OrderNegotiationSignComponent
  },
  {
    name: 'app.execution.orderApproveDecision',
    url: '/sdordApproveDecision',
    component: OrderApproveDesicionComponent
  },
  {
    name: 'app.execution.orderPrintAndSign',
    url: '/sdordPrintAndSignDoc',
    component: OrderPrintandsignComponent
  },
  {
    name: 'app.execution.sdordReviewApprovedRD',
    url: '/sdordReviewApprovedRD',
    component: OrderIntroComponent,
    params: {
      systemCode: 'SDO'
    }
  },
  {
    name: 'app.execution.sdordReviewInner',
    url: '/sdordReviewInner',
    component: OrderReviewInnerComponent,
    params: {
      systemCode: 'SDO'
    }
  },
  {
    name: 'app.execution.orderEdit',
    url: '/sdordEditDoc',
    component: OrderModifyComponent
  },
  {
    name: 'app.execution.orderEditNew',
    url: '/sdordEditDocNew',
    component: OrderModifyNewComponent
  },
  {
    name: 'app.execution.orderEditParal',
    url: '/sdordEditDocParal',
    component: OrderModifyNewComponent,
    params: {
      minParallelApprovals: 2,
      isParallel: true
    }
  },
  {
    name: 'app.execution.orderScan',
    url: '/sdordScanRD',
    component: ProcessScanComponent,
  },
  {
    name: 'app.execution.orderRegister',
    url: '/sdordRegisterRD',
    component: ProcessRegisterComponent,
  },
  {
    name: 'app.execution.orderattachDocCreateApprovalList',
    url: '/sdordattachDocCreateApprovalList',
    component: OrderAttachFileComponent,
  },
  {
    name: 'app.execution.orderassignPermisInAlfresco',
    url: '/sdordassignPermisInAlfresco',
    component: OrderAssignPermissComponent,
  },
  {
    name: 'app.execution.orderapproveOrderDoc',
    url: '/sdordapproveOrderDoc',
    component: OrderApproveDocComponent,
  },
  {
    name: 'app.execution.orderacceptDocChanges',
    url: '/sdordacceptDocChanges',
    component: OrderAcceptChangesComponent,
  },
  {
    name: 'app.execution.orderreworkDocUpdateApprovalList',
    url: '/sdordreworkDocUpdateApprovalList',
    component: OrderReworkDocComponent,
  },
  {
    name: 'app.execution.sdordmkaReviewMKADoc',
    url: '/sdordmkaReviewMKADoc',
    component: ReviewMkaDocComponent
  }
];
