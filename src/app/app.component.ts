import { Component } from '@angular/core';

@Component({
  selector: 'order-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
}
