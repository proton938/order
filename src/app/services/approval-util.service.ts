import { OrderApprovalListItem } from "../models/order/Order";

export class ApprovalNode {
  constructor(val: OrderApprovalListItem) {
    this.val = val;
  }

  items: ApprovalNode[];
  val: OrderApprovalListItem;
  isHidden?: boolean = true;
}

export function isRoot(approvalNum: string) {
  return isLevel(approvalNum, 1);
}

export function isNotRoot(approvalNum: string) {
  return !isLevel(approvalNum, 1);
}

export function isUserInsideChildrenItems(item: ApprovalNode, login) {
  if (!item.items) {
    return false;
  }

  return item.items.some((childItem) => {
    if (childItem.val.agreedBy.accountName === login) {
      return true;
    }

    if (childItem.items) {
      return isUserInsideChildrenItems(childItem, login);
    }

    return false;
  });
}

export function processApprovalList(list: OrderApprovalListItem[] = []): {
  parallel: ApprovalNode[],
  sequential: ApprovalNode[],
  signApprovals: ApprovalNode[]
} {
  let nodeList = list.map(i => new ApprovalNode(i));
  return {
    parallel: getParallelApprovals(nodeList),
    sequential: getSequentialApprovals(nodeList),
    signApprovals: getSignApprovals(nodeList)
  }
}

export function getApprovalsNodes(list: OrderApprovalListItem[]): ApprovalNode[] {
  let nodeList = list.map(i => new ApprovalNode(i));
  return getApprovals(nodeList);
}

function getParallelApprovals(list: ApprovalNode[]): ApprovalNode[] {
  const currentLevel = list.filter((item) => (
    item.val.approvalNum.split('.').length === 1 &&
    item.val.Parallel &&
    !item.val.Signer
  ));

  currentLevel.forEach(item => {
    if (item.val.added) {
      item.items = getApprovals(list, item.val.approvalNum, 1);
    }
  });

  return currentLevel;
}

function getSequentialApprovals(list: ApprovalNode[]): ApprovalNode[] {
  const currentLevel = list.filter((item) => (
    item.val.approvalNum.split('.').length === 1 &&
    !item.val.Parallel &&
    !item.val.Signer
  ));

  currentLevel.forEach(item => {
    if (item.val.added) {
      item.items = getApprovals(list, item.val.approvalNum, 1);
    }
  });

  return currentLevel;
}

function getSignApprovals(list: ApprovalNode[]): ApprovalNode[] {
  const currentLevel = list.filter((item) => (
    item.val.approvalNum.split('.').length === 1 &&
    !item.val.Parallel &&
    item.val.Signer === true
  ));

  currentLevel.forEach(item => {
    if (item.val.added) {
      item.items = getApprovals(list, item.val.approvalNum, 1);
    }
  });

  return currentLevel;
}

function getApprovals(list: ApprovalNode[], parentApprovalNum = '', depth = 0) {
  const currentLevel = list.filter((item) => (
    isLevel(item.val.approvalNum, depth + 1) &&
    item.val.approvalNum.startsWith(parentApprovalNum ? parentApprovalNum + '.' : '') &&
    item.val.approvalNum !== parentApprovalNum
  ));

  currentLevel.forEach(item => {
    if (item.val.added) {
      item.items = getApprovals(list, item.val.approvalNum, depth + 1);
    }
  });

  return currentLevel;
}

function isLevel(approvalNum: string, level: number) {
  return approvalNum.split('.').length === level;
}
