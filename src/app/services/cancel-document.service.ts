import * as angular from "angular";
import {Order} from "../models/order/Order";
import {from, of} from "rxjs/index";
import {NsiResourceService} from "@reinform-cdp/nsi-resource";
import {Injectable} from '@angular/core';
import {AlertService} from "@reinform-cdp/widgets";
import {OrderService} from "./order.service";
import {CancelType} from "../models/order/CancelType";
import {formatDate} from "@angular/common";
import {Observable} from "rxjs/Rx";
import {catchError, mergeMap} from "rxjs/internal/operators";
import {OrderStatus} from "../models/order/OrderStatus";

@Injectable({
  providedIn: 'root'
})
export class CancelDocumentService {

  constructor(private alertService: AlertService,
              private orderService: OrderService,
              private nsiResourceService: NsiResourceService) {
  }

  checkCanceledDocument(cancelDocument: CancelType): Observable<any> {
    if (cancelDocument && cancelDocument.canceledDocumentID) {

      let documentName = cancelDocument.canceledDocumentType;

      if (cancelDocument.canceledDocumentNum) {
        documentName += ' № ' + cancelDocument.canceledDocumentNum;
      }

      if (cancelDocument.canceledDocumentDate) {
        let documentDate = formatDate(cancelDocument.canceledDocumentDate, "dd.MM.yyyy", "en");

        documentName += ' от ' + documentDate;
      }

      return from(this.alertService.confirm({
        message: 'Аннулируемый документ ' +
        documentName +
        ' будет аннулирован. Отмена данной операции и смена аннулируемого документа невозможны. ' +
        'Проверьте правильность выбора аннулируемого документа',
        okButtonText: 'Продолжить',
        type: 'danger'
      }));
    } else {
      return of('');
    }
  }

  cancelDocument(canceledDocument: CancelType, expireDate: Date): Observable<any> {
    if (!canceledDocument || !canceledDocument.canceledDocumentID) {
      return of('');
    }

    return this.orderService.get(canceledDocument.canceledDocumentID).pipe(
      catchError(() => {
        return from(this.alertService.message({
            message: 'Аннулируемый документ отсутствует в подсистеме Приказы'
        })).pipe(
            catchError(() => {
                return of('');
            })
        );
      }),
      mergeMap((order: Order) => {
        let origOrder = angular.copy(order);
        return from(this.nsiResourceService.getBy('mggt_order_OrderStatuses', {nickAttr: 'code', values: ['CANCELED']})).pipe(
          mergeMap((statuses: OrderStatus[]) => {
            let status = statuses[0];
            order.documentStatusCode = status.code;
            order.documentStatusValue = status.name;
            order.documentStatusColor = status.color;
            order.documentExpireDate = expireDate;
            return this.orderService.update(canceledDocument.canceledDocumentID, this.orderService.diff(origOrder, order));
          }),
        );
      }),
      mergeMap(() => {
        return from(this.alertService.message({
          message: 'В аннулируемом документе изменен статус на «Аннулирован», установлена дата аннулирования'
        })).pipe(
          catchError(() => {
            return of('');
          })
        );
      })
    );
  }
}
