import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {map} from 'rxjs/internal/operators';
import {Injectable} from '@angular/core';
import {IJsonPatchData} from '../models/IJsonPatchData';
import {IDocumentLogExtended} from '../models/IDocumentLogExt';

@Injectable({providedIn: 'root'})
export class InstructionRestService {
  root = `/app/sdo/instruction`;

  constructor(private http: HttpClient) {
  }

  get(id: string): Observable<any> {
    let url: string = `${this.root}/instruction/${id}`;
    return this.http.get(url);
  }

  create(data: any): Observable<any> {
    let url: string = `${this.root}/instruction`;
    return this.http.post(url, data, {
      headers: {
        'Content-Type': 'application/json; charset=UTF-8'
      }
    });
  }

  delete(id: string): Observable<void> {
    let url: string = `${this.root}/instruction/${id}`;
    return this.http.delete(url).pipe(map(response => {
      return;
    }));
  }

  patch(id: string, data: IJsonPatchData[]): Observable<void> {
    let url: string = `${this.root}/instruction/${id}`;
    return this.http.patch(url, data, {
      headers: {
        'Content-Type': 'application/json; charset=UTF-8'
      }
    }).pipe(map(response => {
      return;
    }));
  }

  notify(id: string): Observable<void> {
    let url: string = `${this.root}/notify/cancelInstruction/${id}`;
    return this.http.post(url, {}).pipe(map(response => {
      return;
    }));
  }

  recall(id: string, login: string): Observable<void> {
    let url: string = `${this.root}/notify/instructionRecall/${id}/coExecutor?login=${login}`;
    return this.http.post<void>(url, {});
  }

  /*
  log(id: string): Observable<IDocumentLog[]> {
    let url: string = `${this.root}/instruction/log/${id}`;
    return this.http.get<IDocumentLog[]>(url);
  }*/


  log(id: string): Observable<IDocumentLogExtended<any>[]> {
    return this.http.get<IDocumentLogExtended<any>[]>(`${this.root}/log/${id}/INSTRUCTION/ASC`);
  }


  checkEncrypt(account: string): Observable<void> {
    let url: string = `${this.root}/encrypt/check`;
    return this.http.post(url, [account]).pipe(map(response => {
      return;
    }));
  }
}

