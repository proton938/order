import {formatDate} from "@angular/common";
import {isDate} from "util";

export function formatAs(obj: Date | any, template?: string, prop?: string): string {
  if (!template) {
    template = 'yyyy-MM-dd';
  }
  if (isDate(obj)) {
    return formatDate(<Date> obj, template, 'en');
  } else if (prop && obj[prop]){
    obj[prop] = formatDate(<Date> obj[prop], template, 'en');
  }
}

export function formatAsDate(obj: Date | any, prop?: string): string {
  return formatAs(obj, 'yyyy-MM-dd', prop);
}

export function formatAsTime(obj: Date | any, prop?: string): string {
  return formatAs(obj, 'HH:mm:ss', prop);
}

export function formatAsDateTime(obj: Date | any, prop?: string): string {
  return formatAs(obj, 'yyyy-MM-ddTHH:mm:ss', prop);
}

export function parseDates(input: any): any {
  for (let key in input) {
    if (!input.hasOwnProperty(key)) continue;

    if (typeof input[key] === "object") {
      parseDates(input[key]);
    } else {
      let match = /^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}.*$/.exec(input[key]);
      if (typeof input[key] === "string" && match) {
        input[key] = new Date(input[key]);
      }
      match = /^\d{4}-\d{2}-\d{2}$/.exec(input[key]);
      if (typeof input[key] === "string" && match) {
        input[key] = new Date(input[key]);
      }
      match = /^\d{2}:\d{2}:\d{2}$/.exec(input[key]);
      if (typeof input[key] === "string" && match) {
        let timeStr = input[key];
        input[key] = new Date("01-01-1970 " + timeStr);
      }
    }
  }
  return input;
}
