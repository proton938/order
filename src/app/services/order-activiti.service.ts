import * as _ from "lodash";
import {Observable} from "rxjs/Rx";
import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {OrderApprovalListItem} from "../models/order/Order";

@Injectable({
    providedIn: 'root'
})
//TODO: move to platform nsi
export class OrderActivitiService {

    constructor(private http: HttpClient) {
    }

    addApprovers(id: number | string, agreed: OrderApprovalListItem[]): Observable<any> {
        let url: string = '/app/sdo/bpm/approval/changeApprovalPersons';

        let requestData = {
            taskId: id,
            usersCollectionVarName: "ApprovalUsersVar",
            durationsCollectionVarName: "ApprovalDurationsVar",
            elementVarName: "ApprovalUserVar",
            approvalPersons: _.map(agreed, _ => {
                return {
                    userName: _.agreedBy.accountName//,
                    //duration: _.approvalTime
                }
            })
        };

        return <Observable<any>>this.http.post(url, requestData);
    }

}
