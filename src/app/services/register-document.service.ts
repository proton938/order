import {Injectable} from '@angular/core';
import {Order} from "../models/order/Order";
import {Observable} from "rxjs/Rx";
import {AlertService} from "@reinform-cdp/widgets";
import {forkJoin, from, of, throwError} from "rxjs/index";
import {WorkingDateService} from "./working-date.service";
import {mergeMap, tap} from "rxjs/internal/operators";
import {OrderService} from "./order.service";

@Injectable({
  providedIn: 'root'
})
export class RegisterDocumentService {

  constructor(private orderService: OrderService,
              private alertService: AlertService,
              private workingDateService: WorkingDateService) {
  }

  fillDocumentDate(order: Order): Observable<any> {
    if (order.documentDate) {
      if (order.documentDate.getFullYear() !== order.documentYear) {
        return from(this.alertService.message({
          message: 'Дата документа не соответствует году документа'
        }));
      } else {
        return of('');
      }
    } else {
      return this.workingDateService.getClosestWorkingDay().pipe(
        tap(date => order.documentDate = date)
      )
    }
  }

  checkLinkedDocuments(order: Order): Observable<any> {
    if (order.linkedDocument && order.linkedDocument.length > 0) {
      return forkJoin(order.linkedDocument.map(doc => this.orderService.get(doc.linkedDocumentID))).pipe(
        mergeMap(linkedDocs => {
          const notRegistered = linkedDocs.find((doc: Order) => !doc.documentNumber || !doc.documentDate);
          if (notRegistered) {
            return throwError(notRegistered);
          } else {
            return of('');
          }
        })
      );
    }
    else {
      return of('');
    }
  }

  registerDocument(order: Order): Observable<void> {
    return this.orderService.generateNumber(order.documentYear, order.documentTypeCode).pipe(
      mergeMap(documentNumber => {
        order.documentNumber = documentNumber;
        return this.fillDocumentDate(order);
      })
    );
  }
}
