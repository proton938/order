import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";

@Injectable()
export class CompoundReportService {

  rootReporter = "/reporter/v1";

  constructor(private http: HttpClient,) {
  }

  nsi2Preview(request: Nsi2PreviewRequest): Observable<Blob> {
    let url: string = `${this.rootReporter}/wordReporter/compoundReport/nsi2preview`;
    return this.http.post(url, request, {responseType: 'blob'});
  }

  nsi2Filenet(request: Nsi2FilenetRequest): Observable<Nsi2FilenetResponse> {
    let url: string = `${this.rootReporter}/wordReporter/compoundReport/nsi2filenet`;
    return <Observable<Nsi2FilenetResponse>>this.http.post(url, request);
  }

}

export class Nsi2PreviewRequest {
  jsonTxt: string;
  options: {
    jsonSourceDescr: string;
    onProcess: string;
    placeholderCode: string;
    strictCheckMode: boolean;
    xwpfResultDocumentDescr: string;
    xwpfTemplateDocumentDescr: string;
  };
  rootJsonPath: string;
  wordReporterParts: {
    sectionStart: string;
    templateCode: string;
  }[];
}

export class Nsi2FilenetRequest extends Nsi2PreviewRequest {
  filenetDestination: {
    fileAttrs: {
      attrName: string;
      attrNameRu?: string;
      attrValues: string[];
    }[];
    fileName: string;
    fileType: string;
    folderGuid: string;
    versionSeriesGuid?: string;
  }
}

export class Nsi2FilenetResponse {
  dateCreated: String;
  dateLastModified: String;
  fileAttrs: {
    attrName: string;
    attrNameRu: string;
    attrValues: string[];
  }[];
  fileName: string;
  fileSize: number;
  fileType: string;
  fileTypeRu: string;
  hasWarnings: boolean;
  mimeType: string;
  objectGuid: string;
  signGuid: string;
  versionSeriesGuid: string;
}

