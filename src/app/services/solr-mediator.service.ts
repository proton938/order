import {Injectable} from '@angular/core';
import {Observable, from} from 'rxjs';
import {map} from 'rxjs/internal/operators';
import {CdpSearchByIds, CdpSolrResourceService, SolarResourceService} from '@reinform-cdp/search-resource';
import {every, isString, uniq, isArray, concat, compact} from 'lodash';
import {CommonQueryService} from './common-query.service';

@Injectable({
  providedIn: 'root'
})
export class SolrMediatorService {
  types: any = {
    instruction: 'SDO_INSTRUCTION_INSTRUCTION',
    memo: 'SDO_INSTRUCTION_MEMO',
    memoMKA: 'SDO_INSTRUCTION_MEMO-MKA',
    order: 'SDO_ORDER_ORDER',
    agenda: 'SDO_MEETING_AGENDA',
    protocol: 'SDO_MEETING_PROTOCOL',
    question: 'SDO_MEETING_QUESTION'
  };

  constructor(private solr: SolarResourceService,
              private cdpSolr: CdpSolrResourceService,
              private commonQuery: CommonQueryService) {}

  query(params: any, isNew: boolean = true): Observable<any> {
    if (isNew) {
      if (params.common) this.commonToQuery(params.common, params, isNew);
      else delete params.common;
      if (params.query) params.query = params.query.replace(/documentId:|sys_documentId:/g, 'sys_documentId:');
      else delete params.query;
      return this.cdpSolr.query(params).pipe(map(this.modifyDocs.bind(this)));
    } else {
      return from(this.solr.query(params)).pipe(map(this.modifyDocs.bind(this)));
    }
  }

  getByIds(data: CdpSearchByIds[] | string[], isNew = true): Observable<any> {
    if (every(data, i => isString(i))) isNew = false;
    if (isNew) {
      return this.cdpSolr.getByIds(<CdpSearchByIds[]>data).pipe(map(this.modifyDocs.bind(this)));
    } else {
      return from(this.solr.getByIds({ids: <string[]>data})).pipe(map(this.modifyDocs.bind(this)));
    }
  }

  getDocs(res: any) {
    if (res) {
      if (isArray(res)) { // after getByIds
        return compact(concat(res.map(i => i.docs || [])));
      } else { // after query or other
        return res.docs || [];
      }
    } else {
      return [];
    }
  }

  modifyDocs(response: any): any {
    let modifyItem = item => {
      if (!item.documentId && item.sys_documentId) item.documentId = item.sys_documentId;
      return item;
    };
    let modify = r => {
      if (r && r.docs && r.docs.length) r.docs.forEach(i => modifyItem(i));
      return r;
    };
    return isArray(response) ? response.map(i => modify(i)) : modify(response);
  }

  commonToQuery(common: string, params: any, isNew: boolean = true) {
    let fieldCollection: any = {
      [this.types.instruction]: [
        {code: 'instructionNumber'},
        {code: 'creatorFioFull'},
        {code: 'executorFioFull'}
      ]
    };
    let fields: string[] = [];
    params.types.forEach(t => fields.push(...fieldCollection[t]));
    let commonQuery: string = this.commonQuery.commonToQuery(common, uniq(fields), isNew);
    if (commonQuery) {
      params.query = params.query ? params.query + ' AND (' + commonQuery + ')' : commonQuery;
    }
    delete params.common;
  }
}
