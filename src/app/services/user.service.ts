import * as _ from 'lodash';
import {Observable, forkJoin, from, of} from 'rxjs';
import {map, mergeMap} from 'rxjs/internal/operators';
import {Injectable} from '@angular/core';
import {NsiResourceService, UserBean} from '@reinform-cdp/nsi-resource';
import {OrderType} from '../models/order/OrderType';

@Injectable({
    providedIn: 'root'
})
export class UserService {

    private searchCache: any[] = [];

    constructor(private nsi: NsiResourceService) {
    }

    getExecutives(orderType: OrderType): Observable<UserBean[]> {
        return this.getUsers('MGGT_ADMORDER_HEAD', orderType.accessAction);
    }

    getApprovers(orderType: OrderType): Observable<UserBean[]> {
        return this.getUsers('MGGT_ADMORDER_APPROVE', orderType.accessAction);
    }

    getSign(orderType: OrderType): Observable<UserBean[]> {
        return this.getUsers('MGGT_ELECTRONIC_SIGN', orderType.accessAction);
    }

    getUsers(baseGroup: string, accessAction: string): Observable<UserBean[]> {
        return this.getAccessActionGroups(accessAction).pipe(
            mergeMap((groups: string[]) => {
                return groups.length
                    ? forkJoin([
                        this.searchFromCache({group: [baseGroup]}),
                        this.searchFromCache({group: groups}, 'OR')
                    ])
                    : of([[], []]);
            }),
            map((res: UserBean[][]) => {
                return res[0].filter(u => _.some(res[1], i => u.accountName === i.accountName));
            })
        )
    }

    getAccessActionGroups(accessAction: string): Observable<string[]> {
        return from(this.nsi.getDictsFromCache(['Permissions'])).pipe(
            mergeMap(dic => {
                return of(dic['Permissions'].filter(i => i.code === accessAction));
            }),
            map((permissions: { group: string[] }[]) => {
                return permissions ? _.flatten(permissions.map(perm => perm.group)) : [];
            })
        )
    }

    searchFromCache(params: any, operation?: string): Observable<UserBean[]> {
        const key = JSON.stringify(params) + (operation ? '_' + operation : '');
        const cache = _.find(this.searchCache, i => i.key === key);
        return Observable.create(obs => {
            if (cache) {
                obs.next(cache.data);
                obs.complete();
            } else {
                from(this.nsi.searchUsers(params, operation)).subscribe(res => {
                    this.searchCache.push({ key: key, data: res });
                    obs.next(res);
                    obs.complete();
                });
            }
        });
    }
}
