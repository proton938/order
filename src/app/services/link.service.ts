import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import {mergeMap, tap} from 'rxjs/internal/operators';
import {HttpClient} from '@angular/common/http';
import {SolrMediatorService} from './solr-mediator.service';
import * as _ from 'lodash';

@Injectable({
  providedIn: 'root'
})
export class LinkService {
  url = '/app/sdo/link';
  docType = 'LINK';

  constructor(private http: HttpClient,
              private solrMediator: SolrMediatorService) {}

  create(data): Observable<any> {
    return this.http.post<any>(this.url + '/crud/create/' + this.docType + '?flagReindex=true', {
      link: data
    });
  }

  findSdoLinks(id: string): Observable<any[]> {
    let url: string = `${this.url}/link/sdo/${id}/find`;
    return <Observable<any[]>>this.http.get(url);
  }

  findFromSolr(id: string, types: string[] = []): Observable<any[]> {
    return Observable.create(obs => {
      this.findSdoLinks(id).subscribe(res => {
        const links = res && res.length
          ? res.filter(i => _.some([i.link.type1, i.link.type2], t => _.some(types, d => d === t)))
          : [];
        const linkIds: string[] = links.map(i => _.find([i.link.id1, i.link.id2], f => f !== id));
        if (linkIds.length) {
          this.solrMediator.getByIds(types.map(t => {
            return {ids: linkIds, type: t};
          })).pipe(
            mergeMap(res => this.solrMediator.getDocs(res))
          ).subscribe((res: any[]) => {
            obs.next(res || []);
            obs.complete();
          }, error => {
            obs.error(error);
            obs.complete();
          });
        }
      });
    });
  }

}
