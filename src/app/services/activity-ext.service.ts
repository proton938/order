import {Injectable} from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivityResourceService } from '@reinform-cdp/bpm-components';

@Injectable({
    providedIn: 'root'
})
export class ActivityExtService {
    constructor(private http: HttpClient,
                public resource: ActivityResourceService) {}

    getProcessInfo(processDefinitionId: string) {
        return this.http.get('/app/sdo/bpm/bpdefinitions/' + processDefinitionId + '/info');
    }
}
