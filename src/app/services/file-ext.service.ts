import {Injectable} from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable} from 'rxjs';
import { PlatformConfig } from '@reinform-cdp/core';

@Injectable({
    providedIn: 'root'
})
export class FileExtService {
    url: string = '/filestore/v2';
    constructor(private http: HttpClient,private platformConfig: PlatformConfig) {}

    getVersionHistory(fileId: string, systemCode: string, subsystemCode: string) {
        return this.http.get(this.url + '/files/allVersionHistory/' + fileId, {
            params: { systemCode, subsystemCode }
        });
    }

    updateAccess(fileId: string, login: string): Observable<any> {
        let url: string = `${this.url}/files/updateAccess/${fileId}?checkPrincipals=true&systemCode=${this.platformConfig.systemCode}`;
        return this.http.post(url, {
          "access": [
            {
              "permissions": [
                "Coordinator"
              ],
              "principalId": login
            }
          ]
        });
      }
}
