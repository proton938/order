import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from "@angular/common/http";
import {Observable} from "rxjs/Rx";
import {throwError} from 'rxjs'
import {Injectable} from "@angular/core";
import {catchError} from "rxjs/internal/operators";
import {ToastrService} from "ngx-toastr";

@Injectable()
export class HttpErrorInterceptor implements HttpInterceptor {
  constructor(private toastr: ToastrService) {

  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(request).pipe(
      catchError(err => {
        if (err.error instanceof Error) {
          console.error('Произошла ошибка:', err.error.message);
        } else {
          let userMessages: string[] = err.error.userMessages;
          if (userMessages) {
            userMessages.forEach(message => this.toastr.error(message))
          } else {
            this.toastr.error("Произошла ошибка");
          }
        }
        return throwError(err.error);
      })
    );
  }
}
