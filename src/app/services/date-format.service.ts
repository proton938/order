import {Injectable} from '@angular/core';
import {formatAsDate, formatAsDateTime} from "./date-util.service";
import * as angular from "angular";
import {Order, OrderApprovalList} from "../models/order/Order";

@Injectable({
    providedIn: 'root'
})
export class DateFormatService {

    constructor() {
    }

    formatOrderDates(order: Order): any {
        let obj = <any> angular.copy(order);
        if (obj.bpmProcess) {
            if (obj.bpmProcess.bpmProcessDel) {
                formatAsDateTime(obj.bpmProcess.bpmProcessDel, 'date');
            }
            if (obj.bpmProcess.bpmProcessStart) {
                formatAsDateTime(obj.bpmProcess.bpmProcessStart, 'date');
            }
        }
        formatAsDate(obj, 'documentDate');
        formatAsDate(obj, 'documentExpireDate');
        if (obj.canceledDocument) {
            formatAsDate(obj.canceledDocument, 'canceledDocumentDate');
        }
        if (obj.updatedDocument) {
            (obj.updatedDocument.forEach(doc => formatAsDate(doc, 'updatedDocumentDate')))
        }
        formatAsDateTime(obj, 'createDateTime');
        if (obj.files) {
            (obj.files.forEach(file => formatAsDateTime(file, 'fileDate')))
        }
        if (obj.draftFilesRegInfo) {
            formatAsDateTime(obj.draftFilesRegInfo, 'fileDate');
        }
        if (obj.receivedFromMKA) {
            formatAsDateTime(obj.receivedFromMKA, 'receivedDate');
        }
        if (obj.draftFiles) {
            formatAsDateTime(obj.draftFiles, 'draftFilesDate');
        }
        if (obj.draftFilesWithoutAtt) {
            formatAsDateTime(obj.draftFilesWithoutAtt, 'draftFilesDate');
        }
        if (obj.draftFilesWithoutAttWord) {
            formatAsDateTime(obj.draftFilesWithoutAttWord, 'draftFilesDate');
        }
        if (obj.attach) {
            formatAsDateTime(obj.attach, 'draftFilesDate');
        }
        if (obj.attachWord) {
            formatAsDateTime(obj.attachWord, 'draftFilesDate');
        }
        formatAsDateTime(obj, 'mailDateTime');
        if (obj.previousDocument) {
            formatAsDateTime(obj.previousDocument, 'previousDocumentDate');
        }
        if (obj.approval && obj.approval.approvalCycle) {
            this.formatApprovalCycle(obj.approval.approvalCycle);
        }
        if (obj.approvalHistory && obj.approvalHistory.approvalCycle) {
            obj.approvalHistory.approvalCycle.forEach(cycle => {
                this.formatApprovalCycle(cycle);
                if (cycle.approvalCycleFile) {
                    cycle.approvalCycleFile.forEach(file => formatAsDateTime(file, 'fileDate'))
                }
            })
        }
        if (obj.SigningOnPaper) {
            formatAsDateTime(obj.SigningOnPaper, 'signingFactDate');
            if (obj.SigningOnPaper.noteSigning) {
                formatAsDateTime(obj.SigningOnPaper.noteSigning, 'noteDate');
            }
            if (obj.SigningOnPaper.File) {
                obj.SigningOnPaper.File.forEach(file => formatAsDateTime(file, 'fileDate'))
            }
        }
        if (obj.additionalMaterials) {
            (obj.additionalMaterials.forEach(file => formatAsDateTime(file, 'fileDate')))
        }
        if (obj.ds) {
            formatAsDate(obj.ds, 'dsFrom');
            formatAsDate(obj.ds, 'dsTo');
        }
        if (obj.review) {
            obj.review.forEach((review) => {
                formatAsDateTime(review, 'reviewFactDate');
                formatAsDateTime(review, 'reviewPlanDate');
            });
        }
        if (obj.reviewInner) {
            obj.reviewInner.forEach((review) => {
                formatAsDateTime(review, 'reviewFactDate');
                formatAsDateTime(review, 'reviewPlanDate');
            });
        }
        if (obj.receivedFromMKA) {
            formatAsDateTime(obj.receivedFromMKA, 'receivedDate');
        }
        return obj;
    }

    formatApprovalCycle(cycle) {
        formatAsDateTime(cycle, 'approvalCycleDate');
        if (cycle.agreed) {
            cycle.agreed.forEach(agreed => {
                formatAsDateTime(agreed, 'approvalPlanDate');
                formatAsDateTime(agreed, 'approvalFactDate');
                if (agreed.fileApproval) {
                    agreed.fileApproval.forEach(file => formatAsDateTime(file, 'fileDate'))
                }
                if (agreed.agreedDs) {
                    formatAsDate(agreed.agreedDs, 'agreedDsFrom');
                    formatAsDate(agreed.agreedDs, 'agreedDsTo');
                }
            })
        }
    }
}
