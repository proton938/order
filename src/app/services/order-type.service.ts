import {Injectable} from '@angular/core';
import {AuthorizationService} from "@reinform-cdp/security";
import {NsiResourceService} from "@reinform-cdp/nsi-resource";
import {OrderType} from "../models/order/OrderType";
import {Observable} from "rxjs/Rx";
import {from} from "rxjs/index";
import {map} from "rxjs/internal/operators";

@Injectable({
    providedIn: 'root'
})
export class OrderTypeService {

    constructor(private nsiResourceService: NsiResourceService,
                private authorizationService: AuthorizationService) {
    }

    geOrderTypes(): Observable<OrderType[]> {
        return from(this.nsiResourceService.get('mggt_order_OrderTypes')).pipe(
            map((orderTypes: OrderType[]) => {
                return orderTypes.filter(orderType => this.checkOrderType(orderType))
            })
        );
    }

    private checkOrderType(orderType: OrderType): boolean {
        let accessAction = orderType.accessAction;
        return !accessAction || this.authorizationService.check(orderType.accessAction);
    }

}
