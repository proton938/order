import {of} from "rxjs/index";
import * as _ from "lodash";
import * as angular from "angular";
import {compare, Operation} from "fast-json-patch";
import {Injectable} from '@angular/core';
import {map} from "rxjs/internal/operators";
import {Observable} from "rxjs/index";
import {formatAsDate, parseDates} from "./date-util.service";
import {HttpClient} from "@angular/common/http";
import {Order, OrderApprovalListItem, OrderApprover} from "../models/order/Order";
import {DateFormatService} from "./date-format.service";
import {FileType} from "../models/order/FileType";
import {formatDate} from "@angular/common";
import {User} from "../models/order/User";
import {CancelType} from "../models/order/CancelType";
import {UpdateType} from "../models/order/UpdateType";

@Injectable({
    providedIn: 'root'
})
export class OrderService {
    signDurationCode: boolean = false;

    constructor(private http: HttpClient, private dateFormatService: DateFormatService) {
    }

    showComponents: boolean = true;

    formatOrderDates(order: Order) {
        return order;
    }

    get(id: string): Observable<Order> {
        return <Observable<Order>>this.http.get(`/app/sdo/order/document/${id}`).pipe(
            map((result: { document: Order }) => OrderService.processOrder(parseDates(result.document)))
        );
    }

    nextNumber(type: string, year: number): Observable<string> {
        return <Observable<string>>this.http.get(`/app/sdo/order/document/next-number?type=${type}&year=${year}`).pipe(
            map((result: { documentNumber: string }) => result.documentNumber)
        );
    }

    nextBarcode(): Observable<number> {
        return <Observable<number>>this.http.get(`/app/sdo/order/document/next-barcode`).pipe(
            map((result: { barcode: number }) => result.barcode)
        );
    }

    create(order: Order): Observable<Order> {
        return <Observable<Order>>this.http.post(`/app/sdo/order/document/create`, {document: OrderService.cleanOrder(this.dateFormatService.formatOrderDates(order))})
            .pipe(map((result: { document: Order }) => {
                return OrderService.processOrder(result.document);
            }));
    }

    diff(o1: Order, o2: Order) {
        return compare(
            {document: OrderService.cleanOrder(this.dateFormatService.formatOrderDates(o1))},
            {document: OrderService.cleanOrder(this.dateFormatService.formatOrderDates(o2))}
        );
    }

    markAsDeleted(order: Order): Observable<Order> {
        let newOrder = angular.copy(order);
        _.extend(newOrder, {
            documentStatusCode: 'DELETED',
            documentStatusValue: 'Удален',
            documentStatusColor: 'danger'
        });
        return this.update(order.documentID, this.diff(order, newOrder));
    }

    deleteFiles(order: Order): Observable<Order> {
        let newOrder = angular.copy(order);
        newOrder.files = [];
        return this.update(order.documentID, this.diff(order, newOrder));
    }

    deleteAdditionalMaterial(order: Order, fileId: string): Observable<Order> {
        let newOrder = angular.copy(order);
        let index = newOrder.additionalMaterials.findIndex(mat => mat.fileID === fileId);
        newOrder.additionalMaterials.splice(index, 1);
        return this.update(order.documentID, this.diff(order, newOrder));
    }

    addFile(order: Order, file: FileType): Observable<Order> {
        let newOrder = angular.copy(order);
        newOrder.files = newOrder.files || [];
        newOrder.files.push(file);
        return this.update(order.documentID, this.diff(order, newOrder));
    }

    replaceFile(order: Order, file: FileType): Observable<Order> {
      let newOrder = angular.copy(order);
      newOrder.files = [];
      newOrder.files.push(file);

      console.log('oldFiles', order.files);
      console.log('newFiles', newOrder.files);

      return this.update(order.documentID, this.diff(order, newOrder));
  }


    update(id: string, diff: Operation[]): Observable<Order> {
        return <Observable<Order>>this.http.patch(`/app/sdo/order/document/${id}`, diff)
            .pipe(map((result: { document: Order }) => {
                return OrderService.processOrder(parseDates(result.document));
            }));
    }

    generateNumber(year: number, type: string): Observable<string> {
        return <Observable<string>>this.http.get(`/app/sdo/order/document/next-number?year=${year}&type=${type}`).pipe(
            map((result: { documentNumber: string }) => result.documentNumber)
        );
    }

    getRelated(id: string): Observable<string[]> {
        return <Observable<string[]>>this.http.get(`/app/sdo/order/document/${id}/related`);
    }

    mail(id: string) {
        return <Observable<any>>this.http.post(`/app/sdo/order/document/${id}/mail`, {});
    }

    mailReturnToEditNotifications(id: string, users: string[]): Observable<any> {
        return <Observable<any>>this.http.post(`/app/sdo/order/document/${id}/mailReturnToEditNotifications`, users);
    }

    sendToDocumentsDB(id: string): Observable<any> {
        return <Observable<any>>this.http.post(`/app/sdo/order/document/${id}/sendToDocumentsDB`, null);
    }

    send(id: string, includeFile: boolean) {
        //TODO: send to PSO
        return of('');
        // return <Observable<any>>this.http.post(`/app/mggt/order/document/${id}/send?includeFile=${includeFile}`, {});
    }

    private static processOrder(order: Order) {
        if (order.canceledDocument) {
            order.canceledDocument = CancelType.processDocName(order.canceledDocument);
        }
        if (order.updatedDocument) {
            order.updatedDocument = order.updatedDocument.map(doc => UpdateType.processDocName(doc));
        }
        if (order.executor) {
            order.executor = order.executor.map(user => User.processUser(user));
        }
        if (order.mailList) {
            order.mailList = order.mailList.map(user => User.processUser(user));
        }
        if (order.approval && order.approval.approvalCycle && order.approval.approvalCycle.agreed) {
            order.approval.approvalCycle.agreed = order.approval.approvalCycle.agreed
                .map(agreed => OrderService.processOrderApprovalListItem(agreed))
        }
        if (order.approvalHistory && order.approvalHistory.approvalCycle) {
            order.approvalHistory.approvalCycle = order.approvalHistory.approvalCycle
                .map(cycle => {
                    if (cycle.agreed) {
                        cycle.agreed = cycle.agreed.map(agreed => OrderService.processOrderApprovalListItem(agreed));
                    }
                    return cycle;
                });
        }
        return order;
    }

    private static cleanOrder(order: Order) {
        if (order.canceledDocument) {
            order.canceledDocument = CancelType.cleanDocName(order.canceledDocument);
        }
        if (order.updatedDocument) {
            order.updatedDocument = order.updatedDocument.map(doc => UpdateType.cleanDocName(doc));
        }
        if (order.executor) {
            order.executor = order.executor.map(user => User.cleanUser(user));
        }
        if (order.mailList) {
            order.mailList = order.mailList.map(user => User.cleanUser(user));
        }
        if (order.approval && order.approval.approvalCycle && order.approval.approvalCycle.agreed) {
            order.approval.approvalCycle.agreed = order.approval.approvalCycle.agreed
                .map(agreed => OrderService.cleanOrderApprovalListItem(agreed))
        }
        if (order.approvalHistory && order.approvalHistory.approvalCycle) {
            order.approvalHistory.approvalCycle = order.approvalHistory.approvalCycle
                .map(cycle => {
                    if (cycle.agreed) {
                        cycle.agreed = cycle.agreed.map(agreed => OrderService.cleanOrderApprovalListItem(agreed));
                    }
                    return cycle;
                });
        }
        return order;
    }

    private static processOrderApprovalListItem(item: OrderApprovalListItem): OrderApprovalListItem {
        if (item.agreedBy) {
            item.agreedBy = OrderApprover.processOrderApprover(item.agreedBy);
        }
        if (item.factAgreedBy) {
            item.factAgreedBy = OrderApprover.processOrderApprover(item.factAgreedBy);
        }
        return item;
    }

    private static cleanOrderApprovalListItem(item: OrderApprovalListItem): OrderApprovalListItem {
        if (item.agreedBy) {
            item.agreedBy = OrderApprover.cleanOrderApprover(item.agreedBy);
        }
        if (item.factAgreedBy) {
            item.factAgreedBy = OrderApprover.cleanOrderApprover(item.factAgreedBy);
        }
        return item;
    }

}
