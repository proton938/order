import {Observable,forkJoin, of} from "rxjs/index";
import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {formatDate} from "@angular/common";
import { OrderApprovalListItem } from "../models/order/Order";
import { mergeMap, tap } from "rxjs/internal/operators";

@Injectable({
    providedIn: 'root'
})
// TODO: move to platform nsi
export class OrderNsiService {

    constructor(private http: HttpClient) {
    }

    toLocalDateTime(dateTime: Date): Observable<string> {
        return <Observable<string>>this.http.get(`/mdm/api/v1/datetime/zonedDateTime/${dateTime}/toLocalDateTime`, {
            headers: {
                'Accept': 'text/plain'
            },
            responseType: 'text'
        });
    }

    addDuration(date: Date, duration: string): Observable<string> {
        let dateStr = formatDate(date, 'yyyy-MM-ddTHH:mm:ss', 'en');
        return <Observable<string>>this.http.get(`/mdm/api/v1/businessCalendar/addDuration/${dateStr}/${duration}`, {
            responseType: 'text'
        });
    }

    calcDuration(duration: string): Observable<string> {
        return <Observable<string>>this.http.get(`/mdm/api/v1/businessCalendar/calcDuration/${duration}`, {
            responseType: 'text'
        });
    }

    divideTimeBy(duration: string, divideBy: number): Observable<string> {
        return <Observable<string>>this.http.get(`/mdm/api/v1/datetime/duration/${duration}/divideBy/${divideBy}`, {
            responseType: 'text'
        });
    }

    fillApprovalPlanDates(agreed:OrderApprovalListItem[]): Observable<any> {
        if(!agreed.length)return Observable.of<any>(); 
        let parallel = agreed.filter(item=>!!item.Parallel);
        let query = agreed.filter(item=>!item.Parallel);
        let now = new Date(); 
        if(!parallel.length){
            return this.fillQueryApproval(query,now);
        }  
        return forkJoin(
          parallel.map(a => this.fillApprovalPlanDate(a,now))
        ).pipe(mergeMap(() => { 
            let newDate = parallel[0].approvalPlanDate;  
            if(!query.length)return Promise.resolve();          
            return this.fillQueryApproval(query,newDate);
        }));
    }

    getPlanWorkDate(days: number = 1, date: Date = new Date()): Observable<any> {
        let dateStr = formatDate(date, 'yyyy-MM-ddTHH:mm:ss', 'en');
        return <Observable<string>>this.http.get(`/mdm/api/v1/businessCalendar/addWorkDays/${dateStr}/${days}`, {
            responseType: 'text'
        });
    }

    private fillQueryApproval(agreed:OrderApprovalListItem[],startDate:Date): Observable<any>{
        return this.fillApprovalPlanDate(agreed[0],startDate).pipe(mergeMap(()=>{
            let current = agreed.shift();
            if(!agreed.length)return Promise.resolve();
            let newDate = current.approvalPlanDate;            
            return this.fillQueryApproval(agreed,newDate);
        }));
    }
    
    private fillApprovalPlanDate(agreed: OrderApprovalListItem,startDate:Date): Observable<any> {
        let duration = agreed.approvalTerm.duration;
        if (!duration) {
            return of('');
        }
        agreed.approvalTime = 'P' + duration + 'D';
        return this.addDuration(startDate, 'P' + duration + 'D').pipe(
            tap(result => {
            let date = new Date(result);
            agreed.approvalPlanDate = new Date(date.getFullYear(), date.getMonth(), date.getDate(), 23, 59, 59, 0);
            })
        );
    }

}
