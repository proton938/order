import {Injectable} from '@angular/core';
import {Observable} from "rxjs/Rx";
import {NsiResourceService} from "@reinform-cdp/nsi-resource";
import {from, of} from "rxjs/index";
import {mergeMap} from "rxjs/internal/operators";

@Injectable({
    providedIn: 'root'
})
export class WorkingDateService {

    constructor(private nsiResourceService: NsiResourceService) {
    }

    getClosestWorkingDay(): Observable<Date> {
        return this.getNextWorkingDate(new Date(Date.now()));
    }

    getNextWorkingDate(date: Date): Observable<Date> {
        return from(this.nsiResourceService.isWorkDay(date)).pipe(
            mergeMap(result => {
                if (result) {
                    return of(date);
                } else {
                    const nextDate = new Date(date.getTime());
                    nextDate.setDate(nextDate.getDate() + 1);
                    return this.getNextWorkingDate(nextDate)
                }
            })
        )
    }

}
